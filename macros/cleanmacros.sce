// ====================================================================
// Allan CORNET
// DIGITEO 2009
// This file is released into the public domain
// ====================================================================
function nispCleanMacros()
libpath = get_absolute_file_path('cleanmacros.sce');

binfiles = ls(libpath+'/*.bin');
for i = 1:size(binfiles,'*')
  mdelete(binfiles(i));
end

mdelete(libpath+'/names');
mdelete(libpath+'/lib');

exec(fullfile(libpath,"internals/cleanmacros.sce"),-1)
exec(fullfile(libpath,"safunctions/cleanmacros.sce"),-1)
exec(fullfile(libpath,"testfuns/cleanmacros.sce"),-1)

// ====================================================================
endfunction 
nispCleanMacros();
clear nispCleanMacros
