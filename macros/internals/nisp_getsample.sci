// Copyright (C) 2014 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function A = nisp_getsample ( varargin )
    //
    // Calling sequence
    // A = nisp_getsample ( n , nx , __sobolfo_randgen__ )
    // Ai = nisp_getsample ( n , nx , __sobolfo_randgen__ , i )
    //

    function Ai=nisp_getsamplecompute(n,i,__sobolfo_randgen__)
        if ( typeof( __sobolfo_randgen__ ) == "list" ) then
            // List
            __sobolfo_randgen__f = __sobolfo_randgen__ (1);
        end
        if ( typeof( __sobolfo_randgen__ ) == "list" ) then
            // List
            Ai = __sobolfo_randgen__f ( n , i , __sobolfo_randgen__ (2:$));
        else
            // Macro or compiled macro
            Ai = __sobolfo_randgen__ ( n , i );
        end
        // Check sample size
        if ( or(size(Ai)<>[n 1]) ) then
            errmsg = msprintf(gettext("%s: The size of the output of randgen is [%d,%d], which is different from [%d,%d]."), "nisp_getsample" , size(Ai,"r") , size(Ai,"c") , n , 1 );
            error(errmsg)
        end
    endfunction

    [lhs, rhs] = argn()
    n = varargin(1)
    nx = varargin(2)
    __sobolfo_randgen__ = varargin(3)
    //
    // Proceed    
    if (rhs==3) then
        A = zeros(n , nx)
        for i = 1 : nx
            Ai=nisp_getsamplecompute(n,i,__sobolfo_randgen__)
            A(:,i) = Ai
        end
    else
        i = varargin(4)
        A=nisp_getsamplecompute(n,i,__sobolfo_randgen__)
    end

endfunction
