// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Returns x, a m-by-1 matrix of doubles containing uniform random numbers.
// Used in Sobol method for SA.
function x = nisp_soboldefaultrgdef ( m , i )
    x = distfun_unifrnd(0,1,m,1)
endfunction
