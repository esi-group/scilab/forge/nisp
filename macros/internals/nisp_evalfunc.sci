// Copyright (C) 2014 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Evaluates the function y=f(x).
// Manages the case where f is either a function or a list.
// Used in Sobol method for SA.
function y = nisp_evalfunc ( x , __sobolfo_func__ )
    if ( typeof( __sobolfo_func__ ) == "list" ) then
        // List
        __sobolfo_func__f = __sobolfo_func__ (1);
        y = __sobolfo_func__f ( x , __sobolfo_func__ (2:$));
    else
        // Macro or compiled macro
        y = __sobolfo_func__ ( x );
    end
    // Check that the function changes.
    // This may reveal a bug in the function, 
    // and generates divisions by zero later in the 
    // SA.
    stdevy=stdev(y)
    if (stdevy==0.) then
        error(sprintf(gettext("%s: Standard deviation of output of function ""func"" is zero : cannot perform sensitivity analysis. Check the ""func"" argument.\n"), "nisp_evalfunc"));
    end
    // Check the size of the output of func
    n=size(x,"r")
    ny=size(y,"c")
    if (or(size(y)<>[n,ny])) then
        error(sprintf(gettext("%s: Wrong size of output of function ""func"": %d-by-%d instead of %d-by-%d. Check the ""func"" argument.\n"), "nisp_evalfunc", size(y,"r"), size(y,"c"),n,ny));
    end
endfunction
