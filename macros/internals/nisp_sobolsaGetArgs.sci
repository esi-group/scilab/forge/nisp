// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Get arguments of the nisp_sobolsa* functions.
function [ __sobolfo_func__ , nx , __sobolfo_randgen__ , n , inrange ] = nisp_sobolsaGetArgs ( funcname, arginlist )
    rhs = length(arginlist)
    apifun_checkrhs ( funcname , rhs , 2 : 5 )
    //
    __sobolfo_func__ = arginlist(1)
    nx = arginlist(2)
    __sobolfo_randgen__ = apifun_argindefault ( arginlist , 3 , nisp_soboldefaultrgdef )
    n = apifun_argindefault ( arginlist , 4 , 10000 )
    inrange = apifun_argindefault ( arginlist , 5 , %t )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( funcname , __sobolfo_func__ ,   "func" ,  1 , [ "function" "list" ] )
    apifun_checktype ( funcname , nx ,      "nx" ,     2 , "constant" )
    apifun_checktype ( funcname , __sobolfo_randgen__ ,  "randgen" , 3 , [ "function" "list" ] )
    apifun_checktype ( funcname , n ,  "n" , 4 , "constant" )
    apifun_checktype ( funcname , inrange ,  "inrange" , 5 , "boolean" )
    //
    // Check size
    apifun_checkscalar ( funcname, nx, "nx", 2 )
    apifun_checkscalar ( funcname, n, "n", 4 )
    apifun_checkscalar ( funcname, inrange, "inrange", 5 )
    //
    // Check content
    apifun_checkgreq ( funcname , nx ,  "nx" ,  2 , 1 )
    apifun_checkgreq ( funcname , n ,   "n" ,   4 , 1 )
endfunction

