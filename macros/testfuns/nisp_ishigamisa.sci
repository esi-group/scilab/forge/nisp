// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2010 - 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



function exact = nisp_ishigamisa ( a , b )
  // Exact sensitivity analysis for the Ishigami function
  //
  // Calling Sequence
  //   exact = nisp_ishigamisa ( a , b )
  //
  // Parameters
  // a: a 1-by-1 matrix of doubles, the first parameter.
  // b: a 1-by-1 matrix of doubles, the second parameter.
  // exact: a struct with the following fields: expectation, var, S1, S2, S3, S12, S13, S23, S123, ST1, ST2, ST3.
  //
  // Description
  //   This function returns exact sensitivity indices of the Ishigami function.
  //
  // Examples
  // a=7.;
  // b=0.1;
  // exact = nisp_ishigamisa ( a , b )
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  // Copyright (C) 2009 - CEA - Jean-Marc Martinez
  //
  // Bibliography
  // "Sensitivity analysis in practice", Saltelli, Tarantolla, Compolongo, Ratto, Wiley, 2004
  // "An importance quantification technique in uncertainty analysis for computer models", Ishigami, Homma, 1990, Proceedings of the ISUMA'90. First international symposium on uncertainty modelling and Analysis, University of Maryland, USA, pp. 398-403.

    exact.expectation = a/2;
    exact.var = 1/2 + a^2/8 + b*%pi^4/5 + b^2*%pi^8/18;
    exact.S1 = (1/2 + b*%pi^4/5+b^2*%pi^8/50)/exact.var;
    exact.S2 = (a^2/8)/exact.var;
    exact.S3 = 0;
    exact.S12 = 0;
    exact.S23 = 0;
    exact.S13 = b^2*%pi^8/2*(1/9-1/25)/exact.var;
    exact.S123 = 0;
    exact.ST1 = exact.S1 + exact.S13;
    exact.ST2 = exact.S2;
    exact.ST3 = exact.S3 + exact.S13;
endfunction

