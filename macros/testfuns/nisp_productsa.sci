// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2010 - 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



function exact = nisp_productsa ( mu , sigma )
    // Exact sensitivity analysis for the Product function
    //
    // Calling Sequence
    //   exact = nisp_productsa ( mu , sigma )
    //
    // Parameters
    // mu : a nx-by-1 matrix of doubles, the means of X
    // sigma : a nx-by-1 or 1-by-nx matrix of doubles, the standard deviation of X
    // exact: a struct with the following fields: expectation, var, S, ST
    //
    // Description
    //   This function returns exact sensitivity indices of the Product function.
    // We consider nx random variables. 
    // The random variable X(i) has mean mu(i) and standard deviation sigma(i), for 
    // i=1,2,...,nx.
    //
    // Examples
    // mu1 = 1.5;
    // sigma1 = 0.5;
    // mu2 = 3.5;
    // sigma2 = 2.5;
    // exact = nisp_productsa ( [mu1,mu2]' , [sigma1,sigma2]' )
    //
    // Authors
    // Copyright (C) 2008-2011 - INRIA - Michael Baudin
    // Copyright (C) 2009 - CEA - Jean-Marc Martinez
    //
    // Bibliography
    // "Sensitivity analysis in practice", Saltelli, Tarantolla, Compolongo, Ratto, Wiley, 2004
    // "An importance quantification technique in uncertainty analysis for computer models", Ishigami, Homma, 1990, Proceedings of the ISUMA'90. First international symposium on uncertainty modelling and Analysis, University of Maryland, USA, pp. 398-403.

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_productsa" , rhs , 2 : 2 )
    apifun_checklhs ( "nisp_productsa" , lhs , 0:1 )
    //
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_productsa" , mu ,   "mu" ,  1 , "constant" )
    apifun_checktype ( "nisp_productsa" , sigma ,   "sigma" ,  2 , "constant" )
    //
    // Check size
    nx=size(mu,"*")
    apifun_checkvector ( "nisp_productsa" , mu ,    "mu" ,    1 , nx )
    apifun_checkvector ( "nisp_productsa" , sigma ,    "sigma" ,    2 , nx )
    //
    // Check content
    tiniest=number_properties("tiniest")
    apifun_checkgreq  ( "nisp_productsa" , sigma ,    "sigma" ,    2 , tiniest )
    //
    mu=mu(:)
    sigma=sigma(:)
    exact.expectation = prod(mu)
    exact.var = prod(mu.^2+sigma.^2)-prod(mu.^2)
    // This is vectorized :
    // V = prod(mu.^2)./(mu.^2)
    // but does not work if one of the mu is zero.
    // Create a matrix of indices
    i=repmat(1:nx,nx,1)
    // The diagonal indices
    d=sub2ind([nx,nx],1:nx,1:nx)
    // Remove the diagonal (transpose)
    i=i'
    i(d)=[]
    // Reshape the matrix (transpose)
    i=matrix(i',nx,nx-1)
    musq=matrix(mu(i).^2,nx,nx-1)
    V=prod(musq,"c")
    exact.S = (sigma.^2).*V / exact.var
    V = prod(mu.^2+sigma.^2)./(mu.^2+sigma.^2)
    SU = ((mu.^2).*V-prod(mu.^2))/exact.var
    exact.ST = 1-SU
endfunction

