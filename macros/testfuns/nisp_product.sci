// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2010 - 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y = nisp_product ( x )
    // Returns the value of the product function
    //
    // Calling Sequence
    //   y = nisp_product ( x )
    //
    // Parameters
    // x: a np-by-nx matrix of doubles, where np is the number of experiments.
    // y: a np-by-1 matrix of doubles
    //
    // Description
    //   This function returns the value of the Product function.
    //
    // This function is defined by 
    //
    // <latex>
    // \begin{eqnarray}
    // Y = X_1 \cdot X_2 \cdot ... \cdot X_{nx}
    // \end{eqnarray}
    // </latex>
    //
    // where X(i) is a random variable, for i=1,2,...,nx. 
    //
    // Examples
    // mu1 = 1.5;
    // sigma1 = 0.5;
    // mu2 = 3.5;
    // sigma2 = 2.5;
    // np = 1000;
    // // Define a sampling for x1.
    // x1 = distfun_normrnd(mu1,sigma1,np,1);
    // // Define a sampling for x2.
    // x2 = distfun_normrnd(mu2,sigma2,np,1);
    // // Merge the two samplings
    // x = [x1 x2];
    // // Perform the experiments
    // y = nisp_product (x);
    // mean(y)
    // expected = mu1*mu2
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin
    // Copyright (C) 2008-2011 - INRIA - Michael Baudin
    //
    // Bibliography
    // "Sensitivity analysis in practice", Saltelli, Tarantolla, Compolongo, Ratto, Wiley, 2004
    // "An importance quantification technique in uncertainty analysis for computer models", Ishigami, Homma, 1990, Proceedings of the ISUMA'90. First international symposium on uncertainty modelling and Analysis, University of Maryland, USA, pp. 398-403.

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_product" , rhs , 1 : 1 )
    apifun_checklhs ( "nisp_product" , lhs , 0:1 )
    //
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_product" , x ,   "x" ,  1 , "constant" )
    //
    // Check size
    // Nothing to check
    //
    // Check content
    // Nothing to check
    //

    y(:,1) = prod(x,"c")
endfunction

