// Copyright (C) 2014 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function [ si , nbevalf , mi ] = nisp_sobolsaAll ( varargin )
    // Compute sensitivity indices by Sobol, Ishigami, Homma.
    //
    // Calling Sequence
    //   si = nisp_sobolsaAll ( func , nx )
    //   si = nisp_sobolsaAll ( func , nx , randgen )
    //   si = nisp_sobolsaAll ( func , nx , randgen , n )
    //   si = nisp_sobolsaAll ( func , nx , randgen , n , inrange)
    //   [ si , nbevalf ] = nisp_sobolsaAll ( ... )
    //   [ si , nbevalf , mi ] = nisp_sobolsaAll ( ... )
    //
    // Parameters
    //   func : a function or a list, the name of the function to be evaluated. 
    //   nx : a 1-by-1 matrix of floating point integers, the number of inputs of the function.
    //   randgen : a function or a list, the random number generator. (default = uniform random variables)
    //   n : a 1-by-1 matrix of floating point integers (default n=10000), the number of Monte-Carlo experiments, for each sensitivity index
    //   inrange : a 1-by-1 matrix of booleans (default inrange = %t), set to true to restrict the sensitivity indices into [0,1].
    //   si : a nx-by-ny matrix of doubles, the partial sensitivity indices, where nx is the number of input variables and ny is the number of output variables.
    //   nbevalf : a nx-by-1 matrix of doubles, the actual number of function evaluations.
    //   mi : a m-by-nx matrix of doubles, the multi-indices of the variables in si, where m=2^nx - 1. Each row in mi represents a group of variables. We have mi(k,i) = 1 if Xi is in the group of variables and 0 if not. 
    //
    // Description
    // The algorithm uses the Sobol method to compute the partial 
    // sensitivity indices.
    //
    // This method assumes that all the input random variables are 
    // independent.
    //
    // On output, the sensitivity indices are forced to be in the range [0,1].
    //
    // Any optional input argument equal to the empty matrix will be set to its
    // default value.
    //
    // See "Specifications of func" for the specifications 
    // of the "func" and "randgen" arguments.
    //
    // Examples
    // // Compute the partial sensitivity indices of the ishigami function.
    // // Three random variables uniform in [-pi,pi].
    // function y = ishigami (x)
    //   a=7.
    //   b=0.1
    //   s1=sin(x(:,1))
    //   s2=sin(x(:,2))
    //   x34 = x(:,3).^4
    //   y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
    // endfunction
    // function x = myrandgen ( m , i )
    //   x = distfun_unifrnd(-%pi,%pi,m,1)
    // endfunction
    // a=7.;
    // b=0.1;
    // exact = nisp_ishigamisa ( a , b )
    // n = 1000;
    // nx = 3;
    // [ si , nbevalf , mi ] = nisp_sobolsaAll ( ishigami , nx , myrandgen )
    // // The ANOVA decomposition can be seen more easily in
    // [mi si]
    // // The sum of si is 1.
    // sum(si) // expected = 1.
    //
    // Authors
    //  Copyright (C) 2014 - Michael Baudin
    //  Copyright (C) 2011 - Michael Baudin - DIGITEO

    // Load Internals lib
    path = nisp_getpath (  )
    nispinternallib  = lib(fullfile(path,"macros","internals"))

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_sobolsaAll" , rhs , 2 : 5 )
    apifun_checklhs ( "nisp_sobolsaAll" , lhs , [0 1 2 3] )
    //
    [ __sobolfo_func__ , nx , __sobolfo_randgen__ , n , inrange ] = ..
    nisp_sobolsaGetArgs ( "nisp_sobolsaAll", varargin )

    //
    // Proceed...
    // Create a sample A
    A = nisp_getsample ( n , nx , __sobolfo_randgen__ )
    // Create a sample B
    B = nisp_getsample ( n , nx , __sobolfo_randgen__ )
    // Perform the experiments in A
    ya = nisp_evalfunc ( A , __sobolfo_func__ )
    ny=size(ya,"c")
    //
    // Compute all the combinations of variables Xi1, Xi2, ..., Xip.
    // If nx=3, the matrix is :
    // 0 0 0
    // 1 0 0
    // 0 1 0
    // 0 0 1
    // 1 1 0
    // 1 0 1
    // 0 1 1
    // 1 1 1
    // It represents the possible combinations of variables: 
    // 1 means that Xi is in the subset, 0 means no variable Xi.
    mi = specfun_combinerepeat ( [1 0] , nx )'
    ks = sum(mi,"c")
    // Sort by increasing number of variables.
    [ignored,korder]=gsort(ks,"g","i")
    mi = mi(korder,:)
    // Eliminate the combination of variables 
    // where there are less than 1 variables, i.e. delete 
    // the row [0 0 0 ... 0].
    mi(1,:)=[]
    nrows = size(mi,"r")
    si=zeros(nrows,1)
    // Compute the first order sensitivity indices.
    [s,nbevalf] = nisp_sobolsaFirst ( __sobolfo_func__ , nx , __sobolfo_randgen__ , n , inrange )
    // The first nx sensitivity indices are already 
    // computed: just store them.
    for j=1:ny
        for k = 1 : nx
            // Get the variable i in this row: 
            // there is a unique non-zero in the row k.
            i = find(mi(k,:)==1)
            si(k,j) = s(i,j)
        end
    end
    // Compute the sensitivity indices for all these combinations
    // of at least two variables.
    for k = nx + 1 : nrows
        // Compute the sensitivity index for the group of 
        // variables in the row mi(k,:).
        knovar = find(mi(k,:)==0);
        C(:,knovar) = B(:,knovar);
        kvar = find(mi(k,:)==1)
        C(:,kvar)=A(:,kvar);
        yc = nisp_evalfunc ( C , __sobolfo_func__ );
        for j=1:ny
            si(k,j) = corrcoef(ya(:,j),yc(:,j));
            // Remove the lower order sensitivity indices.
            for i = 1 : k-1
                ivar = find(mi(i,:)==1);
                // The sensitivity index si(i) must be subtracted 
                // from si(k) is all variables in ivar are in kvar.
                tf = specfun_ismember ( ivar , kvar );
                if ( and(tf) ) then
                    si(k,j) = si(k,j) - si(i,j);
                end
            end
        end
    end
    nbevalf = nbevalf + ( nrows - nx -1 ) * n
    si = nisp_sensitivityTorange(si,inrange)
endfunction

