// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function nisp_plotsa(varargin)
    // Plot sensitivity indices.
    //
    // Calling Sequence
    //   nisp_plotsa(s)
    //   nisp_plotsa([s,smin,smax])
    //   nisp_plotsa(...,st)
    //   nisp_plotsa(...,[st,stmin,stmax])
    //
    // Parameters
    //   s : a nx-by-1, the first order sensitivity indices
    //   smin : a nx-by-1, the lower bound for the first order sensitivity indices
    //   smax : a nx-by-1, the upper bound for the first order sensitivity indices
    //   st : a nx-by-1 matrix of doubles, the total sensitivity indices
    //   stmin : a nx-by-1 matrix of doubles, the lower bound for the total sensitivity indices
    //   stmax : a nx-by-1 matrix of doubles, the upper bound for the total sensitivity indices
    //
    // Description
    // Plots the sensitivity indices.
    //
    // If the indice for the variable X(i) is exact, we have :
    //
    // s(i) <= st(i)
    //
    // This is not true if the indices are estimates : this 
    // can be checked in the plot.
    //
    // <inlinemediaobject>
    // <imageobject><imagedata fileref="nisp_plotsa.png"/></imageobject>
    // </inlinemediaobject>
    //
    // Examples
    // function x=myrandgen(m, i)
    // x = distfun_unifrnd(-%pi,%pi,m,1)
    // endfunction
    // a=7.;
    // b=0.1;
    // n = 1000;
    // nx = 3;
    // [s,nbevalf,smin,smax]=nisp_sobolsaFirst(..
    //   list(nisp_ishigami,a,b),nx,myrandgen,n);
    // [st,nbevalf,stmin,stmax]=nisp_sobolsaTotal(..
    //   list(nisp_ishigami,a,b),nx,myrandgen,n);
    //
    // scf();
    // nisp_plotsa(s);
    // 
    // scf();
    // subplot(2,2,1)
    // nisp_plotsa([s,smin,smax]);
    // title("[s,smin,smax]")
    // subplot(2,2,2)
    // nisp_plotsa(s,st);
    // title("s,st")
    // subplot(2,2,3)
    // title("[s,smin,smax],[st,stmin,stmax]")
    // nisp_plotsa([s,smin,smax],[st,stmin,stmax]);
    // title("[s,smin,smax],[st,stmin,stmax]")
    // subplot(2,2,4)
    // nisp_plotsa([],st);
    // title("[],st")
    //
    // Authors
    //   Michael Baudin - 2014

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_plotsa" , rhs , 1:2 )
    //
    firstind = varargin(1)
    totalind = apifun_argindefault ( varargin , 2 , [] )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_plotsa" , firstind ,   "s" ,  1 , "constant" )
    apifun_checktype ( "nisp_plotsa" , totalind ,   "st" ,  2 , "constant" )
    //
    // Check size
    if (firstind<>[]) then
        nx=size(firstind,"r")
        if (and(size(firstind,"c")<>[1,3])) then
            apifun_checkdims ( "nisp_plotsa", firstind, "s", [nx,1] )
        end
    end
    if (totalind<>[]) then
        if (size(totalind,"r")<>nx) then
            apifun_checkdims ( "nisp_plotsa", totalind, "st", [nx,1] )
        end
        if (and(size(totalind,"c")<>[1,3])) then
            apifun_checkdims ( "nisp_plotsa", totalind, "st", [nx,1] )
        end
    end
    //
    // Check content
    // Nothing to do
    //
    if (size(firstind,"c")==1) then
        s=firstind
        smin=[]
        smax=[]
        plotsbars=%f
    else
        s=firstind(:,1)
        smin=firstind(:,2)
        smax=firstind(:,3)
        plotsbars=%t
    end
    if (size(totalind,"c")==1) then
        st=totalind
        stmin=[]
        stmax=[]
        plotstbars=%f
    else
        st=totalind(:,1)
        stmin=totalind(:,2)
        stmax=totalind(:,3)
        plotstbars=%t
    end
    // The x shift, so that the first order sensitivity 
    // indice do not overlap the total sensitivity indice
    if (firstind==[]|totalind==[]) then
        // There is no first or no total indice, 
        // so there is no need to shift st.
        delta=0.
    else
        delta=0.3
    end
    //
    // Plot indices
    if (firstind<>[]) then
        plot((1:nx)',s,"bo")
    end
    if (totalind<>[]) then
        plot((1:nx)'+delta,st,"r^")
    end
    // Plot intervals
    if (plotsbars & firstind<>[]) then
        for i=1:nx
            plot([i,i],[smin(i),smax(i)],"b-");
        end
    end
    if (plotstbars & totalind<>[]) then
        for i=1:nx
            plot([i,i]+delta,[stmin(i),stmax(i)],"r-");
        end
    end

    plot([1,nx],[0,0],"k-") // Min bound
    plot([1,nx],[1,1],"k-") // Max bound

    // Legends and labels
    if (totalind==[]) then
        ylabel("First Order");
    elseif (firstind==[]) then
        ylabel(["Total Effect"]);
    else
        ylabel("Sensitivy Indice");
        legend(["First Order","Total Effect"]);
    end

    // Configure X-axis
    g=gca();
    g.data_bounds(:,1)=[0.5;nx+0.5];
    g.data_bounds(:,2)=[-0.1;1.1];
    g.auto_ticks=["off","on","on"];
    drawaxis(x=1:nx,y=0,dir="d",tics="v",sub_int=0);
    g.children(1).tics_labels="X"+string(1:nx);
endfunction
