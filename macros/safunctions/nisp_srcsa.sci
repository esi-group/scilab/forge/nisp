// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function [s,smin,smax] = nisp_srcsa ( varargin )
    // Compute SRC sensitivity indices.
    //
    // Calling Sequence
    //   s = nisp_srcsa( func, nx )
    //   s = nisp_srcsa( func, nx, randgen)
    //   s = nisp_srcsa( func, nx, randgen, n)
    //   s = nisp_srcsa( func, nx, randgen, n, inrange)
    //   s = nisp_srcsa( func, nx, randgen, n, inrange)
    //   s = nisp_srcsa( func, nx, randgen, n, inrange, c)
    //   [s,smin,smax] = nisp_srcsa(...)
    //
    // Parameters
    //   func : a function or a list, the name of the function to be evaluated. 
    //   nx : a 1-by-1 matrix of floating point integers, the number of inputs of the function.
    //   randgen : a function or a list, the random number generator. (default = uniform random variables)
    //   n : a 1-by-1 matrix of floating point integers (default n=10000), the number of Monte-Carlo experiments, for each sensitivity index
    //   inrange : a 1-by-1 matrix of booleans (default inrange = %t), set to true to restrict the sensitivity indices into [0,1].
    //   c : a 1-by-1 matrix of doubles (default c = 1-0.95), the level for the confidence interval. Must be in the range [0,0.5].
    //   s : a nx-by-1 matrix of doubles, the SRC sensitivity indices
    //   smin : a nx-by-ny matrix of doubles, the lower bound of the confidence interval.
    //   smax : a nx-by-ny matrix of doubles, the upper bound of the confidence interval.
    //
    // Description
    // The algorithm estimates the Standardized Regression Coefficients 
    // sensitivity indices. 
    //
    // The method is based on the estimate of a linear model : 
    // if the model is not linear (or approximately linear), 
    // the SRC indices are poor sensitivity indices. 
    // This can be check on output, by checking the condition:
    // <screen>
    // sum(s)>0.9 // Must be : T
    // </screen>
    // If this condition is wrong, this means that 
    // more than 10% of the variance of Y is not explained 
    // by a linear model. 
    // In this case, please use Sobol' indices, 
    // as computed, for example, by nisp_sobolsaFirst.
    //
    // The number of function evaluations is n.
    //
    // This method assumes that all the input random variables are 
    // independent.
    //
    // On output, if <literal>inrange</literal> is true, then 
    // the sensitivity indices are forced to be in the range [0,1]. 
    // Otherwise, if the exact value is zero, then it may happen that 
    // the estimate is negative.
    // Similarily, if the exact value is one, then it may happen that 
    // the estimate greater than one.
    // The <literal>inrange</literal> option manages this situation.
    //
    // Any optional input argument equal to the empty matrix will be set to its
    // default value.
    //
    // The confidence level 1-c is so that 
    //
    // <latex>
    // P(smin \leq s \leq smax)=1-c.
    // </latex>
    // 
    // This is done by the Fisher transformation of the correlation 
    // coefficient, as published by Martinez, 2005 (see below).
    //
    // See "Specifications of func" for the specifications 
    // of the "func" and "randgen" arguments.
    //
    // Examples
    // function x=myrandgen(m, i, mu, sigma)
    //    x = distfun_normrnd(mu(i),sigma(i),m,1)
    // endfunction
    // mu=[0 0 0 0 0]';
    // sigma=[1 2 3 4 5]';
    // a = [4 5 6 7 8 1]'
    // exact = nisp_sumsa ( a , mu , sigma );
    // disp(exact.S)
    // n = 1000;
    // nx = 5;
    // s=nisp_srcsa(list(nisp_sum,a),nx,..
    //     list(myrandgen,mu,sigma),n)
    // nisp_plotsa(s);
    // legend("SRC indices");
    //
    // // Get 95% confidence interval
    // scf();
    // [s,smin,smax]=nisp_srcsa(list(nisp_sum,a),nx,..
    //     list(myrandgen,mu,sigma),n)
    // nisp_plotsa([s,smin,smax]);
    //
    // // Example with ny=2 outputs
    // function y=myfunc(x,a1,a2)
    //     y1=nisp_sum(x,a1)
    //     y2=nisp_sum(x,a2)
    //     y=[y1,y2] // Each column is an output
    // endfunction
    // mu=[0 0 0 0 0]';
    // sigma=[1 2 3 4 5]';
    // a1 = [4 5 6 7 8 1]';
    // a2 = [8 7 6 5 4 3]';
    // n = 100000;
    // nx = 5;
    // s=nisp_srcsa(list(myfunc,a1,a2),nx,..
    //    list(myrandgen,mu,sigma),n);
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin

    // Load Internals lib
    path = nisp_getpath (  )
    nispinternallib  = lib(fullfile(path,"macros","internals"))

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_srcsa" , rhs , 2:6 )
    apifun_checklhs ( "nisp_srcsa" , lhs , 1:3 )
    //
    //
    __sobolfo_func__ = varargin(1)
    nx = varargin(2)
    __sobolfo_randgen__ = apifun_argindefault ( varargin , 3 , nisp_soboldefaultrgdef )
    n = apifun_argindefault ( varargin , 4 , 10000 )
    inrange = apifun_argindefault ( varargin , 5 , %t )
    c = apifun_argindefault ( varargin , 6 , 1-0.95 )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_srcsa" , __sobolfo_func__ ,   "func" ,  1 , [ "function" "list" ] )
    apifun_checktype ( "nisp_srcsa" , nx ,      "nx" ,     2 , "constant" )
    apifun_checktype ( "nisp_srcsa" , __sobolfo_randgen__ ,  "randgen" , 3 , [ "function" "list" ] )
    apifun_checktype ( "nisp_srcsa" , n ,  "n" , 4 , "constant" )
    apifun_checktype ( "nisp_srcsa" , inrange ,  "inrange" , 5 , "boolean" )
    apifun_checktype ( "nisp_sobolsaFirst", c ,   "c" ,  6 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "nisp_srcsa", nx, "nx", 2 )
    apifun_checkscalar ( "nisp_srcsa", n, "n", 4 )
    apifun_checkscalar ( "nisp_srcsa", inrange, "inrange", 5 )
    apifun_checkscalar ( "nisp_sobolsaFirst", c, "c", 6 )
    //
    // Check content
    apifun_checkgreq ( "nisp_srcsa" , nx ,  "nx" ,  2 , 1 )
    apifun_checkgreq ( "nisp_srcsa" , n ,   "n" ,   4 , 1 )
    apifun_checkrange ( "nisp_sobolsaFirst", c ,  "c" ,  6, 0., 0.5 )
    //
    // Proceed...
    // Create a sample X
    X = nisp_getsample ( n , nx , __sobolfo_randgen__ )
    // Perform the experiments in X
    y = nisp_evalfunc ( X , __sobolfo_func__ )
    ny=size(y,"c")
    if (or(size(y)<>[n,ny])) then
        error(sprintf(gettext("%s: Wrong size of output of function ""func"": %d-by-%d instead of %d-by-%d. Check the ""func"" argument.\n"), "nisp_srcsa", size(y,"r"), size(y,"c"),n,ny));
    end
    //
    // Estimate indices
    s=zeros(nx,ny)
    for i=1:nx
        for j=1:ny
            rho = corrcoef ( X(:,i) , y(:,j) );
            s(i,j) = rho^2;
        end
    end
    //
    // Compute the confidence interval
    // Get the C.I. for the correlation coefficients,
    // convert them back into C.I. for SRC indices.
    s = nisp_sensitivityTorange(s,inrange)
    [ smin , smax ] = nisp_sobolsaInterval ( sqrt(s), n, inrange, c )
    smin=smin.^2
    smax=smax.^2
    
endfunction

