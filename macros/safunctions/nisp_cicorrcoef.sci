// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function [rhomin,rhomax] = nisp_cicorrcoef(varargin)
    // Confidence interval for a correlation coefficient
    //
    // Calling Sequence
    //   [rhomin,rhomax] = nisp_cicorrcoef(rho,n)
    //   [rhomin,rhomax] = nisp_cicorrcoef(rho,n,c)
    //
    // Parameters
    // rho: a nx-by-ny matrix of doubles, the correlation coefficient. Should generally be in [0,1], but this is not mandatory.
    // n: a 1-by-1 matrix of doubles, the number of experiments
    // c : a 1-by-1 matrix of doubles (default c = 1-0.95), the level for the confidence interval. Must be in the range [0,0.5].
    // rhomin : a nx-by-ny matrix of doubles, the lower bound of the confidence interval
    // rhomax : a nx-by-ny matrix of doubles, the upper bound of the confidence interval
    //
    // Description
    // Computes a confidence interval at level 1-c for the correlation 
    // coefficient rho, computed with n samples.
    //
    // The default value of c corresponds to a 95% confidence interval.
    //
    // Mathematical notes
    //
    // This method uses the Fisher transformation :
    //
    // <latex>
    // z=\frac{1}{2} \ln\left(\frac{1+\rho}{1-\rho}\right)
    // </latex>
    //
    // Given two samples (Xi,Yi) for i=1,...,n, we denote by R the sample 
    // correlation between Xi and Yi, and suppose that the 
    // pairs (Xi,Yi) are independent.
    // Suppose that (X, Y) has a bivariate normal distribution with 
    // correlation coefficient rho. 
    //
    // Then z is approximately normally distributed with mean :
    //
    // <latex>
    // E(Z)=\frac{1}{2} \ln\left(\frac{1+\rho}{1-\rho}\right),
    // </latex>
    //
    // and variance :
    //
    // <latex>
    // V(Z)=\frac{1}{n-3}.
    // </latex>
    //
    // This allows to compute a confidence interval for Z.
    // The confidence interval for rho is computed from 
    // the inverse transformation :
    //
    // <latex>
    // \rho=\tanh(z)
    // </latex>
    //
    // Examples
    // // Confidence interval at 95%
    // rho=0.3;
    // n=100;
    // [rhomin,rhomax] = nisp_cicorrcoef(rho,n)
    // 
    // // Confidence interval at 99%
    // rho=0.3;
    // n=100;
    // c=1-0.99;
    // [rhomin,rhomax] = nisp_cicorrcoef(rho,n,c)
    //
    // // See C.I. depending on rho and n
    // n=100;
    // c=1-0.95;
    // rho=linspace(0,1)';
    // [rhomin10,rhomax10] = nisp_cicorrcoef(rho,10,c);
    // [rhomin100,rhomax100] = nisp_cicorrcoef(rho,100,c);
    // [rhomin1000,rhomax1000] = nisp_cicorrcoef(rho,1000,c);
    // plot(rho,rhomin10,"g-")
    // plot(rho,rhomin100,"b-")
    // plot(rho,rhomin1000,"r-")
    // plot(rho,rhomax10,"g-")
    // plot(rho,rhomax100,"b-")
    // plot(rho,rhomax1000,"r-")
    // xlabel("rho");
    // ylabel("95% Confidence Interval");
    // title("Sensitivity of the C.I. to rho and n");
    // legend(["n=10","n=100","n=1000"],"in_lower_right");
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Fisher_transformation
    // GdR Ondes & Mascot Num, "Analyse de sensibilite globale par decomposition de la variance", jean-marc.martinez@cea.fr, 13 janvier 2011, Institut Henri Poincare

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_cicorrcoef" , rhs , 2:3 )
    apifun_checklhs ( "nisp_cicorrcoef" , lhs , 2:2 )
    //
    //
    rho = varargin(1)
    n = varargin(2)
    c = apifun_argindefault ( varargin , 3 , 1-0.95 )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_cicorrcoef" , rho ,   "rho" ,  1 , "constant" )
    apifun_checktype ( "nisp_cicorrcoef" , n ,      "n" ,     2 , "constant" )
    apifun_checktype ( "nisp_cicorrcoef" , c ,  "c" , 3 , "constant" )
    //
    // Check size
    // do not test rho : general matrix
    apifun_checkscalar ( "nisp_cicorrcoef", n, "n", 2 )
    apifun_checkscalar ( "nisp_cicorrcoef", c, "c", 3 )
    //
    // Check content
    // Do not check range of s : might be negative in some cases,
    // depending on the accuracy of the estimate
    apifun_checkgreq ( "nisp_cicorrcoef" , n ,   "n" , 2 , 1 )
    apifun_checkrange ( "nisp_cicorrcoef", c ,  "c" , 3, 0., 0.5 )
    //
    // Proceed...
    nx=size(rho,"r")
    ny=size(rho,"c")
    rhomin=zeros(nx,ny)
    rhomax=zeros(nx,ny)
    // 1. For rho>=1.
    rhomin(rho>=1.)=1.
    rhomax(rho>=1.)=1.
    // 2. For rho<1.
    q = c/2
    x=distfun_norminv(q,0,1,%f)
    sigma = x/sqrt(n-3.)
    // Apply the Fisher transformation
    //z = 0.5 * log( (1 + rho) / (1 - rho) );
    // The following is more accurate for small rho.
    k=find(rho<1)
    z(k) = 0.5 * (log1p(rho(k))-log1p(-rho(k)));
    // Then z is Normal with variance sigma.
    zmin(k) = z(k) - sigma
    zmax(k) = z(k) + sigma
    // tanh is the inverse of 0.5*log((1+rho)/(1-rho))
    rhomin(k) = tanh(zmin(k))
    rhomax(k) = tanh(zmax(k))
endfunction
