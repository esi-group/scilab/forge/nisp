// Copyright (C) 2014 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function [st,nbevalf,stmin,stmax] = nisp_sobolsaTotal ( varargin )
    // Compute sensitivity indices by Sobol, Ishigami, Homma.
    //
    // Calling Sequence
    //   st = nisp_sobolsaTotal( func, nx )
    //   st = nisp_sobolsaTotal( func, nx, randgen)
    //   st = nisp_sobolsaTotal( func, nx, randgen, n)
    //   st = nisp_sobolsaTotal( func, nx, randgen, n, inrange)
    //   st = nisp_sobolsaTotal( func, nx, randgen, n, inrange, c)
    //   [st, nbevalf] = nisp_sobolsaTotal( ... )
    //   [st, nbevalf, stmin, stmax] = nisp_sobolsaTotal( ... )
    //
    // Parameters
    //   func : a function or a list, the name of the function to be evaluated. 
    //   nx : a 1-by-1 matrix of floating point integers, the number of inputs of the function.
    //   randgen : a function or a list, the random number generator. (default = uniform random variables)
    //   n : a 1-by-1 matrix of floating point integers (default n=10000), the number of Monte-Carlo experiments, for each sensitivity index
    //   inrange : a 1-by-1 matrix of booleans (default inrange = %t), set to true to restrict the sensitivity indices into [0,1].
    //   c : a 1-by-1 matrix of doubles (default c = 1-0.95), the level for the confidence interval. Must be in the range [0,0.5].
    //   st : a nx-by-ny matrix of doubles, the total sensitivity indices, where nx is the number of input variables and ny is the number of output variables.
    //   nbevalf : a nx-by-1 matrix of doubles, the actual number of function evaluations.
    //   stmin : a nx-by-ny matrix of doubles, the lower bound of the confidence interval.
    //   stmax : a nx-by-ny matrix of doubles, the upper bound of the confidence interval.
    //
    // Description
    // The algorithm uses the Sobol method to compute the total 
    // sensitivity indices.
    //
    // On output, if <literal>inrange</literal> is true, then 
    // the sensitivity indices are forced to be in the range [0,1]. 
    // Otherwise, if the exact value is zero, then it may happen that 
    // the estimate is negative.
    // Similarily, if the exact value is one, then it may happen that 
    // the estimate greater than one.
    // The <literal>inrange</literal> option manages this situation.
    //
    // On output, the sensitivity indices are forced to be in the range [0,1].
    // 
    // The confidence level 1-c is so that 
    //
    // <latex>
    // P(smin \leq s \leq smax)=1-c.
    // </latex>
    // 
    // This is done by the Fisher transformation of the sensitivity 
    // indices, as published by Martinez, 2005 (see below).
    //
    // Any optional input argument equal to the empty matrix will be set to its
    // default value.
    //
    // See "Specifications of func" for the specifications 
    // of the "func" and "randgen" arguments.
    //
    // Examples
    // // Compute the total sensitivity indices of the ishigami function.
    // // Three random variables uniform in [-pi,pi].
    // function y = ishigami (x)
    //   a=7.
    //   b=0.1
    //   s1=sin(x(:,1))
    //   s2=sin(x(:,2))
    //   x34 = x(:,3).^4
    //   y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
    // endfunction
    // function x = myrandgen ( m , i )
    //   x = distfun_unifrnd(-%pi,%pi,m,1)
    // endfunction
    // a=7.;
    // b=0.1;
    // exact = nisp_ishigamisa ( a , b )
    // nx = 3;
    // [ st , nbevalf ] = nisp_sobolsaTotal(ishigami,nx,myrandgen)
    // // Configure number of samples
    // n = 1000;
    // [ st , nbevalf ] = nisp_sobolsaTotal(ishigami,nx,myrandgen,n)
    // // Get confidence intervals
    // [st,nbevalf,stmin,stmax]=nisp_sobolsaTotal(ishigami,nx,myrandgen)
    // // Configure the confidence interval length at 95%
    // [st,nbevalf,stmin,stmax]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,[],1-0.95)
    // // Configure the confidence interval length at 99% 
    // [st,nbevalf,stmin,stmax]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,[],1-0.99)
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    // Load Internals lib
    path = nisp_getpath (  )
    nispinternallib  = lib(fullfile(path,"macros","internals"))


    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_sobolsaTotal" , rhs , 2:6 )
    apifun_checklhs ( "nisp_sobolsaTotal" , lhs , 1:4 )
    //
    __sobolfo_func__ = varargin(1)
    nx = varargin(2)
    __sobolfo_randgen__ = apifun_argindefault ( varargin , 3 , nisp_soboldefaultrgdef )
    n = apifun_argindefault ( varargin , 4 , 10000 )
    inrange = apifun_argindefault ( varargin , 5 , %t )
    c = apifun_argindefault ( varargin , 6 , 1-0.95 )
    //
    // Proceed...
    // Create a sample A
    A = nisp_getsample ( n , nx , __sobolfo_randgen__ )
    // Create a sample B
    B = nisp_getsample ( n , nx , __sobolfo_randgen__ )
    // Perform the experiments in A
    ya = nisp_evalfunc ( A , __sobolfo_func__ )
    ny=size(ya,"c")
    //
    for i = 1 : nx
        C(:,1:i-1) = A(:,1:i-1);
        C(:,i)=B(:,i);
        C(:,i+1:nx) = A(:,i+1:nx);
        yc = nisp_evalfunc ( C , __sobolfo_func__ )
        for j=1:ny
            st(i,j) = 1 - corrcoef ( ya(:,j) , yc(:,j) );
        end
    end
    //
    // Compute the confidence interval
    st = nisp_sensitivityTorange(st,inrange)
    [ tempmin , tempmax ] = nisp_sobolsaInterval(1-st, n, inrange, c )
    stmin=1-tempmax
    stmax=1-tempmin
    //
    // Compute function evaluations
    nbevalf = n + nx * n
endfunction
