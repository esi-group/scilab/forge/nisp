// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- Non-regression test for bug 863 -->
//
// <-- URL -->
// http://forge.scilab.org/index.php/p/nisp/issues/863/
//
// <-- Short Description -->
// polychaos_sasummary fails.

function y = Ishigami (x)
  a = 7; b = 0.1;
  y(1) = sin(x(1)) + a * sin(x(2))**2 + b * x(3)**4 * sin(x(1));
  y(2) = 2. * y(1);
endfunction

// 4.1 Représentation des paramètres incertains
srvu = setrandvar_new();
for i=1:3
  vu(i) = randvar_new("Uniforme", -%pi, +%pi);
  setrandvar_addrandvar ( srvu, vu(i));
end
setrandvar_getlog(srvu)
nx = setrandvar_getdimension ( srvu ); 

// Représentation des variables stochastiques
srvx = setrandvar_new(nx);
setrandvar_getlog(srvx)
ny = 2;
pc = polychaos_new ( srvx , ny );

// 4.3 Plan d'expériences numériques
degre = 6;
planx = setrandvar_buildsample(srvx,"Petras",degre);
np = setrandvar_getsize(srvx);

planu = setrandvar_buildsample( srvu , srvx );

// 4.4 Réalisation du plan d'expériences numériques
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc, np);
for k=1:np
  inputdata = planu(k,:);
  outputdata = Ishigami(inputdata);
  polychaos_settarget(pc,k,outputdata');
end

// 4.5 Calcul des coefficients
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
polychaos_sasummary(pc);
polychaos_destroy(pc);
for i=1:3
  randvar_destroy(vu(i));
end
setrandvar_destroy(srvu);
setrandvar_destroy(srvx);
