// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- Non-regression test for bug 210 -->
//
// <-- URL -->
// http://forge.scilab.org/index.php/p/nisp/issues/210/
//
// <-- Short Description -->
// The computeoutput / getoutput functions do not work.

function y = ishigami(x)
  a=7.;
  b=0.1;
  s1=sin(x(1));
  s2=sin(x(2));
  y(1)=s1+a*s2^2+b*s1*x(3)^4;
endfunction


// Stochastic random variables
nx = 3;
rvx1 = randvar_new("Uniforme");
rvx2 = randvar_new("Uniforme");
rvx3 = randvar_new("Uniforme");

srvx = setrandvar_new( );
setrandvar_addrandvar ( srvx , rvx1 );
setrandvar_addrandvar ( srvx , rvx2 );
setrandvar_addrandvar ( srvx , rvx3 );

// Uncertain random variables
rvu1=randvar_new("Uniforme",-%pi,%pi);
rvu2=randvar_new("Uniforme",-%pi,%pi);
rvu3=randvar_new("Uniforme",-%pi,%pi);
srvu=setrandvar_new();
setrandvar_addrandvar(srvu,rvu1);
setrandvar_addrandvar(srvu,rvu2);
setrandvar_addrandvar(srvu,rvu3);

degre=12;
setrandvar_buildsample(srvx,"Petras",degre);
sampling=setrandvar_buildsample(srvu,srvx);

// Chaos polynomial
pc=polychaos_new(srvx,1);
polychaos_setdegree(pc,degre);
np=setrandvar_getsize(srvu);
polychaos_setsizetarget(pc,np);

// Perform the Design of Experiments
for k=1:np
  inputdata=sampling(k,:);
  outputdata(k)=ishigami(inputdata);
  polychaos_settarget(pc,k,outputdata(k));
end

// Compute the coefficients
polychaos_computeexp(pc,srvx,"Integration");



// Input of the polynomial : stochastic random variables in [0,1]
x = [0.15 0.30 0.45];     
computed = polychaos_compute ( pc , x );

// input of ishigami : in [-pi,pi]
// We could directly set u = (x - 0.5) * 2. * %pi; 
// but the following is more general:
u(1) = randvar_getvalue ( rvu1 , rvx1 , x(1) );
u(2) = randvar_getvalue ( rvu2 , rvx2 , x(2) );
u(3) = randvar_getvalue ( rvu3 , rvx3 , x(3) );

expected = ishigami(u);

assert_checkalmostequal ( computed, expected, 1.e-4 );

polychaos_destroy(pc);
setrandvar_destroy(srvu);
setrandvar_destroy(srvx);
randvar_destroy(rvu1);
randvar_destroy(rvu2);
randvar_destroy(rvu3);
randvar_destroy(rvx1);
randvar_destroy(rvx2);
randvar_destroy(rvx3);


