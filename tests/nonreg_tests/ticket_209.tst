// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- Non-regression test for bug 209 -->
//
// <-- URL -->
// http://forge.scilab.org/index.php/p/nisp/issues/209/
//
// <-- Short Description -->
// The getgroupinter function can fail.

function [ pc , srv , rvx1 , rvx2 ] = createMyPolychaos ()
 rvx1 = randvar_new("Normale");
 rvx2 = randvar_new("Uniforme");
 srv = setrandvar_new();
 setrandvar_addrandvar ( srv,rvx1);
 setrandvar_addrandvar ( srv,rvx2);
 ny = 1;
 pc = polychaos_new ( srv , ny );
 degre = 2;
 setrandvar_buildsample(srv,"Quadrature",degre);
 np = 9;
 polychaos_setsizetarget(pc,np);
 nx = polychaos_getdiminput(pc);
 ny = polychaos_getdimoutput(pc);
 outputdata = [
    0.1566233  
    0.2344555  
    0.3122878  
    1.1690525  
    1.75       
    2.3309475  
    2.1814817  
    3.2655445  
    4.3496073  
  ];
  polychaos_settarget(pc,outputdata);
  polychaos_setdegree(pc,degre);
endfunction


// Test polychaos_setgroupempty ( pc )
// Test polychaos_getgroupinter ( pc , ovar )
// Test polychaos_getgroupinter ( pc )
[ pc , srv , rvx1 , rvx2 ] = createMyPolychaos();
polychaos_computeexp(pc,srv,"Integration");
polychaos_setgroupempty ( pc );
polychaos_setgroupaddvar ( pc , 1 );
polychaos_setgroupaddvar ( pc , 2 );
computed = polychaos_getgroupinter ( pc );
expected = 0.04687499486871491;
assert_checkalmostequal ( computed , expected , 1.e-13 );
polychaos_destroy(pc);
setrandvar_destroy(srv);
randvar_destroy(rvx1);
randvar_destroy(rvx2);


