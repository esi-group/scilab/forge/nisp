// Copyright (C) 2012 - 2013 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- Non-regression test for bug 268 -->
//
// <-- URL -->
// http://forge.scilab.org/index.php/p/nisp/issues/268/

nx = 3;
srvx = setrandvar_new( nx );
degre = 9;
setrandvar_buildsample(srvx,"Petras",degre);
filename=fullfile(TMPDIR,"file.txt");
setrandvar_save(srvx,filename);
path = nisp_getpath();
if ( getos() == "Linux" ) then
    baseref="Petras2variables.linux.txt";
else
    baseref="Petras2variables.txt";
end
reffile = fullfile(path,"tests","nonreg_tests",baseref);
assert_checkfilesequal(reffile,filename);
setrandvar_destroy(srvx);

