// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- Non-regression test for bug 257 -->
//
// <-- URL -->
// http://forge.scilab.org/index.php/p/nisp/issues/257/
//
// <-- Short Description -->
// The polychaos_getgroupind function does not generate an error when 
// the group has not been defined.

// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));

// Test polychaos_getgroupind ( pc , ovar )
// Test polychaos_getgroupind ( pc )
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
// Use an empty group:
polychaos_printgroup(pc);
g = polychaos_getgroup(pc);
assert_checkequal ( g , [] );
computed = polychaos_getgroupind ( pc , 1 );
expected = 0.;
assert_checkalmostequal ( computed , expected );
// Add the first variable:
polychaos_setgroupaddvar ( pc , 1 );
polychaos_printgroup(pc);
g = polychaos_getgroup(pc);
assert_checkequal ( g , 1 );
computed = polychaos_getgroupind ( pc );
expected = 0.765625;
assert_checkalmostequal ( computed , expected , 1.e-7 );
// Add the second variable:
polychaos_setgroupaddvar ( pc , 2 );
polychaos_printgroup(pc);
g = polychaos_getgroup(pc);
assert_checkequal ( g , [1;2] );
computed = polychaos_getgroupind ( pc );
expected = 1.;
assert_checkalmostequal ( computed , expected , 1.e-7 );
// Set an empty group
polychaos_setgroupempty(pc);
polychaos_printgroup(pc);
g = polychaos_getgroup(pc);
assert_checkequal ( g , [] );
// Add the second variable:
polychaos_setgroupaddvar ( pc , 2 );
polychaos_printgroup(pc);
g = polychaos_getgroup(pc);
assert_checkequal ( g , 2 );
computed = polychaos_getgroupind ( pc );
expected = 0.1875;
assert_checkalmostequal ( computed , expected , 1.e-7 );
//
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );


