// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- Non-regression test for bug 211 -->
//
// <-- URL -->
// http://forge.scilab.org/index.php/p/nisp/issues/211/
//
// <-- Short Description -->
// The "SmolyakGauss" sampling does not work.

vx1 = randvar_new("Normale");
vx2 = randvar_new("Uniforme");
srvx = setrandvar_new();
setrandvar_addrandvar ( srvx, vx1);
setrandvar_addrandvar ( srvx, vx2);
degre = 2;
setrandvar_buildsample(srvx,"SmolyakGauss",degre);
setrandvar_buildsample(srvx,"SmolyakFejer",degre);
setrandvar_buildsample(srvx,"SmolyakTrapeze",degre);
randvar_destroy(vx1);
randvar_destroy(vx2);
setrandvar_destroy(srvx);


