// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



// Prints zero tokens
nisp_printall();

// A real use case
nx = 3;
srvx = setrandvar_new( nx );
//
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);
//
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);

degre = 9;
setrandvar_buildsample(srvx,"Petras",degre);
setrandvar_buildsample( srvu , srvx );
pc = polychaos_new ( srvx , 1 );
// Prints 3 random variables, 2 random sets, 1 chaos polynomial
nisp_printall();
randvar_destroy ( rvu1 );
randvar_destroy ( rvu2 );
randvar_destroy ( rvu3 );
setrandvar_destroy ( srvu );
polychaos_destroy ( pc );
setrandvar_destroy ( srvx );
// Prints zero tokens
nisp_printall();

