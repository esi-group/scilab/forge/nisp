// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- JVM NOT MANDATORY -->


// Test setrandvar_getsize, when there is no DOE
srv = setrandvar_new ( );
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
s = setrandvar_getsize ( srv );
assert_checkequal ( s , 0 );
setrandvar_destroy(srv);
randvar_destroy(rv1);
randvar_destroy(rv2);

