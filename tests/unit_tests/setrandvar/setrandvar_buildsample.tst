// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- JVM NOT MANDATORY -->

// Test setrandvar_buildsample with ( name , np ) and with MonteCarlo
nisp_initseed ( 0 );
srv = setrandvar_new ( );
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
np = 1000;
name = "MonteCarlo";
sampling = setrandvar_buildsample ( srv , name , np );
// Check sampling of random variable #1
computed = mean(sampling(1:np,1));
expected = 1.0;
assert_checkalmostequal ( computed , expected , 1.e-1 );
// Check sampling of random variable #2
computed = mean(sampling(1:np,2));
expected = 1.75;
assert_checkalmostequal ( computed , expected , 1.e-1 );
setrandvar_destroy(srv);
randvar_destroy(rv1);
randvar_destroy(rv2);

// Test setrandvar_buildsample with ( name , np ) and with Lhs
nisp_initseed ( 0 );
srv = setrandvar_new ( );
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
np = 1000;
name = "Lhs";
sampling = setrandvar_buildsample ( srv , name , np );
// Check sampling of random variable #1
computed = mean(sampling(1:np,1));
expected = 1.0;
assert_checkalmostequal ( computed , expected , 1.e-15 );
// Check sampling of random variable #2
computed = mean(sampling(1:np,2));
expected = 1.75;
assert_checkalmostequal ( computed , expected , 1.e-15 );
setrandvar_destroy(srv);
randvar_destroy(rv1);
randvar_destroy(rv2);

// Test setrandvar_buildsample with ( name , np ) and with QmcSobol
nisp_initseed ( 0 );
srv = setrandvar_new ( );
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
np = 1000;
name = "QmcSobol";
sampling = setrandvar_buildsample ( srv , name , np );
// Check sampling of random variable #1
computed = mean(sampling(1:np,1));
expected = 1.0;
assert_checkalmostequal ( computed , expected , 1.e-3 );
// Check sampling of random variable #2
computed = mean(sampling(1:np,2));
expected = 1.75;
assert_checkalmostequal ( computed , expected , 1.e-3 );
setrandvar_destroy(srv);
randvar_destroy(rv1);
randvar_destroy(rv2);

// TODO : test with ( name , np ) and names :
//   name = "Quadrature"
//   name = "Petras"
//   name = "SmolyakGauss"
//   name = "SmolyakTrapeze"
//   name = "SmolyakFejer"
//   name = "SmolyakClenshawCurtis"

// Test setrandvar_buildsample with ( name , np , ne ) and with MonteCarlo
srv = setrandvar_new ( );
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
np = 1000;
ne = 10;
name = "MonteCarlo";
// setrandvar_buildsample ( srv , name , np , ne ); > Generates an error which cannot be tested
setrandvar_destroy(srv);
randvar_destroy(rv1);
randvar_destroy(rv2);

// Test setrandvar_buildsample with ( name , np , ne ) and with LhsMaxMin
srv = setrandvar_new ( );
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
np = 1000;
ne = 10;
name = "LhsMaxMin";
sampling = setrandvar_buildsample ( srv , name , np , ne ); 
// Check sampling of random variable #1
computed = mean(sampling(1:np,1));
expected = 1.0;
assert_checkalmostequal ( computed , expected , 1.e-3 );
// Check sampling of random variable #2
computed = mean(sampling(1:np,2));
expected = 1.75;
assert_checkalmostequal ( computed , expected , 1.e-3 );
setrandvar_destroy(srv);
randvar_destroy(rv1);
randvar_destroy(rv2);

// Test setrandvar_buildsample with a source setrandvar
nisp_initseed ( 0 );
srv = setrandvar_new ( );
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
np = 1000;
name = "MonteCarlo";
sampling = setrandvar_buildsample ( srv , name , np ); 
srv2 = setrandvar_new ( );
setrandvar_addrandvar ( srv2 , rv1 );
setrandvar_addrandvar ( srv2 , rv2 );
sampling = setrandvar_buildsample ( srv2 , srv ); 
// Check sampling of random variable #1
computed = mean(sampling(1:np,1));
expected = 1.0;
assert_checkalmostequal ( computed , expected , 1.e-1 );
// Check sampling of random variable #2
computed = mean(sampling(1:np,2));
expected = 1.75;
assert_checkalmostequal ( computed , expected , 1.e-1 );
setrandvar_destroy(srv2);
setrandvar_destroy(srv);
randvar_destroy(rv1);
randvar_destroy(rv2);


