// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- JVM NOT MANDATORY -->

// Try setrandvar_tokens
// At start, no randvar should exist.
srvlist = setrandvar_tokens();
assert_checkequal ( srvlist , [] );
nbsrv = setrandvar_size();
assert_checkequal ( nbsrv , 0 );

// The simplest possible test.
srv = setrandvar_new ( );
setrandvar_destroy(srv);

// Check that multiple destroy generate an error : cannot test this !
srv = setrandvar_new ( );
setrandvar_destroy(srv);
//setrandvar_destroy(srv);

// Create two randvars and check them
// Test setrandvar_size, setrandvar_new, setrandvar_destroy
srv1 = setrandvar_new();
srv2 = setrandvar_new();
nbsrv = setrandvar_size();
assert_checkequal ( nbsrv , 2 );
srvlist = setrandvar_tokens();
assert_checkequal ( size(srvlist,2) , 2 );
setrandvar_destroy(srv1);
setrandvar_destroy(srv2);
nbsrv = setrandvar_size();
assert_checkequal ( nbsrv , 0 );

// Create a set of 5 Uniform random variables
srv1 = setrandvar_new ( 5 );
setrandvar_destroy(srv1);

// Create a set random variables from a file
// TODO : what should be the content of the file ?
//srv1 = setrandvar_new ( "setrandvar.data" );
//setrandvar_destroy(srv1);

// Test freememory
srv1 = setrandvar_new ( 5 );
setrandvar_freememory ( srv1 );
setrandvar_destroy(srv1);
