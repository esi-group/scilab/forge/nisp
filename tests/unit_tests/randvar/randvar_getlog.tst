// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- JVM NOT MANDATORY -->

// Try randvar_getlog
rv = randvar_new("Normale",1.0,0.5);
randvar_getlog(rv);
randvar_destroy(rv);
