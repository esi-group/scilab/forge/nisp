// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- JVM NOT MANDATORY -->

// Try randvar_getvalue on Normal law and 2 parameters
nisp_initseed ( 0 );
mu = 1.0;
sigma = 0.5;
rv = randvar_new("Normale" , mu , sigma);
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
computed = mean (values);
expected = mu;
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = variance (values);
expected = sigma^2;
assert_checkalmostequal ( computed , expected , 1.e-1 );
randvar_destroy(rv);

// Try randvar_getvalue on Normal law and default parameters
nisp_initseed ( 0 );
rv = randvar_new("Normale");
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
mu = 0.0;
sigma = 1.0;
computed = mean (values);
expected = mu;
assert_checkalmostequal ( computed , expected , [], 1.e-1 );
computed = variance (values);
expected = sigma^2;
assert_checkalmostequal ( computed , expected , 1.e-1 );
randvar_destroy(rv);

//
// Test with Uniform law and 2 parameters
//
nisp_initseed ( 0 );
a= 2;
b = 3;
rv = randvar_new("Uniforme" , a , b );
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
computed = mean (values);
expected = (a+b)/2;
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = variance (values);
expected = (b-a)^2/12;
assert_checkalmostequal ( computed , expected , 1.e-1 );
randvar_destroy(rv);

//
// Test with Uniform law and default parameter
//
nisp_initseed ( 0 );
rv = randvar_new("Uniforme");
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
a = 0.0;
b = 1.0;
computed = mean (values);
expected = (a+b)/2;
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = variance (values);
expected = (b-a)^2/12;
assert_checkalmostequal ( computed , expected , 1.e-1 );
randvar_destroy(rv);

// What if the law does not exist ?
// TODO : problem : the unit test system does not allow to test error cases.
//rv1 = randvar_new("Foo",2.0,3.0);

// Test Exponentielle with 1 parameter
nisp_initseed ( 0 );
lambda = 2.0;
rv = randvar_new("Exponentielle" , lambda );
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
computed = mean (values);
expected = 1/lambda;
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = variance (values);
expected = 1/lambda^2;
assert_checkalmostequal ( computed , expected , 1.e-1 );
randvar_destroy(rv);

// Test Exponentielle with default parameter
nisp_initseed ( 0 );
rv = randvar_new("Exponentielle");
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
lambda = 1.0;
computed = mean (values);
expected = 1/lambda;
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = variance (values);
expected = 1/lambda^2;
assert_checkalmostequal ( computed , expected , 1.e-1 );
randvar_destroy(rv);

// Test LogNormale with 2 parameters
nisp_initseed ( 0 );
mu = 1.0;
sigma = 2.0;
rv = randvar_new("LogNormale",mu,sigma);
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
M=exp(mu+sigma^2/2);
V=(exp(sigma^2)-1)*exp(2*mu+sigma^2);
computed = mean (values);
assert_checkalmostequal ( computed , M , 2.e-1 );
computed = variance (values);
assert_checkalmostequal ( computed , V , 9.e-1 );
randvar_destroy(rv);

// Test LogNormale with default parameters
nisp_initseed ( 0 );
rv = randvar_new("LogNormale");
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
mu = 0.;
sigma = 1.0;
M=exp(mu+sigma^2/2);
V=(exp(sigma^2)-1)*exp(2*mu+sigma^2);
computed = mean (values);
assert_checkalmostequal ( computed , M , 1.e-1 );
computed = variance (values);
assert_checkalmostequal ( computed , V , 2.e-1 );
randvar_destroy(rv);

// Test LogUniforme with 2 parameters
nisp_initseed ( 0 );
a = 10.0;
b = 20.0;
rv = randvar_new("LogUniforme",a,b);
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
computed = mean (log(values));
mu = (a+b)/2;
expected = mu;
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = variance (log(values));
expected = (b-a)^2/12;
assert_checkalmostequal ( computed , expected , 1.e-1 );
randvar_destroy(rv);

// Test LogUniforme with default parameters
nisp_initseed ( 0 );
rv = randvar_new("LogUniforme");
nbshots = 1000;
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
a = 0.;
b = 1.;
computed = mean (log(values));
mu = (a+b)/2;
expected = mu;
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = variance (log(values));
expected = (b-a)^2/12;
assert_checkalmostequal ( computed , expected , 1.e-1 );
randvar_destroy(rv);
//
// Check errors
instr = "rv2 = randvar_new(""LogNormale"",-12.0,-12.0)";
errmsg = ["NISP: Error at the library level:";"Nisp(RandomVariable::RandomVariable) : law LogNormale and b= -12 <=0"];
assert_checkerror(instr,errmsg);
