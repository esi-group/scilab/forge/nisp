// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
// <-- JVM NOT MANDATORY -->
//
// Test transformations
//
rtol=1.e-1;
//
// Uniforme -> LogUniforme
// (Transform a Uniform outcome into a LogUniforme outcome)
//
nisp_initseed(0);
a=10.0;
b=20.0;
rv=randvar_new("LogUniforme",a,b);
rv2=randvar_new("Uniforme",2,3);
nbshots=1000;
for i=1:nbshots
  value2=randvar_getvalue(rv2);
  values(i)=randvar_getvalue(rv,rv2,value2);
end
computed=mean (log(values));
expected=(a+b)/2;
assert_checkalmostequal(computed,expected,rtol);
computed=variance(log(values));
expected=(b-a)^2/12;
assert_checkalmostequal(computed,expected,rtol);
randvar_destroy(rv);
randvar_destroy(rv2);
//
// Uniforme -> Uniforme
//
nisp_initseed(0);
a=10.0;
b=20.0;
rv=randvar_new("Uniforme",a,b);
rv2=randvar_new("Uniforme",2,3);
nbshots=1000;
for i=1:nbshots
  value2=randvar_getvalue(rv2);
  values(i)=randvar_getvalue(rv,rv2,value2);
end
computed=mean (values);
expected=(a+b)/2;
assert_checkalmostequal(computed,expected,rtol);
computed=variance(values);
expected=(b-a)^2/12;
assert_checkalmostequal(computed,expected,rtol);
randvar_destroy(rv);
randvar_destroy(rv2);
//
// Uniforme -> Normale
//
nisp_initseed(0);
mu=10.0;
sigma=20.0;
rv=randvar_new("Normale",mu,sigma);
rv2=randvar_new("Uniforme",2,3);
nbshots=1000;
for i=1:nbshots
  value2=randvar_getvalue(rv2);
  values(i)=randvar_getvalue(rv,rv2,value2);
end
computed=mean (values);
expected=mu;
assert_checkalmostequal(computed,expected,rtol);
computed=variance(values);
expected=sigma^2;
assert_checkalmostequal(computed,expected,rtol);
randvar_destroy(rv);
randvar_destroy(rv2);
//
// Uniforme -> LogNormale
//
nisp_initseed(0);
mu=10.0;
sigma=5;
rv=randvar_new("LogNormale",mu,sigma);
rv2=randvar_new("Uniforme",2,3);
nbshots=1000;
for i=1:nbshots
  value2=randvar_getvalue(rv2);
  values(i)=randvar_getvalue(rv,rv2,value2);
end
computed=mean (log(values));
expected=mu;
assert_checkalmostequal(computed,expected,rtol);
computed=variance(log(values));
expected=sigma^2;
assert_checkalmostequal(computed,expected,rtol);
randvar_destroy(rv);
randvar_destroy(rv2);
//
// Uniforme -> Exponentielle
//
nisp_initseed(0);
scale=1/12.0;
rv=randvar_new("Exponentielle",scale);
rv2=randvar_new("Uniforme",2,3);
nbshots=1000;
for i=1:nbshots
  value2=randvar_getvalue(rv2);
  values(i)=randvar_getvalue(rv,rv2,value2);
end
computed=mean (values);
expected=1/scale;
assert_checkalmostequal(computed,expected,rtol);
computed=variance(values);
expected=1/scale^2;
assert_checkalmostequal(computed,expected,rtol);
randvar_destroy(rv);
randvar_destroy(rv2);
// Normale > Uniforme
nisp_initseed(0);
a=2;
b=3;
rv=randvar_new("Uniforme",a,b);
rv2=randvar_new("Normale",5,7);
nbshots=1000;
for i=1:nbshots
  value2=randvar_getvalue(rv2);
  values(i)=randvar_getvalue(rv,rv2,value2);
end
computed=mean (values);
expected=(a+b)/2;
assert_checkalmostequal(computed,expected,rtol);
computed=variance(values);
expected=(b-a)^2/12;
assert_checkalmostequal(computed,expected,rtol);
randvar_destroy(rv);
randvar_destroy(rv2);
//
// LogNormale -> Uniforme
//
nisp_initseed(0);
a=2;
b=3;
mu=10.0;
sigma=5;
rv=randvar_new("Uniforme",a,b);
rv2=randvar_new("LogNormale",mu,sigma);
nbshots=1000;
for i=1:nbshots
  value2=randvar_getvalue(rv2);
  values(i)=randvar_getvalue(rv,rv2,value2);
end
computed=mean (values);
expected=(a+b)/2;
assert_checkalmostequal(computed,expected,rtol);
computed=variance(values);
expected=(b-a)^2/12;
assert_checkalmostequal(computed,expected,rtol);
randvar_destroy(rv);
randvar_destroy(rv2);
//
// LogUniforme -> Uniforme
//
nisp_initseed(0);
a=2;
b=3;
rv=randvar_new("Uniforme",a,b);
rv2=randvar_new("LogUniforme",10,20);
nbshots=1000;
for i=1:nbshots
  value2=randvar_getvalue(rv2);
  values(i)=randvar_getvalue(rv,rv2,value2);
end
computed=mean (values);
expected=(a+b)/2;
assert_checkalmostequal(computed,expected,rtol);
computed=variance(values);
expected=(b-a)^2/12;
assert_checkalmostequal(computed,expected,rtol);
randvar_destroy(rv);
randvar_destroy(rv2);
//
// Exponentielle -> Uniforme
//
nisp_initseed(0);
a=2;
b=3;
rv=randvar_new("Uniforme",a,b);
rv2=randvar_new("Exponentielle",scale);
nbshots=1000;
for i=1:nbshots
  value2=randvar_getvalue(rv2);
  values(i)=randvar_getvalue(rv,rv2,value2);
end
computed=mean (values);
expected=(a+b)/2;
assert_checkalmostequal(computed,expected,rtol);
computed=variance(values);
expected=(b-a)^2/12;
assert_checkalmostequal(computed,expected,rtol);
randvar_destroy(rv);
randvar_destroy(rv2);
