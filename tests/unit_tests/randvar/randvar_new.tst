// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- JVM NOT MANDATORY -->

// The simplest possible test.
rv = randvar_new ( "Exponentielle" );
randvar_destroy(rv);

// Create two randvars and check them
// Test randvar_size, randvar_new, randvar_destroy
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
nbrv = randvar_size();
assert_checkequal ( nbrv , 2 );
rvlist = randvar_tokens();
assert_checkequal ( size(rvlist,2) , 2 );
randvar_destroy(rv1);
randvar_destroy(rv2);
rvlist = randvar_tokens();
assert_checkequal ( rvlist , [] );
nbrv = randvar_size();
assert_checkequal ( nbrv , 0 );

// Check that the size is zero
nbrv = randvar_size();
assert_checkequal ( nbrv , 0 );
rvlist = randvar_tokens();
assert_checkequal ( rvlist , [] );
