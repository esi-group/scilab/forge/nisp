// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- JVM NOT MANDATORY -->

// Try randvar_tokens
// At start, no randvar should exist.
rvlist = randvar_tokens();
assert_checkequal ( rvlist , [] );
nbrv = randvar_size();
assert_checkequal ( nbrv , 0 );

