
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));



// Test polychaos_getindextotal ( pc , ovar1 , ovar2 )
// Test polychaos_getindextotal ( pc , ovar1 )
// Test polychaos_getindextotal ( pc )
// TODO : test polychaos_getindextotal with ny>1
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
computed = polychaos_getindextotal ( pc , 1 , 1 );
expected = 0.8125;
// TODO : is 1.e-7 correct ?
assert_checkalmostequal ( computed , expected , 1.e-7 );
computed = polychaos_getindextotal ( pc , 1 );
expected = 0.8125;
assert_checkalmostequal ( computed , expected , 1.e-7 );
computed = polychaos_getindextotal ( pc );
expected = [
    0.8125     
    0.234375
];
assert_checkalmostequal ( computed , expected , 1.e-7 );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );


