
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 )
  // Clean-up the chaos polynomial created by createMyPolychaos.
  polychaos_destroy(pc);
  setrandvar_destroy(srvx);
  randvar_destroy(rvx1);
  randvar_destroy(rvx2);
  setrandvar_destroy(srvu);
  randvar_destroy(vu1);
  randvar_destroy(vu2);
endfunction

