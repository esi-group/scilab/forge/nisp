
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));



// fileexists --
//   Returns true if the file exists,
//   returns false if the file does not exist.
function e = fileexists ( filename )
  e = (fileinfo(filename)~=[])
endfunction

// Test polychaos_generatecode ( pc , filename , name )
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
polychaos_generatecode ( pc , TMPDIR+"/myfile.c" , "foo" );
computed = fileexists ( TMPDIR+"/myfile.c" );
assert_checkequal ( computed , %T );
// TODO : generate a templated gateway and replace "<T_NAME>" with "foo"
//files=[TMPDIR+"/myfile.c"];
//ilib_build('foo',[],files,[]);
deletefile ( TMPDIR+"/myfile.c" );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );


