
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



function [ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos ()
  //
  // Create a chaos polynomial by Quadrature for the product function.
  //
  function y = Exemple (x)
    y(1) = x(1) * x(2);
  endfunction
 //
 rvx1 = randvar_new("Normale");
 rvx2 = randvar_new("Uniforme");
 srvx = setrandvar_new();
 setrandvar_addrandvar ( srvx , rvx1 );
 setrandvar_addrandvar ( srvx , rvx2 );
 //
 vu1 = randvar_new("Normale",1.0,0.5);
 vu2 = randvar_new("Uniforme",1.0,2.5);
 srvu = setrandvar_new();
 setrandvar_addrandvar ( srvu,vu1);
 setrandvar_addrandvar ( srvu,vu2);
 //
 ny = 1;
 pc = polychaos_new ( srvx , ny );
 degre = 2;
 setrandvar_buildsample(srvx,"Quadrature",degre);
 sampling=setrandvar_buildsample( srvu , srvx );
 np = setrandvar_getsize(srvx);
 polychaos_setsizetarget(pc,np);
 nx = polychaos_getdiminput(pc);
 ny = polychaos_getdimoutput(pc);
 for k=1:np
    inputdata = sampling(k,:);
    outputdata = Exemple(inputdata);
    polychaos_settarget(pc,k,outputdata);
  end
  polychaos_setdegree(pc,degre);
endfunction

