
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



// Load several functions require for the test.
path = nisp_getpath();
disp(path)
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));


  //
  // Create a chaos polynomial by Quadrature for the product function.
  //
  function y = Exemple (x)
    y(1) = x(1) * x(2);
  endfunction

// Test polychaos_compute ( pc , x )
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
x = [0.1 0.9];
computed = polychaos_compute ( pc , x );
u(1) = randvar_getvalue ( vu1 , rvx1 , x(1) );
u(2) = randvar_getvalue ( vu2 , rvx2 , x(2) );
expected = Exemple(u);
assert_checkalmostequal ( computed , expected , 1.e-7 );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );


