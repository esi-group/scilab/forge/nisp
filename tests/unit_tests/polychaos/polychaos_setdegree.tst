
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));



// Test polychaos_setdegree ( pc , no )
// Test no = polychaos_getdegree ( pc )
rv1 = randvar_new("Normale");
rv2 = randvar_new("Uniforme");
srv = setrandvar_new ();
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
pc = polychaos_new ( srv , 10 );
polychaos_setdegree ( pc , 3 );
no = polychaos_getdegree ( pc );
assert_checkequal ( no , 3 );
polychaos_destroy ( pc );
setrandvar_destroy ( srv );
randvar_destroy ( rv1 );
randvar_destroy ( rv2 );


