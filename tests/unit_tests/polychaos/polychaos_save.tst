
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));


// fileexists --
//   Returns true if the file exists,
//   returns false if the file does not exist.
function e = fileexists ( filename )
  e = (fileinfo(filename)~=[])
endfunction

// Test polychaos_save ( pc , file )
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
polychaos_save ( pc , TMPDIR+"/myfile.txt" );
computed = fileexists ( TMPDIR+"/myfile.txt" );
assert_checkequal ( computed , %T );
deletefile ( TMPDIR+"/myfile.txt" );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );

