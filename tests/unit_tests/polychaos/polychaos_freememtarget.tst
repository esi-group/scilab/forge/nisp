
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));

// Test polychaos_freememtarget ( pc )
rv1 = randvar_new("Normale");
rv2 = randvar_new("Uniforme");
srv = setrandvar_new ();
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
ny = 10;
pc = polychaos_new ( srv , ny );
np = 3;
polychaos_setsizetarget ( pc , np );
expected = (1:np).' * (1:ny);
polychaos_settarget ( pc , expected );
polychaos_freememtarget ( pc );
// TODO : what to assert ?
polychaos_destroy ( pc );
setrandvar_destroy ( srv );
randvar_destroy ( rv1 );
randvar_destroy ( rv2 );

