
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));


// Try polychaos_tokens
// At start, no polychaos should exist.
pclist = polychaos_tokens();
assert_checkequal ( pclist , [] );
nbpc = polychaos_size();
assert_checkequal ( nbpc , 0 );

// Test polychaos_new ( srv , ny )
rv1 = randvar_new("Normale");
rv2 = randvar_new("Uniforme");
srv = setrandvar_new ();
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
pc = polychaos_new ( srv , 10 );
pclist = polychaos_tokens();
assert_checkequal ( size(pclist,2) , 1 );
nbpc = polychaos_size();
assert_checkequal ( nbpc , 1 );
polychaos_destroy ( pc );
setrandvar_destroy ( srv );
randvar_destroy ( rv1 );
randvar_destroy ( rv2 );
pclist = polychaos_tokens();
assert_checkequal ( size(pclist,2) , 0 );
nbpc = polychaos_size();
assert_checkequal ( nbpc , 0 );

// TODO : test this : token = polychaos_new ( file )


// Test polychaos_new ( pc , nopt , varopt )
rv1 = randvar_new("Normale");
rv2 = randvar_new("Uniforme");
srv = setrandvar_new ();
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
pc2 = polychaos_new ( srv , 10 );
nopt = 1;
varopt = 1;
// TODO : how to make this work ?
//pc = polychaos_new ( pc2 , nopt , varopt )
polychaos_destroy ( pc2 );
setrandvar_destroy ( srv );
randvar_destroy ( rv1 );
randvar_destroy ( rv2 );


