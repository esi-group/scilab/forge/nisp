// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));

// Test polychaos_getgroupind ( pc , ovar )
// Test polychaos_getgroupind ( pc )
disp('f1')
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
disp('f2')
polychaos_computeexp(pc,srvx,"Integration");
// Use an empty group:

disp('f2.5')
polychaos_computeexp(pc,srvx,"Integration");
disp('f2.75')
polychaos_printgroup(pc);
mprintf("f3\n")
disp('f3-2')
polychaos_computeexp(pc,srvx,"Integration");
g = polychaos_getgroup(pc);
assert_checkequal ( g , [] );
computed = polychaos_getgroupind ( pc , 1 );
expected = 0.;
assert_checkalmostequal ( computed , expected );
// Add the first variable:
polychaos_setgroupaddvar ( pc , 1 );
polychaos_printgroup(pc);
g = polychaos_getgroup(pc);
assert_checkequal ( g , 1 );
disp('f3.5')
polychaos_computeexp(pc,srvx,"Integration");
computed = polychaos_getgroupind ( pc );
expected = 0.765625;
assert_checkalmostequal ( computed , expected , 1.e-7 );
// Add the second variable:
polychaos_setgroupaddvar ( pc , 2 );
disp('f4')
polychaos_computeexp(pc,srvx,"Integration");
polychaos_printgroup(pc);
g = polychaos_getgroup(pc);
assert_checkequal ( g , [1;2] );
computed = polychaos_getgroupind ( pc );
expected = 1.;
assert_checkalmostequal ( computed , expected , 1.e-7 );
disp('f5')
polychaos_computeexp(pc,srvx,"Integration");
// Set an empty group
disp('f6')
polychaos_computeexp(pc,srvx,"Integration");
polychaos_setgroupempty(pc);
polychaos_printgroup(pc);
g = polychaos_getgroup(pc);
assert_checkequal ( g , [] );
// Add the second variable:
polychaos_setgroupaddvar ( pc , 2 );
disp('f7')
polychaos_computeexp(pc,srvx,"Integration");
polychaos_printgroup(pc);
g = polychaos_getgroup(pc);
assert_checkequal ( g , 2 );
computed = polychaos_getgroupind ( pc );
expected = 0.1875;
disp('f8')
polychaos_computeexp(pc,srvx,"Integration");
assert_checkalmostequal ( computed , expected , 1.e-7 );
//
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );


