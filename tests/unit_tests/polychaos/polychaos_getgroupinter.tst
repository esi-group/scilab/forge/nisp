
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));



// Test polychaos_setgroupempty ( pc )
// Test polychaos_getgroupinter ( pc , ovar )
// Test polychaos_getgroupinter ( pc )
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
polychaos_setgroupempty ( pc );
computed = polychaos_getgroupinter ( pc , 1 );
expected = 0.0;
assert_checkalmostequal ( computed , expected , %eps );
computed = polychaos_getgroupinter ( pc );
expected = 0.0;
assert_checkalmostequal ( computed , expected , %eps );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );

// Test polychaos_setgroupempty ( pc )
// Test polychaos_setgroupaddvar ( pc , ivar )
// Test polychaos_getgroupinter ( pc , ovar )
// Test polychaos_getgroupinter ( pc )
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
polychaos_setgroupempty ( pc );
polychaos_setgroupaddvar ( pc , 2 );
computed = polychaos_getgroupinter ( pc , 1 );
expected = 0.1875;
assert_checkalmostequal ( computed , expected , 1.e-7 );
computed = polychaos_getgroupinter ( pc );
expected = 0.1875;
assert_checkalmostequal ( computed , expected , 1.e-7 );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );

// Test polychaos_setgroupempty ( pc )
// Test polychaos_getgroupinter ( pc , ovar )
// Test polychaos_getgroupinter ( pc )
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
polychaos_setgroupempty ( pc );
polychaos_setgroupaddvar ( pc , 1 );
polychaos_setgroupaddvar ( pc , 2 );
computed = polychaos_getgroupinter ( pc );
expected = 0.04687499486871491;
assert_checkalmostequal ( computed , expected , 1.e-6 );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );


