// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// <-- ENGLISH IMPOSED -->

function x=myrandgen(m, i, mu, sigma)
    x = distfun_normrnd(mu(i),sigma(i),m,1)
endfunction
mu=[0 0 0 0 0]';
sigma=[1 2 3 4 5]';
a = [4 5 6 7 8 1]';
exact = nisp_sumsa ( a , mu , sigma );
n = 100000;
nx = 5;
s=nisp_srcsa(list(nisp_sum,a),nx,list(myrandgen,mu,sigma),n);
rtol=0.1;
assert_checkalmostequal(s,exact.S,rtol);

nisp_plotsa(s);
legend("SRC indices");
//
// Multiple output : ny=2
function y=myfunc(x,a1,a2)
    y1=nisp_sum(x,a1)
    y2=nisp_sum(x,a2)
    y=[y1,y2]
endfunction

mu=[0 0 0 0 0]';
sigma=[1 2 3 4 5]';
a1 = [4 5 6 7 8 1]';
a2 = [8 7 6 5 4 3]';
n = 100000;
nx = 5;
s=nisp_srcsa(list(myfunc,a1,a2),nx,list(myrandgen,mu,sigma),n);
exact1 = nisp_sumsa ( a1 , mu , sigma );
exact2 = nisp_sumsa ( a2 , mu , sigma );
rtol=0.1;
assert_checkalmostequal(s(:,1),exact1.S,rtol);
assert_checkalmostequal(s(:,2),exact2.S,rtol);

//
// Test with confidence interval
mu=[0 0 0 0 0]';
sigma=[1 2 3 4 5]';
a = [4 5 6 7 8 1]';
exact = nisp_sumsa ( a , mu , sigma );
n = 100000;
nx = 5;
[s,smin,smax]=nisp_srcsa(list(nisp_sum,a),nx,list(myrandgen,mu,sigma),n);
rtol=0.1;
assert_checkalmostequal(s,exact.S,rtol);
scf();
nisp_plotsa([s,smin,smax]);
assert_checktrue(smin>=0);
assert_checktrue(s>=smin);
assert_checktrue(s<=smax);
assert_checktrue(smax<=1);
ny=1;
assert_checkequal(size(s),[nx,ny]);
assert_checkequal(size(smin),[nx,ny]);
assert_checkequal(size(smax),[nx,ny]);
