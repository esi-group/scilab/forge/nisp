// Copyright (C) 2013 - 2014 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->
// Compute the first order sensitivity indices of the ishigami function.
// Three random variables uniform in [-pi,pi].
function y=ishigami(x)
    a=7.
    b=0.1
    s1=sin(x(:,1))
    s2=sin(x(:,2))
    x34 = x(:,3).^4
    y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
function x=myrandgen(n,i)
    x = distfun_unifrnd(-%pi,%pi,n,1)
endfunction
distfun_seedset(0);
a=7.;
b=0.1;
exact = nisp_ishigamisa(a,b);
n = 500;
nx = 3;
[ s , nbevalf ] = nisp_bruteforcesa ( ishigami , nx , myrandgen , n );
assert_checkalmostequal ( s(1), exact.S1 , 1.e-1 );
assert_checkalmostequal ( s(2), exact.S2 , 1.e-1 );
assert_checkalmostequal ( s(3), exact.S3 , [] , 1.e-1 );
assert_checkequal ( nbevalf , 750500 );
//
// Test on product function
function x = myrandgen2 ( n, i)
    if (i==1) then
        x = distfun_normrnd(4,1,n,1)
    elseif (i==2) then
        x = distfun_normrnd(2,2,n,1)
    elseif (i==3) then
        x = distfun_normrnd(1,4,n,1)
    end
endfunction
distfun_seedset(0);
mu=[4 2 1];
sigma=[1 2 4];
exact = nisp_productsa(mu,sigma);
n=2^7;
nx=3;
s=nisp_bruteforcesa(nisp_product,nx,myrandgen2,n);
assert_checkalmostequal(s,exact.S,[],0.2);
//
// Test on sum function
function x = myrandgen3 ( n, i, mu, sigma)
    x = distfun_normrnd(mu(i),sigma(i),n,1)
endfunction
distfun_seedset(0);
mu=[0 0 0]';
sigma=[1 2 4]';
a = [1 2 4 0]';
exact = nisp_sumsa ( a , mu , sigma );
n=2^7;
nx=3;
s=nisp_bruteforcesa(list(nisp_sum,a),nx,..
list(myrandgen3,mu,sigma),n);
assert_checkalmostequal(s,exact.S,[],0.1);
//
// Test with default random number generator
function y=ishigamiUniform(x)
    x=2*%pi*x-%pi
    a=7.
    b=0.1
    s1=sin(x(:,1))
    s2=sin(x(:,2))
    x34 = x(:,3).^4
    y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
distfun_seedset(0);
a=7.;
b=0.1;
exact = nisp_ishigamisa(a,b);
n = 500;
nx = 3;
[ s , nbevalf ] = nisp_bruteforcesa ( ishigamiUniform , nx , [] , n );
assert_checkalmostequal ( s(1), exact.S1 , 1.e-1 );
assert_checkalmostequal ( s(2), exact.S2 , 1.e-1 );
assert_checkalmostequal ( s(3), exact.S3 , [] , 1.e-1 );
assert_checkequal ( nbevalf , 750500 );
////////////////////////////////////////////////////////////////
//
// Multiple output : ny=2
function y=ishigaminy2(x,a1,b1,a2,b2)
    y1=nisp_ishigami(x,a1,b1)
    y2=nisp_ishigami(x,a2,b2)
    y=[y1,y2]
endfunction
function checkIshigaminy2S(s,a1,b1,a2,b2)
    // Check sensitivity indices for y1
    exact1 = nisp_ishigamisa ( a1 , b1 );
    SE1=[exact1.S1;exact1.S2;exact1.S3];
    assert_checkalmostequal ( s(:,1), SE1 , [] , 1.e-1 );
    // Check sensitivity indices for y2
    exact2 = nisp_ishigamisa ( a2 , b2 );
    SE2=[exact2.S1;exact2.S2;exact2.S3];
    assert_checkalmostequal ( s(:,2), SE2 , [] , 1.e-1 );
endfunction
n = 500;
nx = 3;
a1=7.;
b1=0.1;
a2=1.;
b2=1.;
myfuncny1=list(ishigaminy2,a1,b1,a2,b2);
s=nisp_bruteforcesa(myfuncny1, nx, myrandgen, n);
checkIshigaminy2S(s,a1,b1,a2,b2);
//
////////////////////////////////////////////////////////////////
