// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// <-- ENGLISH IMPOSED -->

function x=myrandgen(m, i)
x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction
a=7.;
b=0.1;
n = 1000;
nx = 3;
[s,nbevalf,smin,smax]=nisp_sobolsaFirst(..
list(nisp_ishigami,a,b),nx,myrandgen,n);

scf();
nisp_plotsa(s);

[st,nbevalf,stmin,stmax]=nisp_sobolsaTotal(..
list(nisp_ishigami,a,b),nx,myrandgen,n);

scf();
nisp_plotsa([s,smin,smax]);

scf();
nisp_plotsa(s,st);

scf();
nisp_plotsa([s,smin,smax],[st,stmin,stmax]);


scf();
nisp_plotsa([],st);

// Check that a subplot is possible
scf();
subplot(1,2,1)
nisp_plotsa(s);
subplot(1,2,2)
nisp_plotsa([],[st,stmin,stmax]);
