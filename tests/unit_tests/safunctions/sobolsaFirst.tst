// Copyright (C) 2011-2016 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

// Compute the first order sensitivity indices of the ishigami function.
// Three random variables uniform in [-pi,pi].
function y = ishigami (x)
    a=7.
    b=0.1
    s1=sin(x(:,1))
    s2=sin(x(:,2))
    x34 = x(:,3).^4
    y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
function x = myrandgen ( m , i )
    x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction
distfun_seedset(0);
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b );
SE(1) = exact.S1;
SE(2) = exact.S2;
SE(3) = exact.S3;
//
// Martinez's estimator
n = 100000;
nx = 3;
[ s , nbevalf ] = nisp_sobolsaFirst ( ishigami , nx , myrandgen , n );
assert_checkalmostequal ( s, SE , 1.e-1 , 1.e-2 );
assert_checkequal ( nbevalf , 400000 );
//
// Saltelli's estimator
n = 100000;
nx = 3;
[ s , nbevalf ] = nisp_sobolsaFirst ( ishigami , nx , myrandgen , n, [], [], "Saltelli" );
assert_checkalmostequal ( s, SE , 1.e-1 , 1.e-2 );
assert_checkequal ( nbevalf , 400000 );
//
// With default n
distfun_seedset(0);
[ s , nbevalf ] = nisp_sobolsaFirst ( ishigami , nx , myrandgen );
assert_checkalmostequal ( s, SE , 1.e-1 , 1.e-2 );
assert_checkequal ( nbevalf , 40000 );
//
// With default randgen
distfun_seedset(0);
function y = ishigami3 ( x )
    // From [0,1] to [-pi,pi]
    x = 2*%pi.*x - %pi
    a=7.
    b=0.1
    s1=sin(x(:,1))
    s2=sin(x(:,2))
    x34 = x(:,3).^4
    y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
[ s , nbevalf ] = nisp_sobolsaFirst ( ishigami3 , nx );
assert_checkalmostequal ( s, SE , 1.e-1 , 1.e-2 );
assert_checkequal ( nbevalf , 40000 );
//
// With additionnal arguments for f.
function y = ishigami2 ( x , a , b )
    s1=sin(x(:,1))
    s2=sin(x(:,2))
    x34 = x(:,3).^4
    y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
a=7.;
b=0.1;
distfun_seedset(0);
[ s , nbevalf ] = nisp_sobolsaFirst ( list(ishigami2,a,b) , nx , myrandgen );
assert_checkalmostequal ( s, SE , 1.e-1 , 1.e-2 );
assert_checkequal ( nbevalf , 40000 );
//
// With additionnal arguments for randgen.
function x = myrandgen2 ( n , i , xmin , xmax )
    x = distfun_unifrnd(xmin,xmax,n,1)
endfunction
distfun_seedset(0);
[ s , nbevalf ] = nisp_sobolsaFirst ( ishigami , nx , list(myrandgen2,-%pi,%pi) );
assert_checkalmostequal ( s, SE , 1.e-1 , 1.e-2 );
assert_checkequal ( nbevalf , 40000 );

//
// Do not set into [0,1] range 
distfun_seedset(0);
s = nisp_sobolsaFirst ( ishigami , nx , myrandgen, [], %f );
assert_checkalmostequal ( s, SE , 1.e-1 , 1.e-2 );
// This is the check: if inrange is true, s(3) is zero
assert_checktrue ( s(3) < 0 ); 

//
// Set into [0,1] range 
distfun_seedset(0);
s = nisp_sobolsaFirst ( ishigami , nx , myrandgen);
assert_checkalmostequal ( s, SE , 1.e-1 , 1.e-2 );
// This is the check: if inrange is true, s(3) is zero
assert_checkequal ( s(3) , 0 );
 
//
// Get confidence interval
distfun_seedset(0);
[ s , nbevalf, smin, smax ] = nisp_sobolsaFirst ( ishigami , nx , myrandgen, [], [], 1-0.95 );
assert_checkalmostequal ( s, SE , 1.e-1 , 1.e-2 );
assert_checkequal ( nbevalf , 40000 );
assert_checkalmostequal ( smin, [0.2873819  0.4287371  0.]', 1.e-1 , 1.e-2 );
assert_checkalmostequal ( smax, [0.3229305  0.4601909  0.0114629]', 1.e-1 , 1.e-2 );
//
// Check error message - wrong function
function y=funcWrong(x)
    y=x(1,:)
    // Dimension of y must be [n,1]
endfunction
n = 1000;
nx = 3;
instr="[s,nbevalf]=nisp_sobolsaFirst(funcWrong,nx,myrandgen,n)";
msg=msprintf(gettext("%s: Wrong size of output of function ""func"": %d-by-%d instead of %d-by-%d. Check the ""func"" argument.\n"), "nisp_evalfunc", 1, 3,n,3);
assert_checkerror(instr, msg , 10000 );
            
//
// Check error message - wrong random generator
function x=myrandgenWrong(m, i)
    x = distfun_unifrnd(-%pi,%pi,1,1)
    // Dimension of x must be [n,1]
endfunction
n = 1000;
nx = 3;
instr="[s,nbevalf]=nisp_sobolsaFirst(ishigami,nx,myrandgenWrong,n)";
msg = msprintf(gettext("%s: The size of the output of randgen is [%d,%d], which is different from [%d,%d]."), "nisp_getsample" , 1, 1, n , 1 );
assert_checkerror(instr, msg , 10000 );

////////////////////////////////////////////////////////////////
//
// Multiple output : ny=2
function y=ishigaminy2(x,a1,b1,a2,b2)
    y1=nisp_ishigami(x,a1,b1)
    y2=nisp_ishigami(x,a2,b2)
    y=[y1,y2]
endfunction

function checkIshigaminy2S(s,a1,b1,a2,b2)
    // Check sensitivity indices for y1
    exact1 = nisp_ishigamisa ( a1 , b1 );
    SE1=[exact1.S1;exact1.S2;exact1.S3];
    assert_checkalmostequal ( s(:,1), SE1 , [] , 1.e-1 );
    // Check sensitivity indices for y2
    exact2 = nisp_ishigamisa ( a2 , b2 );
    SE2=[exact2.S1;exact2.S2;exact2.S3];
    assert_checkalmostequal ( s(:,2), SE2 , [] , 1.e-1 );
endfunction

n = 10000;
nx = 3;
a1=7.;
b1=0.1;
a2=1.;
b2=1.;
myfuncny1=list(ishigaminy2,a1,b1,a2,b2);
s=nisp_sobolsaFirst(myfuncny1, nx, myrandgen, n);
checkIshigaminy2S(s,a1,b1,a2,b2);
// 
// Check confidence intervals
[s,nbevalf,smin,smax]=nisp_sobolsaFirst(myfuncny1, nx, myrandgen, n);
checkIshigaminy2S(s,a1,b1,a2,b2);
assert_checkequal ( nbevalf , 40000 );
assert_checktrue(smin>=0);
assert_checktrue(s>=smin);
assert_checktrue(s<=smax);
assert_checktrue(smax<=1);
ny=2;
assert_checkequal(size(s),[nx,ny]);
assert_checkequal(size(smin),[nx,ny]);
assert_checkequal(size(smax),[nx,ny]);

//
////////////////////////////////////////////////////////////////
//
// Constant function
function y=myconstantfunc(x)
    m=size(x,"r")
    y=ones(m,1)
endfunction

n = 1000;
nx = 3;
instr="s=nisp_sobolsaFirst(myconstantfunc, nx, myrandgen, n)";
msg=msprintf(gettext("%s: Standard deviation of output of function ""func"" is zero : cannot perform sensitivity analysis. Check the ""func"" argument.\n"), "nisp_evalfunc");
assert_checkerror(instr, msg , 10000 );
////////////////////////////////////////////////////////////////
//
function y=funcx1(x)
    y=x(:,1)
endfunction
function x=myrandgen(m, i)
    x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction
n = 100;
nx = 3;
s=nisp_sobolsaFirst(funcx1,nx,myrandgen,n);
assert_checkalmostequal(s,[1;0;0],[],1.e-1);

