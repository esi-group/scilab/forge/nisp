// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// <-- ENGLISH IMPOSED -->

// nx=3, ny=1
s=[0.2;0.1;0.7];
n=1000;
[smin,smax]=nisp_sobolsaInterval(s,n);
smax_expected=[0.258784512557;0.160995024699;0.730301532293];
smin_expected=[0.139739493911;0.038244003886;0.666949287031];
assert_checkalmostequal(smin,smin_expected);
assert_checkalmostequal(smax,smax_expected);
//
// nx=3, ny=1 - inrange=%t
s=[0.2;0.;0.7];
n=1000;
inrange=%t;
[smin,smax]=nisp_sobolsaInterval(s,n,inrange);
smax_expected=[0.258784512557;0.061993082483;0.730301532293];
smin_expected=[0.139739493911;0;0.666949287031];
assert_checkalmostequal(smin,smin_expected);
assert_checkalmostequal(smax,smax_expected);
//
// nx=3, ny=1 - inrange=%f
s=[0.2;0.;0.7];
n=1000;
inrange=%f;
[smin,smax]=nisp_sobolsaInterval(s,n,inrange);
smax_expected=[0.258784512557;0.061993082483;0.730301532293];
smin_expected=[0.139739493911;-0.061993082483;0.666949287031];
assert_checkalmostequal(smin,smin_expected);
assert_checkalmostequal(smax,smax_expected);
//
// nx=3, ny=1, 99%
s=[0.2;0.;0.7];
n=1000;
c=1.-0.99;
[smin,smax]=nisp_sobolsaInterval(s,n,[],c);
smax_expected=[0.276889268116;0.081396851017;0.739274613135];
smin_expected=[0.12056588567;0;0.655979409785];
assert_checkalmostequal(smin,smin_expected);
assert_checkalmostequal(smax,smax_expected);
//
// nx=3, ny=3
s=[
0.2 0.9 0.1
0.  0.2 0.3
0.7 0.6 0.5
];
n=1000;
[smin,smax]=nisp_sobolsaInterval(s,n);
smax_expected=[
    0.258784512557    0.911156237098    0.160995024699  
    0.061993082483    0.258784512557    0.355383683482  
    0.730301532293    0.638252730002    0.545096962019  
];
smin_expected=[
    0.139739493911    0.887525303953    0.038244003886  
    0.                0.139739493911    0.242517234799  
    0.666949287031    0.558791647535    0.452017909284  
];
assert_checkalmostequal(smin,smin_expected);
assert_checkalmostequal(smax,smax_expected);
