// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


//
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

x = [1 2 3 4];
a = [5 6 7 8 9]';
y = nisp_sum (x,a);
expected = 79;
assert_checkequal ( y , expected );
//
x = [
  1 2 3 4
  5 6 7 8
];
a = [5 6 7 8 9]';
y = nisp_sum (x,a);
expected = [
79
183
];
assert_checkequal ( y , expected );
