// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


//
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

mu=[0 0 0 0]';
sigma=[1 2 3 4]';
a = [1 1 1 1 0]';
exact = nisp_sumsa ( a , mu , sigma );
expected.expectation = 0;
expected.var = 30;
expected.S = [1/30 4/30 9/30 16/30]';
expected.ST = [1/30 4/30 9/30 16/30]';
assert_checkalmostequal ( exact.expectation , expected.expectation , 1.e-5 );
assert_checkalmostequal ( exact.var , expected.var , 1.e-5 );
assert_checkalmostequal ( exact.S , expected.S , 1.e-5 );
assert_checkalmostequal ( exact.ST , expected.ST , 1.e-5 );
//
mu=(1:10)';
sigma=(11:20)';
a = (21:31)';
exact = nisp_sumsa ( a , mu , sigma );
expected.expectation = 1516;
expected.var = 1767333;
expected.S = [
    0.0301930  
    0.0394357  
    0.0505853  
    0.0638793  
    0.0795690  
    0.0979193  
    0.1192084  
    0.1437284  
    0.1717848  
    0.2036968  
];
expected.ST = [
    0.0301930  
    0.0394357  
    0.0505853  
    0.0638793  
    0.0795690  
    0.0979193  
    0.1192084  
    0.1437284  
    0.1717848  
    0.2036968  
];
assert_checkalmostequal ( exact.expectation , expected.expectation , 1.e-5 );
assert_checkalmostequal ( exact.var , expected.var , 1.e-5 );
assert_checkalmostequal ( exact.S , expected.S , 1.e-5 );
assert_checkalmostequal ( exact.ST , expected.ST , 1.e-5 );

