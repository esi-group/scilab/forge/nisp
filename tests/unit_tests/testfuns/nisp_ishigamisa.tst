
// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


//
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


a=7.;
b=0.1;
//
exact = nisp_ishigamisa ( a , b );
expected.expectation = 3.5;
expected.var = 13.844588;
expected.S1 = 0.3139052;
expected.S2 = 0.4424111;
expected.S3 = 0;
expected.S12 = 0;
expected.S13 = 0.2436837;
expected.S23 = 0;
expected.S123 = 0;
expected.ST1 = 0.5575889;
expected.ST2 = 0.4424111;
expected.ST3 = 0.2436837;

assert_checkalmostequal ( exact.expectation , expected.expectation , 1.e-5 );
assert_checkalmostequal ( exact.var , expected.var , 1.e-5 );
assert_checkalmostequal ( exact.S1 , expected.S1 , 1.e-5 );
assert_checkalmostequal ( exact.S2 , expected.S2 , 1.e-5 );
assert_checkalmostequal ( exact.S3 , expected.S3 , 1.e-5 );
assert_checkalmostequal ( exact.S12 , expected.S12 , 1.e-5 );
assert_checkalmostequal ( exact.S13 , expected.S13 , 1.e-5 );
assert_checkalmostequal ( exact.S23 , expected.S23 , 1.e-5 );
assert_checkalmostequal ( exact.S123 , expected.S123 , 1.e-5 );
assert_checkalmostequal ( exact.ST1 , expected.ST1 , 1.e-5 );
assert_checkalmostequal ( exact.ST2 , expected.ST2 , 1.e-5 );
assert_checkalmostequal ( exact.ST3 , expected.ST3 , 1.e-5 );

