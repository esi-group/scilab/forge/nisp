// Copyright (C) 2008-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function nisp_initseed ( seed )
  // Sets the seed of the uniform random number generator.
  // 
  // Calling Sequence
  //   nisp_initseed ( seed )
  //
  // Parameters
  // seed : a 1-by-1 matrix of floating point integers, the current verbose level
  //
  // Description
  // Sets the seed for the uniform random number generator.
  // This seed has an effect on the randvar_getvalue and setrandvar_buildsample functions.
  //
  // Examples
  // nisp_initseed ( 0 )
  // nisp_initseed ( 123456789 )
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //

endfunction


