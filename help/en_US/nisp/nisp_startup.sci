// Copyright (C) 2008-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function nisp_startup ( )
  // Starts up the NISP toolbox.
  // 
  // Calling Sequence
  //   nisp_startup ( )
  //
  // Description
  //   This function allows to connect Scilab to the NISP library.
  //  <itemizedlist>
  //    <listitem>
  //      <para>
  //        The function connects the standard output to the Scilab message system, so that the messages
  //        are printed in Scilab's console, instead of the terminal.
  //      </para>
  //    </listitem>
  //    <listitem>
  //      <para>
  //        The function connects the Nisp error function to Scilab's error function, so that error messages are
  //        generated when required.
  //      </para>
  //    </listitem>
  //    <listitem>
  //      <para>
  //        The function initializes to the seed of the random number
  //        generator, so that the behaviour of the toolbox can be reproduced.
  //      </para>
  //    </listitem>
  //  </itemizedlist>
  //
  // Examples
  // nisp_startup ( )
  // nisp_shutdown ( )
  // nisp_startup ( )
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //

endfunction


