// Copyright (C) 2008-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function nisp_shutdown ( )
  // Shuts down the NISP toolbox.
  // 
  // Calling Sequence
  //   nisp_shutdown ( )
  //
  // Description
  //   This function is currently a no-op, but might be modified in future releases of the toolbox.
  //
  // Examples
  // nisp_startup ( )
  // nisp_shutdown ( )
  // nisp_startup ( )
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //

endfunction


