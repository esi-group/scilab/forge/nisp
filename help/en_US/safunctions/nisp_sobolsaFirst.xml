<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from nisp_sobolsaFirst.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="nisp_sobolsaFirst" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>nisp_sobolsaFirst</refname>
    <refpurpose>Compute sensitivity indices by Sobol, Ishigami, Homma.</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   s = nisp_sobolsaFirst( func, nx )
   s = nisp_sobolsaFirst( func, nx, randgen)
   s = nisp_sobolsaFirst( func, nx, randgen, n)
   s = nisp_sobolsaFirst( func, nx, randgen, n, inrange)
   s = nisp_sobolsaFirst( func, nx, randgen, n, inrange, c)
   [s, nbevalf ] = nisp_sobolsaFirst( ... )
   [s, nbevalf, smin] = nisp_sobolsaFirst( ... )
   [s, nbevalf, smin, smax] = nisp_sobolsaFirst( ... )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>func :</term>
      <listitem><para> a function or a list, the name of the function to be evaluated.</para></listitem></varlistentry>
   <varlistentry><term>nx :</term>
      <listitem><para> a 1-by-1 matrix of floating point integers, the number of inputs of the function.</para></listitem></varlistentry>
   <varlistentry><term>randgen :</term>
      <listitem><para> a function or a list, the random number generator. (default = uniform random variables)</para></listitem></varlistentry>
   <varlistentry><term>n :</term>
      <listitem><para> a 1-by-1 matrix of floating point integers (default n=10000), the number of Monte-Carlo experiments, for each sensitivity index</para></listitem></varlistentry>
   <varlistentry><term>inrange :</term>
      <listitem><para> a 1-by-1 matrix of booleans (default inrange = %t), set to true to restrict the sensitivity indices into [0,1].</para></listitem></varlistentry>
   <varlistentry><term>c :</term>
      <listitem><para> a 1-by-1 matrix of doubles (default c = 1-0.95), the level for the confidence interval. Must be in the range [0,0.5].</para></listitem></varlistentry>
   <varlistentry><term>s :</term>
      <listitem><para> a nx-by-ny matrix of doubles, the first order sensitivity indices, where nx is the number of input variables and ny is the number of output variables.</para></listitem></varlistentry>
   <varlistentry><term>nbevalf :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the actual number of function evaluations.</para></listitem></varlistentry>
   <varlistentry><term>smin :</term>
      <listitem><para> a nx-by-ny matrix of doubles, the lower bound of the confidence interval.</para></listitem></varlistentry>
   <varlistentry><term>smax :</term>
      <listitem><para> a nx-by-ny matrix of doubles, the upper bound of the confidence interval.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
The algorithm uses the Sobol method to compute the first order
sensitivity indices.
   </para>
   <para>
This method assumes that all the input random variables are
independent.
   </para>
   <para>
On output, the variable s(i,j) corresponds to the sensitivity
of the j-output for the i-th input, for i=1,...,nx,
j=1,...,ny.
The bounds smin and smax have the same specifications.
   </para>
   <para>
On output, if <literal>inrange</literal> is true, then
the sensitivity indices are forced to be in the range [0,1].
Otherwise, if the exact value is zero, then it may happen that
the estimate is negative.
Similarily, if the exact value is one, then it may happen that
the estimate greater than one.
The <literal>inrange</literal> option manages this situation.
   </para>
   <para>
The confidence level 1-c is so that
   </para>
   <para>
<latex>
P(smin \leq s \leq smax)=1-c.
</latex>
   </para>
   <para>
This is done by the Fisher transformation of the correlation
coefficient, as published by Martinez, 2005 (see below).
   </para>
   <para>
Any optional input argument equal to the empty matrix will be set to its
default value.
   </para>
   <para>
See "Specifications of func" for the specifications
of the "func" and "randgen" arguments.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Compute the first order sensitivity indices of the ishigami function.
// Three random variables uniform in [-pi,pi].
function y = ishigami (x)
a=7.
b=0.1
s1=sin(x(:,1))
s2=sin(x(:,2))
x34 = x(:,3).^4
y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
function x = myrandgen ( m , i )
x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b )
n = 1000;
nx = 3;
[s,nbevalf]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n)

// See the confidence interval.
[s,nbevalf,smin,smax]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n)

// Configure inrange
distfun_seedset(0);
// s(3) is zero
s=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,%t)
// s(3) is negative
s=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,%f)

// Configure the confidence interval length at 95%
[s,nbevalf,smin,smax]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,[],1-0.95)
// Configure the confidence interval length at 99%
[s,nbevalf,smin,smax]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,[],1-0.99)

// See the variability of the sensitivity indices.
for k = 1 : 100
[s,nbevalf]=nisp_sobolsaFirst(ishigami,nx,myrandgen,1000);
sall(k,:) = s';
end
scf();
subplot(2,2,1);
histo(sall(:,1));
xtitle("Variability of the sensitivity index for X1","S1","Frequency");
subplot(2,2,2);
histo(sall(:,2));
xtitle("Variability of the sensitivity index for X2","S2","Frequency");
subplot(2,2,3);
histo(sall(:,3));
xtitle("Variability of the sensitivity index for X3","S3","Frequency");

// See the convergence of the sensitivity indices
n=10;
stacksize("max");
for k = 1 : 100
tic();
[s,nbevalf]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n);
sc(k,:) = s';
t = toc();
mprintf("Run #%d, n=%d, t=%.2f (s)\n",k,n,t);
if ( t > 1 ) then
break
end
n = ceil(1.2*n);
end
h = scf();
subplot(1,2,1);
plot(1:k,sc(1:k,1),"bx-");
plot(1:k,sc(1:k,2),"ro-");
plot(1:k,sc(1:k,3),"g*-");
mytitle="Convergence of the sensitivity indices";
xtitle(mytitle,"Number of simulations","S");
legend(["S1","S2","S3"]);
subplot(1,2,2);
plot(1:k,abs(sc(1:k,1)-exact.S1),"bx-");
plot(1:k,abs(sc(1:k,2)-exact.S2),"ro-");
plot(1:k,abs(sc(1:k,3)-exact.S3),"g*-");
mytitle="Convergence of the sensitivity indices";
xtitle(mytitle,"Number of simulations","|S-exact|");
legend(["S1","S2","S3"]);
h.children(1).log_flags="lnn";

// Example with ny=2 outputs
function y=ishigaminy2(x,a1,b1,a2,b2)
y1=nisp_ishigami(x,a1,b1)
y2=nisp_ishigami(x,a2,b2)
y=[y1,y2]
endfunction
n = 1000;
nx = 3;
a1=7.;
b1=0.1;
a2=1.;
b2=1.;
s=nisp_sobolsaFirst(list(ishigaminy2,a1,b1,a2,b2), ..
nx, myrandgen, n)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2014 - Michael Baudin</member>
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>GdR Ondes &amp; Mascot Num, "Analyse de sensibilite globale par decomposition de la variance", jean-marc.martinez@cea.fr, 13 janvier 2011, Institut Henri Poincare</para>
   <para>"Contribution à l'analyse de sensibilité et à l'analyse discriminante généralisée", 2005, Julien Jacques, Thèse</para>
</refsection>
</refentry>
