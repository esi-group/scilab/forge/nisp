// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Draw the scatter plots of the product function.
// Two normal random variables.
//

//
// Another case.
// mu2=0 implies S1=0.
// But Y is not independent of X1.
// First variable
// Normal
mu1 = 0.1;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 0;
sigma2 = 2.5;
exact = nisp_productsa ( [mu1,mu2]' , [sigma1,sigma2]' );
//
np = 10000;

// Define a sampling for x1.
x1 = distfun_normrnd(mu1,sigma1,np,1);
// Define a sampling for x2.
x2 = distfun_normrnd(mu2,sigma2,np,1);
// Merge the two samplings
x = [x1 x2];
// Perform the experiments
y = nisp_product (x);
// Scatter plots : y depending on X_i
scf();
for k=1:2
  subplot(2,1,k);
  plot(x(:,k),y,'rx');
  xistr="X"+string(k);
  xtitle("Case mu2=0 - Scatter plot for "+xistr,xistr,"Y");
end

// Histogram of Y
scf();
histplot(50,y);
xtitle("Histogram of Y","Y","P(Y)");
//
// The probability distribution function of a product of 
// two normal variables with zero mean is a 
// modified Bessel function of the second kind.
// Here, the means are nonzero... but the 
// function fits quite accurately enough anyway.
x = linspace(-10,10,1000);
alpha = 0;
p =  besselk(alpha,abs(x)/(sigma1*sigma2))/(%pi*sigma1*sigma2);
plot(x,p,"r-");
legend(["Experiment","Bessel 2nd kind"]);

//
// Load this script into the editor
//
filename = 'product_scatter2.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

