// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

//
// Test the chaos polynomial decomposition on the product x1*x2 function.
// Two random variables:
// * a normal random variable with mu=1.0 and sigma=0.5,
// * a uniform random variable in [1,2.5].
// Create a Quadrature sampling and compute the coefficients of the polynomial 
// by integration.
// Use a degree 2 polynomial.
//

//
// "Specifications scientifiques et informatiques : Chaos Polynomial"
// D-WP1/08/01/A 
// MARTINEZ JM. - CEA
//

function y = Exemple (x)
  // Returns the output y of the product x1 * x2.
  // Parameters
  // x: a np-by-nx matrix of doubles, where np is the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  y(:,1) = x(:,1).* x(:,2)
endfunction
// 1. Two stochastic (normalized) variables
vx1 = randvar_new("Normale");
vx2 = randvar_new("Uniforme");
// 2. A collection of stoch. variables.
srvx = setrandvar_new();
setrandvar_addrandvar ( srvx,vx1);
setrandvar_addrandvar ( srvx,vx2);
// 3. Two uncertain parameters
vu1 = randvar_new("Normale",1.0,0.5);
vu2 = randvar_new("Uniforme",1.0,2.5);
// 4. A collection of uncertain parameters
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu,vu1);
setrandvar_addrandvar ( srvu,vu2);
// 5. Create the Design Of Experiments
degre = 2;
setrandvar_buildsample(srvx,"Quadrature",degre);
inputdata = setrandvar_buildsample( srvu , srvx );
// 6. Create the polynomial
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
// 7. Perform the DOE
outputdata = Exemple(inputdata);
polychaos_settarget(pc,outputdata);
// 8. Compute the coefficients of the polynomial expansion
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 9. Get the sensitivity indices
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);
mprintf("Mean     = %f\n",average);
mprintf("Variance = %f\n",var);
S = polychaos_getindexfirst(pc);
mprintf("S1 = %f\n",S(1));
mprintf("S2 = %f\n",S(2));
ST = polychaos_getindextotal(pc);
mprintf("ST1 = %f\n",ST(1));
mprintf("ST2 = %f\n",ST(2));
// Clean-up
polychaos_destroy(pc);
randvar_destroy(vu1);
randvar_destroy(vu2);
randvar_destroy(vx1);
randvar_destroy(vx2);
setrandvar_destroy(srvu);
setrandvar_destroy(srvx);
//
// Load this script into the editor
//
filename = 'polychaos_product.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

