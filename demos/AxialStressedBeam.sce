// Copyright (C) 2009-2011 - INRIA - Michael Baudin


// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// A Scilab port of the AxialStressedBeam.py demo of Open Turns.
// http://trac.openturns.org/wiki/ExampleAxialStressedBeam
// TODO : finish this !


// Random generator initialization:
nisp_initseed(0)

// Analytical model definition:
function G = LimitState ( x )
  R = x(:,1)
  F = x(:,2)
  G = R-F./(%pi.*100)
endfunction

// Test of the limit state function:
x = [300 75000];
mprintf ("x=[%s]\n" , strcat ( string ( x ) , " " ) );
mprintf ("G(x)=%f\n" , LimitState(x) );

// Initialization of the distribution collection:
srvu = setrandvar_new ( );

// Create a first marginal : Normal distribution, 
// parameterized by its mean and standard deviation
mu1 = 300;
sigma1 = 30;
// Create the first variable.
vu1 = randvar_new("Normale" , mu1 , sigma1 );
// Graphical output of the PDF
x = linspace ( 200 , 400 , 1000 );
p = distfun_normpdf ( x , mu1 , sigma1 );
scf();
plot ( x , p );
xtitle ( "Yield strength PDF" , "X" , "PDF" );

// Fill the first marginal 
setrandvar_addrandvar ( srvu , vu1 );

// Create a second marginal : Normal distribution
mu2 = 75000;
sigma2 = 5000;
vu2 = randvar_new("Normale" , mu2 , sigma2 );
// Graphical output of the PDF
x = linspace ( 60000 , 90000 , 1000 );
p = distfun_normpdf ( x , mu2 , sigma2 );
scf();
plot ( x , p );
xtitle ( "Traction_load PDF" , "X" , "PDF" );

// Fill the second marginal of aCollection
setrandvar_addrandvar ( srvu , vu2 );

//
// Using Monte-Carlo simulations
//
NbSim = 100000;
sampling = setrandvar_buildsample ( srvu , "MonteCarlo" , NbSim );
// Perform the analysis:
G = LimitState ( sampling );
failed = find(G<0);
Pf = size(failed,"*")/NbSim;
mprintf("Number of executed iterations =%d\n" , NbSim);
mprintf("Pf = %f\n" , Pf);
//
// Using polynomial chaos
mprintf("With polynomial chaos\n");
//
// 1. Two stochastic (normalized) variables
vx1 = randvar_new("Normale");
vx2 = randvar_new("Uniforme");
// 2. A collection of stoch. variables.
srvx = setrandvar_new();
setrandvar_addrandvar ( srvx,vx1);
setrandvar_addrandvar ( srvx,vx2);
// 3. Two uncertain parameters
// Already Done.
// 4. A collection of uncertain parameters
// Already Done.
// 5. Create the Design Of Experiments
degre = 2;
setrandvar_buildsample(srvx,"Quadrature",degre);
inputdata = setrandvar_buildsample( srvu , srvx );
// 6. Create the polynomial
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
// 7. Perform the DOE
outputdata = LimitState(inputdata);
polychaos_settarget(pc,outputdata);
// 8. Compute the coefficients of the polynomial expansion
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 9. Get the sensitivity indices
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);
mprintf("Number of function evaluations: %d\n",np);
mprintf("Mean     = %f\n",average);
mprintf("Variance = %f\n",var);
S = polychaos_getindexfirst(pc);
mprintf("S1 = %f\n",S(1));
mprintf("S2 = %f\n",S(2));
ST = polychaos_getindextotal(pc);
mprintf("ST1 = %f\n",ST(1));
mprintf("ST2 = %f\n",ST(2));

// Clean-up
polychaos_destroy(pc);
randvar_destroy(vu1);
randvar_destroy(vu2);
randvar_destroy(vx1);
randvar_destroy(vx2);
setrandvar_destroy(srvu);
setrandvar_destroy(srvx);

//
// Using Sobol, Homma and Saltelli method.
//
function x = myrandgen ( m , i)
  if ( i == 1 ) then
    mu1 = 300;
    sigma1 = 30;
    x = distfun_normrnd(mu1,sigma1,m,1)
  else
    mu2 = 75000;
    sigma2 = 5000;
    x = distfun_normrnd(mu2,sigma2,m,1)
  end
endfunction

[ s , nbevalf1 ] = nisp_sobolsaFirst ( LimitState , 2 , myrandgen );
[ st , nbevalf2 ] = nisp_sobolsaTotal ( LimitState , 2 , myrandgen );

mprintf("Sobol method for sensitivity analysis.\n");
mprintf("Number of function evaluations: %d\n",nbevalf1+nbevalf2);
mprintf("S1 = %f\n",s(1));
mprintf("S2 = %f\n",s(2));
mprintf("ST1 = %f\n",st(1));
mprintf("ST2 = %f\n",st(2));

//
// Load this script into the editor
//
filename = 'AxialStressedBeam.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

