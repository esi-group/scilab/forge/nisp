// Copyright (C) 2016 - Michael Baudin
// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Compute the first order sensitivity indices of the ishigami function.
// Uses the Sobol, Homma, Saltelli method.
// Three random variables uniform in [-pi,pi].
// Uses Monte-Carlo experiments to compute the sensitivity indices.
//

tic();

function s = sensitivityindex(ya,yc)
  // Returns the sensitivity index associated with experiments ya and yc.
  C = cov (ya, yc)
  s= C(1,2)/ (stdev(ya) * stdev(yc))
endfunction

// Exact results
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b );

function x = myrandgen ( m , i )
    x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction

// Create the uncertain parameters
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);
// The number of uncertain parameters is :
nx = setrandvar_getdimension(srvu);
np = 10000;
// Create a first sampling A
A = setrandvar_buildsample(srvu,"Lhs",np);
// Create a first sampling B
B = setrandvar_buildsample(srvu,"Lhs",np);
// Perform the experiments in A
ya = nisp_ishigami ( A , a , b );
// Compute the first order sensitivity index for X1
C = B;
C(:,1)=A(:,1);
yc = nisp_ishigami(C, a, b);
s1 = sensitivityindex(ya,yc);
mprintf("S1 : %f (expected = %f)\n", s1, exact.S1);
// Compute the first order sensitivity index for X2
C = B;
C(:,2)=A(:,2);
yc = nisp_ishigami(C, a, b);
s2 = sensitivityindex(ya,yc);
mprintf("S2 : %f (expected = %f)\n", s2, exact.S2);
// Compute the first order sensitivity index for X3
C = B;
C(:,3)=A(:,3);
yc = nisp_ishigami(C, a, b);
s3 = sensitivityindex(ya,yc);
mprintf("S3 : %f (expected = %f)\n", s3, exact.S3);
// Compute the sensitivity index for {X1,X2}
C = B;
C(:,[1 2])=A(:,[1 2]);
yc = nisp_ishigami(C, a, b);
s12 = sensitivityindex(ya,yc)-s1-s2;
mprintf("S12 : %f (expected = %f)\n", s12, exact.S12);
// Compute the sensitivity index for {X1,X3}
C = B;
C(:,[1 3])=A(:,[1 3]);
yc = nisp_ishigami(C, a, b);
s13 = sensitivityindex(ya,yc)-s1-s3;
mprintf("S13 : %f (expected = %f)\n", s13, exact.S13);
// Compute the sensitivity index for {X2,X3}
C = B;
C(:,[2 3])=A(:,[2 3]);
yc = nisp_ishigami(C, a, b);
s23 = sensitivityindex(ya,yc)-s2-s3;
mprintf("S23 : %f (expected = %f)\n", s23, exact.S23);
// Compute the sensitivity index for {X1,X2,X3}
C = B;
C(:,[1 2 3])=A(:,[1 2 3]);
yc = nisp_ishigami(C, a, b);
s123 = sensitivityindex(ya,yc)-s1-s2-s3-s12-s23-s13;
mprintf("S123 : %f (expected = %f)\n", s123, exact.S123);
// Compute the total sensitivity index for X1
C = A;
C(:,1)=B(:,1);
yc = nisp_ishigami(C, a, b);
st1 = 1-sensitivityindex(ya,yc);
mprintf("ST1 : %f (expected = %f)\n", st1, exact.ST1);
// Compute the total sensitivity index for X2
C = A;
C(:,2)=B(:,2);
yc = nisp_ishigami(C, a, b);
st2 = 1-sensitivityindex(ya,yc);
mprintf("ST2 : %f (expected = %f)\n", st2, exact.ST2);
// Compute the total sensitivity index for X3
C = A;
C(:,3)=B(:,3);
yc = nisp_ishigami(C, a, b);
st3 = 1-sensitivityindex(ya,yc);
mprintf("ST3 : %f (expected = %f)\n", st3, exact.ST3);

//
// Clean-up
randvar_destroy(rvu1);
randvar_destroy(rvu2);
randvar_destroy(rvu3);
setrandvar_destroy(srvu);

t = toc();
mprintf("Time = %f (s)\n", t);
//
// Load this script into the editor
//
filename = 'ishigami_hommasaltelli.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

