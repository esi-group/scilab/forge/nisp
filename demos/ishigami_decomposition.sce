// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Compute Sobol decomposition of the ishigami function.
// Three random variables uniform in [-pi,pi].
//




function y = ishigami_f0 ( x , a , b )
    y = a/2
endfunction

function y = ishigami_f1 ( x , a , b )
    s1=sin(x(:,1))
    y = s1.*(1+b*%pi^4/5)
endfunction

function y = ishigami_f2 ( x , a , b )
    s2=sin(x(:,2))
    y = -a/2 + a.*s2.^2
endfunction

function y = ishigami_f3 ( x , a , b )
    y = 0
endfunction

function y = ishigami_f12 ( x , a , b )
    y = 0
endfunction

function y = ishigami_f13 ( x , a , b )
    s1=sin(x(:,1))
    x34 = x(:,3).^4
    y = b.*s1.*(x34-%pi^4/5)
endfunction

function y = ishigami_f23 ( x , a , b )
    y = 0
endfunction

function y = ishigami_f123 ( x , a , b )
    y = 0
endfunction

// Exact results
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b )

// Get a sample uniform x
x = distfun_unifrnd(-%pi,%pi,1,3);
mprintf("x=[%f,%f,%f]\n",x(1),x(2),x(3))
// Compare the function and its decomposition
y1 = nisp_ishigami (x,a,b);
mprintf("Full function value:%f\n",y1)
y2 = ..
    ishigami_f0 ( x , a , b ) + ..
    ishigami_f1 ( x , a , b ) + ..
    ishigami_f2 ( x , a , b ) + ..
    ishigami_f3 ( x , a , b ) + ..
    ishigami_f12 ( x , a , b ) + ..
    ishigami_f13 ( x , a , b ) + ..
    ishigami_f23 ( x , a , b ) + ..
    ishigami_f123 ( x , a , b );
mprintf("Function decomposition value:%f\n",y2)
// Plot the function with respect to each of 
// its arguments.
// This is Figure 8 in "Analyse the sensibilite globale",
// Julien Jacques, 2010.
np = 1000;
A = distfun_unifrnd(-%pi,%pi,np,3);
y = nisp_ishigami (A,a,b);
h = scf();
subplot(1,3,1);
plot(A(:,1),y,"bo")
xtitle("","X1","f(X1,X2,X3)");
subplot(1,3,2);
plot(A(:,2),y,"bo")
xtitle("","X2","f(X1,X2,X3)");
subplot(1,3,3);
plot(A(:,3),y,"bo")
xtitle("","X3","f(X1,X2,X3)");
// Put the marks as transparent
h.children(1).children.children.mark_background = 0;
h.children(2).children.children.mark_background = 0;
h.children(3).children.children.mark_background = 0;


//
// Load this script into the editor
//
filename = 'ishigami_decomposition.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

