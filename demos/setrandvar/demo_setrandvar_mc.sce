// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Show a Monte-Carlo Design

mprintf("Creating a Uniforme variable ...\n")
vu1 = randvar_new("Uniforme",-1.0,1.0);
mprintf("Creating a Uniform variable ...\n")
vu2 = randvar_new("Uniforme",-1.0,1.0);

mprintf("Creating a set of random variables ...\n")
srv = setrandvar_new ( );
setrandvar_addrandvar ( srv , vu1 );
setrandvar_addrandvar ( srv , vu2 );

mprintf("Building MonteCarlo sampling with 1000 experiments ...\n")
setrandvar_buildsample ( srv , "MonteCarlo" , 1000 );

sampling = setrandvar_getsample ( srv );

mprintf("Plotting sampling \n")
my_handle = scf();
plot(sampling(:,1),sampling(:,2));
my_handle.children.children.children.line_mode = "off";
my_handle.children.children.children.mark_mode = "on";
my_handle.children.children.children.mark_size = 2;
my_handle.children.title.text = "Monte-Carlo Sampling";
my_handle.children.x_label.text = "X1 : Uniform(-1,1)";
my_handle.children.y_label.text = "X2 : Uniform(-1.0,1.0)";

scf();
subplot(2,1,1);
histplot ( 50 , sampling(:,1));
xtitle("Variable #1 : Uniforme,-1.0,1.0","X1","P(X1)");
subplot(2,1,2);
histplot ( 50 , sampling(:,2));
xtitle("Variable #2 : Uniforme,-1.0,1.0","X2","P(X2)");

for ivar = 1:2
  m = mean(sampling(:,ivar));
  mprintf("Variable #%d, Mean : %f\n",ivar,m);
  v = variance(sampling(:,ivar));
  mprintf("Variable #%d, Variance : %f\n",ivar,v);
end

randvar_destroy(vu1);
randvar_destroy(vu2);
setrandvar_destroy(srv);

