// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Draw the scatter plots of the product function.
// Two normal random variables.
//


// First variable
// Normal
mu1 = 1.5;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 3.5;
sigma2 = 2.5;
exact = nisp_productsa ( [mu1,mu2]' , [sigma1,sigma2]' )

np = 1000;

// Define a sampling for x1.
x1 = distfun_normrnd(mu1,sigma1,np,1);
// Define a sampling for x2.
x2 = distfun_normrnd(mu2,sigma2,np,1);
// Merge the two samplings
x = [x1 x2];
// Perform the experiments
y = nisp_product (x);
// Scatter plots : y depending on X_i
scf();
for k=1:2
  subplot(2,1,k);
  plot(x(:,k),y,'rx');
  xistr="X"+string(k);
  xtitle("Scatter plot for "+xistr,xistr,"Y");
end
// Histogram of Y
scf();
histplot(50,y);
xtitle("Histogram of Y","Y","P(Y)");


//
// Load this script into the editor
//
filename = 'product_scatter.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

