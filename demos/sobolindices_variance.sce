// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Compute the first order sensitivity indices of the ishigami function.
// Repeat this estimate nrepeat times and draw the empirical 
// histograms of S1, S2, S3.
//

function y=shiftedishigami(x,a,b,c)
    y=nisp_ishigami(x,a,b)+c
endfunction

function x = myrandgen ( m , i )
    x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction
a=7.;
b=0.1;
c=100;
exact = nisp_ishigamisa ( a , b );
sexact=[exact.S1,exact.S2,exact.S3];
//
nsample = 100;
nx = 3;
// method="Martinez";
method="Saltelli";
nrepeat=500; 
sarray=zeros(nrepeat,3);
for i=1:nrepeat
    if (modulo(i,5)==0) then
        mprintf("Rep #%d\n", i)
    end
    s=nisp_sobolsaFirst(list(shiftedishigami,a,b,c),nx,myrandgen,nsample, [], [], method );
    sarray(i,1)=s(1);
    sarray(i,2)=s(2);
    sarray(i,3)=s(3);
end

// Draw histograms
scf();
nclasses=ceil(log2(nrepeat)+1);
edges=linspace(0,1,nclasses);
nx=3;
for i=1:nx
    subplot(1,nx,i);
    histo(sarray(:,i),edges)
    xlabel("S"+string(i));
    ylabel("Occurences");
    plot(sexact(i),0,"r*");
    CV=stdev(sarray(:,i))/mean(sarray(:,i));
    legend(msprintf("CV=%.2f",CV))
end
subplot(1,nx,1);
title(msprintf("Estimator=%s",method));
subplot(1,nx,2);
title(msprintf("Size=%d, Reps=%d",nsample,nrepeat));
