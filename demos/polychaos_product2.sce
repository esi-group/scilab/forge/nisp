// Copyright (C) 2008-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

//
// Test the chaos polynomial decomposition on the product x1*x2 function.
// Two random variables:
// * a normal random variable with mu=1.0 and sigma=0.5,
// * a normal random variable with mu=3.5 and sigma=2.5.
// Create a Quadrature sampling and compute the coefficients of the polynomial 
// by integration.
// Use a degree 2 polynomial.
// The exact decomposition is known.
//
// References
// "Specifications scientifiques et informatiques : Chaos Polynomial"
// D-WP1/08/01/A 
// MARTINEZ JM. - CEA
//

tic();

function y = Exemple (x)
  // Returns the output y of the product x1 * x2.
  // Parameters
  // x: a np-by-nx matrix of doubles, where np is the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  y(:,1) = x(:,1) .* x(:,2);
endfunction

// First variable
// Normal
mu1 = 1.5;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 3.5;
sigma2 = 2.5;

// Exact results
exact = nisp_productsa ( [mu1,mu2]' , [sigma1,sigma2]' );

// 1. Two stochastic (normalized) variables
vx1 = randvar_new("Normale");
vx2 = randvar_new("Normale");
// 2. A collection of stoch. variables.
srvx = setrandvar_new();
setrandvar_addrandvar ( srvx,vx1);
setrandvar_addrandvar ( srvx,vx2);
// 3. Two uncertain parameters
vu1 = randvar_new("Normale",mu1,sigma1);
vu2 = randvar_new("Normale",mu2,sigma2);
// 4. A collection of uncertain parameters
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu,vu1);
setrandvar_addrandvar ( srvu,vu2);
// 5. Create the Design Of Experiments
degre = 2;
setrandvar_buildsample(srvx,"Quadrature",degre);
inputdata = setrandvar_buildsample( srvu , srvx );
// 6. Create the polynomial
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
mprintf("Number of experiments : %d\n",np);
polychaos_setsizetarget(pc,np);
// 7. Perform the DOE
outputdata = Exemple(inputdata);
polychaos_settarget(pc,outputdata);
// 8. Compute the coefficients of the polynomial expansion
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 9. Get the sensitivity indices
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);
mprintf("Mean     = %f (exact = %f)\n",average,exact.expectation);
mprintf("Variance = %f (exact = %f)\n",var,exact.var);
S = polychaos_getindexfirst(pc);
mprintf("S1 = %f (exact = %f)\n",S(1),exact.S(1));
mprintf("S2 = %f (exact = %f)\n",S(2),exact.S(2));
ST = polychaos_getindextotal(pc);
mprintf("ST1 = %f (exact = %f)\n",ST(1),exact.ST(1));
mprintf("ST2 = %f (exact = %f)\n",ST(2),exact.ST(2));
// Clean-up
polychaos_destroy(pc);
randvar_destroy(vu1);
randvar_destroy(vu2);
randvar_destroy(vx1);
randvar_destroy(vx2);
setrandvar_destroy(srvu);
setrandvar_destroy(srvx);
t = toc();
mprintf("Time = %f (s)\n", t);
//
// Load this script into the editor
//
filename = 'polychaos_product2.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );


