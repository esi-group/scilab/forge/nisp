// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

subdemolist = [
"demo_randvar3", "randvar/demo_randvar3.sce"; ..
"demo_randvar2", "randvar/demo_randvar2.sce"; ..
"demo_randvar1", "randvar/demo_randvar1.sce"; ..
];

subdemolist(:,2) = demopath + subdemolist(:,2)

