// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Consider an affine model Y, where the inputs X_i are normal random variables.
// Create the scatter plots.
// Compute the Standardized Regression Coefficients.
//

// Coefficients of the affine model:
// Y = X1 + X2 + X3 + X4
a = [1 1 1 1 0]';
// 4 random normal variables with means 0 and standard 
// deviations 1, 2, 3 and 4.
mu=[0 0 0 0]';
sigma=[1 2 3 4]';
exact = nisp_sumsa ( a , mu , sigma )

// Initialisation de la graine aleatoire
nisp_initseed ( 0 );

// Create the random variables.
rvu1 = randvar_new("Normale",0,1);
rvu2 = randvar_new("Normale",0,2);
rvu3 = randvar_new("Normale",0,3);
rvu4 = randvar_new("Normale",0,4);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);
setrandvar_addrandvar ( srvu, rvu4);
// Create a sampling by a Latin Hypercube Sampling with size 5000.
nbshots = 5000;
sampling = setrandvar_buildsample(srvu, "Lhs",nbshots);
// Perform the experiments.
y=nisp_sum(sampling,a);
// Scatter plots : y depending on X_i
scf();
for k=1:4
  subplot(2,2,k)
  plot(sampling(:,k),y,'rx');
  xistr="X"+string(k);
  xtitle("Scatter plot for "+xistr,xistr,"Y");
end

// Histogram of Y
scf();
histplot(50,y);
xtitle("Histogram of Y","Y","P(Y)");

// Compute the sample linear correlation coefficients
rho1 = corrcoef ( sampling(:,1) , y );
SRC1 = rho1^2;
mprintf("SRC_1=%.5f (expected=%.5f)\n",SRC1,exact.S(1));
//
rho2 = corrcoef ( sampling(:,2) , y );
SRC2 = rho2^2;
mprintf("SRC_2=%.5f (expected=%.5f)\n",SRC2,exact.S(2));
//
rho3 = corrcoef ( sampling(:,3) , y );
SRC3 = rho3^2;
mprintf("SRC_3=%.5f (expected=%.5f)\n",SRC3,exact.S(3));
//
rho4 = corrcoef ( sampling(:,4) , y );
SRC4 = rho4^2;
mprintf("SRC_4=%.5f (expected=%.5f)\n",SRC4,exact.S(4));
//
SUM = SRC1 + SRC2 + SRC3 + SRC4;
SUMexpected = 1;
mprintf("SUM=%.5f (expected=%.5f)\n",SUM,SUMexpected);

//
// Clean-up
randvar_destroy(rvu1);
randvar_destroy(rvu2);
randvar_destroy(rvu3);
randvar_destroy(rvu4);
setrandvar_destroy(srvu);
//
// Load this script into the editor
//
filename = 'affine_SRC_scatter.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

