// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'un quantile
// Experience B
// Estimation d'un quantile en queue haute.
// Une probabilite alpha est donnee, et 
// on cherche x tel que P(X>x)=alpha.
//
mu=2;
sigma=3;
al=1.e-3;
mprintf("Variable Log-normale\n");
mprintf("mu=%f\n",mu);
mprintf("sigma:%e\n",sigma);
mprintf("alpha:%e\n",al);
// Calcul exact
xExact=distfun_logninv(al,mu,sigma,%f);
mprintf("x (exact):%e\n",xExact);
// Estimation Monte-Carlo
Nsample=100000;
mprintf("Nombre de simulations:%d\n",Nsample);
X=distfun_lognrnd(mu,sigma,Nsample,1);
X=gsort(X,"g","d");
i=floor(Nsample*al);
x=X(i);
mprintf("x (estimation):%e\n",x);
