// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'une probabilite de depassement

// Experience A
// Probabilite de depassement d'un seuil 
// d'une variable log-normale
mu=2;
sigma=3;
seuil=1.e4;
mprintf("Variable Log-normale\n");
mprintf("mu=%f\n",mu);
mprintf("sigma:%e\n",sigma);
mprintf("seuil:%e\n",seuil);
// Calcul exact
pfExacte=distfun_logncdf(seuil,mu,sigma,%f);
mprintf("Pf (exact):%e\n",pfExacte);
// Estimation Monte-Carlo
Nsample=100000;
mprintf("Nombre de simulations:%d\n",Nsample);
X=distfun_lognrnd(mu,sigma,Nsample,1);
i=find(X>seuil);
nfail=size(i,"*");
mprintf("Nombre de depassements:%d\n",nfail);
pf=nfail/Nsample;
mprintf("Pf (estimation):%e\n",pf);
