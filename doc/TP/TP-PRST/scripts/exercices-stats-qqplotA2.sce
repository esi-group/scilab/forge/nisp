// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// QQ-Plot
// Experience A2
n=20;
mu=0; 
sigma= 1;
p=linspace(0.01,0.99,n);
y=distfun_norminv(p,mu,sigma);
scf();
plot(y,y,"bo");
plot(y,y,"r-");
xtitle("QQ Plot","Normal Quantiles","Normal Quantiles");
ymax=max(y);
for i=1:n
    plot([y(i),y(i)],[-ymax,ymax])
    plot([-ymax,ymax],[y(i),y(i)])
end
