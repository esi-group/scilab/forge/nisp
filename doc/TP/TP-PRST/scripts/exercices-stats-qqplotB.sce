// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// QQ-Plot
// Experience B (stixbox)
mu=1; 
sigma= 3;
//
scf();
k=0;
for n=[10 50 100 1000]
    k=k+1;
    subplot(2,2,k)
    x=distfun_normrnd(mu,sigma,1,n);
    x=gsort(x,"g","i");
    p=(1:n)/(n+1);
    y=distfun_norminv(p,mu,sigma);
    qqplot(y,x,"bo");
    i1=ceil(0.25*n);
    i3=ceil(0.75*n);
    plot([y(i1),y(i3)],[x(i1),x(i3)],"r-")
    strtitle=msprintf("n=%d",n);
    xtitle(strtitle,"Normal Quantile","Data Quantile");
end
