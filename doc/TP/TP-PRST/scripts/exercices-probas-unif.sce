// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Loi Uniforme
N = 1000;
a = 6;
b = 13;
// Esperance:
m = (a+b)/2
// Variance:
v = (b-a)^2/12
[M,V]=distfun_unifstat(a,b)
R = distfun_unifrnd(a,b,N,1);
mean(R)
variance(R)

// Make a plot of the actual distribution of the numbers
a = 6;
b = 13;
data = distfun_unifrnd(a,b,1,1000);
scf();
histo(data,[],%t);
x = linspace(a-1,b+1,1000);
y = distfun_unifpdf(x,a,b);
plot(x,y)
xtitle("Uniform random numbers","X","Density");
legend(["Empirical","PDF"]);
