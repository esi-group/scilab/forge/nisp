// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'une probabilite de depassement
// Experience D
// Distribution d'une probabilite de defaillance empirique
n=200;
Nrepeat=500;
mu=3;
seuil=8;
// Calcul exact
pfExacte=distfun_expcdf(seuil,mu,%f);
mprintf("Pf (exact):%e\n",pfExacte);
// Estimation Monte-Carlo
X=distfun_exprnd(mu,n,Nrepeat);
y=zeros(X);
y(X>seuil)=1;
b=sum(y,"r");
pf=b/n;
nbclasses=ceil(log2(Nrepeat)+1);
v=pfExacte*(1-pfExacte)*n;
sigma=sqrt(v);
x=linspace(n*pfExacte-3*sigma,n*pfExacte+3*sigma,100);
x=floor(x);
x=max(0,x);
x=unique(x);
y=distfun_binopdf(x,n,pfExacte);
ymax=max(y);
//
scf();
histo(pf*n,unique(pf*n),%t);
plot([pfExacte*n,pfExacte*n],[0,ymax],"r-");
e=gce();
e.children.thickness=2;
plot(x,y,"b*-")
legend(["Donnees","Exact","Binomial"]);
strtitle=msprintf("%d realisations, %d repetitions",n,Nrepeat);
xtitle(strtitle,"Pf*n","Frequence");
