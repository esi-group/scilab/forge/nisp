// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Intervalle de confiance de la moyenne d'une variable normale
// Experience C
// Distribution de Q=n*S/sigma^2
// quand X est normal
mu=0;
sigma=1;
n=5;
Nsample=10000;
X=distfun_normrnd(mu,sigma,Nsample,n);
S=variance(X,"c",%nan);
Q=n*S/sigma^2;
//
scf();
histo(Q,[],%t);
x=linspace(0,20,100);
y = distfun_chi2pdf(x,n-1);
plot(x,y);
xtitle("Distribution de Q","Q","Frequence");
legend(["Data","$\chi^2_{4}$"]);
