N=20;
pr=0.5;
x=1;
// Avec factorial:
c=factorial(TODO)/factorial(TODO)/factorial(TODO)
P=TODO
mprintf("P(X=1) (factorial)=%f\n",P)
// Avec nchoosek:
P=specfun_nchoosek(TODO)*TODO
mprintf("P(X=1) (nchoosek)=%f\n",P)
// Avec binopdf:
P=distfun_binopdf(TODO)
mprintf("P(X=1) (binopdf)=%f\n",P)
//
scf();
y1 = distfun_binopdf(TODO);
plot(x,y1,"bo-")
y2 = distfun_binopdf(TODO);
plot(x,y2,"go-")
y3 = distfun_binopdf(TODO);
plot(x,y3,"ro-")
legend(["pr=0.5, N=20","pr=0.7, N=20","pr=0.5, N=40"]);
xtitle("Binomial PDF","x","P(x)")
