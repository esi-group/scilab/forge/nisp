mu=12;
mprintf("mu=%f\n",mu);
// 1. Calculer la moyenne, la variance exactes de X
M=TODO; // Moyenne
V=TODO; // Variance
// 2. Utiliser distfun_expstat
[M,V] = distfun_expstat(mu);
// 3. Generer n realisations
n=1000;
X=distfun_exprnd(TODO);
// 4. Estimer la moyenne empirique
Mn=mean(TODO);
mprintf("E(X)=%f, V(X)=%f\n",TODO);
mprintf("E(Mn)=%f, V(Mn)=%f\n",TODO);
mprintf("Mean(X)=%f\n",TODO);
