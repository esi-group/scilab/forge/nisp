// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation de la moyenne

// Experience D
// Evolution de la PDF de la moyenne empirique 
// de n variables normales standard.
//
mu=0;
sigma=1;
x=linspace(-4,4,100);
y1=distfun_normpdf(x,mu,sigma/sqrt(1));
y2=distfun_normpdf(x,mu,sigma/sqrt(2));
y4=distfun_normpdf(x,mu,sigma/sqrt(4));
y10=distfun_normpdf(x,mu,sigma/sqrt(10));
scf();
plot(x,y1,"b-");
plot(x,y2,"r--");
plot(x,y4,"g:");
plot(x,y10,"k-.");
xtitle("Distribution of the sample mean","M","Density");
legend(["n=1","n=2","n=4","n=10"]);
