// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Changement de loi Uniforme vers Exponentielle
N=10000;
mu=5;
U=distfun_unifrnd(0,1,N,1);
R=-mu*log(U);
scf();
histo(R,[],%t);
x=linspace(0,40,100);
y=distfun_exppdf(x,mu);
plot(x,y);
legend(["Random","Density"]);
xtitle("Uniforme vers Exponentielle","R","Densite")

