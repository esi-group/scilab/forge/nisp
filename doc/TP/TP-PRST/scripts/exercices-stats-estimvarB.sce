// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation de la variance

// Experience B
// Variance biaisee / non biaisee
// "Correction de Bessel"
mu=5;
mprintf("Variable Exponentielle (mu=%f)\n",mu);
[M,V] = distfun_expstat(mu);
mprintf("E(X)=%f\n",M);
mprintf("V(X)=%f\n",V);
mprintf("Esperance(Variance non biaisee):%f\n",V);
n=2;
Nsample=10000;
X=distfun_exprnd(mu,Nsample,n);
//
// Non biaisee
S=variance(X,"c",0);
mprintf("Moyenne(Variance non biaisee):%f\n",mean(S));
//
// Biaisee
Vb=V*(n-1)/n;
mprintf("Esperance(Variance biaisee):%f\n",Vb);
Sb=variance(X,"c",1);
mprintf("Moyenne(Variance biaisee):%f\n",mean(Sb));
