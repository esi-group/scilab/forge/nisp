// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'un quantile
// Experience C
// Estimation d'un quantile en queue haute.
// Une probabilite alpha est donnee, et 
// on cherche x tel que P(X>x)=alpha.
//
mu=2;
sigma=3;
al=1.e-3;
mprintf("Variable Log-normale\n");
mprintf("mu=%f\n",mu);
mprintf("sigma:%e\n",sigma);
// Calcul exact
xExact=distfun_logninv(al,mu,sigma,%f);
mprintf("x (exact):%e\n",xExact);
x=[];
// Estimation Monte-Carlo
Ntab=2 .^(10:20);
for Nsample=Ntab
    X=distfun_lognrnd(mu,sigma,Nsample,1);
    X=gsort(X,"g","d");
    i=floor(Nsample*al);
    if (i>0) then
        x($+1)=X(i);
    else
        x($+1)=0;
    end
end
h=scf();
plot(Ntab,x,"rx");
h.children.log_flags="lnn";
plot([2^10,2^20],[xExact,xExact],"b-");
xtitle("Convergence d''un quantile alpha=1.e-3","n","x");
legend(["Empirique","Exact"]);
