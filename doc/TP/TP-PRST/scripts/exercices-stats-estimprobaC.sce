// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'une probabilite de depassement
// Experience C
// Comparaison entre Monte-Carlo simple et 
// la transformation arcsin(sqrt(p))
Nsample=1000;
al=0.05;
q = al/2.;
f = distfun_norminv(q,0,1,%f);
n=20;
ptab=10.^linspace(-3,-2,n);
ptab=ptab';
lowTCL=[];
upTCL=[];
lowAS=[];
upAS=[];
for i=1:n
    p=ptab(i);
    // Via le TCL
    s=f*sqrt(p*(1-p)/Nsample);
    lowTCL(i)=max(0,p-s);
    upTCL(i)=p+s;
    // Via la transformation arcsin(p)
    t=asin(sqrt(p));
    s=f/(2*sqrt(Nsample));
    lowAS(i)=sin(max(0,t-s))^2;
    upAS(i)=sin(t+s)^2;
end
scf();
plot(ptab,lowTCL,"b--");
plot(ptab,lowAS,"r-");
plot(ptab,ptab,"k-");
e=gce();
e.children.thickness=2;
plot(ptab,upTCL,"b--");
plot(ptab,upAS,"r-");
xtitle("1000 realisations","p","Intervalle de confiance à 95%");
legend(["TCL","Arcsin","pn"],"in_upper_left");
