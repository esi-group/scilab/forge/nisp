// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Intervalle de confiance de la moyenne d'une variable normale
// Experience D
// Distribution de T=(M-mu)/sqrt(S/(n-1))
// quand X est normal
mu=0;
sigma=1;
n=5;
Nsample=10000;
X=distfun_normrnd(mu,sigma,Nsample,n);
M=mean(X,"c");
S=variance(X,"c",%nan);
T=(M-mu)./(sqrt(S/(n-1)));
//
scf();
x=linspace(-5,5,50);
histo(T,x,%t);
x=linspace(-5,5,100);
y=distfun_tpdf(x,n-1);
plot(x,y,"r-");
xtitle("Distribution de T","T","Frequence");
legend(["Data","Densite de T4"]);
