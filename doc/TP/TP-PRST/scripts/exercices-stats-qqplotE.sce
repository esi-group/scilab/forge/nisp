// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// QQ-Plot
// Experience E (Variable uniforme)
n=10000;
x=distfun_unifrnd(0,1,n,1);
mu=mean(x); 
sigma= sqrt(variance(x));
p=linspace(0.01,0.99,100)';
q=quantile(x,p);
nbclasses=ceil(log2(n)+1);
y=distfun_norminv(p,mu,sigma);
//
scf();
subplot(1,2,1)
qqplot(y,q,"bo");
plot([y(25),y(75)],[q(25),q(75)],"r-")
xtitle("U(0,1) - n=10 000","Normal Quantile","Data Quantile");
subplot(1,2,2)
histo(x,[],%t);
xtitle("U(0,1) - n=10 000","X","Frequency");
