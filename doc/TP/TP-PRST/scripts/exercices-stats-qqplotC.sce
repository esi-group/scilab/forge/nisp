// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// QQ-Plot
// Experience C (Temperature du corps)
x=fscanfMat("normtemp.dat.txt");
// Retire la seconde colonne (le sexe)
x(:,2)=[];
n=size(x,"r");
m=size(x,"c");
mu=mean(x,"r"); 
sigma= sqrt(variance(x,"r"));
p=(1:n)'/(n+1);
strleg=["Temp. Corps (F)","Pulsations Cardiaques (batt/min)"];
nbclasses=ceil(log2(n)+1);
scf();
k=0;
for i=1:m
    y=distfun_norminv(p,mu(i),sigma(i));
    //
    k=k+1;
    subplot(m,2,k)
    qqplot(y,x(:,i),"b*-");
    plot(x(:,i),x(:,i),"r-")
    strtitle=msprintf("%s",strleg(i));
    xtitle(strtitle,"Normal Quantile","Data Quantile");
    //
    k=k+1;
    subplot(m,2,k)
    histo(x(:,i),[],%t);
    xtitle(strtitle,"X","Frequency");
end
//
// Remarque
// Run Sequence Plot.
// Sur la temperature du corps, les donnees 
// sont triees: les hommes, puis les femmes, 
// par ordre croissant. 
// D'ou la forme en deux "S".
// La moyenne et la variance des pulsations semblent 
// constantes.
scf();
subplot(1,2,1);
plot(1:n,x(:,1)',"ro-");
xtitle("","Indice","Temp. Corps (F)")
subplot(1,2,2);
plot(1:n,x(:,2)',"ro-");
xtitle("","Indice","Pulsations Cardiaques (batt/min)")
//
// Lag Plot
// Les temperatures sont triees.
// Les pulsations semblent aleatoires.
scf();
subplot(1,2,1);
plot(x(1:$-1,1),x(2:$,1),"ro");
xtitle("Temp. Corps (F)","X(i-1)","X(i)")
subplot(1,2,2);
plot(x(1:$-1,2),x(2:$,2),"ro");
xtitle("Pulsations Cardiaques (batt/min)","X(i-1)","X(i)")
