// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'un intervalle de confiance sur la moyenne.

// Experience A
// Estimation par intervalle de la moyenne 
// d'une loi Log Normale Y = exp(X)
//
n = 100; // taille echantillon
mu = 2;
sigma = 1;
mux=distfun_lognstat(mu,sigma); // esperance de X
X = distfun_lognrnd(mu,sigma,n,1); // Echantillon X
Mn = mean(X); // moyenne empirique
Sn2 = variance(X,"r",%nan); // variance empirique (biaisee)
level=0.05; // =1-0.95
al=level/2;
// Quand n n'est pas tres grand:
q = distfun_tinv(al,n-1,%f); // quantile 0.025 ~ 1.98
// Quand n est grand:
//q = distfun_norminv(al,0,1,%f); // quantile 0.025 ~ 1.96
delta = q * sqrt(Sn2/(n-1));
low=Mn-delta;
up=Mn+delta;
mprintf("Moyenne exacte = %f\n",mux);
mprintf("Moyenne empirique = %f\n",Mn);
mprintf("Intervalle a 0.95%%: [%f,%f]\n",low,up);
