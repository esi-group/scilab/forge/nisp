mu=12;
mprintf("mu=%f\n",mu);
// 1. Calculer la moyenne, la variance
M=TODO; // Moyenne
V=TODO; // Variance
// 2. Utiliser distfun_expstat
[M,V] = distfun_expstat(TODO);
// 3. Generer 10000 realisations de la 
// moyenne empirique
Nsample=10000;
n=2;
X=distfun_exprnd(TODO);
Mn=mean(TODO);
// 4. Estimer les valeurs empiriques
// Comparer avec les valeurs exactes
mprintf("n=%d\n",n);
mprintf("E(Mn)=%f, V(Mn)=%f\n",TODO,TODO);
mprintf("Mean(Mn)=%f, Variance(Mn)=%f\n",.. 
mean(TODO),variance(TODO));
// 5. Repeter avec n=1,2,4,8
mprintf("Variable Exponentielle (mu=12)\n");
for n=[1 2 4 8]
    mprintf("n=%d\n",n);
    mprintf("E(Mn)=%f, V(Mn)=%f\n",TODO,TODO);
    X=distfun_exprnd(TODO);
    Mn=mean(TODO);
    mprintf("Mean(Mn)=%f, Variance(Mn)=%f\n",.. 
    mean(TODO),variance(TODO));
end
