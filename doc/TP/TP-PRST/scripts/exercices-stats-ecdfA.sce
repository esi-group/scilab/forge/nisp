// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Fonction de repartition empirique
// Experience A (loi normale)
n = 100; // taille de l'echantillon
X = distfun_normrnd(0,1,n,1); // echantillon X ~ N(0,1)
X = gsort(X,"g","i"); // on reordonne par valeurs croissantes
// Fonction de repartition aux points echantillon
p = distfun_normcdf(X,0,1);
scf();
plot(X,(1:n)/n,"b-"); // Fonction de repartition empirique
plot(X,p,"r-"); // Fonction de repartition
xtitle("Normale(0,1) - 100 realisations","x","P(X<x)");
legend(["CDF Empirique","CDF"],"in_upper_left");
