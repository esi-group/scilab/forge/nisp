// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Fonction de repartition empirique
// Experience C (convergence quand n augmente)
function plotExpEcdf(n,mu)
    X = distfun_exprnd(mu,n,1);
    X = gsort(X,"g","i");
    p = distfun_expcdf(X,mu);
    plot(X,(1:n)/n,"b-");
    plot(X,p,"r-");
    stitle=msprintf("Exp(%.2f) - %d realisations",mu,n);
    xtitle(stitle,"x","P(X<x)");
    legend(["CDF Empirique","CDF"],"in_lower_right");
endfunction

mu=5;
h=scf();
subplot(2,2,1);
plotExpEcdf(100,mu);
subplot(2,2,2);
plotExpEcdf(200,mu);
subplot(2,2,3);
plotExpEcdf(500,mu);
subplot(2,2,4);
plotExpEcdf(1000,mu);
h.children(1).data_bounds(2,1)=30;
h.children(2).data_bounds(2,1)=30;
h.children(3).data_bounds(2,1)=30;
h.children(4).data_bounds(2,1)=30;
