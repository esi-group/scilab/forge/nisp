// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Produits defaillants sur une ligne de production
P0 = distfun_binopdf(0,3,0.8)
mprintf("P(X=0)=%f\n",P0)
P1 = distfun_binopdf(1,3,0.8)
mprintf("P(X=1)=%f\n",P1)
P2 = distfun_binopdf(2,3,0.8)
mprintf("P(X=2)=%f\n",P2)
P3 = distfun_binopdf(3,3,0.8)
mprintf("P(X=3)=%f\n",P3)
mprintf("Somme=%f\n",P0+P1+P2+P3)
