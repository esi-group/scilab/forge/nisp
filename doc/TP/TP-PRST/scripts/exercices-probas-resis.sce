// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Puissance dissipee par une resistance
mu=6
sigma=1
R=1/3
EW=(mu^2+sigma^2)/R
threshold=120
t=sqrt(threshold*R)
PW = distfun_normcdf(t,mu,sigma,%f)

// Variation du seuil
threshold = linspace(1,300,100);
t=sqrt(threshold*R);
PW = distfun_normcdf(t,mu,sigma,%f);
scf();
plot(threshold,PW)
xtitle("Puissance dissipee","Seuil","P(W>Seuil)");

