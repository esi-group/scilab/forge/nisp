// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Fonction de repartition empirique
// Experience B (loi exponentielle)
n = 100; // taille de l'echantillon
mu=5;
X = distfun_exprnd(mu,n,1); // echantillon X ~ Exp(mu)
X = gsort(X,"g","i");
p = distfun_expcdf(X,mu);
scf();
plot(X,(1:n)/n,"b-");
plot(X,p,"r-");
stitle=msprintf("Exp(%f) - %d realisations",mu,n);
xtitle(stitle,"x","P(X<x)");
legend(["CDF Empirique","CDF"],"in_lower_right");
