// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Quantile de Wilks
// Experience A
function r = wilks(alpha,bet,n)
    // Calcule le rang r, tel que :
    // P(Y(r)>y(alpha))>bet
    // avec y(alpha) le quantile de probabilite alpha, i.e.
    // P(Y<y(alpha))=alpha.
    // Si il n'y a pas assez de donnees, renvoit r=0.
    // Require: specfun, distfun
    if (n < specfun_log1p(-bet)/log(alpha)) then 
        r=0;
    else
        r = distfun_binoinv(bet,n,alpha)
        r = r + 1
    end
endfunction
// mediane (alpha=0.5)
wilks(0.5,0.5,100) // => 51 // confiance 0.5
wilks(0.5,0.95,100) // => 59 // confiance 0.95
// quantile 0.95, confiance 0.95
wilks(0.95,0.95,53) // => 0 // pas assez de donnees
wilks(0.95,0.95,59) // => 59 // la valeur extreme
wilks(0.95,0.95,124) // => 122 // l'avant derniere valeur
wilks(0.95,0.95,153) // => 150 ...
