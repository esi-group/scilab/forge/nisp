// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation de la moyenne

// Experience A
// Distribution de la moyenne empirique d'une variable exponentielle.
//
// mu : moyenne de la variable exponentielle
// M : moyenne de la variable exponentielle (exacte)
// V : variance de la variable exponentielle (exacte)
// Nsample : nombre de repetitions de l'experience
// n : nombre de realisations de la variable aleatoire
// X : matrice de taille Nsample-par-n, 
//     realisations de la variable aleatoire
// Mn : matrice de taille Nsample-par-1, moyenne empirique
//
mu=12;
mprintf("mu=%f\n",mu);
// 1. Calculer la moyenne, la variance
M=mu; // Moyenne
V=mu^2; // Variance
// 2. Utiliser distfun_expstat
[M,V] = distfun_expstat(mu);
// 3. Generer 10000 realisations de la 
// moyenne empirique
Nsample=10000;
n=2;
X=distfun_exprnd(mu,Nsample,n);
Mn=mean(X,"c");
// 4. Estimer les valeurs empiriques
// Comparer avec les valeurs exactes
mprintf("n=%d\n",n);
mprintf("E(Mn)=%f, V(Mn)=%f\n",M,V/n);
mprintf("Mean(Mn)=%f, Variance(Mn)=%f\n",.. 
mean(Mn),variance(Mn));
// 5. Repeter avec n=1,2,4,8
mprintf("Variable Exponentielle (mu=12)\n");
for n=[1 2 4 8]
    mprintf("n=%d\n",n);
    mprintf("E(Mn)=%f, V(Mn)=%f\n",M,V/n);
    X=distfun_exprnd(mu,Nsample,n);
    Mn=mean(X,"c");
    mprintf("Mean(Mn)=%f, Variance(Mn)=%f\n",.. 
    mean(Mn),variance(Mn));
end
