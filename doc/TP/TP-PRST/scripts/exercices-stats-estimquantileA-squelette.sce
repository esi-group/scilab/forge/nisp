mu=2;
sigma=3;
al=0.1;
// Calcul exact
xExact=distfun_logninv(TODO);
mprintf("x (exact):%e\n",xExact);
// Estimation Monte-Carlo
Nsample=100000;
X=distfun_lognrnd(TODO);
X=gsort(X,"g","i");
i=floor(Nsample*al);
x=X(i);
mprintf("x (estimation):%e\n",x);
