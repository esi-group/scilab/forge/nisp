// Plot the PDF (with distfun_normpdf)
mu = 5;
sigma = 7;
scf();
x = linspace(TODO);
y = distfun_normpdf(TODO);
plot(x,y,"r-")
xtitle("Densite de probabilite Normale - mu=5, sigma=7",..
"x","f(x)");

// Plot the CDF
mu = 5;
sigma = 7;
scf();
x = linspace(TODO);
p = distfun_normcdf(TODO);
plot(x,p,"b-")
xtitle("Fonction Repartition Normale - mu=5, sigma=7",..
"x","$P(X\leq x)$");
