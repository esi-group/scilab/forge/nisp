// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Intervalle de confiance de la moyenne d'une variable normale
// Experience A (variance connue)
mu=2;
sigma=3;
mprintf("Variable normale, mu=%f, sigma=%f\n",..
mu,sigma);
Nsample=10000;
level=0.05; // 1-0.95
mprintf("Number of samples:%d, level=%f",..
Nsample,level);
X=distfun_normrnd(mu,sigma,Nsample,1);
Mn=mean(X);
al=level/2;
z=distfun_norminv(al,0,1,%f);
delta=z*sigma/sqrt(Nsample);
low=Mn-delta;
up=Mn+delta;
mprintf("Moyenne empirique: %f\n",Mn)
mprintf("Int. Conf.:[%f,%f]\n",low,up)
