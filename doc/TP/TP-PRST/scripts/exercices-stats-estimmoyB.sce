// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation de la moyenne

// Experience B
// Distribution de la moyenne empirique d'une variable exponentielle.
mu=12;
Nsample=1000;
[M,V]=distfun_expstat(mu);
x=linspace(0,40,100);
scf();
//
k=0;
for n=[1,2,4,8]
    k=k+1;
    X = distfun_exprnd(mu,Nsample,n);
    Mn=mean(X,"c");
    subplot(2,2,k);
    histo(Mn,[],%t);
    y=distfun_normpdf(x,M,sqrt(V/n));
    plot(x,y,"r-");
    xtitle("Sample Mean - n="+string(k),"M","Frequency");
    legend(["Data","Normal PDF"]);
end
