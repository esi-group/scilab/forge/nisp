// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'un intervalle de confiance sur la moyenne.

// Experience B : Verification.
//
mu=2;
sigma=1;
n=100
Nsample=10000;
X = distfun_lognrnd(mu,sigma,n,Nsample); // Echantillon X
Mn = mean(X,"r");
Sn2 = variance(X,"r",%nan); // variance empirique (biaisee)
delta = q * sqrt(Sn2/(n-1));
low=Mn-delta;
up=Mn+delta;
x=linspace(5,25,50);
scf();
histo(low,x,%t,1);
histo(up,x,%t,2);
plot([mux,mux],[0,0.3],"r-");
legend(["Lower Bound","Upper Bound","E(X)"]);
xtitle("Invervalle de confiance a 95% - X~Log-Normale",..
"Mean","Frequency")
//
// Calcul de P(I contains mux)
//
i=find(mux>low&mux<up);
nInBounds=size(i,"*");
pInBounds=nInBounds/Nsample;
mprintf("P(I contains E(X))=%f\n",pInBounds);
