// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'une probabilite de depassement
// Experience B
// Calcul d'un intervalle de confiance à 1-alpha=95% pour Pf
// alpha=0.05
al=0.05;
q = al/2.;
f = distfun_norminv(q,0,1,%f);
low = pf - f * sqrt(pf*(1-pf)/Nsample);
up = pf + f * sqrt(pf*(1-pf)/Nsample);
mprintf("95%% Int. de Conf.:[%e,%e]\n" , low,up);
