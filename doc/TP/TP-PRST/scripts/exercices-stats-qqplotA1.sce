// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// QQ-Plot

// Experience A1
function [y,p] = quantileEmpirique(x)
    n = length(x);
    y = gsort(x,"g","i");
    p = [1:n] / (n+1);
endfunction
n=50; // taille de l'echantillon
mu=1; 
sigma= 3;
x=distfun_normrnd(mu,sigma,1,n); // echantillon de la loi normale
[x,p]=quantileEmpirique(x); // quantiles et probabilite associes
y=distfun_norminv(p,mu,sigma); // quantiles de la loi normale
scf();
plot(x,y,"bo");
plot([x(n/4),x(3*n/4)],[y(n/4),y(3*n/4)],"r-");
xtitle("QQ Plot","Data Quantile","Normal Quantile");
