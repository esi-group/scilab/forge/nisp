// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Show a Petras Design

mprintf("Creating a Uniforme variable ...\n")
vu1 = randvar_new("Uniforme",0.0,1.0);
mprintf("Creating a Uniform variable ...\n")
vu2 = randvar_new("Uniforme",0.0,1.0);

mprintf("Creating a set of random variables ...\n")
srv = setrandvar_new ( );
setrandvar_addrandvar ( srv , vu1 );
setrandvar_addrandvar ( srv , vu2 );

mprintf("Building Petras sampling with 20 degrees ...\n")
setrandvar_buildsample ( srv , "Petras" , 20 );

sampling = setrandvar_getsample ( srv );

mprintf("Plotting sampling\n")
my_handle = scf();
plot(sampling(:,1),sampling(:,2));
my_handle.children.children.children.line_mode = "off";
my_handle.children.children.children.mark_mode = "on";
my_handle.children.children.children.mark_size = 2;
my_handle.children.title.text = "Petras Sampling";
my_handle.children.x_label.text = "Variable #1 : Uniforme,0.0,1.0";
my_handle.children.y_label.text = "Variable #2 : Uniforme,0.0,1.0";


randvar_destroy(vu1);
randvar_destroy(vu2);
setrandvar_destroy(srv);
//
// Load this script into the editor
//
filename = 'demo_setrandvar_petras.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

