// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// See the LHS Max Min sampling in NISP.
//

srv = setrandvar_new(2);
np = 10;
ntry = 100;
setrandvar_buildsample ( srv , "LhsMaxMin" , np , ntry );
sampling = setrandvar_getsample ( srv );
scf();
plot(sampling(:,1),sampling(:,2),"bo");
xtitle("LHS Max Min Design","X1","X2");
// Add the cuts
cut = linspace ( 0 , 1 , np + 1 );
for i = 1 : np + 1
  plot( [cut(i) cut(i)] , [0 1] , "-" )
end
for i = 1 : np + 1
  plot( [0 1] , [cut(i) cut(i)] , "-" )
end
setrandvar_destroy ( srv );

//
// Load this script into the editor
//
filename = 'demo_setrandvar_lhsmaxmin.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

