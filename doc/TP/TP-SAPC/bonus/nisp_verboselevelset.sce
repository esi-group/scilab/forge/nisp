//
// This help file was automatically generated from nisp_verboselevelset.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_verboselevelset.sci
//

level = nisp_verboselevelget ( )
nisp_verboselevelset ( 1 )
nisp_verboselevelget ( )
nisp_verboselevelset ( 0 )
nisp_verboselevelget ( )
nisp_verboselevelset ( level )
halt()   // Press return to continue
 
// The following generates an error:
// the level must be 0 or 1.
nisp_verboselevelset ( -1 );
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_verboselevelset.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
