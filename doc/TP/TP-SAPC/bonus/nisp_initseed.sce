//
// This help file was automatically generated from nisp_initseed.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_initseed.sci
//

nisp_initseed ( 0 )
nisp_initseed ( 123456789 )
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_initseed.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
