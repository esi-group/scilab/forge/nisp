// Copyright (C) 2010 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Build a LHS design with 2 variables

// References
//
// "A User's Guide to LHS: Sandia's Latin Hypercube Sampling Software"
// Gregory D. Wyss and Kelly H. Jorgensen
// 1998
//
// "A Comparison of Three Methods for Selecting Values of Input Variables in the Analysis of
// Output from a Computer Code"
// M. D. McKay, R. J. Beckman, W. J. Conover
// Technometrics, Vol. 21, No. 2 (May, 1979), pp. 239-245

scf();
n = 5;
s =  2;
sampling = nisp_buildlhs ( s , n );
plot ( sampling(:,1) , sampling(:,2) , "bo" );
// Add the cuts
cut = linspace ( 0 , 1 , n + 1 );
for i = 1 : n + 1
  plot( [cut(i) cut(i)] , [0 1] , "-" )
end
for i = 1 : n + 1
  plot( [0 1] , [cut(i) cut(i)] , "-" )
end

scf();
n = 1000;
s =  2;
sampling = nisp_buildlhs ( s , n );
plot ( sampling(:,1) , sampling(:,2) , "bo" );

