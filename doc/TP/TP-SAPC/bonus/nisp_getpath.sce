//
// This help file was automatically generated from nisp_getpath.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_getpath.sci
//

path = nisp_getpath (  )
// See the provided documents in the doc directory.
ls(fullfile(path,"doc"))
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_getpath.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
