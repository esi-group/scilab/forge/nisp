// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html




nb = randvar_size();
mprintf("Number of variables : %d\n",nb)
tokenmatrix = randvar_tokens();
mprintf("List of variables : %s\n",strcat(string(tokenmatrix)," "))
mprintf("Creating several variables ...\n")
vu1 = randvar_new("Uniforme");
vu2 = randvar_new("Uniforme");
vu3 = randvar_new("Uniforme");
nb = randvar_size();
mprintf("Number of variables : %d\n",nb)
tokenmatrix = randvar_tokens();
mprintf("List of variables : %s\n",strcat(string(tokenmatrix)," "))
mprintf("Destroying variables ...\n")
randvar_destroy(vu1);
randvar_destroy(vu2);
randvar_destroy(vu3);
nb = randvar_size();
mprintf("Number of variables : %d\n",nb)
tokenmatrix = randvar_tokens();
mprintf("List of variables : %s\n",strcat(string(tokenmatrix)," "))
//
// Load this script into the editor
//
filename = 'demo_randvar3.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

