// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU 
// Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// A demo of the setrandvar class.
// Reference :
// "Specifications scientifiques et 
// informatiques : Chaos Polynomial"
// D-WP1/08/01/A, MARTINEZ JM. - CEA

nisp_initseed ( 0 );
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
srv = setrandvar_new ( );
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
np = 1000;
name = "MonteCarlo";
setrandvar_buildsample ( srv , name , np );
sampling = setrandvar_getsample ( srv );
// Check sampling of random variable #1
m = mean(sampling(:,1));
mprintf("Mean of variable #1 : %f (expected : 1.0)\n", m);
// Check sampling of random variable #2
m = mean(sampling(:,2));
mprintf("Mean of variable #2 : %f (expected : 1.75)\n", m);
setrandvar_destroy(srv);
randvar_destroy(rv1);
randvar_destroy(rv2);
