// Parameters
sigma1=TODO;
sigma2=TODO;
sigma3=TODO;
sigma4=TODO;
// 1. Indices SRC exacts
varY=TODO; // Variance de Y
SRCexact(1)=TODO
SRCexact(2)=TODO
SRCexact(3)=TODO
SRCexact(4)=TODO
mprintf("SRC1=%f\n",SRCexact(1))
mprintf("SRC2=%f\n",SRCexact(2))
mprintf("SRC3=%f\n",SRCexact(3))
mprintf("SRC4=%f\n",SRCexact(4))
mprintf("SUM=%f\n",sum(SRCexact))
// 2. Definir la fonction
function y = Exemple (x)
    y = TODO
endfunction
// 3. Create the random variables rvu1,...,rvu4
rvu1 = randvar_new(TODO);
rvu2 = randvar_new(TODO);
rvu3 = randvar_new(TODO);
rvu4 = randvar_new(TODO);
// 4. Create the set of random variables srvu
srvu = setrandvar_new();
setrandvar_addrandvar (TODO);
setrandvar_addrandvar (TODO);
setrandvar_addrandvar (TODO);
setrandvar_addrandvar (TODO);
// 5. Create a sampling by a 
// Latin Hypercube Sampling with size 5000.
nbshots = TODO;
setrandvar_buildsample(srvu,TODO);
sampling = setrandvar_getsample(TODO);
// 6. Perform the experiments.
y=TODO
// 7. Scatter plots : y depending on X_i
// X1 :
scf();
subplot(2,2,1)
plot(TODO);
xtitle("Scatter plot for X1","X1","Y");
// X2, X3, X4 :
TODO
// 8. Estimate the SRC indices
rho1 = corrcoef(TODO)
SRC(1) = TODO
mprintf("SRC_1=%.5f (expected=%.5f)\n",SRC(1),SRCexact(1));
// SRC(2),SRC(3),SRC(4) :
TODO
// 9. Compute the sum of SRC indices
mprintf("SUM=%.5f (expected=%.5f)\n",sum(SRC),1.);
// 10. Clean-up
nisp_destroyall();
