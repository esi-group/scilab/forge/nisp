// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU 
// Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Compute the first order sensitivity indices, 
// the naive way.

// 1. Definir la fonction exemple
function y = Exemple (x)
    y(:,1) = x(:,1) .* x(:,2)
endfunction

// 2. Set parameters
// First variable
// Normal
mu1 = 1.5;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 3.5;
sigma2 = 2.5;
// Compute exact E(Y) and V(Y).
EYexact= mu1*mu2;
VYexact = mu2^2*sigma1^2 + ..
mu1^2*sigma2^2 + sigma1^2*sigma2^2;
// Compute exact S1 and S2.
S1exact = ( mu2^2*sigma1^2 ) / VYexact;
S2exact = ( mu1^2*sigma2^2 ) / VYexact;

// 3. Estimate the mean and variance of y.
n = 1000;
mprintf("Number of experiments : %d\n",1+2*n^2);
// Define a sample for x1.
x1 = distfun_normrnd ( mu1 , sigma1 , n , 1 );
// Define a sample for x2.
x2 = distfun_normrnd ( mu2 , sigma2 , n , 1 );
// Merge the two samples
x = [x1 x2];
// Perform the experiments
y = Exemple (x);
Ey= mean(y);
Vy= variance(y);
mprintf("Expectation y = %.5f (exact = %.5f)\n", ..
Ey, EYexact);
mprintf("Variance y = %.5f (exact = %.5f)\n", ..
Vy, VYexact);
//
// 4. Estimate S1
// Estimate E(y|x1) for several values of x1.
// Generate a sampling for x1.
x1 = distfun_normrnd ( mu1 , sigma1 , n , 1 );
// For each value of x1, generate a sample for x2.
x2 = distfun_normrnd ( mu2 , sigma2 , n , n );
for i = 1 : n
    // Merge the current value of x1, i.e. x1(i), 
    // with the sample for x2.
    x = [x1(i)*ones(n,1) x2(:,i)];
    // Perform the experiments
    y = Exemple (x);
    // Store E(y|x1) in Ey_all(i)
    Ey_all(i) = mean(y);
end
// Estimate S1
S1 = variance(Ey_all)/Vy;
mprintf("First order /x1=%.5f (exact = %.5f)\n", ..
    S1, S1exact);
//
// 4. Compute S2.
// Estimate E(y|x2) for several values of x2.
// Generate a sample for x2.
x2 = distfun_normrnd ( mu2 , sigma2 , n , 1 );
// For each value of x2, generate a sample for x1.
x1 = distfun_normrnd ( mu1 , sigma1 , n , n );
for i = 1 : n
    // Merge the current value of x2, i.e. x2(i), 
    // with the sampling for x1.
    x = [x1(:,i) x2(i)*ones(n,1)];
    // Perform the experiments
    y = Exemple (x);
    // Store E(y|x2) in Ey_all(i)
    Ey_all(i) = mean(y);
end
// Compute SE
S2 = variance(Ey_all)/Vy;
mprintf("First order /x2=%.5f (exact = %.5f)\n", ..
    S2, S2exact);
