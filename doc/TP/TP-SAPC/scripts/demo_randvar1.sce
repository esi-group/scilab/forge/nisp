// Copyright (C) 2012 - 2014 - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//

// This file must be used under the terms of the GNU 
// Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// A demo of the randvar class.
mu = 1.0;
sigma = 0.5;
v = randvar_new("Normale",mu,sigma);
randvar_getlog(v); 
values = randvar_getvalue(v); 
disp(values)
n = 1000;
for i=1:n
    values(i) = randvar_getvalue(v);
end
moyenne = mean (values);
disp(moyenne)
ecarttype = st_deviation (values);
disp(ecarttype)
randvar_destroy(v);
