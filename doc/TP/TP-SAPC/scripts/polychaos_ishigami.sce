// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU 
// Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Test the chaos polynomial decomposition on the 
// Ishigami function.

// 1. Definir la fonction ishigami
function y = ishigami (x)
  // Returns the output y of the product x1 * x2.
  // Parameters
  // x: a np-by-nx matrix of doubles, where np is 
  //    the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  a=7.
  b=0.1
  s1=sin(x(:,1))
  s2=sin(x(:,2))
  x34 = x(:,3).^4
  y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction

// 2. Compute exact indices
a=7.;
b=0.1;
EY = a/2; 
V1=(1+b*%pi**4/5)**2/2
V2=a**2/8;
V13=b**2*%pi**8*8/225
VY=V1+V2+V13
S1exact=V1/VY
S2exact=V2/VY
S3exact=0.

// 3. Create a group of stochastic variables
nx = 3;
srvx = setrandvar_new( nx );

// 4. Create a group of uncertain variables
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);

// 5. Create a Petras sampling
degre = 9;
setrandvar_buildsample(srvx,"Petras",degre);
setrandvar_buildsample( srvu , srvx );

// 6. Create the chaos polynomial
pc = polychaos_new ( srvx , 1 );
polychaos_setdegree(pc,degre);

// 7. Perform the experiments
np = setrandvar_getsize(srvu);
mprintf("Number of experiments = %d\n",np);
polychaos_setsizetarget(pc,np);
inputdata = setrandvar_getsample(srvu);
outputdata = ishigami(inputdata);
polychaos_settarget(pc,outputdata);

// 8. Compute the coefficients by integration
polychaos_computeexp(pc,srvx,"Integration");

// 9. Get the sensitivity indices
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);

mprintf("Mean        = %f (expected=%f)\n",..
	average,EY);
mprintf("Variance    = %f (expected=%f)\n",var,VY);
disp (" First order sensitivity indice : "); 
disp ( polychaos_getindexfirst (pc )')
disp (" Total sensitivity indices: "); 
disp ( polychaos_getindextotal (pc )')

// Clean-up
nisp_destroyall();
