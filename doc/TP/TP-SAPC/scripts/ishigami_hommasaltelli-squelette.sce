// 1. Define the function
function y = ishigami (x)
    y=TODO
endfunction

// 2. Compute exact indices
a=TODO
b=TODO
V1=TODO
V2=TODO
V13=TODO
VY=TODO
S1exact=TODO
S2exact=TODO
S3exact=TODO

// 3. Create the uncertain parameters
rvu1 = randvar_new(TODO);
rvu2 = randvar_new(TODO);
rvu3 = randvar_new(TODO);
srvu = setrandvar_new();
setrandvar_addrandvar(TODO);
setrandvar_addrandvar(TODO);
setrandvar_addrandvar(TODO);

// 4. Create a sample A
n = 10000;
setrandvar_buildsample(srvu,TODO);
A = setrandvar_getsample(srvu);
// Create a sample B
setrandvar_buildsample(srvu,TODO);
B = setrandvar_getsample(srvu);

// 5. Perform the experiments in A
ya =TODO;

// 6. Compute the first order sensitivity index for X1
C =TODO
yc =TODO
s1 = corrcoef(TODO);
mprintf("S1 : %f (expected = %f)\n", s1, S1exact);

// 7. Compute the first order sensitivity index for X2
TODO

// 8. Compute the first order sensitivity index for X3
TODO
