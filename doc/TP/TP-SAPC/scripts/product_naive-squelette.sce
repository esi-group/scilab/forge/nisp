// 1. Definir la fonction exemple
function y = Exemple (x)
    y=TODO
endfunction

// 2. Set parameters
// First variable
// Normal
mu1 = TODO;
sigma1 = TODO;
// Second variable
// Normal
mu2 = TODO;
sigma2 = TODO;
// Compute exact E(Y) and V(Y).
EYexact= TODO;
VYexact=TODO;
// Compute exact S1 and S2.
S1exact=TODO;
S2exact=TODO;

// 3. Estimate the mean and variance of y.
n = 1000;
// Define a sample for x1.
x1 = distfun_normrnd (TODO);
// Define a sample for x2.
x2 = distfun_normrnd (TODO);
// Merge the two samples
x = [x1 x2];
// Perform the experiments
y=Exemple (x);
Ey=TODO;
Vy=TODO;
mprintf("Expectation y = %.5f (exact = %.5f)\n", ..
    Ey, EYexact);
mprintf("Variance y = %.5f (exact = %.5f)\n", ..
    Vy, VYexact);

// 4. Estimate S1
// Estimate E(y|x1) for several values of x1.
// Generate a sampling for x1.
x1=distfun_normrnd(TODO);
// For each value of x1, generate a sample for x2.
x2=distfun_normrnd(TODO);
for i = 1 : n
    // Merge the current value of x1, i.e. x1(i), 
    // with the sample for x2.
    x = [x1(i)*ones(n,1) x2(:,i)];
    // Perform the experiments
    y = Exemple (x);
    // Estimate E(y|X1=x1(i))
    Ey_all(i) = TODO;
end
// Estimate S1
S1 = TODO;
mprintf("First order /x1=%.5f (exact = %.5f)\n", ..
    S1, S1exact);
//
// 4. Compute S2.
TODO
