% Copyright (C) 2013 - 2015 - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

\section{Multivariate polynomials}

In this section, we define the multivariate polynomials which are 
used in the context of polynomial chaos decomposition.
We consider the regular polynomials as well as multivariate orthogonal 
polynomials.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Occupancy problems}
\label{sec-occpbs}

In the next section, we will count the number of monomials of degree $d$ with $p$ 
variables. 
This problems is the same as placing $d$ balls into $p$ cells, which is 
the topic of the current section.

Consider the problem of placing $d=3$ balls into $p=3$ cells. 
The figure \ref{fig-ballsstars1} presents the 10 ways to place the balls. 

\begin{figure}
\begin{center}
\begin{tabular}{llclclcl}
   & & Cell 1& &Cell 2& &Cell 3 &\\
   \hline
 1 & |&*** &|&~-~ &|&~-~&| \\
 2 & |&~-~ &|&*** &|&~-~&| \\
 3 & |&~-~ &|&~-~ &|&***&| \\
 4 & |&**~ &|&~*~ &|&~-~&| \\
 5 & |&**~ &|&~-~ &|&~*~&| \\
 6 & |&~*~ &|&**~ &|&~-~&| \\
 7 & |&~-~ &|&**~ &|&~*~&| \\
 8 & |&~*~ &|&~-~ &|&~**&| \\
 9 & |&~-~ &|&~*~ &|&~**&| \\
10 & |&~*~ &|&~*~ &|&~*~&| \\
\hline
\end{tabular}
\end{center}
\caption{
Placing $d=3$ balls into $p=3$ cells. 
The balls are represented by stars $*$.
}
\label{fig-ballsstars1}
\end{figure}

Let us denote by $\alpha_i\geq 0$ the number of balls in the $i$-th cell, 
for $i=1,2,...,p$. 
Let $\balpha=(\alpha_1,\alpha_2,...,\alpha_p)$ be the associated vector. 
Each particular configuration is so that the total number of balls is $d$, 
i.e. we have
\begin{eqnarray*}
\alpha_1+\alpha_2+...+\alpha_p = d.
\end{eqnarray*}

\begin{example}
(\emph{Case where $d=8$ and $p=6$})
Consider the case where there are $d=8$ balls to distribute into $p=6$ cells. 
We represent the balls with stars "*" and the cells are represented 
by the spaces between the $p+1$ bars "|". 
For example, the string "|***|*|~|~|~|****|" represents the 
configuration where $\balpha=(3,1,0,0,0,4)$. 
\end{example}

\begin{proposition}
\label{prop-multisetnumber}
(\emph{Occupancy problem})
The number of ways to distribute $d$ balls into $p$ cells is:
\begin{eqnarray}
\label{eq-multisetnumber}
\left(\choosefun{p}{d}\right) = \choosefun{p+d-1}{d}.
\end{eqnarray}
\end{proposition}

The left hand side of the previous equation 
is the \emph{multiset coefficient}.

\begin{proof}
Within the string, there is a total of $p+1$ bars : the first and last bars, 
and the intermediate $p-1$ bars. 
We are free to move only the $p-1$ bars and the $d$ stars, for a total 
of $p+d-1$ symbols. 
Therefore, the problem reduces to finding the place of the $d$ stars 
in a string of $p+d-1$ symbols. 
From combinatorics, we know that the number of ways to choose $k$ items 
in a set of $n$ items is
\begin{eqnarray}
\label{eq-multisetnumber1}
\choosefun{n}{k} = \frac{n!}{k! (n-k)!},
\end{eqnarray}
for any nonnegative integers $n$ and $k$, and $k\leq n$.
Hence, the number of ways to choose the place of the $d$ stars 
in a string of $p+d-1$ symbols is defined in the equation \ref{eq-multisetnumber}. 
\end{proof}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Multivariate monomials}
\label{sec-multimonom}

The four one-dimensional monomials $1$, $x$, $x^2$ can 
be used to create a multivariate monomial, just by multiplying 
the appropriate components. 
Consider, for example, $p=3$ and let $\bx\in\RR^3$ be a point in the three-dimensional 
space. 
The function
\begin{eqnarray*}
x_2 x_3^2
\end{eqnarray*}
is the monomial associated to the vector $\balpha=(0,1,2)$, 
which entries are the exponents of $\bx=(x_1,x_2,x_3)$.

\index{Multivariate monomial}
\begin{definition}
\label{def-multimonom}
(\emph{Multivariate monomial})
Let $\bx\in\RR^p$. 
The function $M_\balpha(\bx)$ is a multivariate monomial if there is a vector 
$\balpha=(\alpha_1,\alpha_2,...,\alpha_p)$ of integers exponents, 
where $\alpha_i\in\{0,1,...,p\}$, for $i=1,2,...,p$ such that
\begin{eqnarray}
\label{eq-multimonom}
M_\balpha(\bx)=x_1^{\alpha_1} x_2^{\alpha_2} ... x_p^{\alpha_p},
\end{eqnarray}
for any $\bx\in\RR^p$.
\end{definition}

\index{multi-index}
The vector $\balpha$ is called a \emph{multi-index}.

The previous equation can be rewritten in a more general way:
\begin{eqnarray}
\label{eq-multimonom1}
M_\balpha(\bx)= \prod_{i=1}^p x_i^{\alpha_i},
\end{eqnarray}
for any $\bx\in\RR^p$.

\begin{definition}
(\emph{Degree of a multivariate monomial})
Let $M(\bx)$ be a monomial associated with the exponents 
$\balpha$, for any $\bx\in\RR^p$. 
The degree of $M$ is 
\begin{eqnarray*}
d=\alpha_1+\alpha_2+...+\alpha_p.
\end{eqnarray*}
\end{definition}

The degree $d$ of a multivariate monomial is also denoted by $|\balpha|$.

The figure \ref{fig-monod2p4} presents the 10 monomials with degree 
$d=2$ and $p=4$ variables. 

\begin{figure}
\begin{center}
\begin{tabular}{llll}
$\alpha_1$&$\alpha_2$&$\alpha_3$&$\alpha_4$ \\
\hline
2 & 0 & 0 & 0 \\
1 & 1 & 0 & 0 \\
1 & 0 & 1 & 0 \\
1 & 0 & 0 & 1 \\
0 & 2 & 0 & 0 \\
0 & 1 & 1 & 0 \\
0 & 1 & 0 & 1 \\
0 & 0 & 2 & 0 \\
0 & 0 & 1 & 1 \\
0 & 0 & 0 & 2 \\
\hline
\end{tabular}
\end{center}
\caption{The monomials with degree $d=2$ and $p=4$ variables.}
\label{fig-monod2p4}
\end{figure}

\begin{proposition}
\label{prop-multimonomnb}
(\emph{Number of multivariate monomials})
The number of degree $d$ multivariate monomials of $p$ variables is:
\begin{eqnarray}
\label{eq-numberofmonomials}
\left(\choosefun{p}{d}\right) = \choosefun{p+d-1}{d}.
\end{eqnarray}
\end{proposition}

\begin{proof}
The problem can be reduced to distributing $d$ balls into 
$p$ cells: the $i$-th cell represents the variable $x_i$ 
where we have to put $\alpha_i$ balls. 
It is then straightforward to use the proposition \ref{prop-multisetnumber}. 
\end{proof}

The figure \ref{fig-monocount} presents the number of monomials 
for various $d$ and $p$.

\begin{figure}
\begin{center}
\begin{tabular}{l|llllll}
\hline
p\textbackslash d & 0 & 1 & 2 & 3 & 4 & 5 \\
\hline
 1&1&1&1&1&1&1      \\
 2&1&2&3&4&5&6      \\
 3&1&3&6&10&15&21   \\
 4&1&4&10&20&35&56  \\
 5&1&5&15&35&70&126 \\
 \hline
\end{tabular}
\end{center}
\caption{Number of degree $d$ monomials with $p$ variables.}
\label{fig-monocount}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Multivariate polynomials}

\index{Multivariate polynomial}
\begin{definition}
\label{def-multipoly}
(\emph{Multivariate polynomial})
Let $\bx\in\RR^p$. 
The function $P(\bx)$ is a degree $d$ multivariate polynomial if there is a set of multivariate 
monomials $M_\balpha(\bx)$ and a set of real numbers $\beta_\balpha$ such that
\begin{eqnarray}
\label{eq-multipoly}
P(\bx) = \sum_{|\balpha|\leq d} \beta_\balpha M_\balpha(\bx),
\end{eqnarray}
for any $\bx\in\RR^p$.
\end{definition}

We can plug the equation \ref{eq-multimonom1} into \ref{eq-multipoly} and get
\begin{eqnarray}
\label{eq-multipoly2}
P(\bx) = \sum_{|\balpha|\leq d} \beta_\balpha \prod_{i=1}^p x_i^{\alpha_i},
\end{eqnarray}
for any $\bx\in\RR^p$.

\begin{example}
(\emph{Case where $d=2$ and $p=3$})
Consider the degree $d=2$ multivariate polynomial with $p=3$ variables:
\begin{eqnarray}
\label{eq-multipoly3}
P(x_1,x_2,x_3) = 4 + 2 x_3 + 5 x_1^2 - 3 x_1 x_2 x_3
\end{eqnarray}
for any $\bx\in\RR^3$.
The relevant multivariate monomials exponents $\balpha$ are:
\begin{eqnarray*}
(0,0,0),  \qquad (0,0,1), \\
(1,0,0),  \qquad (1,1,1).
\end{eqnarray*}
The only nonzero coefficients $\beta$ are:
\begin{eqnarray*}
\beta_{(0,0,0)} = 4, \qquad \beta_{(0,0,1)} = 2, \\
\beta_{(1,0,0)} = 5,  \qquad \beta_{(1,1,1)} = -3.
\end{eqnarray*}
\end{example}

\begin{proposition}
\label{prop-multipolymnb}
(\emph{Number of multivariate polynomials})
The number of degree $d$ multivariate polynomials of $p$ variables is:
\begin{eqnarray}
\label{eq-multipolymnb1}
P_d^p=\choosefun{p+d}{d}.
\end{eqnarray}
\end{proposition}


\begin{proof}
The proof is by recurrence on $d$. 
The only polynomial of degree $d=0$ is:
\begin{eqnarray}
\label{eq-multipolymnb2}
P_0(\bx) = 1.
\end{eqnarray}
Hence, there are:
\begin{eqnarray*}
1=\choosefun{p}{0}
\end{eqnarray*}
multivariate polynomials of degree $d=0$. 
This proves that the equation \ref{eq-multipolymnb1} is true for $d=0$. 
The polynomials of degree $d=1$ are:
\begin{eqnarray}
\label{eq-multipolymnb3}
P_i(\bx) = x_i,
\end{eqnarray}
for $i=1,2,...,p$. 
Hence, there is a total of $1+p$ polynomials of degree $d=1$, which writes
\begin{eqnarray*}
1+p
&=&\choosefun{p+1}{1}.
\end{eqnarray*}
This proves that the equation \ref{eq-multipolymnb1} is true for $d=1$. 

Now, assume that the equation \ref{eq-multipolymnb1} is true for $d$, 
and let us prove that it is true for $d+1$.
For a multivariate polynomial $P(\bx)$ of degree $d+1$, 
the equation \ref{eq-multipoly} implies 
\begin{eqnarray*}
P(\bx) 
&=& \sum_{|\balpha|\leq d+1} \beta_\balpha M_\balpha(\bx), \\
&=& \sum_{|\balpha|\leq d} \beta_\balpha M_\balpha(\bx) + \sum_{|\balpha|=d+1} \beta_\balpha M_\balpha(\bx),
\end{eqnarray*}
for any $\bx\in\RR^p$.
Hence, the number of multivariate polynomials of degree $d+1$ is the 
sum of the number of multivariate polynomials of degree $d$ 
and the number of multivariate \emph{monomials} of degree $d+1$. 
From the equation \ref{eq-numberofmonomials}, 
the number of such monomials is
\begin{eqnarray}
\label{eq-multipolymnb5}
\left(\choosefun{p}{d+1}\right) = \choosefun{p+d}{d+1}.
\end{eqnarray}
Hence, the number of multivative polynomials of degree $d+1$ is:
\begin{eqnarray}
\label{eq-multipolymnb6}
\choosefun{p+d}{d+1} + \choosefun{p+d}{d}.
\end{eqnarray}
However, we know from combinatorics that
\begin{eqnarray}
\label{eq-multipolymnb7}
\choosefun{n}{k} + \choosefun{n}{k-1} = \choosefun{n+1}{k},
\end{eqnarray}
for any nonnegative integers $n$ and $k$. 
We apply the previous equality with $n=p+d$ and $k=d+1$, 
and get
\begin{eqnarray}
\label{eq-multipolymnb8}
\choosefun{p+d}{d+1} + \choosefun{p+d}{d} = \choosefun{p+d+1}{d+1},
\end{eqnarray}
which proves that the equation \ref{eq-multipolymnb1} is 
also true for $d+1$, and concludes the proof.
\end{proof}

The figures \ref{fig-polycount1} and \ref{fig-polycount2} present the number of polynomials  
for various $d$ and $p$.

\begin{figure}
\begin{center}
\begin{tabular}{l|lllllll}
p\textbackslash d & 0 & 1 & 2 & 3 & 4 & 5 & 6 \\
\hline
 1&1&2&3&4&5&6&7    \\
 2&1&3&6&10&15&21&28    \\
 3&1&4&10&20&35&56&84   \\
 4&1&5&15&35&70&126&210    \\
 5&1&6&21&56&126&252&462    \\
 6&1&7&28&84&210&462&924   \\
\end{tabular}
\end{center}
\caption{Number of degree $d$ polynomials with $p$ variables.}
\label{fig-polycount1}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.6\textwidth]{figures/multidpoly-Cdp.pdf}
\end{center}
\caption{Number of degree $d$ polynomials with $p$ variables.}
\label{fig-polycount2}
\end{figure}

One possible issue with the equation \ref{eq-multipoly} is that 
it does not specify a way of ordering the $P_d^p$ multivariate polynomials of degree 
$d$.
However, we will present in the next section a constructive 
way of ordering the multi-indices is such a way that 
there is a one-to-one mapping from the single index $k$, 
in the range from 1 to $P_d^p$, to the corresponding multi-index $\balpha^{(k)}$. 
With this ordering, the equation \ref{eq-multipoly} becomes 
\begin{eqnarray}
\label{eq-multipoly9}
P(\bx) = \sum_{k=1}^{P_d^p} \beta_k M_{\balpha^{(k)}}(\bx),
\end{eqnarray}
for any $\bx\in\RR^p$.

\begin{example}
(\emph{Case where $d=2$ and $p=2$})
Consider the degree $d=2$ multivariate polynomial with $p=2$ variables:
\begin{eqnarray}
\label{eq-multipoly10}
P(x_1,x_2) = \beta_{(0,0)} + \beta_{(1,0)} x_1 + \beta_{(0,1)} x_2 + \beta_{(2,0)} x_1^2 + \beta_{(1,1)} x_1 x_2 + \beta_{(0,2)} x_2^2
\end{eqnarray}
for any $x_1,x_2\in\RR^2$. 
This corresponds to the equation
\begin{eqnarray}
\label{eq-multipoly11}
P(x_1,x_2) = \beta_1 + \beta_2 x_1 + \beta_3 x_2 + \beta_4 x_1^2 + \beta_5 x_1 x_2 + \beta_6 x_2^2
\end{eqnarray}
for any $x_1,x_2\in\RR^2$. 
\end{example}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Generating multi-indices}
\label{sec-genmultiindices}

In this section, we present an algorithm to generate 
all the multi-indices $\balpha$ associated with a 
degree $d$ multivariate polynomial of $p$ variables.

The following \scivar{polymultiindex} function implements the algorithm suggested 
by Lema\^itre and Knio in the appendix of \cite{LeMaitreKnio2010}. 
The function returns a matrix $a$ with $p$ columns and $P_d^p$ rows. 
Each row \scivar{a(i,:)} represents the \scivar{i}-th multivariate polynomial, 
for $i=1,2,...,P_d^p$. 
For each row \scivar{i}, the entry \scivar{a(i,j)} is the exponent 
of the $j$-th variable $x_j$, for $i=1,2,...,P_d^p$ and $j=1,2,...,p$.
The sum of exponents for each row is lower or equal to $d$. 
The first row is the zero-degree polynomial, so that all entries in 
\scivar{a(1,1:p)} are zero. 
The rows \scivar{2} to \scivar{p+1} are the first degree polynomials: 
all columns are zero, except one entry which is equal to 1.

\lstset{language=scilabscript}
\begin{lstlisting}
function a=polymultiindex(p,d)
    // Zero-th order polynomial
    a(1,1:p)=zeros(1,p)
    // First order polynomials
    a(2:p+1,1:p)=eye(p,p)
    P=p+1
    pmat=[]
    pmat(1:p,1)=1
    for k=2:d
        L=P
        for i=1:p
            pmat(i,k)=sum(pmat(i:p,k-1))
        end
        for j=1:p
            for m=L-pmat(j,k)+1:L
                P=P+1
                a(P,1:p)=a(m,1:p)
                a(P,j)=a(P,j)+1
            end
        end
    end
endfunction
\end{lstlisting}

For example, in the following session, we compute the list of multi-indices 
corresponding to multivariate polynomials of degree $d=3$ with $p=3$ variables. 
\lstset{language=scilabscript}
\begin{lstlisting}
-->p=3;
-->d=3;
-->a=polymultiindex(p,d)
 a  =
    0.    0.    0.  
    1.    0.    0.  
    0.    1.    0.  
    0.    0.    1.  
    2.    0.    0.  
    1.    1.    0.  
    1.    0.    1.  
    0.    2.    0.  
    0.    1.    1.  
    0.    0.    2.  
    3.    0.    0.  
    2.    1.    0.  
    2.    0.    1.  
    1.    2.    0.  
    1.    1.    1.  
    1.    0.    2.  
    0.    3.    0.  
    0.    2.    1.  
    0.    1.    2.  
    0.    0.    3.  
\end{lstlisting}

There are 20 rows in the previous matrix \scivar{a}, which is consistent 
with the table \ref{fig-polycount1}. 
In the previous session, the first row correspond to the zero-th degree 
polynomial, the rows 2 to 4 corresponds to the degree one monomials, 
the rows from 5 to 10 corresponds 
to the degree 2 monomials, whereas the rows from 11 to 20 corresponds to the 
degree 3 monomials.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Multivariate orthogonal functions}
\label{sec-orthomulti}

In this section, we introduce the weighted space of square integrable functions in $\RR^p$, 
so that we can define the orthogonality of multivariate functions.

\begin{definition}
(\emph{Interval of $\RR^p$})
\label{def-tensorinterval}
An interval $I$ of $\RR^p$ is a subspace of $\RR^p$, such that $\bx\in I$ if
\begin{eqnarray*}
x_1 \in I_1, \quad x_2 \in I_2, ..., \quad x_p\in I_p,
\end{eqnarray*}
where $I_1$, $I_2$,..., $I_p$ are intervals of $\RR$.
We denote this tensor product by:
\begin{eqnarray*}
I=I_1 \otimes I_2 \otimes ...\otimes I_p.
\end{eqnarray*}
\end{definition}

Now that we have multivariate intervals, we can consider a multivariate weight 
function on such an interval.

\begin{definition}
(\emph{Multivariate weight function of $\RR^p$})
\label{def-multiweight}
Let $I$ be an interval in $\RR^p$. 
A weight function $w$ on $I$ is a nonnegative integrable function of $\bx\in I$. 
\end{definition}

Such a weight can be created by making the tensor product of univariate 
weights. 

\begin{proposition}
(\emph{Tensor product of univariate weights})
\label{def-tensorweight}
Let $I_1,I_2,...,I_p$ be intervals of 
$R$. 
Assume that $w_1,w_2,...,w_p$ are univariate weights on $I_1,I_2,...,I_p$. 
Therefore, the tensor product function 
\begin{eqnarray*}
w(\bx)=w_1(x_1) w_2(x_2) ... w_p(x_p),
\end{eqnarray*}
for any $\bx\in\RR$, is a weight function of $\bx\in I$. 
\end{proposition}

\begin{proof}
Indeed, its integral is
\begin{eqnarray*}
\int_{I} w(\bx) d\bx 
&=& \int_{I} w_1(x_1) w_2(x_2) ... w_p(x_p) dx_1 dx_2 ... dx_p \\
&=& \left(\int_{I_1} w_1(x_1) dx_1\right) \left(\int_{I_2} w_2(x_2) dx_2\right) ... \left(\int_{I_p} w_p(x_p) dx_p\right).
\end{eqnarray*}
However, all the individual integrals are finite, since each function $w_i$ is, by 
hypothesis, a weight on $I_i$, for $i=1,2,...,p$. 
Hence, the product is finite, so that the function $w$ is, indeed, a weight on $I$.
\end{proof}

\begin{example}
(\emph{Multivariate Gaussian weight})
In the particular case where all weights are the same, 
the expression simplifies further. 
Consider, for example, the Gaussian weight 
\begin{eqnarray}
\label{eq-weighthermite1}
\exp(-x^2/2),
\end{eqnarray}
for $x\in\RR$.
We can then consider the tensor product:
\begin{eqnarray}
\label{eq-weighthermite2}
w(\bx)= w(x_1) w(x_2) ... w(x_p),
\end{eqnarray}
for $x_1,x_2,...,x_p\in\RR$.
\end{example}

\begin{example}
(\emph{Multivariate Hermite-Legendre weight})
Consider the weight for Hermite polynomials 
$$
w_1(x_1)=\exp(-x_1^2/2)
$$
for $x_1\in \RR$, 
and consider the weight for Legendre polynomials $w_2(x_2)=1$ for $x_2\in [-1,1]$. 
Therefore, 
\begin{eqnarray*}
w(\bx)=w_1(x_1) w_2(x_2)
\end{eqnarray*}
for $\bx\in I$ is a weight on $I=\RR\otimes [-1,1]$.
\end{example}

In the remaining of this document, we will not make a notation difference between the 
univariate weight function $w(x)$ and the multivariate weight $w(\bx)$, 
assuming that it is based on a tensor product if needed.

\begin{definition}
(\emph{Multivariate weighted $L^2$ space in $\RR^p$})
\label{def-tensorweightedl2}
Let $I$ be an interval in $\RR^p$. 
Let $w$ be a multivariate weight function on $I$.
Let $L^2_w(I)$ be the set of 
functions $g$ which are square integrable with respect to the weight 
function $w$, i.e. such that the integral 
\begin{eqnarray}
\label{eq-tensorweightedl21}
\|g\|^2 = \int_I g(\bx)^2 w(\bx) d\bx
\end{eqnarray}
is finite. 
In this case, the norm of $g$ is $\|g\|$.
\end{definition}

\begin{definition}
(\emph{Multivariate inner product in $L^2_w(I)$ space})
\label{def-multiscalprodl2w}
Let $I$ be an interval of $\RR^p$. 
Let $w$ be a multivariate weight function on $I$. 
For any $g,h\in L^2_w(I)$, the inner product of $g$ and $h$ is
\begin{eqnarray}
\label{eq-multiscalprodl2w1}
\lwdotprod{g}{h}=\int_I g(\bx) h(\bx) w(\bx) d\bx.
\end{eqnarray}
\end{definition}

Let $I$ be an interval of $\RR^p$ and assume that $g\in L^2_w(I)$.
We can combine the equations \ref{eq-tensorweightedl21} and \ref{eq-multiscalprodl2w1}, which 
implies that the $L^2_w(I)$ norm of $g$ can be expressed as an inner product:
\begin{eqnarray}
\label{eq-multiscalprodl2w2}
\|g\|^2 = \lwdotprod{g}{g}.
\end{eqnarray}

\begin{proposition}
(\emph{Multivariate probability distribution function})
\label{def-multipdf}
Let $I$ be an interval of $\RR^p$. 
Let $w$ be a multivariate weight function on $I$. 
Assume $\{X_i\}_{i=1,2,...,p}$ are independent random variables 
associated with the probability distribution functions $f_i$, derived from the 
weight functions $w_i$, for $i=1,2,...,p$.
Therefore, the function 
\begin{eqnarray}
\label{eq-multipdf1}
f(\bx)=f_1(x_1)f_2(x_2)...f_p(x_p),
\end{eqnarray}
for $\bx\in I$ is a probability distribution function.
\end{proposition}

\begin{proof}
We must prove that the integral of $f$ is equal to one. 
Indeed, 
\begin{eqnarray*}
\int_I f(\bx) d\bx
&=& \int_I f_1(x_1)f_2(x_2)...f_p(x_p) dx_1 dx_2 ... dx_p \\
&=& \left(\int_{I_1} f_1(x_1)dx_1\right) \left(\int_{I_2} f_2(x_2)dx_2\right) ... 
    \left(\int_{I_p} f_p(x_p)dx_p\right) \\
&=& 1,
\end{eqnarray*}
where each integral is equal to one since, by hypothesis, $f_i$ is a probability 
distribution function for $i=1,2,...,p$.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Tensor product of orthogonal polynomials}
\label{sec-tensorpoly}

In this section, we present a method to create multivariate 
orthogonal polynomials. 

Consider the cas where $p=2$ and let's try to create a bivariate Hermite orthogonal 
polynomials. 
Therefore, we consider the inner product:
\begin{eqnarray}
\lwdotprod{g}{h}=\int_{\RR^2} g(x_1,x_2) h(x_1,x_2) w(x_1)w(x_2) dx_1 dx_2,
\label{eq-tensorpoly0}
\end{eqnarray}
associated with the multivariate Gaussian weight $w(\bx)$.

We can make the tensor product of some Hermite polynomials, which leads, 
for example, to:
\begin{eqnarray*}
\Psi_1(x_1,x_2)&=&He_1(x_1) \\
\Psi_2(x_1,x_2)&=&He_1(x_1) He_2(x_2),
\end{eqnarray*}
associated with the multivariate Gaussian weight $w(\bx)$.
These polynomials are of degree 2. 
We may wonder if these two polynomials are orthogonal. 
We have
\begin{eqnarray*}
&&\lwdotprod{\Psi_1}{\Psi_2} \\
&&=\int_{\RR^2} \Psi_1(x_1,x_2) \Psi_2(x_1,x_2) w(x_1)w(x_2) dx_1 dx_2 \\
&&=\int_{\RR^2} He_1(x_1)^2 He_2(x_2) w(x_1)w(x_2) dx_1 dx_2
\end{eqnarray*}
which implies 
\begin{eqnarray}
&&\lwdotprod{\Psi_1}{\Psi_2} \\
&&=\left(\int_\RR He_1(x_1)^2 w(x_1) dx_1\right)\left( \int_\RR  He_2(x_2) w(x_2)dx_2 \right).
\label{eq-tensorpoly1}
\end{eqnarray}
By the orthogonality of Hermite polynomials, we have
\begin{eqnarray*}
\int_\RR  He_2(x_2) w(x_2) dx_2
&=&\int_\RR  He_0(x_2) He_2(x_2) w(x_2) dx_2\\
&=&0.
\end{eqnarray*}
Hence, 
\begin{eqnarray*}
\lwdotprod{\Psi_1}{\Psi_2} = 0,
\end{eqnarray*}
which implies that $\Psi_1$ and $\Psi_2$ are orthogonal.
However,
\begin{eqnarray*}
\|\Psi_1\|^2
&=&\int_{\RR^2} He_1(x_1)^2 w(x_1) w(x_2) dx_1 dx_2 \\
&=&\left(\int_\RR He_1(x_1)^2 w(x_1) dx_1\right)\left(\int_\RR w(x_2) dx_2 \right)\\
&=&\sqrt{2\pi} \|He_1\|^2.
\end{eqnarray*}

\begin{definition}
(\emph{Tensor product of orthogonal polynomials})
\label{def-multiorthpoly}
Let $I$ be a tensor product interval of $\RR^p$ and let $w$ the associated multivariate 
tensor product weight function. 
Let $\phi_{\alpha_i^{(k)}}(x_i)$ be a family of univariate orthogonal 
polynomials.
The associated multivariate tensor product polynomials are 
\begin{eqnarray}
\label{eq-multiorthpoly1}
\Psi_k(\bx)=\prod_{i=1}^p \phi_{\alpha_i^{(k)}}(x_i)
\end{eqnarray}
for $k=1,2,...,P_d^p$, where the degree of $\Psi_k$ is
\begin{eqnarray}
\label{eq-multiorthpoly2}
d=\sum_{i=1}^p \alpha_i^{(k)}.
\end{eqnarray}
\end{definition}

\begin{example}
(\emph{Multivariate Hermite polynomials})
The figure \ref{fig-multiorthpoly3} presents the 
multivariate Hermite polynomials of $p=2$ variables and degree $d=3$. 
The figure \ref{fig-multipoly2D} presents the multivariate Hermite polynomials, 
with degree $d=2$ and $p=2$ variables.
\end{example}


\begin{figure}
\begin{center}
\begin{tabular}{llll}
\hline
$d$ & Multi-Index & Polynomial \\
\hline
0 & $\balpha^{(1)}=[0,0]   $&$\Psi_1(\bx)=He_0(x_1)He_0(x_2)=1$    \\
1 & $\balpha^{(2)}=[1,0]   $&$\Psi_2(\bx)=He_1(x_1)He_0(x_2)=x_1$    \\
1 & $\balpha^{(3)}=[0,1]   $&$\Psi_3(\bx)=He_0(x_1)He_1(x_2)=x_2$    \\
2 & $\balpha^{(4)}=[2,0]   $&$\Psi_4(\bx)=He_2(x_1)He_0(x_2)=x_1^2-1$    \\
2 & $\balpha^{(5)}=[1,1]   $&$\Psi_5(\bx)=He_1(x_1)He_1(x_2)=x_1x_2$    \\
2 & $\balpha^{(6)}=[0,2]   $&$\Psi_6(\bx)=He_0(x_1)He_2(x_2)=x_2^2-1$    \\
3 & $\balpha^{(7)}=[3,0]   $&$\Psi_7(\bx)=He_3(x_1)He_0(x_2)=x_1^3-3x_1$    \\
3 & $\balpha^{(8)}=[2,1]   $&$\Psi_8(\bx)=He_2(x_1)He_1(x_2)=(x_1^2-1)x_2$    \\
3 & $\balpha^{(9)}=[1,2]   $&$\Psi_9(\bx)=He_1(x_1)He_2(x_2)=x_1(x_2^2-1)$    \\
3 & $\balpha^{(10)}=[0,3]$&$\Psi_{10}(\bx)=He_0(x_1)He_3(x_2)=x_2^3-3x_2$
\end{tabular}
\end{center}
\caption{
Multivariate Hermite polynomials of $p=2$ variables and degree $d=3$. 
}
\label{fig-multiorthpoly3}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.9\textwidth]{figures/multipoly2D}
\end{center}
\caption{
The multivariate Hermite polynomials, with degree $d=2$ and $p=2$ 
variables.
}
\label{fig-multipoly2D}
\end{figure}

\begin{proposition}
\label{prop-multipolymorth}
(\emph{Multivariate orthogonal polynomials})
The multivariate tensor product polynomials defined in \ref{def-multiorthpoly} 
are orthogonal.
\end{proposition}

\begin{proof}
We must prove that, for two different integers $k$ and $\ell$, we have
\begin{eqnarray*}
\lwdotprod{\Psi_k}{\Psi_{\ell}}=0.
\end{eqnarray*}
By the definition of the inner product on $L^2_w(I)$, we have
\begin{eqnarray*}
\lwdotprod{\Psi_k}{\Psi_{\ell}}
&=&\int_I \Psi_k(\bx) \Psi_{\ell}(\bx) w(\bx) d\bx
\end{eqnarray*}
By assumption, the multivariate weight function $w$ on $I$ is the tensor product of 
univariate weight functions $w_i$ on $I_i$. 
Hence, 
\begin{eqnarray*}
\lwdotprod{\Psi_k}{\Psi_{\ell}}
&=&\int_I \prod_{i=1}^p \phi_{\alpha_i^{(k)}}(x_i) \prod_{i=1}^p \phi_{\alpha_i^{(\ell)}}(x_i) \prod_{i=1}^p w_i(x_i) d\bx \\
&=&\prod_{i=1}^p \int_{I_i} \phi_{\alpha_i^{(k)}}(x_i) \phi_{\alpha_i^{(\ell)}}(x_i) w_i(x_i) dx_i.
\end{eqnarray*}
In other words, 
\begin{eqnarray}
\label{prop-multipolymorth2}
\lwdotprod{\Psi_k}{\Psi_{\ell}}
&=&\prod_{i=1}^p \lwdotprod{\phi_{\alpha_i^{(k)}}}{\phi_{\alpha_i^{(\ell)}}}.
\end{eqnarray}
However, the multi-indice ordering implies that, 
if $k\neq \ell$, then there exists an integer $i\in\{1,2,...,p\}$, such that 
\begin{eqnarray*}
\alpha_i^{(k)} \neq \alpha_i^{(\ell)}.
\end{eqnarray*}
By assumption, the $\phi_{\alpha_i^{(k)}}(x_i)$ polynomials are orthogonal. 
This implies 
\begin{eqnarray*}
\lwdotprod{\phi_{\alpha_i^{(k)}}}{\phi_{\alpha_i^{(\ell)}}} = 0,
\end{eqnarray*}
which concludes the proof.
\end{proof}

We can use the equation \ref{prop-multipolymorth2} to compute the 
$L^2_w(I)$ norm of $\Psi_k$. 
Indeed, 
\begin{eqnarray*}
\|\Psi_k\|^2
&=& \lwdotprod{\Psi_k}{\Psi_k} \\
&=& \prod_{i=1}^p \lwdotprod{\phi_{\alpha_i^{(k)}}}{\phi_{\alpha_i^{(k)}}} \\
&=& \prod_{i=1}^p \|\phi_{\alpha_i^{(k)}}\|^2.
\end{eqnarray*}
This proves the following proposition.

\begin{proposition}
\label{prop-mulorthpolnorm}
(\emph{Norm of multivariate orthogonal polynomials})
The $L^2_w(I)$ norm of the multivariate orthogonal polynomials defined in \ref{def-multiorthpoly} 
is:
\begin{eqnarray}
\label{eq-mulorthpolnorm1}
\|\Psi_k\|^2
&=& \prod_{i=1}^p \|\phi_{\alpha_i^{(k)}}\|^2.
\end{eqnarray}
\end{proposition}

\begin{example}
(\emph{Norm of the multivariate Hermite polynomials})
Consider the multivariate Hermite polynomials in the case where $p=2$. 
The figure \ref{fig-multiorthpoly3} indicates that 
\begin{eqnarray}
\label{eq-normmulherm}
\Psi_8(\bx)=He_2(x_1)He_1(x_2),
\end{eqnarray}
for $x_1,x_2\in\RR$.
Hence, the equation \ref{eq-mulorthpolnorm1} implies:
\begin{eqnarray*}
\|\Psi_8\|^2=\|He_2\|^2\|He_1\|^2.
\end{eqnarray*}
The norm of the univariate Hermite polynomial is given by the 
equation \ref{eq-hermitepolyorth}. 
Hence, 
\begin{eqnarray*}
\|\Psi_8\|^2
&=&\sqrt{2\pi} \cdot 2! \cdot \sqrt{2\pi} \cdot 1!\\
&=&4\pi.
\end{eqnarray*}
For any $p\geq 1$, we have
\begin{eqnarray*}
\|\Psi_k\|^2
&=& \prod_{i=1}^p \sqrt{2\pi} \alpha_i^{(k)}! \\
&=& (2\pi)^{2/p} \prod_{i=1}^p \alpha_i^{(k)}!.
\end{eqnarray*}
\end{example}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Multivariate orthogonal polynomials and probabilities}

In this section, we present the properties of $\Psi_k(\bX)$, 
where $\bX$ is a multivariate random variable associated with multivariate 
orthogonal polynomials $\{\Psi_k\}_{k\geq 1}$.

\begin{proposition}
(\emph{Expectation of multivariate orthogonal polynomials})
\label{def-expmop}
Let $I$ be a tensor product interval of $\RR^p$ and let $w$ the associated multivariate 
tensor product weight function. 
Let $\Psi_k$ be the family of tensor product orthogonal multivariate polynomials, 
associated with the weight $w$. 
Let $\bX$ be a multivariate random variable associated with 
the $f$ multivariate probability distribution function, 
where $\{X_i\}_{i=1,2,...,p}$ are independent random variables.
Therefore, 
\begin{eqnarray}
\label{def-expmop1}
E(\Psi_1(\bX))=1
\end{eqnarray}
and
\begin{eqnarray}
\label{def-expmop2}
E(\Psi_k(\bX))=0,
\end{eqnarray}
for $k>1$.
\end{proposition}

\begin{proof}
Indeed, 
\begin{eqnarray*}
E(\Psi_1(\bX))
&=&\int_I \Psi_1(\bx)f(\bx)d\bx \\
&=&\int_I f(\bx)d\bx \\
&=&1,
\end{eqnarray*}
since $\Psi_1(\bx)=1$, for any $\bx\in I$.
Moreover, for any $k>1$, we have
\begin{eqnarray*}
E(\Psi_k(\bX))
&=&\int_I \Psi_k(\bx)f(\bx)d\bx \\
&=&\int_I \prod_{i=1}^p \phi_{\alpha_i^{(k)}} (x_i) \prod_{i=1}^p f_i(x_i)dx_1 dx_2 ... dx_p \\
&=&\prod_{i=1}^p \int_{I_i} \phi_{\alpha_i^{(k)}} (x_i) f_i(x_i)dx_i \\
\end{eqnarray*}
which implies 
\begin{eqnarray}
E(\Psi_k(\bX))
&=&\prod_{i=1}^p E\left(\phi_{\alpha_i^{(k)}} (x_i)\right).
\label{def-expmop3}
\end{eqnarray}
However, the proposition \ref{prop-expecpoly} implies:
\begin{eqnarray*}
E\left(\phi_{\alpha_i^{(k)}}(\bX)\right)=1
\end{eqnarray*}
if $\alpha_i^{(k)}=0$ and 
\begin{eqnarray*}
E\left(\phi_{\alpha_i^{(k)}}(\bX)\right)&=& 0
\end{eqnarray*}
if $\alpha_i^{(k)}\geq 1$. 

Since $k>1$, there is at least one integer $i\in\{1,2,...,p\}$ such that $\alpha_i^{(k)}\geq 1$. 
If this was not true, this would imply that $\alpha_i^{(k)}=0$, for $i=1,2,...,p$.  
This implies that $k=1$, which contradicts the hypothesis.

Therefore, there is at least one integer $i$ such that the expectation is zero, 
so that the product in the equation \ref{def-expmop3} is also zero.
\end{proof}

\begin{proposition}
(\emph{Variance of multivariate orthogonal polynomials})
\label{def-varmop}
Under the same hypotheses as in proposition \ref{def-expmop}, we have
\begin{eqnarray}
\label{def-varmop1}
V(\Psi_1(\bX))=0
\end{eqnarray}
and
\begin{eqnarray}
\label{def-varmop2}
V(\Psi_k(\bX))=\prod_{i=1}^p E\left(\phi_{\alpha_i^{(k)}}(X_i)^2\right),
\end{eqnarray}
for $k>1$.
\end{proposition}

\begin{proof}
Obviously, the random variable $\Psi_1(\bX)=1$ has a zero variance. 
Furthermore, for $k>1$, we have 
\begin{eqnarray*}
V(\Psi_k(\bX))
&=&E\left(\left(\Psi_k(\bX)-E(\Psi_k(\bX))\right)^2\right) \\
&=&E\left(\Psi_k(\bX)^2\right),
\end{eqnarray*}
since $E(\Psi_k(\bX))=0$. 
We then use the tensor product definition of both $\Psi_k$ and $f$, 
and finally get to the equation \ref{def-varmop2}.
\end{proof}

For $k>1$, we have
\begin{eqnarray*}
E\left(\phi_{\alpha_i^{(k)}}(X_i)^2\right) 
&=& V\left(\phi_{\alpha_i^{(k)}}(X_i)\right) + E\left(\phi_{\alpha_i^{(k)}}(X_i)\right)^2.
\end{eqnarray*}
Therefore, based on the propositions \ref{prop-expecpoly} and \ref{prop-varpoly}, we 
can use the equation \ref{def-varmop2} to evaluate the variance of $\Psi_k$. 

\begin{proposition}
(\emph{Covariance of multivariate orthogonal polynomials})
\label{def-covarmop}
Under the same hypotheses as in proposition \ref{def-expmop}, 
for any two integers $k$ and $\ell$, we have
\begin{eqnarray}
\label{def-covarmop1}
Cov(\Psi_k(\bX),\Psi_\ell(\bX))=0,
\end{eqnarray}
if $k\neq \ell$.
\end{proposition}

\begin{proof}
Assume that $k>1$. 
Therefore,
\begin{eqnarray*}
Cov(\Psi_1(\bX),\Psi_k(\bX))
&=&E\left(\left(\Psi_1(\bX)-\mu_1\right)\left(\Psi_k(\bX)-\mu_k\right)\right),
\end{eqnarray*}
where $\mu_1=E(\Psi_1(\bX))$ and $\mu_k=E(\Psi_k(\bX))$.
However, we have $\Psi_1(\bX)=E(\Psi_1(\bX))=1$. 
Hence, 
\begin{eqnarray*}
Cov(\Psi_1(\bX),\Psi_k(\bX))
&=&0.
\end{eqnarray*}

The same is true if we consider
\begin{eqnarray*}
Cov(\Psi_k(\bX),\Psi_1(\bX))
&=&Cov(\Psi_1(\bX),\Psi_k(\bX)) \\
&=& 0,
\end{eqnarray*}
by the symmetry property of the covariance.

Assume now that $k$ and $\ell$ are two integers such that $k,\ell>1$. 
We have,
\begin{eqnarray*}
Cov(\Psi_k(\bX),\Psi_\ell(\bX))
&=&E\left(\left(\Psi_k(\bX)-\mu_k\right)\left(\Psi_\ell(\bX)-\mu_\ell\right)\right) \\
&=&E\left(\Psi_k(\bX)\Psi_\ell(\bX)\right),
\end{eqnarray*}
since, by the proposition \ref{def-expmop}, we have $\mu_k=\mu_\ell=0$.
Hence, 
\begin{eqnarray*}
Cov(\Psi_k(\bX),\Psi_\ell(\bX))
&=&\int_I \Psi_k(\bx)\Psi_\ell(\bx) f(\bx) d\bx \\
&=&\frac{1}{\int_I w(\bx)d\bx}\int_I \Psi_k(\bx)\Psi_\ell(\bx) w(\bx) d\bx \\
&=&\frac{1}{\int_I w(\bx)d\bx} \lwdotprod{\Psi_k}{\Psi_{\ell}}
\end{eqnarray*}
by the definition of the weight function $w$.
By the proposition \ref{prop-multipolymorth}, the polynomials $\Psi_k$ 
and $\Psi_\ell$ are orthogonal, which concludes the proof.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Notes and references}

The stars and bars proof used in section \ref{sec-occpbs} is 
presented in Feller's book \cite{Feller68}, in the section 
"Application to occupancy problems" of the chapter 2 "Elements 
of combinatorial analysis". 
The book \cite{LeMaitreKnio2010} and the thesis \cite{LeMaitre2005} 
present spectral methods, including the multivariate orthogonal polynomials 
involved in polynomial chaos. 
The figures \ref{fig-multiorthpoly3} and \ref{fig-multipoly2D} are 
presented in several papers and slides related to multivariate 
orthogonal polynomials, 
including \cite{Lucor2008}, for example.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
