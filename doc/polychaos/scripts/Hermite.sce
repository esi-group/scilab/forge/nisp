// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

///////////////////////////////////
//
// Check that E(Hn(X))=1 if n=0, 
// and E(Hn(X))=0, for n>0.
//
if (%f) then
    N=10000;
    X = distfun_normrnd(0,1,N,1);
    n=2;
    Hx=hermite_eval(X,n);
    disp(mean(Hx))
    scf();
    histplot(linspace(-1,6,20),Hx);
    xtitle("N=10000, n=2","He(X)","Frequency");
    //
    n=3;
    Hx=hermite_eval(X,n);
    scf();
    histplot(linspace(-10,10,20),Hx);
    xtitle("N=10000, n=3","He(X)","Frequency");

    //
    N=100000;
    X = distfun_normrnd(0,1,N,1);
    k=1;
    scf();
    for n=0:5
        Hx=hermite_eval(X,n);
        v=perctl(Hx,1);
        xmin=v(1);
        v=perctl(Hx,99);
        xmax=v(1);
        subplot(2,3,k);
        if (n==0) then
            histplot(20,Hx);
        else
            histplot(linspace(xmin,xmax,20),Hx);
        end
        str=msprintf("n=%d",n);
        xtitle(str,"He(X)","Frequency");
        k=k+1;
        //
        m=mean(Hx);
        v=variance(Hx);
        disp([n,m,v,factorial(n)])
    end
end

if (%f) then
    // Check that V(Hn(x))=n!
    // for n>=1
    m=100000;
    for n=0:5
        mprintf("n=%d\n",n)
        x=distfun_normrnd(0,1,m,1);
        y=hermite_eval(x,n);
        mprintf("    Empirical Variance(Hen(X))=%f\n",variance(y))
        mprintf("    Exact Variance(Hen(X))=%f\n",hermite_variance(n))
    end
end

if (%f) then
    //
    // Some Hermite polynomials
    for i=0:9
        P=hermite_poly(i);
        c=coeff(P);
        mprintf("%d&",i)
        for j=1:i+1
            if (c(j)==0) then
                mprintf("&")
            else
                mprintf("%d&",c(j))
            end
        end
        for j=i+2:9
            mprintf("&")
        end
        mprintf("\\\\\n")
    end
end
