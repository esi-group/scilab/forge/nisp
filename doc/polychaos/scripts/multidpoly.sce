// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// References
// http://en.wikipedia.org/wiki/Monomial
//
// O.P. Le Maitre and O.M. Knio. 
// Spectral methods for uncertainty quantification. 
// Springer,
// 2010.

// Requires: distfun

function m = multimonocount(p,d)
    // Returns the number of monomials of degree d with p variables.
    m=specfun_nchoosek(p+d-1,d)
endfunction

if (%f) then
    // Print the table of number of monomials
    // lines: p=1,...,5
    // columns: d=1,...,5
    for p=1:5
        d=0:5;
        m = multimonocount(p,d);
        s=[p, m];
        //disp(s)
        disp(strcat(string(s),"&"))
    end
end

function m = multipolycount(p,d)
    // Returns the number of polynomials of degree d with p variables.
    m=specfun_nchoosek(p+d,d)
endfunction

// Print the table of number of polynomials
// lines: p=1,...,6
// columns: d=1,...,6
// See p54, Tab. 4.3
for p=1:6
    d=0:6;
    m = multipolycount(p,d);
    s=[p, m];
    //disp(s)
    disp(strcat(string(s),"&"))
end

if (%f) then
    // Print the 3D graphics of number of polynomials
    // lines: p=1,...,6
    // columns: d=1,...,6
    // See p54, Fig. 4.7
    // Print the table of number of polynomials
    // lines: p=1,...,6
    // columns: d=1,...,6
    // See p54, Tab. 4.3
    m=[];
    for p=1:5
        d=(0:15)';
        m(1:16,p) = log10(multipolycount(p,d));
    end
    h=scf();
    surf(m);
    h.color_map=graycolormap(10);
    h.children.children.color_mode=0;
    xtitle("","d","p","$C_d^p$");;
    h.children.rotation_angles= [55,-137];
    for i=1:5
        h.children.z_ticks.labels(i)=string(10^(i-1))
    end
end

// Appendix C 
// Implementation of Product and Moment Formulas
// C.2.1 Multi-Index Construction
function a=polymultiindex(p,d)
    // Returns the multi-indices of the degree d multivariate polynomials 
    // with p variables.
    //
    // Description
    // Each row a(i,:) represents the i-th polynomial. 
    // The number of rows is the number of degree d polynomials 
    // with p variables.
    // For each row i, a(i,j) is the exponent of the j-th variable, 
    // for j=1,2,...,p.
    // The sum of exponents for each row is lower or equal to d:
    // sum(a(i,:))<=d.
    // The first row is the zero-degree polynomial with 
    // all exponents equal to zero.
    // The rows 2 to p+1 are the first degree polynomials: 
    // all columns are zero, except one entry equal to 1.

    // Zero-th order polynomial
    a(1,1:p)=zeros(1,p)
    // First order polynomials
    a(2:p+1,1:p)=eye(p,p)
    P=p+1
    pmat=[]
    pmat(1:p,1)=1
    for k=2:d
        L=P
        for i=1:p
            pmat(i,k)=sum(pmat(i:p,k-1))
        end
        for j=1:p
            for m=L-pmat(j,k)+1:L
                P=P+1
                a(P,1:p)=a(m,1:p)
                a(P,j)=a(P,j)+1
            end
        end
    end
endfunction

if (%f) then
    p=3;
    d=3;
    a=polymultiindex(p,d)

    p=2;
    d=3;
    a=polymultiindex(p,d)

end
