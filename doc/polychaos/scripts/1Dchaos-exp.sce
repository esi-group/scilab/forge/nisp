// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// "Polynômes de chaos", Jean-Marc Martinez
// Decompose the function 
//
// Y=g(X) 
//
// where X is a univariate random variable into:
// Y=a0*P0(x)+a1*P1(x)+...+ad*Pd(x)
//
// where Pi are orthogonal polynomials.

// Reference
// An introduction to polynomial interpolation
// Eric Gourgoulhon
// Meudon, 14-18 November 2005
function y=unipc_integralNumerator(x,myfunction,i,pctype)
    // Evaluates the numerator integral for 1D PC decomposition
    // on orthogonal polynomials:
    // returns g(x) P(x) w(x)
    // where :
    // g is the function of the random variable to decompose
    // P is the orthogonal polynomial
    // w is the weight corresponding to the orthogonal polynomial
    if (pctype=="hermite") then
        Px = hermite_eval(x,i)
        wx = hermite_weight(x)
    elseif (pctype=="legendre")
        Px = legendre_eval(x,i)
        wx = legendre_weight(x)
    elseif (pctype=="laguerre")
        Px = laguerre_eval(x,i)
        wx = laguerre_weight(x)
    else
        error(msprintf(gettext("%s: Unknown orthogonal polynomial: %s"),..
        "integralNumeratorForPC",pctype))
    end
    gx = myfunction(x)
    y = Px*gx*wx
endfunction

function y=unipc_eval(x,a,pctype)
    // Evaluates the PC decomposition at point x.
    d=size(a,"*")-1
    y=0
    for i=1:d+1
        if (pctype=="hermite") then
            Px = hermite_eval(x,i-1)
        elseif (pctype=="legendre")
            Px = legendre_eval(x,i-1)
        elseif (pctype=="laguerre")
            Px = laguerre_eval(x,i-1)
        else
            error(msprintf(gettext("%s: Unknown orthogonal polynomial: %s"),..
            "integralNumeratorForPC",pctype))
        end
        y=y+a(i)*Px
    end
endfunction

function [M,V]=unipc_stat(a,pctype)
    // Compute mean and variance of PC
    M=a(1)
    d=size(a,"*")
    pcvar=unipc_variance(1:d-1,pctype)'
    V=sum(a(2:$).^2 .*pcvar)
endfunction

function a=unipc_decompose(myg,d,xmin,xmax,pctype,atol)
    // Computes the coefficients a in the OP decomposition
    for i=1:d+1
        numerator=intg(xmin,xmax,..
        list(unipc_integralNumerator,myg,i-1,pctype),atol);
        PCNorm=unipc_norm(i-1,pctype);
        a(i)=numerator/PCNorm;
    end
endfunction


function y=unipc_norm(i,pctype)
    // Norm of the i-th univariate polynomial
    if (pctype=="hermite") then
        y = hermite_norm(i)
    elseif (pctype=="legendre")
        y = legendre_norm(i)
    elseif (pctype=="laguerre")
        y = laguerre_norm(i)
    else
        error(msprintf(gettext("%s: Unknown orthogonal polynomial: %s"),..
        "unipc_norm",pctype))
    end
endfunction

function y=unipc_variance(i,pctype)
    // Norm of the i-th univariate polynomial
    if (pctype=="hermite") then
        y = hermite_variance(i)
    elseif (pctype=="legendre")
        y = legendre_variance(i)
    elseif (pctype=="laguerre")
        y = laguerre_variance(i)
    else
        error(msprintf(gettext("%s: Unknown orthogonal polynomial: %s"),..
        "unipc_variance",pctype))
    end
endfunction

function unipc_orthocheck(xmin,xmax,imax,pctype,atol)
    // Checks orthogonality of orthogonal polynomials.
    //
    // Description
    // Considers O.P. of type pctype and checks the 
    // actual orthogonality of polynomials. 
    //
    // Computes the inner product :
    //
    // (Pi,Pj)=int_I Pi(x)Pj(x) w(x) dx
    //
    // for i,j=0,1,...,imax. 
    //
    // To do so, uses a numerical method to approximate the 
    // integral, and considers the interval [xmin,xmax] 
    // with the absolute tolerance atol.
    // 
    function y=unipc_orthocheck_f(x,i,j,pctype)
        if (pctype=="hermite") then
            Pix = hermite_eval(x,i)
            Pjx = hermite_eval(x,j)
            wx = hermite_weight(x)
        elseif (pctype=="legendre")
            Pix = legendre_eval(x,i)
            Pjx = legendre_eval(x,j)
            wx = legendre_weight(x)
        elseif (pctype=="laguerre")
            Pix = laguerre_eval(x,i)
            Pjx = laguerre_eval(x,j)
            wx = laguerre_weight(x)
        else
            error(msprintf(gettext("%s: Unknown orthogonal polynomial: %s"),..
            "unipc_orthocheck_f",pctype))
        end
        y=Pix*Pjx*wx
    endfunction
    mprintf("Polynomials: %s\n",pctype)
    for i=0:imax
        for j=0:imax
            if (j>=i) then
                v=intg(xmin,xmax,list(unipc_orthocheck_f,i,j,pctype),atol);
                mprintf("(P_%d,P_%d)=%f",i,j,v);
                if (j>i)
                    mprintf("\n")                
                end
                if (i==j) then
                    pinorm=unipc_norm(i,pctype);
                    mprintf("    Predicted |P%d|\^2=%f\n",i,pinorm);
                end
            end
        end
    end
endfunction

function unipc_decompositionprint(a)
    // Print the decomposition from the coefficients a
    mprintf("G(x)=\n")
    d=size(a,"*")
    for i=1:d
        if (i>1) then
            if (a(i)>0) then
                mprintf("+")
            end
            if (abs(a(i))>0) then
                mprintf("%f * P_%d \n",a(i),i-1)
            end
        end
    end
endfunction

if (%f) then

    /////////////////////////////////////////////////////////////////
    //
    // Decompose G into Hermite polynomials.
    // Assume that X is standard normal.
    function y=myg(x)
        //y=exp(x)
        //y=3
        //y=x
        //y=x.^4
        //y=x^xpower
        y=sin(x)
        //y=exp(-x.^2)
        //y=abs(x)
        //y=x^2*abs(sin(x))
        //y=cos(x)
    endfunction

    d=14;
    pctype="hermite";
    xmin=-3;
    xmax=3;
    atol=1.e-8;
    a=unipc_decompose(myg,d,xmin,xmax,pctype,atol)
    //
    // Print the decomposition
    unipc_decompositionprint(a)

    //
    // Plot the coefficients
    scf();
    plot((1:d+1)',a,"bo")
    plot(1:d+1,zeros(1,d+1),"r-")
    xtitle(pctype+", d="+string(d),"i","a(i)")


    x=2
    myg(x)
    unipc_eval(x,a,pctype)

    //
    // Show a degree 9 PC
    nx=100;
    x1=linspace(xmin,xmax,nx);
    y=zeros(d,nx);
    for i=1:d
        y(i,:)=unipc_eval(x1,a(1:i),pctype);
    end
    scf();
    for i=1:d
        subplot(3,3,i)
        x2=linspace(xmin,xmax,100);
        plot(x2,myg(x2),"r-")
        plot(x1,y(i,:),"b-")
        xtitle(pctype+" PC - d="+string(i),"x","");
        legend(["g(x)","PC"]);
    end
    //
    // Validate the model
    x=distfun_normrnd(0,1,100,1);
    y1=myg(x);
    y2=unipc_eval(x,a,pctype);
    scf();
    plot(y1,y2,"bo")
    plot(y1,y1,"r-")
    xtitle("PC Validation","G(x)","PC(x)")
    //
    // Evaluate the mean, the variance
    x=distfun_normrnd(0,1,10000,1);
    y=myg(x);
    disp(mean(y))
    disp(variance(y))
    [M,V]=unipc_stat(a,pctype)
end


if (%f) then

    /////////////////////////////////////////////////////////////////
    //
    // Decompose G into Laguerre polynomials.
    // Assume that X is standard exponential.
    function y=myg(x)
        //y=exp(x)
        //y=3
        //y=x
        //y=x^2
        //y=x^3
        y=sin(x)
    endfunction

    d=9;
    pctype="laguerre";
    xmin=0;
    xmax=20;
    atol=1.e-8;
    a=unipc_decompose(myg,d,xmin,xmax,pctype,atol)

    x=2
    myg(x)
    unipc_eval(x,a,pctype)

    // Show a degree 9 PC
    xmin=0;
    xmax=6;
    nx=10;
    x1=linspace(xmin,xmax,10);
    y=zeros(d,nx);
    for i=1:d
        y(i,:)=unipc_eval(x1,a(1:i),pctype);
    end
    scf();
    for i=1:d
        subplot(3,3,i)
        x2=linspace(xmin,xmax,100);
        plot(x2,myg(x2),"r-")
        plot(x1,y(i,:),"bo")
        xtitle(pctype+" PC - d="+string(i),"x","");
        legend(["g(x)","PC"]);
    end
    //
    // Validate the model
    x=distfun_exprnd(1,100,1);
    y1=myg(x);
    y2=unipc_eval(x,a,pctype);
    scf();
    plot(y1,y2,"bo")
    plot(y1,y1,"r-")
    xtitle("PC Validation","G(x)","PC(x)")
    //
    // Evaluate the mean, the variance
    x=distfun_exprnd(1,10000,1);
    y=myg(x);
    disp(mean(y))
    disp(variance(y))
    [M,V]=unipc_stat(a,pctype)
end

if (%f) then

    /////////////////////////////////////////////////////////////////
    //
    // Decompose G into Legendre polynomials.
    // Assume that X is U(-1,1).
    function y=myg(x)
        //y=exp(x)
        //y=3
        //y=x
        //y=x^2
        //y=x^3
        y=sin(4*x)
    endfunction

    d=9;
    pctype="legendre";
    xmin=-1;
    xmax=1;
    atol=1.e-8;
    a=unipc_decompose(myg,d,xmin,xmax,pctype,atol)
    //
    // Plot the coefficients
    scf();
    plot(1:d+1,abs(a),"bo")
    plot(1:d+1,0,"r-")
    xtitle(pctype+", d="+string(d),"i","abs(a(i))")

    x=2
    myg(x)
    unipc_eval(x,a,pctype)

    // Show a degree 9 PC
    xmin=-1;
    xmax=1;
    nx=10;
    x1=linspace(xmin,xmax,10);
    y=zeros(d,nx);
    for i=1:d
        y(i,:)=unipc_eval(x1,a(1:i),pctype);
    end
    scf();
    for i=1:d
        subplot(3,3,i)
        x2=linspace(xmin,xmax,100);
        plot(x2,myg(x2),"r-")
        plot(x1,y(i,:),"bo")
        xtitle(pctype+" PC - d="+string(i),"x","");
        legend(["g(x)","PC"]);
    end
    //
    // Validate the model
    x=distfun_unifrnd(-1,1,100,1);
    y1=myg(x);
    y2=unipc_eval(x,a,pctype);
    scf();
    plot(y1,y2,"bo")
    plot(y1,y1,"r-")
    xtitle("PC Validation","G(x)","PC(x)")
    //
    // Evaluate the mean, the variance
    x=distfun_unifrnd(-1,1,10000,1);
    y=myg(x);
    disp(mean(y))
    disp(variance(y))
    [M,V]=unipc_stat(a,pctype)
end
if (%f) then
    //////////////////////////////////
    // Check orthogonality

    // Check orth. for Hermite polynomials
    unipc_orthocheck(-10,10,5,"hermite",1.e-10);
    // Check orth. for Legendre polynomials
    unipc_orthocheck(-1,1,5,"legendre",1.e-10);
    // Check orth. for Laguerre polynomials
    unipc_orthocheck(0,50,5,"laguerre",1.e-10);

end

if (%f) then
    // Decompose G into Hermite polynomials.
    // Assume that X is standard normal.
    function y=mygexptwo(x)
        y=exp(-x^2)
    endfunction
    function y=mygsin(x)
        y=sin(x)
    endfunction
    function y=mygcos(x)
        y=cos(x)
    endfunction
    function y=mygexp(x)
        y=exp(x)
    endfunction
    function y=mygabs(x)
        y=abs(x)
    endfunction
    function y=mygx7(x)
        y=x^7
    endfunction

    d=10;
    pctype="hermite";
    xmin=-10;
    xmax=10;
    atol=1.e-8;
    aksin=unipc_decompose(mygsin,d,xmin,xmax,pctype,atol);
    akcos=unipc_decompose(mygcos,d,xmin,xmax,pctype,atol);
    akexp=unipc_decompose(mygexp,d,xmin,xmax,pctype,atol);
    akexptwo=unipc_decompose(mygexptwo,d,xmin,xmax,pctype,atol);
    akabs=unipc_decompose(mygabs,d,xmin,xmax,pctype,atol);
    atol=1.e-4;
    akx7=unipc_decompose(mygx7,d,xmin,xmax,pctype,atol);
    //
    // Plot the coefficients
    scf();
    //
    subplot(3,2,1)
    xtitle("g(x)=cos(x)","i","a(i)")
    plot(1:d+1,akcos,"bo")
    //
    subplot(3,2,2)
    xtitle("g(x)=sin(x)","i","a(i)")
    plot(1:d+1,aksin,"bo")
    //
    subplot(3,2,3)
    xtitle("g(x)=exp(x)","i","a(i)")
    plot(1:d+1,akexptwo,"bo")
    //
    subplot(3,2,4)
    xtitle("g(x)=exp(-x^2)","i","a(i)")
    plot(1:d+1,akexptwo,"bo")
    //
    subplot(3,2,5)
    xtitle("g(x)=|x|","i","a(i)")
    plot(1:d+1,akabs,"bo")
    //
    subplot(3,2,6)
    xtitle("g(x)=x^7","i","a(i)")
    plot(1:d+1,akx7,"bo")
end

if (%f) then

    /////////////////////////////////////////////////////////////////
    //
    // Decompose G into Laguerre polynomials.
    // Assume that X is standard exponential.
    // See convergence when degree increases.
    function y=myg(x)
        //y=exp(x)
        //y=3
        //y=x
        //y=x^2
        //y=x^3
        y=sin(x)
    endfunction

    pctype="laguerre";
    xmin=0;
    xmax=20;
    atol=1.e-8;
    nx=10;
    x=linspace(xmin,xmax,nx);
    dmax=20;
    a=unipc_decompose(myg,dmax,xmin,xmax,pctype,atol);
    //
    // Plot error in infinite norm
    abserr=zeros(1,dmax);
    for d=1:dmax
        ypc=unipc_eval(x,a(1:d),pctype);
        yg=myg(x);
        abserr(d)=max(abs(ypc-yg));
    end
    scf();
    plot(1:dmax,abserr)
    xlabel("Polynomial Degree")
    ylabel("Absolute Error")
    //
    // Plot error in L2w norm
    function y=shift(x,a,pctype)
        ypc=unipc_eval(x,a,pctype)
        yg=myg(x)
        if (pctype=="hermite") then
            w = hermite_weight(x)
        elseif (pctype=="legendre")
            w = legendre_weight(x)
        elseif (pctype=="laguerre")
            w = laguerre_weight(x)
        else
            error(msprintf(gettext("%s: Unknown orthogonal polynomial: %s"),..
            "unipc_weight",pctype))
        end
        y=(ypc-yg).^2.*w
    endfunction

    l2werr=zeros(1,dmax);
    for d=1:dmax
        mprintf("Degree %d\n",d)
        intfunc=list(shift,a(1:d),pctype);
        I=intg(xmin,xmax,intfunc);
        l2werr(d)=sqrt(I);
    end
    scf();
    plot(1:dmax,l2werr)
    xlabel("Polynomial Degree")
    ylabel("L2w Error")
    a=gca()
    a.log_flags="nln"
end
