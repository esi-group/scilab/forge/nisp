// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://en.wikipedia.org/wiki/nisp_laguerrepolynomials

if (%f) then

    // Check the L(X) for n=5
    n=5;
    m=100000;
    x=distfun_exprnd(1,m,1);
    y=laguerre_eval(x,n);
    scf();
    plot((1:m)',y,"b.");

    //Valeur bizarre
    n=5;
    x=10.875555515289306640625
    y=laguerre_eval(x,n)
    // P(X>x)
    distfun_expcdf(x,1,%f)
    //
    n=5;
    x=linspace(0,20,100);
    y=laguerre_eval(x,n);
    scf();
    plot(x,y,"b-")
    //
    n=5;
    m=1000000;
    x=distfun_exprnd(1,m,1);
    y=laguerre_eval(x,n);
    scf();
    histplot(linspace(1,40,40),y)
end
