// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// References
// http://en.wikipedia.org/wiki/Monomial
//
// O.P. Le Maitre and O.M. Knio. 
// Spectral methods for uncertainty quantification. 
// Springer,
// 2010.

// Evaluate the k-th Multivariate Hermite polynomial of 
// degree d with p variables
function y=multiHermiteEval(x,k,d,p)
    a=polymultiindex(p,d)
    a=a(k,:)
    nrows=size(x,"r")
    y=ones(nrows,1)
    for i=1:p
        He=Hermite(x(:,i),a(i))
        y=y.*He
    end
endfunction
p=2;
d=3;
k=3
x=[
1,0.5
0.5,1
]
y=multiHermiteEval(x,k,d,p)
//
function Z=multiHermiteEval2D(X1,X2,k,d,p)
    Z=zeros(n,n);
    for i=1:n
        for j=1:n
            x=[X1(i,j),X2(i,j)];
            Z(i,j)=multiHermiteEval(x,k,d,p);
        end
    end
endfunction
//
// Make a plot of the tensor product of Multivariate Hermite 
// polynomials
n=10;
x1=linspace(-3,3,n)
x2=x1;
[X1,X2]=meshgrid(x1,x2);
p=2;
d=3;
//
polytab=[
"$\Psi_1=1$"
"$\Psi_2=x_1$"
"$\Psi_3=x_2$"
"$\Psi_4=x_1^2-1$"
"$\Psi_5=x_1x_2$"
"$\Psi_6=x_2^2-1$"
];
//
h=scf();
for k=1:6
    Z=multiHermiteEval2D(X1,X2,k,d,p);
    subplot(2,3,k);
    surf(x1,x2,Z);
    xtitle(polytab(k),"$x_1$","$x_2$","");
    h.children(1).children.color_mode=0;
end
h.children(2).rotation_angles = [71,-165];

// Compute the L2 norm of the k-th tensor product of Multivariate Hermite 
// polynomials
function normHk=multiHermiteNorm(k,d,p)
    a=polymultiindex(p,d)
    a=a(k,:)
    normHk=1
    for i=1:p
        nki=HermiteNorm(a(i))
        normHk=normHk*nki
    end
endfunction
p=2;
d=3;
for k=1:8
    normHk=multiHermiteNorm(k,d,p);
    disp([k,normHk])
end
