Documentation : 
The Non Intrusive Spectral Projection (NISP) module manual.
Introduction to sensitivity analysis with NISP.

Abstract
--------

These documents are providing informations about the 
Non Intrusive Spectral Projection (NISP) module manual.
The documents are the following.

 * "Spécifications scientifiques et informatiques : Chaos Polynomial"

This document presents the scientific and functional details of the 
NISP module from the OPUS plateform. 
This corresponds to the Work Package 1 on the Chaos Polynomial in the 
NISP library and its integration in the Work Package 4.

 * ChaosPolynomialSpecificationsFonctionnelles-Martinez.pdf

This document presents two examples of sensitivity analysis based on 
chaos polynomials.
The first example is the product function and the second 
function is the Ishigami function.

 * "NISP Toolbox Manual"

This document is a brief introduction to the NISP module. 
We present the installation process of the module in binary from ATOMS or 
from the sources.
We present the configuration functions and the randvar, setrandvar and polychaos classes.

Authors
-------

 * Copyright (C) 2008-2011 - INRIA - Michael Baudin
 * Copyright (C) 2008-2011 - CEA - Jean-Marc Martinez
 * Copyright (C) 2012 - Michael Baudin

Licence
-------

This document is released under the terms of the Creative Commons Attribution-ShareAlike 
3.0 Unported License :

http://creativecommons.org/licenses/by-sa/3.0/

TODO
----
 * Add Sobol' decomposition of the product function X1 * X2.
 * Add Dirichlet decomposition of the product function X1 * X2.
 * Prove that E(Y|Xi) is the function Y=h(Xi)
   which minimizes int (Y-h(xi))pX dx
 
