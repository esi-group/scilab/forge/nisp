changelog of the NISP Toolbox Manual

"Introduction to sensitivity analysis with NISP" (not released yet)
 * Clarified the link between the expectation and the decomposition.
 * Clarified the decomposition of the variance.
 * Clarified the recursive abstract definition of gu
 * Created a new result for Cov(gu,gv) and proved that 
   V(E(Y|Xu)) is the sum of V(gv) for v in u.
 * Clarified the Ishigami decomposition.
 * Created law of total probability.
 * Fixed typos in Ishigami example.
 * Added sensitivity of a group of variables.
 * Added total sensitivity of a group of variables.
 * Added proof that STu + SNon(u)=1
 * Started generalization of Sobol' decomposition.

"Introduction to sensitivity analysis with NISP" (v0.3)
 * Separated "Nisp toolbox manual" and 
   "Introduction to sensitivity analysis with NISP"
 * "Introduction to sensitivity analysis with NISP":
   Added sections on conditional expectation and variance
 * "Introduction to sensitivity analysis with NISP":
   Clarified notations: f is the distribution function, 
   g is the model function.
 * "Introduction to sensitivity analysis with NISP":
   Organized the sections.
 * "Introduction to sensitivity analysis with NISP":
   Clarified that SRC=Corr(Y,Xi)^2 for an affine model.

NISP User Manual (v0.4 - January 2012)
 * Updated the Log-Normal distribution parameters according 
   to the changes in NISP v2.5.
 * Added a section on Log-Uniform distribution.
 * Updated the section "Uniform random number generation" 
   according to the changes in the library (Mersenne-Twister).
 * Moved internal details at the end of the report. 

NISP User Manual (v0.3 - January 2012)
 * Fixed typos in Section 7.1 Sensitivity analysis
 * Fixed overfull hbox in many places


NISP User Manual (v0.2)
 * TODO
 * Michael Baudin, January 2011

NISP User Manual (v0.1)
 * Michael Baudin, July 2009

