% Copyright (C) 2012-2015 - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Ishigami function}

In this section, we consider the model 
\begin{eqnarray}
\label{eq-ishigamimodel}
Y=g(X_1,X_2,X_3)=\sin(X_1) + a \sin^2(X_2) + b X_3^4 \sin(X_1)
\end{eqnarray}
where $X_1,X_2,X_3$ are three independent random variables uniform in $[-\pi,\pi]$.
This implies that the distribution function of the variable $X_i$, 
$f_i$, satisfies the equation 
\begin{eqnarray*}
f_i(X_i) = \frac{1}{2\pi},
\end{eqnarray*}
for $i=1,2,3$.

We are going to compute the expectation, the variance and the sensitivity 
indices of this function.
Before this, we need auxiliary results which are presented first. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Elementary integration}

We first notice that the integral of the $\sin$ function in the interval $[-\pi,\pi]$ is 
zero, since this function is symetric.
Hence, 
\begin{eqnarray*}
\int_{-\pi}^{\pi} \sin(x)dx = 0.
\end{eqnarray*}

\begin{proposition}
We have
\begin{eqnarray}
\label{eq-sinsquare}
\int_{-\pi}^{\pi} \sin^2(x)dx = \pi.
\end{eqnarray}
\end{proposition}

\begin{proof}
Indeed, if we integrate the $\sin^2(x)$ function by part, we get
\begin{eqnarray}
\int_{-\pi}^{\pi} \sin^2(x)dx 
&=& \left[-\cos(x)\sin(x)\right]_{-\pi}^{\pi} - \int_{-\pi}^{\pi} (-\cos(x))\cos(x) dx \\
&=& 0 + \int_{-\pi}^{\pi} \cos^2(x) dx.
\label{eq-sinsquare1}
\end{eqnarray}
On the other hand, the equality $\cos^2(x)+\sin^2(x)=1$ implies 
\begin{eqnarray}
\int_{-\pi}^{\pi} \sin^2(x)dx 
&=& \int_{-\pi}^{\pi} (1 - \cos^2(x) ) dx  \\
&=& 2\pi - \int_{-\pi}^{\pi} \cos^2(x) dx.
\label{eq-sinsquare2}
\end{eqnarray}
We now combine \ref{eq-sinsquare1} and \ref{eq-sinsquare2} and get
\begin{eqnarray*}
\int_{-\pi}^{\pi} \cos^2(x) dx = 2\pi - \int_{-\pi}^{\pi} \cos^2(x) dx.
\end{eqnarray*}
The previous equality implies that 
\begin{eqnarray*}
2 \int_{-\pi}^{\pi} \cos^2(x) dx = 2\pi,
\end{eqnarray*}
which leads to 
\begin{eqnarray*}
\int_{-\pi}^{\pi} \cos^2(x) dx = \pi.
\end{eqnarray*}
Finally, the previous equality, combined with \ref{eq-sinsquare1} immediately 
leads to \ref{eq-sinsquare}.
\end{proof}

\begin{proposition}
We have
\begin{eqnarray}
\label{eq-sin4}
\int_{-\pi}^{\pi} \sin^4(x)dx = \frac{3\pi}{4}.
\end{eqnarray}
\end{proposition}

\begin{proof}
Indeed, if we integrate the $\sin^4(x)$ function by part, we get
\begin{eqnarray}
\int_{-\pi}^{\pi} \sin^4(x)dx 
&=& \left[-\cos(x)\sin^3(x)\right]_{-\pi}^{\pi} - \int_{-\pi}^{\pi} (-\cos(x)) (3 \sin^2(x)\cos(x)) dx \\
&=& 0 + 3 \int_{-\pi}^{\pi} \cos^2(x) \sin^2(x) dx.
\label{eq-sin41}
\end{eqnarray}
On the other hand, the equality $\cos^2(x)+\sin^2(x)=1$ implies  
\begin{eqnarray*}
\int_{-\pi}^{\pi} \sin^4(x)dx 
&=&\int_{-\pi}^{\pi} \sin^2(x) \sin^2(x)dx \\
&=& \int_{-\pi}^{\pi} (1 - \cos^2(x) )\sin^2(x) dx  \\
&=& \int_{-\pi}^{\pi} \sin^2(x) dx  - \int_{-\pi}^{\pi} \cos^2(x) \sin^2(x) dx.
\end{eqnarray*}
We plug the equality \ref{eq-sinsquare} into the previous equation and get
\begin{eqnarray}
\int_{-\pi}^{\pi} \sin^4(x)dx 
&=& \pi - \int_{-\pi}^{\pi} \cos^2(x) \sin^2(x) dx.
\label{eq-sin42}
\end{eqnarray}
We combine \ref{eq-sin41} and \ref{eq-sin42} and get
\begin{eqnarray*}
3 \int_{-\pi}^{\pi} \cos^2(x) \sin^2(x) dx = \pi - \int_{-\pi}^{\pi} \cos^2(x) \sin^2(x) dx.
\end{eqnarray*}
The previous equation leads to 
\begin{eqnarray*}
4 \int_{-\pi}^{\pi} \cos^2(x) \sin^2(x) dx = \pi,
\end{eqnarray*}
which implies 
\begin{eqnarray}
\int_{-\pi}^{\pi} \cos^2(x) \sin^2(x) dx = \frac{\pi}{4},
\label{eq-sin43}
\end{eqnarray}
We finally plug the equation \ref{eq-sin43} into \ref{eq-sin41} and 
get the equation \ref{eq-sin4}.
\end{proof}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Expectation}
\label{section-ishigamiexp}

By assumption, the three random variables $X_1$, $X_2$ and $X_3$ are independent, so that the 
joint distribution function is the product of the three distribution functions 
$f_i$, i.e.
\begin{eqnarray*}
f_{1,2,3}(x_1,x_2,x_3) = f_1(x_1) f_2(x_2) f_3(x_3).
\end{eqnarray*}

\begin{proposition}
The expectation of the output $Y$ is 
\begin{eqnarray}
\label{eq-ishigamiexp}
E(Y) 
&=& \frac{a}{2}.
\end{eqnarray}
\end{proposition}

\begin{proof}
By definition, the expectation of the random variable $\sin(X_1)$ is 
\begin{eqnarray*}
E(\sin(X_1)) 
&=& \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \sin(x_1) f_{1,2,3} (x_1,x_2,x_3) dx_1 dx_2 dx_3 \\
&=& \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \sin(x_1) f_1(x_1) f_2(x_2) f_3(x_3) dx_1 dx_2 dx_3 \\
&=& \frac{1}{2\pi} \int_{-\pi}^{\pi} \sin(x_1) dx_1,
\end{eqnarray*}
which implies
\begin{eqnarray}
E(\sin(X_1)) &=& 0.
\label{eq-ishigami-expsinx1}
\end{eqnarray}

By definition, the expectation of the random variable $\sin^2(X_2)$ is 
\begin{eqnarray*}
E(\sin^2(X_2)) 
&=& \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \sin^2(x_2) f_{1,2,3} (x_1,x_2,x_3) dx_1 dx_2 dx_3 \\
&=& \int_{-\pi}^{\pi} \sin^2(x_2) f_2(x_2) dx_2 \\
&=& \frac{1}{2\pi} \int_{-\pi}^{\pi} \sin^2(x_2) dx_2.
\end{eqnarray*}
The equality \ref{eq-sinsquare} then implies 
\begin{eqnarray}
E(\sin^2(X_2)) 
&=& \frac{1}{2\pi} \cdot \pi \nonumber \\
&=& \frac{1}{2}.
\label{eq-ishigami-expsin2x2}
\end{eqnarray}

By definition, the expectation of the random variable $X_3^4$ is 
\begin{eqnarray*}
E(X_3^4) 
&=& \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} x_3^4 f_{1,2,3} (x_1,x_2,x_3) dx_1 dx_2 dx_3 \\
&=& \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} x_3^4 f_1(x_1) f_2(x_2) f_3(x_3) dx_1 dx_2 dx_3 \\
&=& \int_{-\pi}^{\pi} x_3^4 f_3(x_3) dx_3 \\
&=& \frac{1}{2\pi} \int_{-\pi}^{\pi} x_3^4  dx_3, \\
&=& \frac{1}{2\pi} \left[ \frac{1}{5} x_3^5 \right]_{-\pi}^{\pi} \\
&=& \frac{1}{2\pi} \left( \frac{1}{5} \pi^5 - \frac{1}{5} (-\pi)^5 \right) \\
&=& \frac{1}{2\pi} \frac{2}{5} \pi^5,
\end{eqnarray*}
which finally implies :
\begin{eqnarray}
E(X_3^4) &=& \frac{1}{5} \pi^4.
\label{eq-ishigami-expx34}
\end{eqnarray}

We are now going to use the expectations \ref{eq-ishigami-expsinx1}, 
\ref{eq-ishigami-expsin2x2} and \ref{eq-ishigami-expx34} in order to 
compute the expectation of the output $Y$. 
The model \ref{eq-ishigamimodel} is a sum of functions. 
Since the expectation of a sum of random variables is the sum of the 
expectations (be the variables independent or not), we have 
\begin{eqnarray*}
E(Y) 
&=& E(\sin(X_1)) + E(a \sin^2(X_1)) + E(bX_3^4 \sin(X_1)) \\
&=& E(\sin(X_1)) + a E(\sin^2(X_1)) + b E(X_3^4 \sin(X_1)).
\end{eqnarray*}

The expectation of the variable $X_3^4 \sin(X_1)$ is 
\begin{eqnarray*}
E(X_3^4 \sin(X_1)) 
&=& \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} ( x_3^4 \sin(x_1) ) f_{1,2,3} (x_1,x_2,x_3) dx_1 dx_2 dx_3 \\
&=& \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} \int_{-\pi}^{\pi} ( x_3^4 \sin(x_1) ) f_1(x_1) f_2(x_2) f_3(x_3) dx_1 dx_2 dx_3 \\
&=& E(X_3^4) E(\sin(X_1)).
\end{eqnarray*}

Hence, the expectation of $Y$ is 
\begin{eqnarray*}
E(Y) 
&=& E(\sin(X_1)) + a E(\sin^2(X_1)) + b E(X_3^4) E(\sin(X_1)).
\end{eqnarray*}
We now combine the equations \ref{eq-ishigami-expsinx1}, 
\ref{eq-ishigami-expsin2x2} and \ref{eq-ishigami-expx34} and get
\begin{eqnarray*}
E(Y) 
&=& 0 + a \frac{1}{2} + b \frac{1}{5} \pi^4 \cdot 0,
\end{eqnarray*}
which leads to the equation \ref{eq-ishigamiexp}.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Variance}

\begin{proposition}
The variance of the output $Y$ is 
\begin{eqnarray}
V(Y) 
&=& \frac{1}{2} +  \frac{a^2}{8} +  \frac{b^2 \pi^8}{18} +  \frac{b\pi^4}{5}
\label{eq-ishigami-variance}
\end{eqnarray}
\end{proposition}

\begin{proof}
The variance of the output $Y$ is
\begin{eqnarray*}
V(Y) &=& E(Y^2) - E(Y)^2 \\
&=& E\left( \left(\sin(X_1) + a \sin^2(X_2) + b X_3^4 \sin(X_1)\right)^2\right) - E(Y)^2 \\
&=& E\left( \sin^2(X_1) + a^2 \sin^4(X_2) + b^2 X_3^8 \sin^2(X_1) + \right.\\
  && \left. 2\sin(X_1) a \sin^2(X_2) +  2 \sin^2(X_1) b X_3^4 + \right.\\
  && \left. 2 a \sin^2(X_2) b X_3^4 \sin(X_1) \right) - E(Y)^2 \\
&=& E( \sin^2(X_1) ) + a^2 E(\sin^4(X_2)) + b^2 E(X_3^8) E(\sin^2(X_1)) + \\
  && 2aE(\sin(X_1)) E(\sin^2(X_2)) + 2b E(\sin^2(X_1)) E(X_3^4) + \\
  &&  2 ab E(\sin^2(X_2)) E(X_3^4) E(\sin(X_1)) - E(Y)^2.
\end{eqnarray*}
By the equality \ref{eq-ishigami-expsinx1}, the expectation of $\sin(X_1)$ is zero in the interval $[-\pi,\pi]$.
Therefore, the terms associated with $E(\sin(X_1))$ can be simplified in the previous 
equality. 
This leads to 
\begin{eqnarray}
V(Y) 
&=& E(\sin^2(X_1)) + a^2 E(\sin^4(X_2)) + b^2 E(X_3^8)E(\sin^2(X_1)) \nonumber \\
  && + 2bE(X_3^4)E(\sin^2(X_1)) - E(Y)^2
\label{eq-ishigami-variance2}
\end{eqnarray}
We now compute the terms appearing in the previous equality. 
Actually, we do not have much to compute, since the equalities 
\ref{eq-ishigami-expsin2x2} and \ref{eq-ishigami-expx34} are already 
available. 
Indeed, the equality \ref{eq-ishigami-expsin2x2} immediately leads to 
\begin{eqnarray}
E(\sin^2(X_1)) 
&=& E(\sin^2(X_2)) \nonumber \\
&=& \frac{1}{2}.
\label{eq-ishigami-expsin2x1}
\end{eqnarray}
What remains to compute is $E(\sin^4(X_2))$ and $E(X_3^8)$.

By definition, the expectation of the random variable $X_3^8$ is 
\begin{eqnarray*}
E(X_3^8) 
&=& \frac{1}{2\pi} \int_{-\pi}^{\pi} x_3^8  dx_3, \\
&=& \frac{1}{2\pi} \left[ \frac{1}{9} x_3^9 \right]_{-\pi}^{\pi} \\
&=& \frac{1}{2\pi} \left( \frac{1}{9} \pi^9 - \frac{1}{9} (-\pi)^9 \right) \\
&=& \frac{1}{2\pi} \frac{2}{9} \pi^9,
\end{eqnarray*}
which implies 
\begin{eqnarray}
E(X_3^8) 
&=& \frac{1}{9} \pi^8.
\label{eq-ishigami-expx38}
\end{eqnarray}

On the other hand, the expectation of the random variable $\sin^4(X_2)$ is 
\begin{eqnarray*}
E(\sin^4(X_2)) 
&=& \frac{1}{2\pi} \int_{-\pi}^{\pi} \sin^4(x_2) dx_2.
\end{eqnarray*}
The equality \ref{eq-sin4} then implies that 
\begin{eqnarray}
E(\sin^4(X_2)) 
&=& \frac{1}{2\pi} \cdot \frac{3\pi}{4} \nonumber \\
&=& \frac{3}{8}.
\label{eq-ishigami-expsin4x2}
\end{eqnarray}

We now plug the equalities \ref{eq-ishigami-expsin2x1}, \ref{eq-ishigami-expx34}, 
\ref{eq-ishigami-expx38} and \ref{eq-ishigami-expsin4x2} into \ref{eq-ishigami-variance2}, 
and get
\begin{eqnarray*}
V(Y) 
&=& \frac{1}{2} + a^2 \frac{3}{8} + b^2 \frac{1}{9} \pi^8 \frac{1}{2} 
  + 2b \frac{1}{5} \pi^4 \frac{1}{2} - \left( \frac{a}{2} \right)^2 \\
&=& \frac{1}{2} +  \frac{3a^2}{8} +  \frac{b^2 \pi^8}{18} +  \frac{b\pi^4}{5}   - \frac{a^2}{4},
\end{eqnarray*}
which leads to the equation \ref{eq-ishigami-variance} and concludes the proof.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sobol' decomposition}

In this section, we perform the Sobol' decompotion of the function $g$.

\begin{proposition}
(\emph{Sobol' decomposition of the Ishigami function})
The Sobol' decomposition functions are
\begin{eqnarray*}
h_0      &=& \frac{a}{2} \\
h_1(x_1) &=& \sin(x_1) \left(1 + b\frac{\pi^4}{5} \right) \\
h_2(x_2) &=& - \frac{a}{2} + a \sin^2(x_2) \\
h_3(x_3) &=&0 \\
h_{1,2}(x_1,x_2) &=&0 \\
h_{1,3}(x_1,x_3) &=&  b \sin(x_1) \left( x_3^4 - \frac{\pi^4}{5} \right) \\
h_{2,3}(x_2,x_3) &=& 0\\
h_{1,2,3}(x_1,x_2,x_3) &=&0.
\end{eqnarray*}
The Sobol' decomposition variances are
\begin{eqnarray*}
V_1     &=& \frac{1}{2} \left(1 + b\frac{\pi^4}{5} \right)^2 \\
V_2     &=& \frac{a^2}{8} \\
V_3     &=& 0 \\
V_{1,2} &=& 0 \\
V_{1,3} &=& b^2 \pi^8 \frac{8}{225} \\
V_{2,3} &=& 0 \\
V_{1,2,3} &=& 0.
\end{eqnarray*}
\end{proposition}

\begin{proof}
We have
\begin{eqnarray*}
h_0 
&=& E(Y) \\
&=&\frac{a}{2}.
\end{eqnarray*}

We are first interested in the first order decomposition functions $h_1$, 
$h_2$ and $h_3$ and their associated variances $V_1$, $V_2$ and $V_3$.

We have 
\begin{eqnarray*}
h_1(x_1) 
&=& - h_0 + \int_0^1 \int_0^1 g(x)dx_{\compset{1}} \\
&=& - \frac{a}{2} + \int_0^1 \int_0^1 g(x) dx_2 dx_3 \\
&=& - \frac{a}{2} + \int_0^1 \int_0^1 \left(\sin(x_1) + a \sin^2(x_2) + b x_3^4 \sin(x_1) \right) dx_2 dx_3\\
\end{eqnarray*}
The previous equation implies
\begin{eqnarray*}
h_1(x_1) 
&=& - \frac{a}{2} + \sin(x_1)  + a E(\sin^2(X_2)) + b E(X_3^4)\sin(x_1)\\
&=& - \frac{a}{2} + \sin(x_1)  + a \frac{1}{2} + bE(X_3^4)\sin(x_1)\\
&=& - \frac{a}{2} + \sin(x_1)  + a \frac{1}{2} + b\frac{\pi^4}{5} \sin(x_1)\\
&=& \sin(x_1) \left(1 + b\frac{\pi^4}{5} \right).
\end{eqnarray*}

The variance of $h_1$ is
\begin{eqnarray*}
V_1 
&=& V(h_1(X_1)) \\
&=& E(h_1(X_1)^2) - E(h_1(X_1))^2.
\end{eqnarray*}
We have
\begin{eqnarray*}
V_1 
&=& E(h_1(X_1)^2) \\
&=& E\left( \left(\sin(X_1) \left(1 + b\frac{\pi^4}{5} \right) \right)^2\right) \\
&=& E(\sin^2(X_1)) \left(1 + b\frac{\pi^4}{5} \right)\\
&=& \frac{1}{2} \left(1 + b\frac{\pi^4}{5} \right)^2.
\end{eqnarray*}

Similarily, we have 
\begin{eqnarray*}
h_2(x_2) 
&=& - h_0 + \int_0^1 \int_0^1 g(x)dx_{\compset{2}} \\
&=& - \frac{a}{2} + \int_0^1 \int_0^1 g(x) dx_1 dx_3 \\
&=& - \frac{a}{2} + E(\sin(X_1)) + a \sin^2(x_2) + b E(X_3^4)E(\sin(X_1)) \\
&=& - \frac{a}{2} + a \sin^2(x_2).
\end{eqnarray*}

Hence,
\begin{eqnarray*}
V_2 
&=& E(h_2(X_2)^2) \\
&=& E((- \frac{a}{2} + a \sin^2(X_2))^2) \\
&=& E(\frac{a^2}{4} + a^2 \sin^4(X_2) - a^2 \sin^2(X_2)) \\
&=& \frac{a^2}{4} + a^2 E( \sin^4(X_2) ) - a^2 E(\sin^2(X_2)) \\
&=& \frac{a^2}{4} + \frac{3}{8} a^2 -  \frac{a^2}{2} \\
&=& \frac{a^2}{8}.
\end{eqnarray*}

Similarily, we have 
\begin{eqnarray*}
h_3(x_3) 
&=& - h_0 + \int_0^1 \int_0^1 g(x)dx_{\compset{3}} \\
&=& - \frac{a}{2} + \int_0^1 \int_0^1 g(x) dx_1 dx_2 \\
&=& - \frac{a}{2} + E(\sin(X_1)) + a E(\sin^2(X_2)) + b x_3^4 E(\sin(X_1)) \\
&=& - \frac{a}{2} + \frac{a}{2} \\
&=&0.
\end{eqnarray*}

Hence, 
\begin{eqnarray*}
V_3 
&=& E(h_3(X_3)^2) \\
&=& 0.
\end{eqnarray*}

We can immediately conclude that $S_3$ is zero. 
The fact that $S_3$ is zero illustrates the fact that conditional variances 
may be difficult to analyze. 
Indeed, the equation $S_3=0$ implies that the fraction of the variance that 
can be explained by the effect of $X_3$ alone is zero. 
It does not imply that the variable $X_3$ has no effect: $X_3$ as an effect, 
when we consider its interaction with $X_1$, because of the $b\sin(X_1)$ term.

We are now interested in the second order decomposition functions $h_{1,2}$, 
$h_{1,3}$ and $h_{2,3}$.

We have 
\begin{eqnarray*}
h_{1,2}(x_1,x_2) 
&=& - h_0 - h_1(x_1) - h_2(x_2) + \int_0^1 g(x)dx_{\compset{\{1,2\}}} \\
&=& - h_0 - h_1(x_1) - h_2(x_2) + \int_0^1  g(x) dx_3 \\
\end{eqnarray*}
Hence, 
\begin{eqnarray*}
h_{1,2}(x_1,x_2) 
&=& - \frac{a}{2} - \sin(x_1) \left(1 + b\frac{\pi^4}{5} \right) + \frac{a}{2} - a \sin^2(x_2) + \\
   && \sin(x_1) + a\sin^2(x_2) + bE(X_3^4)\sin(x_1) \\
&=&  - \sin(x_1) \left(1 + b\frac{\pi^4}{5} \right) - a \sin^2(x_2) + \\
   && \sin(x_1) + a\sin^2(x_2) + b \frac{\pi^4}{5} \sin(x_1) \\
&=&0.
\end{eqnarray*}
Therefore, 
\begin{eqnarray*}
V_{1,2} 
&=& V(h_{1,2}(X_1,X_2)) \\
&=& 0.
\end{eqnarray*}

We can immediately conclude that $S_{1,2}$ is zero. 
This can be predicted from the equation \ref{eq-ishigamimodel}, since there 
is no interaction between the variables $X_1$ and $X_2$.

We have 
\begin{eqnarray*}
h_{1,3}(x_1,x_3) 
&=& - h_0 - h_1(x_1) - h_3(x_3) + \int_0^1 g(x) dx_{\compset{\{1,3\}}} \\
&=& - h_0 - h_1(x_1) - h_3(x_3) + \int_0^1 g(x) dx_2 \\
&=& - \frac{a}{2} - \sin(x_1) \left(1 + b\frac{\pi^4}{5} \right) + \\
&& \sin(x_1) + aE(\sin^2(X_2)) + b x_3^4 \sin(x_1) \\
&=& - \frac{a}{2} - \sin(x_1) \left(1 + b\frac{\pi^4}{5} \right) + 
 \sin(x_1) + \frac{a}{2} + b x_3^4 \sin(x_1) \\
&=&  - \sin(x_1) b\frac{\pi^4}{5}  + b x_3^4 \sin(x_1) \\
&=&  b \sin(x_1) \left( x_3^4 - \frac{\pi^4}{5} \right).
\end{eqnarray*}

Hence, 
\begin{eqnarray*}
V_{1,3} 
&=& E(h_{1,3}(X_1,X_3)^2) \\
&=& b^2 E(\sin^2(X_1)) E\left(\left( X_3^4 - \frac{\pi^4}{5} \right)^2\right)
\end{eqnarray*}
The previous equation implies
\begin{eqnarray*}
V_{1,3} 
&=& \frac{b^2}{2} E\left( X_3^8 + \frac{\pi^8}{25} - 2 X_3^4 \frac{\pi^4}{5}\right)\\
&=& \frac{b^2}{2} \left( E(X_3^8) + \frac{\pi^8}{25} - 2 E(X_3^4) \frac{\pi^4}{5}\right)\\
&=& \frac{b^2}{2} \left( \frac{\pi^8}{9} + \frac{\pi^8}{25} - 2 \frac{\pi^4}{5} \frac{\pi^4}{5}\right)\\
&=& \frac{b^2}{2} \left( \frac{\pi^8}{9} + \frac{\pi^8}{25} - 2 \frac{\pi^8}{25}\right)\\
&=& \frac{b^2}{2} \left( \frac{\pi^8}{9} - \frac{\pi^8}{25}\right) \\
&=& \frac{b^2 \pi^8}{2} \left( \frac{1}{9} - \frac{1}{25}\right) \\
&=& \frac{b^2 \pi^8}{2} \frac{16}{225} \\
&=& b^2 \pi^8 \frac{8}{225}
\end{eqnarray*}

We have 
\begin{eqnarray*}
h_{2,3}(x_2,x_3) 
&=& - h_0 - h_2(x_2) - h_3(x_3) + \int_0^1 g(x) dx_{\compset{\{2,3\}}} \\
&=& - h_0 - h_2(x_2) - h_3(x_3) + \int_0^1 g(x) dx_1 \\
&=& - \frac{a}{2} - \left(- \frac{a}{2} + a \sin^2(x_2)\right) - 0 + \\
 && E(\sin(X_1)) + a\sin^2(x_2) + b x_3^4 E(\sin(X_1)) \\
&=& 0.
\end{eqnarray*}
Hence,
\begin{eqnarray*}
V_{2,3} &=& 0.
\end{eqnarray*}


Finally, we can compute the function $h_{1,2,3}$ and the variance $V_{1,2,3}$.
We have 
\begin{eqnarray*}
h_{1,2,3}(x_1,x_2,x_3) 
&=& - h_0 - h_1(x_1) - h_2(x_2) - h_3(x_3) - h_{1,2}(x_1,x_2)  \\
&&  - h_{1,3}(x_1,x_3) - h_{2,3}(x_2,x_3) + g(x_1,x_2,x_3) \\
&=&0.
\end{eqnarray*}
Hence 
\begin{eqnarray*}
V_{1,2,3} = 0,
\end{eqnarray*}
which concludes the proof.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Summary of the results}

In this section, we present a summary of the results 
for the Ishigami function
\begin{eqnarray}
\label{eq-ishigamimodel1}
Y=g(X_1,X_2,X_3)=\sin(X_1) + a \sin^2(X_2) + b X_3^4 \sin(X_1)
\end{eqnarray}
where $X_1,X_2,X_3$ are three random variables uniform in $[-\pi,\pi]$.
The expectation and the variance of $Y$ are 
\begin{eqnarray*}
E(Y)  &=& \frac{a}{2} \\
V(Y) &=& \frac{1}{2} +  \frac{a^2}{8} +  \frac{b^2 \pi^8}{18} +  \frac{b\pi^4}{5}
\end{eqnarray*}
The Sobol' decomposition functions are
\begin{eqnarray*}
h_0      &=& \frac{a}{2} \\
h_1(x_1) &=& \sin(x_1) \left(1 + b\frac{\pi^4}{5} \right) \\
h_2(x_2) &=& - \frac{a}{2} + a \sin^2(x_2) \\
h_3(x_3) &=&0 \\
h_{1,2}(x_1,x_2) &=&0 \\
h_{1,3}(x_1,x_3) &=&  b \sin(x_1) \left( x_3^4 - \frac{\pi^4}{5} \right) \\
h_{2,3}(x_2,x_3) &=& 0\\
h_{1,2,3}(x_1,x_2,x_3) &=&0.
\end{eqnarray*}
The Sobol' decomposition variances are
\begin{eqnarray*}
V_1     &=& \frac{1}{2} \left(1 + b\frac{\pi^4}{5} \right)^2 \\
V_2     &=& \frac{a^2}{8} \\
V_3     &=& 0 \\
V_{1,2} &=& 0 \\
V_{1,3} &=& b^2 \pi^8 \frac{8}{225} \\
V_{2,3} &=& 0 \\
V_{1,2,3} &=& 0.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Numerical results}

In this section, we present numerical results associated 
with the Ishigami function.

We begin by defining the Ishigami function, which takes the 
n-by-2 matrix of doubles \scivar{x} as input argument and 
returns the n-by-1 matrix of doubles \scivar{y}. 
Here, each row of \scivar{x} represents an experiment and each column 
represents a variable. 
\lstset{language=scilabscript}
\begin{lstlisting}
function y = ishigami (x)
  a=7.
  b=0.1
  s1=sin(x(:,1))
  s2=sin(x(:,2))
  x34 = x(:,3).^4
  y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
\end{lstlisting}

In the following script, we perform a Monte-Carlo 
sampling and compute the output of the Ishigami function. 
Then we plot the output $Y$ against its input 
arguments $X_1$, $X_2$ and $X_3$.
\lstset{language=scilabscript}
\begin{lstlisting}
np = 1000;
A = distfun_unifrnd(-%pi,%pi,np,3);
y = ishigami (A);
scf();
subplot(1,3,1);
plot(A(:,1),y,"bo")
xtitle("","X1","g(X1,X2,X3)");
subplot(1,3,2);
plot(A(:,2),y,"bo")
xtitle("","X2","g(X1,X2,X3)");
subplot(1,3,3);
plot(A(:,3),y,"bo")
xtitle("","X3","g(X1,X2,X3)");
// Put the marks as transparent
h.children(1).children.children.mark_background = 0;
h.children(2).children.children.mark_background = 0;
h.children(3).children.children.mark_background = 0;
\end{lstlisting}

The previous script produces the figure \ref{fig-ishigami-outputscatter}.
\begin{figure}[htbp]
\begin{center}
\includegraphics[width=\textwidth]{ishigami_outputscatter.png}
\end{center}
\caption{Output of the Ishigami function with respect to each input argument.}
\label{fig-ishigami-outputscatter}
\end{figure}

The following function returns the exact expectation, 
variance and sensitivity indices of the Ighigami function.
\lstset{language=scilabscript}
\begin{lstlisting}
function exact = ishigami_saexact ( a , b )
    // Exact results for the Ishigami function
    exact.expectation = a/2;
    exact.var = 1/2 + a^2/8 + b*%pi^4/5 + b^2*%pi^8/18;
    // Sensitivity indices.
    exact.S1 = (1/2 + b*%pi^4/5+b^2*%pi^8/50)/exact.var;
    exact.S2 = (a^2/8)/exact.var;
    exact.S3 = 0;
    exact.S12 = 0;
    exact.S23 = 0;
    exact.S13 = b^2*%pi^8*(8/225)/exact.var;
    exact.S123 = 0;
    exact.ST1 = exact.S1 + exact.S13;
    exact.ST2 = exact.S2;
    exact.ST3 = exact.S13;
endfunction
\end{lstlisting}

The following script allows to get the results associated 
with the parameters $a=7$ and $b=0.1$.
\lstset{language=scilabscript}
\begin{lstlisting}
a=7.;
b=0.1;
exact = ishigami_saexact ( a , b )
\end{lstlisting}
The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->exact = ishigami_saexact ( a , b )
 exact  =
   expectation: 3.5
   var: 13.844588
   S1: 0.3139052
   S2: 0.4424111
   S3: 0
   S12: 0
   S23: 0
   S13: 0.2436837
   S123: 0
   ST1: 0.5575889
   ST2: 0.4424111
   ST3: 0.2436837
\end{lstlisting}
