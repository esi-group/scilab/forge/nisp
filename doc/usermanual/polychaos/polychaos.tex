% Copyright (C) 2008-2010 - INRIA - Michael Baudin
% Copyright (C) 2008-2011 - CEA - Jean-Marc Martinez
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

\section{The \scifun{polychaos} class}

\subsection{Introduction}

The \scifun{polychaos} class allows to manage a polynomial chaos 
expansion. The coefficients of the expansion are computed based on given 
numerical experiments which creates the association between the inputs 
and the outputs. Once computed, the expansion can be used as a regular 
function. The mean, standard deviation or quantile can also be directly 
retrieved.

The tool allows to get the following results:
\begin{itemize}
\item mean,
\item variance,
\item quantile,
\item correlation, etc...
\end{itemize}
Moreover, we can generate the C source code which computes the 
output of the polynomial chaos expansion. This C source code is stand-alone,
that is, it is independent of both the NISP library and Scilab. It can be used 
as a meta-model.

The figure \ref{fig-polychaos-methodsoutline} presents the most 
commonly used methods available in the \scifun{polychaos} class.
More methods are presented in figure \ref{fig-polychaos-methodsoutline2}.
The inline help contains the detailed calling sequence for each 
function and will not be repeated here. More than 50 methods are 
available and most of them will not be presented here.

\begin{figure}[htbp]
\begin{center}
\begin{tabular}{|l|}
\hline
\textbf{Constructors}\\
 \scifun{pc = polychaos\_new ( file )} \\
 \scifun{pc = polychaos\_new ( srv , ny )} \\
 \scifun{pc = polychaos\_new ( pc , nopt , varopt )} \\
\hline
\textbf{Methods}\\
 \scifun{polychaos\_setsizetarget ( pc , np )} \\
 \scifun{polychaos\_settarget ( pc , output ) } \\
 \scifun{polychaos\_setinput ( pc , invalue )} \\
 \scifun{polychaos\_setdimoutput ( pc , ny )} \\
 \scifun{polychaos\_setdegree ( pc , no )} \\
 \scifun{polychaos\_getvariance ( pc )} \\
 \scifun{polychaos\_getmean ( pc )} \\
\hline
\textbf{Destructor}\\
 \scifun{polychaos\_destroy (pc) } \\
\hline
\textbf{Static methods}\\
 \scifun{tokenmatrix = polychaos\_tokens ()} \\
 \scifun{nb = polychaos\_size () } \\
\hline
\end{tabular}
\end{center}
\caption{Outline of the methods of the \scifun{polychaos} class}
\label{fig-polychaos-methodsoutline}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\begin{tabular}{|l|}
\hline
\textbf{Methods}\\
 \scifun{output = polychaos\_gettarget ( pc )} \\
 \scifun{np = polychaos\_getsizetarget ( pc )} \\
 \scifun{polychaos\_getsample ( pc , k , ovar )} \\
 \scifun{polychaos\_getquantile ( pc , k )} \\
 \scifun{polychaos\_getsample ( pc )} \\
 \scifun{polychaos\_getquantile ( pc , alpha )} \\
 \scifun{polychaos\_getoutput ( pc )} \\
 \scifun{polychaos\_getmultind ( pc )} \\
 \scifun{polychaos\_getlog ( pc )} \\
 \scifun{polychaos\_getinvquantile ( pc , threshold )} \\
 \scifun{polychaos\_getindextotal ( pc )} \\
 \scifun{polychaos\_getindexfirst ( pc )} \\
 \scifun{ny = polychaos\_getdimoutput ( pc )} \\
 \scifun{nx = polychaos\_getdiminput ( pc )} \\
 \scifun{p = polychaos\_getdimexp ( pc )} \\
 \scifun{no = polychaos\_getdegree ( pc )} \\
 \scifun{polychaos\_getcovariance ( pc )} \\
 \scifun{polychaos\_getcorrelation ( pc )} \\
 \scifun{polychaos\_getanova ( pc )} \\
 \scifun{polychaos\_generatecode ( pc , filename , funname )} \\
 \scifun{polychaos\_computeoutput ( pc )} \\
 \scifun{polychaos\_computeexp ( pc , srv , method )} \\
 \scifun{polychaos\_computeexp ( pc , pc2 , invalue , varopt )} \\
 \scifun{polychaos\_buildsample ( pc , type , np , order )} \\
\hline
\end{tabular}
\end{center}
\caption{More methods from the \scifun{polychaos} class}
\label{fig-polychaos-methodsoutline2}
\end{figure}

More informations about the Oriented Object system used in this toolbox
can be found in the section \ref{section-nisp-oo-system}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Examples}

In this section, we present to examples of use of the 
\scifun{polychaos} class.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Product of two random variables}

In this section, we present the polynomial expansion of the 
product of two random variables. We analyse the Scilab script 
and present the methods which are available to perform the sensitivity analysis.
This script is based on the NISP methodology, which has been presented in
the Introduction chapter. We will use the figure \ref{NISP-methodology} as 
a framework and will follow the steps in order.

In the following Scilab script, we define the function \scifun{Example}
which takes a vector of size 2 as input and returns a scalar as output.
\lstset{language=scilabscript}
\begin{lstlisting}
function y = Exemple (x)
  y(:,1) = x(:,1) .* x(:,2)
endfunction
\end{lstlisting}

We now create a collection of two stochastic (normalized) random
variables. Since the random variables are normalized, we use the 
default parameters of the \scifun{randvar\_new} function.
The normalized collection is stored in the variable \scivar{srvx}.
\lstset{language=scilabscript}
\begin{lstlisting}
vx1 = randvar_new("Normale");
vx2 = randvar_new("Uniforme");
srvx = setrandvar_new();
setrandvar_addrandvar ( srvx , vx1 );
setrandvar_addrandvar ( srvx , vx2 );
\end{lstlisting}

We create a collection of two uncertain parameters. 
We explicitely set the parameters of each random variable, 
that is, the first Normal variable is associated with a
mean equal to 1.0 and a standard deviation equal to 0.5, while 
the second Uniform variable is in the interval $[1.0,2.5]$.
This collection is stored in the variable \scivar{srvu}.
\lstset{language=scilabscript}
\begin{lstlisting}
vu1 = randvar_new("Normale",1.0,0.5);
vu2 = randvar_new("Uniforme",1.0,2.5);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu , vu1 );
setrandvar_addrandvar ( srvu , vu2 );
\end{lstlisting}

The first design of experiment is build on the 
stochastic set \scivar{srvx} and based on a 
Quadrature type of DOE. Then this DOE is transformed 
into a DOE for the uncertain collection of parameters 
\scivar{srvu}.
\lstset{language=scilabscript}
\begin{lstlisting}
degre = 2;
setrandvar_buildsample ( srvx , "Quadrature" , degre );
setrandvar_buildsample ( srvu , srvx );
\end{lstlisting}

The next steps will be to create the polynomial and actually perform the DOE.
But before doing this, we can take a look at the DOE associated 
with the stochastic and uncertain collection of random variables. 
We can use the \scifun{setrandvar\_getsample} twice and get 
the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->setrandvar_getsample(srvx)
 ans  =
 
  - 1.7320508    0.1127017  
  - 1.7320508    0.5        
  - 1.7320508    0.8872983  
    0.           0.1127017  
    0.           0.5        
    0.           0.8872983  
    1.7320508    0.1127017  
    1.7320508    0.5        
    1.7320508    0.8872983  
-->setrandvar_getsample(srvu)
 ans  =
 
    0.1339746    1.1690525  
    0.1339746    1.75       
    0.1339746    2.3309475  
    1.           1.1690525  
    1.           1.75       
    1.           2.3309475  
    1.8660254    1.1690525  
    1.8660254    1.75       
    1.8660254    2.3309475  
\end{lstlisting}
These two matrices are a $9\times 2$ matrices, where each line represents an 
experiment and each column represents an input random variable.
The stochastic (normalized) \scivar{srvx} DOE has been created first,
then the \scivar{srvu} has been deduced from \scivar{srvx} based on random variable 
transformations.

We now use the \scifun{polychaos\_new} function and 
create a new polynomial \scivar{pc}. The number of input variables corresponds to the 
number of variables in the stochastic collection \scivar{srvx}, that is 2,
and the number of output variables is given as the input argument 
\scivar{ny}. In this particular case, the number of experiments to 
perform is equal to \scivar{np=9}, as returned by the 
\scifun{setrandvar\_getsize} function. This parameter is passed to the 
polynomial \scivar{pc} with the \scifun{polychaos\_setsizetarget} 
function.
\lstset{language=scilabscript}
\begin{lstlisting}
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
\end{lstlisting}

In the next step, we perform the simulations prescribed by the DOE.
We perform this loop in the Scilab language and make a loop over the 
index \scivar{k}, which represents the index of the current experiment,
while \scivar{np} is the total number of experiments to perform. 
For each loop, we get the input from the uncertain collection \scivar{srvu}
with the \scifun{setrandvar\_getsample} function, 
pass it to the \scifun{Exemple} function, get back the 
output which is then transferred to the polynomial \scivar{pc}
by the \scifun{polychaos\_settarget} function.
\lstset{language=scilabscript}
\begin{lstlisting}
inputdata = setrandvar_getsample(srvu);
outputdata = Exemple(inputdata);
polychaos_settarget(pc,outputdata);
\end{lstlisting}

We can compute the polynomial expansion based on numerical integration
so that the coefficients of the polynomial are determined.
This is done with the \scifun{polychaos\_computeexp} function,
which stands for "compute the expansion".
\lstset{language=scilabscript}
\begin{lstlisting}
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
\end{lstlisting}

Everything is now ready for the sensitivity analysis.
Indeed, the \scifun{polychaos\_getmean} returns the mean
while the \scifun{polychaos\_getvariance} returns the variance.
\lstset{language=scilabscript}
\begin{lstlisting}
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);
mprintf("Mean    = %f\n",average);
mprintf("Variance    = %f\n",var);
mprintf("Indice de sensibilite du 1er ordre\n");
mprintf("    Variable X1 = %f\n",polychaos_getindexfirst(pc,1));
mprintf("    Variable X2 = %f\n",polychaos_getindexfirst(pc,2));
mprintf("Indice de sensibilite Totale\n");
mprintf("    Variable X1 = %f\n",polychaos_getindextotal(pc,1));
mprintf("    Variable X2 = %f\n",polychaos_getindextotal(pc,2));
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
Mean    = 1.750000
Variance    = 1.000000
Indice de sensibilite du 1er ordre
    Variable X1 = 0.765625
    Variable X2 = 0.187500
Indice de sensibilite Totale
    Variable X1 = 0.812500
    Variable X2 = 0.234375
\end{lstlisting}

In order to free the memory required for the computation, 
it is necessary to delete all the objects created so far.
\lstset{language=scilabscript}
\begin{lstlisting}
polychaos_destroy(pc);
randvar_destroy(vu1);
randvar_destroy(vu2);
randvar_destroy(vx1);
randvar_destroy(vx2);
setrandvar_destroy(srvu);
setrandvar_destroy(srvx);
\end{lstlisting}

Prior to destroying the objects, we can inquire a little more 
about the density of the output of the chaos polynomial.
In the following script, we create a Latin Hypercube Sampling 
made of 10 000 points. 
Then get the output of the polynomial on these inputs and plot the 
histogram of the output.
This is done with the \scifun{histo} function, from the 
\scifun{Stixbox} toolbox.
\lstset{language=scilabscript}
\begin{lstlisting}
polychaos_buildsample(pc,"Lhs",10000);
sample_output = polychaos_getsample(pc);
scf();
histo(sample_output);
xtitle("Product function - Empirical Histogram","X","P(X)");
\end{lstlisting}

The previous script produces the figure \ref{fig-polychaos-product-output}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=14cm]{polychaos/product_histogramPCoutput.pdf}
\end{center}
\caption{Product function - Histogram of the output on a LHS design with 10000 experiments.}
\label{fig-polychaos-product-output}
\end{figure}

We may explore the following topics.
\begin{itemize}
\item Perform the same analysis where the variable 
$X_2$ is a normal variable with mean 2 and standard deviation $2$.
\item Check that the development in polynomial chaos on a Hermite-Hermite basis 
does not allow to get exact results. 
See that the convergence can be obtained by increasing the degree.
\item Check that the development on a basis Hermite-Legendre 
allows to get exact results with degree 2.
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{A note on performance}

In this section, we emphasize vectorization which 
can be used to improve the performance of a script when 
we compute the output of a function on a given sampling.

In order to use vectorization, the core feature that we used in 
the \scifun{Exemple} is the use of the elementwise multiplication, 
denoted by \scivar{.*}. 
In the Exemple function below (reproduced here for simplicity), the 
input \scivar{x} is a np-by-2 matrix of doubles, where np is the number 
of experiments, and y is a np-by-1 matrix of doubles.
\lstset{language=scilabscript}
\begin{lstlisting}
function y = Exemple (x)
  y(:,1) = x(:,1) .* x(:,2)
endfunction
\end{lstlisting}
The elementwise multiplication allows to multiply the two first columns 
of \scivar{x}, and sets the result into the output \scivar{y}, in 
one single statement.
Since Scilab uses optimized numerical libraries, this allows to 
get the best performance in most situations. 

In the previous section, we have shown that we can compute the 
output of the \scifun{Exemple} function in one single call to the 
function.
\lstset{language=scilabscript}
\begin{lstlisting}
outputdata = Exemple(inputdata);
\end{lstlisting}
This call allows to produce all the outputs as fast as possible 
and is the recommended method. 
The reason is that the previous script lets Scilab perform 
computations with large matrices. 

In fact, there is another, slower, method to perform the same computation. 
We make a loop over the index \scivar{k}, which represents the index of the 
current experiment, while \scivar{np} is the total number of experiments to perform. 
For each loop, we get the input from the uncertain collection \scivar{srvu}
with the \scifun{setrandvar\_getsample} function, 
pass it to the \scifun{Exemple} function, get back the 
output which is then transferred to the polynomial \scivar{pc}
by the \scifun{polychaos\_settarget} function.
\lstset{language=scilabscript}
\begin{lstlisting}
// This is slow.
for k=1:np
  inputdata = setrandvar_getsample(srvu,k);
  outputdata = Exemple(inputdata);
  mprintf ( "Experiment #%d, input =[%f %f], output = %f\n", k, ..
    inputdata(1), inputdata(2) , outputdata )
  polychaos_settarget(pc,k,outputdata);
end
\end{lstlisting}
The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
Experiment #1, input =[0.133975 1.169052], output = 0.156623
Experiment #2, input =[0.133975 1.750000], output = 0.234456
Experiment #3, input =[0.133975 2.330948], output = 0.312288
Experiment #4, input =[1.000000 1.169052], output = 1.169052
Experiment #5, input =[1.000000 1.750000], output = 1.750000
Experiment #6, input =[1.000000 2.330948], output = 2.330948
Experiment #7, input =[1.866025 1.169052], output = 2.181482
Experiment #8, input =[1.866025 1.750000], output = 3.265544
Experiment #9, input =[1.866025 2.330948], output = 4.349607
\end{lstlisting}

While the previous script is perfectly correct, it can be very slow 
when the number of experiments is large. 
This is because the interpreter has to perform a large number 
of loops with matrices of small size. 
In general, this produces much slower script and should be avoided. 
More details on this topic are presented in \cite{Proginscilab2010}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{The Ishigami test case}

In this section, we present the Ishigami test case.

The function \scifun{Exemple} is the model that we consider 
in this numerical experiment. This function takes a vector of size 
3 in input and returns a scalar output.
\lstset{language=scilabscript}
\begin{lstlisting}
function y = Exemple (x)
  a=7.
  b=0.1
  s1=sin(x(:,1))
  s2=sin(x(:,2))
  y(:,1) = s1 + a.*s2.*s2 + b.*x(:,3).*x(:,3).*x(:,3).*x(:,3).*s1
endfunction
\end{lstlisting}

We create 3 uncertain parameters which are uniform in the interval
$[-\pi,\pi]$ and put these random variables into the collection 
\scivar{srvu}.
\lstset{language=scilabscript}
\begin{lstlisting}
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);

srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);
\end{lstlisting}

The collection of stochastic variables is created with the 
function \scifun{setrandvar\_new}. The calling sequence 
\scifun{srvx = setrandvar\_new( nx )} allows to create 
a collection of \scivar{nx=3} random variables uniform in the interval
$[0,1]$. Then we create a Petras DOE for the stochastic collection
\scivar{srvx} and transform it into a DOE for the uncertain parameters 
\scivar{srvu}.
\lstset{language=scilabscript}
\begin{lstlisting}
nx = setrandvar_getdimension ( srvu );
srvx = setrandvar_new( nx );
degre = 9;
setrandvar_buildsample(srvx,"Petras",degre);
setrandvar_buildsample( srvu , srvx );
\end{lstlisting}

We use the \scifun{polychaos\_new} function and create the 
new polynomial \scivar{pc} with 3 inputs and 1 output.
\lstset{language=scilabscript}
\begin{lstlisting}
noutput = 1;
pc = polychaos_new ( srvx , noutput );
\end{lstlisting}

The next step allows to perform the simulations associated with the 
DOE prescribed by the collection \scivar{srvu}. 
Here, we must perform \scivar{np=751} experiments. 
\lstset{language=scilabscript}
\begin{lstlisting}
np = setrandvar_getsize(srvu);
polychaos_setsizetarget(pc,np);
inputdata = setrandvar_getsample(srvu);
outputdata = Exemple(inputdata);
polychaos_settarget(pc,outputdata);
\end{lstlisting}

We can now compute the polynomial expansion by integration.
\lstset{language=scilabscript}
\begin{lstlisting}
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
\end{lstlisting}

Everything is now ready so that we can do the sensitivy 
analysis, as in the following script.
The \scifun{polychaos\_sasummary} prints a summary of the 
sensitivity analysis.
\lstset{language=scilabscript}
\begin{lstlisting}
polychaos_sasummary(pc);
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
Mean     = 3.5
Variance = 13.842473
First order sensitivity indices
  Output #1
    Variable X1 = 0.3139532
    Variable X2 = 0.4423253
    Variable X3 = 8.087D-31
Total sensitivity indices
  Output #1
    Variable X1 = 0.5576747
    Variable X2 = 0.4423255
    Variable X3 = 0.2437215
Functional ANOVA:
1 0 0  : 0.313953
0 1 0  : 0.442325
1 1 0  : 1.55229e-009
0 0 1  : 8.08718e-031
1 0 1  : 0.243721
0 1 1  : 8.43733e-031
1 1 1  : 1.6007e-007
\end{lstlisting}

We now focus on the variance generated by the variables $X_1$ and 
$X_3$. We set the group to the empty group with the 
\scifun{polychaos\_setgroupempty} function and 
add variables with the \scifun{polychaos\_set\-groupaddvar} function.
\lstset{language=scilabscript}
\begin{lstlisting}
groupe = [1 3];
polychaos_setgroupempty ( pc );
polychaos_setgroupaddvar ( pc , groupe(1) );
polychaos_setgroupaddvar ( pc , groupe(2) );
mprintf("Fraction of the variance of a group of variables\n");
mprintf("    Groupe X1 et X2 =%f\n",polychaos_getgroupind(pc));
\end{lstlisting}
The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
Fraction of the variance of a group of variables
    Groupe X1 et X2 =0.557674
\end{lstlisting}

We can compute the density function associated with the output 
variable of the function. In order to compute it, we use the 
\scifun{polychaos\_buildsample} function and create a Latin
Hypercube Sampling with 10000 experiments. The 
\scifun{polychaos\_getsample} function allows to quiery the 
polynomial and get the outputs.
This produces the figure \ref{fig-polychaos-ishigami-output}.
\lstset{language=scilabscript}
\begin{lstlisting}
polychaos_buildsample(pc,"Lhs",10000);
sample_output = polychaos_getsample(pc);
scf();
histo(sample_output)
xtitle("Ishigami - Histogram");
\end{lstlisting}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.8\textwidth]{polychaos/ishigami_output.pdf}
\end{center}
\caption{Ishigami function - Histogram of the output of the chaos polynomial on a LHS design with 10 000 experiments.}
\label{fig-polychaos-ishigami-output}
\end{figure}

We can plot a bar graph of the sensitivity indices, as presented 
in figure \ref{fig-polychaos-ishigami-sensitivity}.
\lstset{language=scilabscript}
\begin{lstlisting}
for i=1:nx
  indexfirst(i)=polychaos_getindexfirst(pc,i);
  indextotal(i)=polychaos_getindextotal(pc,i);
end
scf();
bar(indextotal,0.2,'blue');
bar(indexfirst,0.15,'yellow');
legend(["Total" "First order"],pos=1);
xtitle("Ishigami - Sensitivity indices");
\end{lstlisting}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{polychaos/ishigami_sensitivity}
\end{center}
\caption{Ishigami function - Sensitivity indices.}
\label{fig-polychaos-ishigami-sensitivity}
\end{figure}

