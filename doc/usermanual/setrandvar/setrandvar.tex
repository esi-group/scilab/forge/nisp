% Copyright (C) 2012-2013 - Michael Baudin
% Copyright (C) 2008-2010 - INRIA - Michael Baudin
% Copyright (C) 2008-2011 - CEA - Jean-Marc Martinez
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

\section{The \scifun{setrandvar} class}

In this chapter, we presen the \scifun{setrandvar} class.
The first section gives a brief outline of the features of this 
class and the second section present several examples.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Introduction}

The \scifun{setrandvar} class allows to manage a collection 
of random variables and to build a Design Of Experiments (DOE).
Several types of DOE are provided:
\begin{itemize}
\item Monte-Carlo,
\item Latin Hypercube Sampling,
\item Smolyak.
\end{itemize}
Once a DOE is created, we can retrieve the information experiment by experiment
or the whole matrix of experiments. This last feature allows to benefit from 
the fact that Scilab can natively manage matrices, so that we do not 
have to perform loops to manage the complete DOE. Hence, good performances
can be observed, even if the language still is interpreted.

The figure \ref{fig-setrandvar-methodsoutline} presents the methods available 
in the \scifun{setrandvar} class.
A complete description of the input and output arguments of each function is available 
in the inline help and will not be repeated here.

\begin{figure}
\begin{center}
\begin{tabular}{|l|}
\hline
\textbf{Constructors}\\
\scifun{srv = setrandvar\_new ( )} \\
\scifun{srv = setrandvar\_new ( n )} \\
\scifun{srv = setrandvar\_new ( file )} \\
\hline
\textbf{Methods}\\
\scifun{setrandvar\_setsample ( srv , name , np )} \\
\scifun{setrandvar\_setsample ( srv , k , i , value )} \\
\scifun{setrandvar\_setsample ( srv , k , value )} \\
\scifun{setrandvar\_setsample ( srv , value )} \\
\scifun{setrandvar\_save ( srv , file )} \\
\scifun{np = setrandvar\_getsize ( srv )} \\
\scifun{sample = setrandvar\_getsample ( srv , k , i )} \\
\scifun{sample = setrandvar\_getsample ( srv , k )} \\
\scifun{sample = setrandvar\_getsample ( srv )} \\
\scifun{setrandvar\_getlog ( srv )} \\
\scifun{nx = setrandvar\_getdimension ( srv )} \\
\scifun{setrandvar\_freememory ( srv )} \\
\scifun{setrandvar\_buildsample ( srv , srv2 )} \\
\scifun{setrandvar\_buildsample ( srv , name , np )} \\
\scifun{setrandvar\_buildsample ( srv , name , np , ne )} \\
\scifun{setrandvar\_addrandvar ( srv , rv )} \\
\hline
\textbf{Destructor}\\
\scifun{setrandvar\_destroy ( srv )} \\
\hline
\textbf{Static methods}\\
\scifun{tokenmatrix = setrandvar\_tokens ()} \\
\scifun{nb = setrandvar\_size ()} \\
\hline
\end{tabular}
\end{center}
\caption{Outline of the methods of the \scifun{setrandvar} class}
\label{fig-setrandvar-methodsoutline}
\end{figure}

More informations about the Oriented Object system used in this toolbox
can be found in the section \ref{section-nisp-oo-system}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Examples}

In this section, we present examples of use of the \scifun{setrandvar}
class. In the first example, we present a Scilab session where we create 
a Latin Hypercube Sampling. In the second part, we present various 
types of DOE which can be generated with this class.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{A Monte-Carlo design with 2 variables}


In the following example, we build a Monte-Carlo design of experiments, with 2 input random variables. 
The first variable is associated with a Normal distribution function and the second variable is associated 
with a Uniform distribution function. 
The simulation is based on 1000 experiments.

The function \scifun{nisp\_initseed} is used to set the value of the seed to zero, 
so that the results can be reproduced. 
The \scifun{setrandvar\_new} function is used to create a 
new set of random variables. 
Then we create two new random variables with the \scifun{randvar\_new} function. 
These two variables are added to the set with the \scifun{setrandvar\_addrandvar} function. 
The \scifun{setrandvar\_buildsample} allows to build the design of experiments, 
which can be retrieved as matrix with the \scifun{setrandvar\_getsample} function. 
The sampling matrix has \scivar{np} rows and 2 columns (one for each input variable).

\lstset{language=scilabscript}
\begin{lstlisting}
nisp_initseed(0);
rvu1 = randvar_new("Normale",1,3);
rvu2 = randvar_new("Uniforme",2,3);
//
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
//
np = 5000;
setrandvar_buildsample(srvu, "MonteCarlo",np);
sampling = setrandvar_getsample(srvu);
// Check sampling of random variable #1
mean(sampling(:,1)) // Expectation : 1
// Check sampling of random variable #2
mean(sampling(:,2)) // Expectation : 2.5
//
scf();
histplot(50,sampling(:,1));
xtitle("Empirical histogram of X1");
scf();
histplot(50,sampling(:,2));
xtitle("Empirical histogram of X2");
//
// Clean-up
setrandvar_destroy(srvu);
randvar_destroy(rvu1);
randvar_destroy(rvu2);
\end{lstlisting}
The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->mean(sampling(:,1)) // Expectation : 1
 ans  =
    1.0064346  
-->mean(sampling(:,2)) // Expectation : 2.5
 ans  =
    2.5030984  
\end{lstlisting}
The prevous script also produces the figures \ref{fig-setrandvar-demoMC2var-x1} and 
\ref{fig-setrandvar-demoMC2var-x2}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/demo_MonteCarlo2var-x1.pdf}
\end{center}
\caption{Monte-Carlo Sampling - Normal random variable.}
\label{fig-setrandvar-demoMC2var-x1}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/demo_MonteCarlo2var-x2.pdf}
\end{center}
\caption{Monte-Carlo Sampling - Uniform random variable.}
\label{fig-setrandvar-demoMC2var-x2}
\end{figure}

We may now want to add the exact distribution to these histograms and compare. 
The Normal distribution function is not provided by Scilab, but is provided 
by the Distfun module. 
Indeed, the \scifun{distfun\_normpdf} function of the Distfun module computes the 
Normal probability distribution function. 
In order to install this module, we can run the \scifun{atomsInstall} function, 
as in the following script.
\lstset{language=scilabscript}
\begin{lstlisting}
atomsInstall("distfun")
\end{lstlisting}
The following script compares the empirical and theoretical distributions. 
\lstset{language=scilabscript}
\begin{lstlisting}
scf();
histplot(50,sampling(:,1));
xtitle("Empirical histogram of X1");
x=linspace(-15,15,1000);
y = dnorm(x,1,3);
plot(x,y,"r-")
legend(["Empirical","Exact"]);
\end{lstlisting}

The previous script produces the figure \ref{fig-setrandvar-demoMC2var-x1-compare}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/demo_MonteCarlo2var-x1-compare.pdf}
\end{center}
\caption{Monte-Carlo Sampling - Histogram and exact distribution functions for the first variable.}
\label{fig-setrandvar-demoMC2var-x1-compare}
\end{figure}

The following script performs the same comparison for the second variable.
\lstset{language=scilabscript}
\begin{lstlisting}
scf();
histplot(50,sampling(:,2));
xtitle("Empirical histogram of X2");
x=linspace(2,3,1000);
y=ones(1000,1);
plot(x,y,"r-");
\end{lstlisting}

The previous script produces the figure \ref{fig-setrandvar-demoMC2var-x2-compare}.
\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/demo_MonteCarlo2var-x2-compare.pdf}
\end{center}
\caption{Monte-Carlo Sampling - Histogram and exact distribution functions for the second variable.}
\label{fig-setrandvar-demoMC2var-x2-compare}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{A Monte-Carlo design with 2 variables}

In this section, we create a Monte-Carlo design with 2 variables.

We are going to use the exponential distribution function, which is not 
defined in Scilab. 
The following \scifun{exppdf} function computes the probability distribution 
function of the exponential distribution function.
\lstset{language=scilabscript}
\begin{lstlisting}
function p = exppdf ( x , lambda )
  p = lambda.*exp(-lambda.*x)
endfunction
\end{lstlisting}

The following script creates a Monte-Carlo sampling where 
the first variable is Normal and the second variable is Exponential.
Then we compare the empirical histogram and the exact distribution 
function.
We use the \scifun{dnorm} function defined in the Stixbox module.
\lstset{language=scilabscript}
\begin{lstlisting}
nisp_initseed ( 0 );
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Exponentielle",5.);
// Definition d'un groupe de variables aleatoires
srv = setrandvar_new ( );
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
np = 1000;
setrandvar_buildsample ( srv , "MonteCarlo" , np );
//
sampling = setrandvar_getsample ( srv );
// Check sampling of random variable #1
mean(sampling(:,1)), variance(sampling(:,1))
// Check sampling of random variable #2
min(sampling(:,2)), max(sampling(:,2))
// Plot
scf();
histplot(40, sampling(:,1))
x = linspace(-1,3,1000)';
p = dnorm(x,1,0.5);
plot(x,p,"r-")
xtitle("Empirical histogram of X1","X","P(X)");
legend(["Empirical","Exact"]);
scf();
histplot(40, sampling(:,2))
x = linspace(0,2,1000)';
p = exppdf ( x , 5 );
plot(x,p,"r-")
xtitle("Empirical histogram of X2","X","P(X)");
legend(["Empirical","Exact"]);
// Clean-up
setrandvar_destroy(srv);
randvar_destroy(rv1);
randvar_destroy(rv2);
\end{lstlisting}

The previous script produces the figures \ref{fig-setrandvar-MonteCarlo2-x1} and 
\ref{fig-setrandvar-MonteCarlo2-x2}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/demo_MonteCarlo2-x1-compare.pdf}
\end{center}
\caption{Monte-Carlo Sampling - Histogram and exact distribution functions for the first variable.}
\label{fig-setrandvar-MonteCarlo2-x1}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/demo_MonteCarlo2-x2-compare.pdf}
\end{center}
\caption{Monte-Carlo Sampling - Histogram and exact distribution functions for the second variable.}
\label{fig-setrandvar-MonteCarlo2-x2}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{A LHS design}

In this section, we present the creation of a Latin Hypercube Sampling.
In our example, the DOE is based on two random variables, the first 
being Normal with mean 1.0 and standard deviation 0.5 and the second being Uniform
in the interval $[2,3]$.

We begin by defining two random variables with the \scifun{randvar\_new} function.
\lstset{language=scilabscript}
\begin{lstlisting}
vu1 = randvar_new("Normale",1.0,0.5);
vu2 = randvar_new("Uniforme",2.0,3.0);
\end{lstlisting}

Then, we create a collection of random variables with the 
\scifun{setrandvar\_new} function which creates here an empty 
collection of random variables. Then we add the two random 
variables to the collection.
\lstset{language=scilabscript}
\begin{lstlisting}
srv = setrandvar_new ( );
setrandvar_addrandvar ( srv , vu1 );
setrandvar_addrandvar ( srv , vu2 );
\end{lstlisting}

We can now build the DOE so that it is a LHS sampling 
with 1000 experiments.
\lstset{language=scilabscript}
\begin{lstlisting}
setrandvar_buildsample ( srv , "Lhs" , 1000 );
\end{lstlisting}

At this point, the DOE is stored in the memory space of the NISP 
library, but we do not have a direct access to it.
We now call the \scifun{setrandvar\_getsample} function and 
store that DOE into the \scivar{sampling} matrix.
\lstset{language=scilabscript}
\begin{lstlisting}
sampling = setrandvar_getsample ( srv );
\end{lstlisting}

The \scivar{sampling} matrix has 1000 rows, corresponding 
to each experiment, and 2 columns, corresponding to each 
input random variable.

The following script allows to plot the sampling, which is is presented 
in figure \ref{fig-setrandvar-lhs_2D}.
\lstset{language=scilabscript}
\begin{lstlisting}
my_handle = scf();
clf(my_handle,"reset");
plot(sampling(:,1),sampling(:,2));
my_handle.children.children.children.line_mode = "off";
my_handle.children.children.children.mark_mode = "on";
my_handle.children.children.children.mark_size = 2;
my_handle.children.title.text = "Latin Hypercube Sampling";
my_handle.children.x_label.text = "Variable #1 : Normale,1.0,0.5";
my_handle.children.y_label.text = "Variable #2 : Uniforme,2.0,3.0";
\end{lstlisting}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=14cm]{setrandvar/sampling_lhs_2D.png}
\end{center}
\caption{Latin Hypercube Sampling - The first variable is Normal, the second variable is Uniform.}
\label{fig-setrandvar-lhs_2D}
\end{figure}

The following script allows to plot the histogram of the two variables, which 
are presented in figures \ref{fig-setrandvar-lhs_2D_var1} and \ref{fig-setrandvar-lhs_2D_var2}. 

\lstset{language=scilabscript}
\begin{lstlisting}
// Plot Var #1
my_handle = scf();
clf(my_handle,"reset");
histplot ( 50 , sampling(:,1))
my_handle.children.title.text = "Variable #1 : Normale,1.0,0.5";
// Plot Var #2
my_handle = scf();
clf(my_handle,"reset");
histplot ( 50 , sampling(:,2))
my_handle.children.title.text = "Variable #2 : Uniforme,2.0,3.0";
\end{lstlisting}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_lhs_var1.pdf}
\end{center}
\caption{Latin Hypercube Sampling - Normal random variable.}
\label{fig-setrandvar-lhs_2D_var1}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_lhs_var2.pdf}
\end{center}
\caption{Latin Hypercube Sampling - Uniform random variable.}
\label{fig-setrandvar-lhs_2D_var2}
\end{figure}

We can use the \scifun{mean} and \scifun{variance} on each 
random variable and check that the expected result is computed.
We insist on the fact that the \scifun{mean} and \scifun{variance}
functions are not provided by the NISP library: these are pre-defined 
functions which are available in the Scilab library. That means
that any Scilab function can be now used to process the data 
generated by the toolbox.
\lstset{language=scilabscript}
\begin{lstlisting}
for ivar = 1:2
  m = mean(sampling(:,ivar))
  mprintf("Variable #%d, Mean : %f\n",ivar,m)
  v = variance(sampling(:,ivar))
  mprintf("Variable #%d, Variance : %f\n",ivar,v)
end
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
Variable #1, Mean : 1.000000
Variable #1, Variance : 0.249925
Variable #2, Mean : 2.500000
Variable #2, Variance : 0.083417
\end{lstlisting}

Our numerical simulation is now finished, but we must destroy the 
objects so that the memory managed by the toolbox is deleted.
\lstset{language=scilabscript}
\begin{lstlisting}
randvar_destroy(vu1)
randvar_destroy(vu2)
setrandvar_destroy(srv)
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{A note on the LHS samplings}

We emphasize that the LHS sampling which is provided by the 
\scifun{setrandvar\_buildsample} function is so that the points 
are centered within their cells. 

In the following script, we create a LHS sampling with 10 points.
\lstset{language=scilabscript}
\begin{lstlisting}
srv = setrandvar_new(2);
np = 10;
setrandvar_buildsample ( srv , "Lhs" , np );
sampling = setrandvar_getsample ( srv );
scf();
plot(sampling(:,1),sampling(:,2),"bo");
xtitle("LHS Design","X1","X2");
// Add the cuts
cut = linspace ( 0 , 1 , np + 1 );
for i = 1 : np + 1
  plot( [cut(i) cut(i)] , [0 1] , "-" )
end
for i = 1 : np + 1
  plot( [0 1] , [cut(i) cut(i)] , "-" )
end
setrandvar_destroy ( srv )
\end{lstlisting}

The previous script produces the figure \ref{fig-setrandvar-LHScenter}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_lhscenters.pdf}
\end{center}
\caption{Latin Hypercube Sampling - Computed with \scifun{setrandvar\_buildsample} and the "Lhs" option.}
\label{fig-setrandvar-LHScenter}
\end{figure}

The "LhsMaxMin" sampling provided by the \scifun{setrandvar\_buildsample} 
function tries to maximize the minimum distance between the 
points in the sampling. 
The \scivar{ntry} parameter is the number of random points generated 
before the best is accepted in the sampling.

\lstset{language=scilabscript}
\begin{lstlisting}
np = 10;
ntry = 100;
setrandvar_buildsample ( srv , "LhsMaxMin" , np , ntry );
sampling = setrandvar_getsample ( srv );
\end{lstlisting}

The previous script produces the figure \ref{fig-setrandvar-LHSmaxmin}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_lhsmaxmin.pdf}
\end{center}
\caption{Latin Hypercube Sampling - Computed with \scifun{setrandvar\_buildsample} and the "LhsMaxMin" option.}
\label{fig-setrandvar-LHSmaxmin}
\end{figure}

On the other hand, the \scifun{nisp\_buildlhs} function produces a 
more classical LHS sampling, where the points are randomly picked within
their cells. 

\lstset{language=scilabscript}
\begin{lstlisting}
n = 5;
s = 2;
sampling = nisp_buildlhs ( s , n );
scf();
plot ( sampling(:,1) , sampling(:,2) , "bo" );
// Add the cuts
cut = linspace ( 0 , 1 , n + 1 );
for i = 1 : n + 1
plot( [cut(i) cut(i)] , [0 1] , "-" )
end
for i = 1 : n + 1
plot( [0 1] , [cut(i) cut(i)] , "-" )
end
\end{lstlisting}

The previous script produces the figure \ref{fig-setrandvar-LHSbuild}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_nispbuildlhs.pdf}
\end{center}
\caption{Latin Hypercube Sampling - Computed with \scifun{nisp\_buildlhs}.}
\label{fig-setrandvar-LHSbuild}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Other types of DOEs}

The following Scilab session allows to generate a Monte-Carlo
sampling with two uniform variables in the interval $[-1,1]$.
The figure \ref{fig-setrandvar-mc_2D} presents this 
sampling and the figures \ref{fig-setrandvar-mc_2D_var1} and 
\ref{fig-setrandvar-mc_2D_var2} present the histograms 
of the two uniform random variables.

\lstset{language=scilabscript}
\begin{lstlisting}
vu1 = randvar_new("Uniforme",-1.0,1.0);
vu2 = randvar_new("Uniforme",-1.0,1.0);
srv = setrandvar_new ( );
setrandvar_addrandvar ( srv , vu1 );
setrandvar_addrandvar ( srv , vu2 );
setrandvar_buildsample ( srv , "MonteCarlo" , 1000 );
sampling = setrandvar_getsample ( srv );
randvar_destroy(vu1);
randvar_destroy(vu2);
setrandvar_destroy(srv);
\end{lstlisting}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=14cm]{setrandvar/sampling_MonteCarlo_2D.png}
\end{center}
\caption{Monte-Carlo Sampling - Two uniform variables in the interval $[-1,1]$.}
\label{fig-setrandvar-mc_2D}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_MonteCarlo_2D_var1.pdf}
\end{center}
\caption{Latin Hypercube Sampling - First uniform variable in $[-1,1]$.}
\label{fig-setrandvar-mc_2D_var1}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_MonteCarlo_2D_var2.pdf}
\end{center}
\caption{Latin Hypercube Sampling - Second uniform variable in $[-1,1]$.}
\label{fig-setrandvar-mc_2D_var2}
\end{figure}

With the \scifun{setrandvar\_buildsample} function, we can change the 
type of sampling by changing the second argument.
This way, we can create the Petras, Quadrature and Sobol sampling presented
in figures \ref{fig-setrandvar-petras_2D}, \ref{fig-setrandvar-quadrature_2D} and 
\ref{fig-setrandvar-sobol_2D}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_Petras_2D.png}
\end{center}
\caption{Petras sampling - Two uniform variables in the interval $[-1,1]$.}
\label{fig-setrandvar-petras_2D}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_Quadrature_2D.png}
\end{center}
\caption{Quadrature sampling - Two uniform variables in the interval $[-1,1]$.}
\label{fig-setrandvar-quadrature_2D}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{setrandvar/sampling_Sobol_2D.png}
\end{center}
\caption{Sobol sampling - Two uniform variables in the interval $[-1,1]$.}
\label{fig-setrandvar-sobol_2D}
\end{figure}

