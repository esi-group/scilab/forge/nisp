% Copyright (C) 2013 - Michael Baudin
% Copyright (C) 2008-2010 - INRIA - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

\section{Installation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Architecture of the toolbox}
\label{sec-archi}

Let us present some details of the the internal components of the toolbox.
The following list is an overview of the content of the directories:
\begin{itemize}
\item \emph{tbxnisp/demos} : demonstration scripts
\item \emph{tbxnisp/doc} : the documentation
\item \emph{tbxnisp/doc/usermanual} : the \LaTeX sources of this manual
\item \emph{tbxnisp/etc} : startup and shutdow scripts for the toolbox
\item \emph{tbxnisp/help} : inline help pages
\item \emph{tbxnisp/macros} : Scilab macros files *.sci
\item \emph{tbxnisp/sci\_gateway} : the sources of the gateway
\item \emph{tbxnisp/src} : the sources of the NISP library
\item \emph{tbxnisp/tests} : tests
\item \emph{tbxnisp/tests/nonreg\_tests} : tests after some bug has been identified
\item \emph{tbxnisp/tests/unit\_tests} : unit tests
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Installing the toolbox from the sources}
\label{sec-installsrc}

In this section, we present the steps which are required in order 
to install the toolbox from the sources.

In order to install the toolbox from the sources, a compiler
is required to be installed on the machine.
This toolbox can be used with Scilab v5.1 and Scilab v5.2.
We suppose that the archive has been unpacked in the "tbxnisp" 
directory. The following is a short list of the steps which are 
required to setup the toolbox.

\begin{enumerate}
\item build the toolbox : run the \emph{tbxnisp/builder.sce} script to 
create the binaries of the library, create the binaries 
for the gateway, generate the documentation
\item load the toolbox : run the \emph{tbxnisp/load.sce} script to 
load all commands and setup the documentation
\item setup the startup configuration file of your Scilab system so that the toolbox is 
known at startup (see below for details),
\item run the unit tests : run the \emph{tbxnisp/runtests.sce} script to 
perform all unit tests and check that the toolbox is OK
\item run the demos : run the \emph{tbxnisp/rundemos.sce} script to 
run all demonstration scripts and get a quick interactive 
overview of its features
\end{enumerate}

The following script presents the messages
which are generated when the builder of the toolbox is launched.
The builder script performs the following steps:
\begin{itemize}
\item compile the NISP C++ library,
\item compile the C++ gateway library (the glue between the library and Scilab),
\item generate the Java help files from the .xml files,
\item generate the loader script.
\end{itemize}

\lstset{language=scilabsession}
\begin{lstlisting}
-->exec C:\tbxnisp\builder.sce;
Building sources...
   Generate a loader file
   Generate a Makefile
   Running the Makefile
   Compilation of utils.cpp
   Compilation of blas1_d.cpp
   Compilation of dcdflib.cpp
   Compilation of faure.cpp
   Compilation of halton.cpp
   Compilation of linpack_d.cpp
   Compilation of niederreiter.cpp
   Compilation of reversehalton.cpp
   Compilation of sobol.cpp
   Building shared library (be patient)
   Generate a cleaner file
   Generate a loader file
   Generate a Makefile
   Running the Makefile
   Compilation of nisp_gc.cpp
   [...]
   Compilation of nisp_smolyak.cpp
   Building shared library (be patient)
   Generate a cleaner file
Building gateway...
   Generate a gateway file
   Generate a loader file
   Generate a Makefile: Makelib
   Running the makefile
   Compilation of nisp_gettoken.cpp
   Compilation of nisp_gwsupport.cpp
   Compilation of nisp_PolynomialChaos_map.cpp
   Compilation of nisp_RandomVariable_map.cpp
   Compilation of nisp_SetRandomVariable_map.cpp
   Compilation of sci_nisp_startup.cpp
   [...]
   Compilation of sci_polychaos_generatecode.cpp
   Building shared library (be patient)
   Generate a cleaner file
Generating loader_gateway.sce...
Building help...
Building the master document:
	C:\tbxnisp\help\en_US
Building the manual file [javaHelp] in 
C:\tbxnisp\help\en_US. 
(Please wait building ... this can take a while)
Generating loader.sce...
\end{lstlisting}

The following script presents the messages
which are generated when the loader of the toolbox is launched.
The loader script performs the following steps:
\begin{itemize}
\item load the gateway (and the NISP library),
\item load the help,
\item load the demo.
\end{itemize}

\lstset{language=scilabsession}
\begin{lstlisting}
-->exec C:\tbxnisp\loader.sce;
Start NISP Toolbox
	Load gateways
	Load help
	Load demos
\end{lstlisting}

It is now necessary to setup your Scilab system so that the toolbox is 
loaded automatically at startup. The way to do this is to configure the 
Scilab startup configuration file. The directory where this file is located is stored in the 
Scilab variable \scivar{SCIHOME}. In the following Scilab session, we 
use Scilab v5.2.0-beta-1 in order to know the value of the \scivar{SCIHOME}
global variable.

\lstset{language=scilabsession}
\begin{lstlisting}
-->SCIHOME
 SCIHOME  =
 C:\Users\baudin\AppData\Roaming\Scilab\scilab-5.2.0-beta-1   
\end{lstlisting}

On my Linux system, the Scilab 5.1 startup file is located
in 
\begin{verbatim}
/home/myname/.Scilab/scilab-5.1/.scilab. 
\end{verbatim}
On my Windows system, the Scilab 5.1 startup file is located
in 
\begin{verbatim}
C:/Users/myname/AppData/Roaming/Scilab/scilab-5.1/.scilab.
\end{verbatim}
This file is a regular Scilab script which is automatically 
loaded at Scilab's startup. If that file does not already 
exist, create it. Copy the following lines into the \emph{.scilab} file 
and configure the path to the toolboxes, stored in the \emph{SCILABTBX} variable.

\lstset{language=scilabsession}
\begin{lstlisting}
exec("C:\tbxnisp\loader.sce");
\end{lstlisting}

The following script presents the messages which are generated 
when the unit tests script of the toolbox is launched.

\lstset{language=scilabsession}
\begin{lstlisting}
-->exec C:\tbxnisp\runtests.sce;
Tests beginning the 2009/11/18 at 12:47:45
   TMPDIR = C:\Users\baudin\AppData\Local\Temp\SCI_TMP_6372_
   001/004 - [tbxnisp] nisp..................passed : ref created 
   002/004 - [tbxnisp] polychaos1............passed : ref created 
   003/004 - [tbxnisp] randvar1..............passed : ref created 
   004/004 - [tbxnisp] setrandvar1...........passed : ref created 
   --------------------------------------------------------------
   Summary
   tests                        4 - 100 % 
   passed                       0 -   0 % 
   failed                       0 -   0 % 
   skipped                      0 -   0 % 
   length                          3.84 sec 
   --------------------------------------------------------------
Tests ending the 2009/11/18 at 12:47:48
\end{lstlisting}


