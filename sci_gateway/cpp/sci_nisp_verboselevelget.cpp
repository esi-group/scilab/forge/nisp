
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "sci_nisp_startup.hxx" 
#include "nisp_conf.h"

// level = nisp_verboselevelget ( ) : returns the current verbose level
int sci_nisp_verboselevelget (char *fname,unsigned long fname_len) {
	int m, n;
	int level;
	double * mydata = NULL;
	double *pdblFinalVar = NULL; //SCILAB return Var

	/* check that we have 0 parameters input */
	/* check that we have 0,1 parameters output */
	CheckInputArgument(pvApiCtx,0,0) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	level = nisp_verboselevelget ();
	// Returns the token as the result
	m=1;
	n=1;
	allocMatrixOfDouble(pvApiCtx,Rhs+1, m, n, &pdblFinalVar);
	pdblFinalVar[0] = level;
	LhsVar(1) = Rhs+1;
	return 0;
}
