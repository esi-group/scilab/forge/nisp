
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// polychaos_getinvquantile ( token , threshold , ovar )
//   returns the probability of having an output variable ovar, 
//     where 1<= ovar <= ny over a given threshold.
// polychaos_getinvquantile ( token , threshold )
//   returns the probability of having an output variable 
//     over a given threshold, as a 1 x (ny) matrix
//
int sci_polychaos_getinvquantile (char *fname, unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int ovar;
	double quantile;
	int ny;
	double threshold;

	CheckInputArgument(pvApiCtx,2,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	if ( Rhs == 2 ) {
		// polychaos_getinvquantile ( token , threshold )
		nispgw_GetOneDouble ( fname , 2 , &threshold, pvApiCtx);
	}
	else if ( Rhs == 3 ) 
	{
		// polychaos_getinvquantile ( token , threshold , ovar )
		nispgw_GetOneDouble ( fname , 2 , &threshold, pvApiCtx);
		nispgw_GetOneInteger ( fname , 3 , &ovar, pvApiCtx);
	}
	// Get object
	nispgw_token2Polychaos ( fname , 1 , token , &pc);
	if ( Rhs == 2 ) {
		// polychaos_getinvquantile ( token , threshold )
		ny = pc-> GetDimensionOutput();
		nRows=1;
		nCols=ny;
		allocMatrixOfDouble(pvApiCtx, Rhs+1, nRows, nCols, &pdblFinalVar);
		pc-> GetInvQuantile ( threshold , pdblFinalVar );
	} else if ( Rhs == 3 ) {
		// polychaos_getinvquantile ( token , threshold , ovar )
		quantile = pc->GetInvQuantile ( threshold , ovar );
		// Returns the result
		nRows=1;
		nCols=1;
		allocMatrixOfDouble(pvApiCtx, Rhs+1, nRows, nCols, &pdblFinalVar);
		pdblFinalVar[0] = quantile;
	}
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

