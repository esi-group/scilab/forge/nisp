
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// polychaos_getanovaordco ( token , threshold , r )
//   prints out the anova ordered coefficients
// polychaos_getanovaordco ( token , threshold )
//   prints out the anova ordered coefficients
// polychaos_getanovaord ( token )
//   prints out the anovaco ordered coefficients
//
int sci_polychaos_getanovaordco (char *fname,unsigned long fname_len)
{
	int token;
	PolynomialChaos * pc = NULL;
	int r;
	double threshold;

	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	if ( (Rhs==2) | (Rhs==3) ) {
		nispgw_GetOneDouble ( fname , 2 , &threshold, pvApiCtx);
	}
	if ( Rhs==3 ) {
		nispgw_GetOneInteger ( fname , 3 , &r, pvApiCtx);
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if (Rhs==1) {
		pc -> GetAnovaOrderedCoefficients ( );
	} else if (Rhs==2) {
		pc -> GetAnovaOrderedCoefficients ( threshold );
	} else if (Rhs==3) {
		pc -> GetAnovaOrderedCoefficients ( threshold , r );
	}
	nispgw_CreateLhsInteger ( 1 , token, pvApiCtx);
	return 0;
}
/* ==================================================================== */

