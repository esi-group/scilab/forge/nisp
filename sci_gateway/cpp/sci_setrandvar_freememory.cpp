
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
#include "api_scilab.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// setrandvar_freememory (token)
//   free the memory of the object associated with token
//
int sci_setrandvar_freememory (char *fname,unsigned long fname_len)
{
	int token;
	SetRandomVariable * srv = NULL;

	CheckInputArgument(pvApiCtx,1,1) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	nispgw_token2Setrandvar ( fname , 1 , token , &srv );
	srv->FreeMemory();
	nispgw_CreateLhsInteger ( 1 , token, pvApiCtx);
	return 0;
}
/* ==================================================================== */

