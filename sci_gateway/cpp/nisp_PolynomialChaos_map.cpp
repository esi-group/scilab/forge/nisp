
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


//
// nisp_PolynomialChaos_map.cpp --
//   A map to manage PolynomialChaos from NISP for the tbxnisp Scilab Toolbox
//

#include <stdlib.h>
#include <map>

#include "nisp_pc.h"

#include "nisp_PolynomialChaos_map.hxx"

using namespace std;
typedef map<int , PolynomialChaos *> nisp_PolynomialChaos_map_type;
nisp_PolynomialChaos_map_type nisp_PolynomialChaos_map;
int PolynomialChaos_counter = 0;

int nisp_PolynomialChaos_map_add ( PolynomialChaos * pc )
{
	int token;
	token = PolynomialChaos_counter;
	PolynomialChaos_counter = PolynomialChaos_counter + 1;
	nisp_PolynomialChaos_map[token] = pc;
	return token;
}

void nisp_PolynomialChaos_map_remove ( int token )
{
	nisp_PolynomialChaos_map_type::iterator it;
	it = nisp_PolynomialChaos_map.find (token);
	nisp_PolynomialChaos_map.erase(it);
}

PolynomialChaos * nisp_PolynomialChaos_map_getobject ( int token )
{
	PolynomialChaos * pc = NULL;
	if ( nisp_PolynomialChaos_map.size()!=0 ) {
		nisp_PolynomialChaos_map_type::iterator it;
		it = nisp_PolynomialChaos_map.find (token);
		if ( it!= nisp_PolynomialChaos_map.end()) {
			pc = it->second;
		}
	}
	return pc;
}

int nisp_PolynomialChaos_map_size ()
{
	return nisp_PolynomialChaos_map.size();
}

void nisp_PolynomialChaos_map_tokens (int * tokens)
{
	int index = 0;
	int token;
	for(nisp_PolynomialChaos_map_type::iterator it = nisp_PolynomialChaos_map.begin(); it != nisp_PolynomialChaos_map.end(); ++it)
	{
		token = it->first;
		tokens[index] = token;
		index ++;
	}
}