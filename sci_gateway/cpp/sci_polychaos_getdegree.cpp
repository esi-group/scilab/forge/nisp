
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// no = polychaos_getdegree ( token )
//   returns the degree of the polynomial (no)
//
int sci_polychaos_getdegree (char *fname, unsigned long fname_len)
{
	int m, n;
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int no;

	CheckInputArgument(pvApiCtx,1,1) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	no = pc -> GetDegree ( );
	// Returns the degree as the result
	m=1;
	n=1;
	allocMatrixOfDouble(pvApiCtx, Rhs+1, m, n, &pdblFinalVar);
	pdblFinalVar[0] = no;
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

