
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// polychaos_getmean ( token , ovar )
//   returns the mean of the output variable which index is ovar, where 1<= ovar<= ny.
// polychaos_getmean ( token )
//   returns the mean of the all output variables, as a 1 x (ny) matrix
//
int sci_polychaos_getmean (char *fname, unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int ovar;
	double mean;
	int ny;
	double * mydata = NULL;

	CheckInputArgument(pvApiCtx,1,2) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	if ( Rhs == 2 ) {
		// polychaos_getmean ( token , ovar )
		nispgw_GetOneInteger ( fname , 2 , &ovar, pvApiCtx);
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc);
	if ( Rhs == 1 ) {
		// polychaos_getmean ( token )
		ny = pc-> GetDimensionOutput();
		nRows=1;
		nCols=ny;
		allocMatrixOfDouble(pvApiCtx,Rhs+1, nRows, nCols, &pdblFinalVar);
		pc -> GetMean ( pdblFinalVar );
	} else if ( Rhs == 2 ) {
		// polychaos_getmean ( token , ovar )
		mean = pc -> GetMean ( ovar );
		// Returns the result
		nRows=1;
		nCols=1;
		allocMatrixOfDouble(pvApiCtx, Rhs+1, nRows, nCols, &pdblFinalVar);
		pdblFinalVar[0] = mean;
	}
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

