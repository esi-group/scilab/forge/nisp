
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

#include "nisp_gwsupport.hxx"
#include "nisp_va.h"
#include "nisp_gva.h"
#include "nisp_pc.h"

#include "nisp_RandomVariable_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// nispgw_GetTokenIndex --
//   Returns the value of the token corresponding to RHS #ivar.
//   Checks that the rhs variable #ivar is a matrix of double with 1 row and 1 column.
//   Returns 0 in case of error, returns 1 in case of regular run.
//
int nispgw_GetTokenIndex ( char * fname , int ivar , int * token,void* pvApiCtx )
{
	if (nispgw_GetOneIntegerArgument ( fname , ivar , token, pvApiCtx ) == 0 ) {
		return 0;
	}
	return 1;
}
/* ==================================================================== */

// 
// nispgw_token2RandomVariable --
//   Sets into rv the Random Variable which corresponds to the token.
//   Returns 0 in case of error, returns 1 in case of regular run.
//
int nispgw_token2RandomVariable( char * fname, int ivar , int token, RandomVariable ** rv)
{
	*rv = nisp_RandomVariable_map_getobject ( token );
	if (*rv==NULL) {
		Scierror(999,_("%s: Wrong randvar object %d in argument #%d.\n"),fname,token,ivar);
		return 0;
	}
	return 1;
}

// 
// nispgw_token2SetRandomVariable --
//   Sets into rv the Set Random Variable which corresponds to the token.
//   Returns 0 in case of error, returns 1 in case of regular run.
//
int nispgw_token2SetRandomVariable( char * fname, int ivar , int token, SetRandomVariable ** srv)
{
	*srv = nisp_SetRandomVariable_map_getobject ( token );
	if (*srv==NULL) {
		Scierror(999,_("%s: Wrong setrandvar object %d in argument #%d.\n"),fname,token,ivar);
		return 0;
	}
	return 1;
}

// 
// nispgw_token2PolynomialChaos --
//   Sets into rv the Set Random Variable which corresponds to the token.
//   Returns 0 in case of error, returns 1 in case of regular run.
//
int nispgw_token2PolynomialChaos( char * fname, int ivar , int token, PolynomialChaos ** pc)
{
	*pc = nisp_PolynomialChaos_map_getobject ( token );
	if (*pc==NULL) {
		Scierror(999,_("%s: Wrong polychaos object %d in argument #%d.\n"),fname,token,ivar);
		return 0;
	}
	return 1;
}
