
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// np = setrandvar_getsize ( token )
//   returns the number of experiments of the set random variable
//
int sci_setrandvar_getsize (char *fname, unsigned long fname_len)
{
	int token;
	SetRandomVariable * srv = NULL;
	int size;

	CheckInputArgument(pvApiCtx,1,1) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	nispgw_token2Setrandvar ( fname , 1 , token , &srv );
	size = srv->GetSize ( );
	nispgw_CreateLhsInteger ( 1 , size, pvApiCtx);
	return 0;
}
/* ==================================================================== */

