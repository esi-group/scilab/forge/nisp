
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


/* ==================================================================== */

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

#include "nisp_gwsupport.hxx" 
#include "nisp_RandomVariable_map.hxx" 

/* ==================================================================== */
// 
// token = randvar_new ( "Normale" , 1.0 , 0.5 ) 
//   creates a new random variable with law Normale 
//     and parameters 1.0 and 0.5.
// token = randvar_new ( "Exponentielle" , 1.0 ) 
//   creates a new random variable with law Exponentielle 
//     and parameter 1.0.
// token = randvar_new ( "Exponentielle" ) 
//   creates a new random variable with law Exponentielle 
//     and default parameter.
//
int sci_randvar_new (char *fname, unsigned long fname_len)
{
	int nRows, nCols;
	char ** stringdata = NULL;
	char * name;
	int token;
	double * parameter1 = NULL;
	double * parameter2 = NULL;
	double * mydata = NULL;
	RandomVariable * rv = NULL;
	double *pdblFinalVar = NULL; //SCILAB return Var
	double a , b;
	int* piAddr;	

	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,1,1) ;
	nispgw_AssertVarType(fname , 1 , sci_strings,pvApiCtx);
	// Get name of law
	//GetRhsVar( 1, MATRIX_OF_STRING_DATATYPE, &nRows,   &nCols,   &stringdata);
	getVarAddressFromPosition(pvApiCtx,1,&piAddr);	
	getAllocatedMatrixOfString(pvApiCtx,piAddr,&nRows,&nCols,&stringdata);	
	nispgw_AssertNRows ( fname , 1 , nRows , 1 );
	nispgw_AssertNColumns ( fname , 1 , nCols , 1 );
	// Get name
	name = stringdata[0];
	if ( (Rhs == 2) | (Rhs == 3) ) {
		//token = nisp_RandomVariable_map_new2 ( name , a );
		//token = nisp_RandomVariable_map_new3 ( name , a, b);
		nispgw_AssertVarType(fname , 2 , sci_matrix,pvApiCtx);
		// Get a
		//GetRhsVarMatrixDouble(2, &nRows, &nCols, &parameter1);
		getVarAddressFromPosition(pvApiCtx,2,&piAddr);	
		getMatrixOfDouble(pvApiCtx,piAddr,&nRows,&nCols,&parameter1);
		nispgw_AssertNRows ( fname , 2 , nRows , 1 );
		nispgw_AssertNColumns ( fname , 2 , nCols , 1 );
		a = parameter1[0];
	}
	if ( Rhs == 3 ) {
		//token = nisp_RandomVariable_map_new3 ( name , a, b);
		nispgw_AssertVarType(fname , 3 , sci_matrix, pvApiCtx);
		// Get b
		//GetRhsVarMatrixDouble(3, &nRows, &nCols, &parameter2);
		getVarAddressFromPosition(pvApiCtx,3,&piAddr);	
		getMatrixOfDouble(pvApiCtx,piAddr,&nRows,&nCols,&parameter2);
		nispgw_AssertNRows ( fname , 3 , nRows , 1 );
		nispgw_AssertNColumns ( fname , 3 , nCols , 1 );
		b = parameter2[0];
	}
	//
	// Create the object
	//
	if ( Rhs == 1 ) 
	{
		//token = nisp_RandomVariable_map_new1 ( name );
		rv = new RandomVariable( name );
	} 
	else if ( Rhs == 2 ) 
	{
		//token = nisp_RandomVariable_map_new2 ( name , a );
		rv = new RandomVariable( name , a );
	} 
	else	
	{
		//token = nisp_RandomVariable_map_new3 ( name , a, b);
		rv = new RandomVariable( name , a , b );
	}
	token = nisp_RandomVariable_map_add ( rv );
	nispgw_CreateLhsInteger ( 1 , token,pvApiCtx);
	return 0;
}
/* ==================================================================== */
