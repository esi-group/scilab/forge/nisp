// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 

/* ==================================================================== */
// 
// polychaos_buildsample ( token , type , np )
//   build an internal sampling of given type, with np simulations
//
int sci_polychaos_buildsample (char *fname,unsigned long fname_len)
{
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int np;
	char * type = NULL;
	int order;

	CheckInputArgument(pvApiCtx,3,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx );
	nispgw_GetOneChar ( fname , 2 , &type, pvApiCtx);
	nispgw_GetOneInteger ( fname , 3 , &np, pvApiCtx);
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	// Always order the sample.
	// This avoids errors from the user.
	order=1;
	pc-> BuildSample ( type , np , order );
	nispgw_CreateLhsInteger ( 1 , token, pvApiCtx );
	return 0;
}
/* ==================================================================== */

