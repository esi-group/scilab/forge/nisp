
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// polychaos_computeexp ( token , srv , method )
//   computes the coefficients of the polynomial chaos expansion
//     where srv is a setrandvar token and method is 
//     a string representing the computation algorithm
// polychaos_computeexp ( token , pc2 , invalue , varopt )
//   compute the coefficient based on another polynomial chaos and a reduced set of variables
//
int sci_polychaos_computeexp (char *fname,unsigned long fname_len)
{
	int token;
	PolynomialChaos * pc = NULL;
	int srvtoken;
	char * method;
	SetRandomVariable * srv = NULL;
	int pctoken2;
	int nRowsInvalue, nColsInvalue;
	int nColsVaropt;
	int nopt;
	double * inputvector = NULL;
	double * doublevaropt = NULL;
	int nx;
	int * intvaropt = NULL;
	PolynomialChaos * pc2 = NULL;
	double * inputvectorNX = NULL;
	int* piAddr;

	CheckInputArgument(pvApiCtx,3,4) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	if (Rhs==3) {
		// polychaos_computeexp ( token , srv , method )
		nispgw_gettoken ( fname , 2 , &srvtoken, pvApiCtx);
		nispgw_GetOneChar ( fname , 3 , &method, pvApiCtx);
	} 
	else if (Rhs==4) 
	{
		// polychaos_computeexp ( token , pc2 , invalue , varopt )
		// How to make the difference between this calling sequence and the previous one?
		nispgw_gettoken ( fname , 2 , &pctoken2, pvApiCtx);
		nispgw_AssertVarType(fname , 3 , sci_matrix, pvApiCtx );
		getVarAddressFromPosition(pvApiCtx,3,&piAddr);
		getMatrixOfDouble(pvApiCtx , piAddr, &nRowsInvalue, &nColsInvalue, &inputvector);
		//GetRhsVarMatrixDouble ( 3, &nRowsInvalue, &nColsInvalue, &inputvector);
		nispgw_AssertVarType(fname , 4 , sci_matrix, pvApiCtx);
		//GetRhsVarMatrixDouble ( 4, &nopt, &nColsVaropt, &doublevaropt);
		getVarAddressFromPosition(pvApiCtx,4,&piAddr);
		getMatrixOfDouble(pvApiCtx , piAddr, &nopt, &nColsVaropt, &doublevaropt);

	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if (Rhs==3) {
		nispgw_token2Setrandvar ( fname , 2 , srvtoken , &srv );
		pc -> ComputeChaosExpansion ( srv , method );
	}
	else if (Rhs==4) 
	{
		nx = pc-> GetDimensionInput();
		nispgw_AssertNRows    ( fname , 3 , nRowsInvalue , 1 );
		nispgw_AssertNColumns ( fname , 3 , nColsInvalue , nopt );
		nispgw_AssertNColumns ( fname , 4 , nColsVaropt , 1 );
		nispgw_token2Polychaos ( fname , 2 , pctoken2 , &pc2 );
		if ( (nopt <= 0) | (nopt > nx) ) {
			Scierror(999,_("%s: Inconsistent size of argument #%d and #%d: should have the same number of rows.\n"),fname,3,4);
			return 0;
		} 
		else 
		{
			intvaropt = (int *) malloc (nopt * sizeof (int));
			for(int i=0;i<nopt;i++) {
				nispgw_Double2Integer(fname, 4, doublevaropt[i],&(intvaropt[i]))
			}
			inputvectorNX = (double *) malloc ((nx+1) * sizeof (double));
			for(int i=1;i<nx;i++) {
				inputvectorNX[i] = inputvector[i-1];
			}
			pc-> ComputeChaosExpansion ( pc2 , inputvectorNX , intvaropt , nopt );
			free(intvaropt);
			free(inputvectorNX);
		} 
	}
	nispgw_CreateLhsInteger ( 1 , token, pvApiCtx );
	return 0;
}
/* ==================================================================== */

