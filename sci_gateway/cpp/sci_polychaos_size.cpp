
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// nb = polychaos_size () 
//   returns the number of objects currently in use
//
int sci_polychaos_size (char *fname,unsigned long fname_len)
{
	int m, n;
	double * mydata = NULL;
	int size;
	double *pdblFinalVar = NULL; //SCILAB return Var

	CheckInputArgument(pvApiCtx,0,0) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	size = nisp_PolynomialChaos_map_size ();
	// Returns the size as the result
	m=1;
	n=1;
	allocMatrixOfDouble(pvApiCtx,Rhs+1, m, n, &pdblFinalVar);
	pdblFinalVar[0] = size;
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

