
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 
#include "nisp_gva.h" 
#include "nisp_pc.h" 

/* ==================================================================== */
// 
// token = polychaos_new ( file )
//   Creates a new polychaos from a data file.
// token = polychaos_new ( srv , ny )
//   Creates a new PolynomialChaos based on given SetRandomVariable srv and number of output ny
// token = polychaos_new ( pc , nopt , varopt )
//   Create a new polychaos from another polychaos and an array of integers varopt with size nopt
//
int sci_polychaos_new (char *fname,unsigned long fname_len)
{
	int nRows, nCols;
	char ** stringdata = NULL;
	char * charfile;
	int token;
	double * mydata = NULL;
	double *pdblFinalVar = NULL; //SCILAB return Var
	int srvtoken;
	int pctoken;
	int ny;
	int nopt;
	double * doublevaropt = NULL;
	SetRandomVariable * srv = NULL;
	PolynomialChaos * pc = NULL;
	PolynomialChaos * pc2 = NULL;
	int * intvaropt = NULL;
	int *piAddr;
	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,1,1) ;
	if ( (Rhs == 1) ) 
	{
		// token = polychaos_new ( file )
		nispgw_GetOneChar ( fname , 1 , &charfile, pvApiCtx);
	} 
	else if ( (Rhs == 2) )
	{
		// token = polychaos_new ( srv , ny )
		nispgw_gettoken ( fname , 1 , & srvtoken, pvApiCtx);
		nispgw_GetOneInteger ( fname , 2 , &ny, pvApiCtx);
	} 
	else if ( (Rhs == 3) )
	{
		// token = polychaos_new ( pc , nopt , varopt )
		nispgw_gettoken ( fname , 1 , & pctoken, pvApiCtx);
		nispgw_GetOneInteger ( fname , 2 , &nopt, pvApiCtx);
		// Get varopt into doublevaropt
		nispgw_AssertVarType(fname , 3 , sci_matrix, pvApiCtx);
		getVarAddressFromPosition(pvApiCtx,3,&piAddr);
		getMatrixOfDouble(pvApiCtx, piAddr, &nRows, &nCols, &doublevaropt);		
		//GetRhsVarMatrixDouble(3, &nRows, &nCols, &doublevaropt);
		nispgw_AssertNRows ( fname , 3 , nRows , 1 );
		nispgw_AssertNColumns ( fname , 3 , nCols , nopt );
	}
	//
	// Create the object
	//
	if ( Rhs == 1 ) 
	{
		// token = polychaos_new ( file )
		pc = new PolynomialChaos ( charfile );
	} 
	else if ( Rhs == 2 ) 
	{
		// token = polychaos_new ( srv , ny )
		nispgw_token2Setrandvar ( fname , 1 , srvtoken , &srv );
		pc = new PolynomialChaos( srv , ny );
	} else	{
		// token = polychaos_new ( pc , nopt , doublevaropt )
		pc2 = nisp_PolynomialChaos_map_getobject ( pctoken );
		if (nopt > 0) {
			intvaropt = (int *) malloc (nopt * sizeof (int));
		}
		for(int i=0;i<nopt;i++) {
			nispgw_Double2Integer(fname,3,doublevaropt[i],&(intvaropt[i]))
		}
		pc = new PolynomialChaos ( pc2 , intvaropt , nopt );
		if (nopt > 0) {
			free(intvaropt);
			intvaropt = NULL;
		}
	}
	token = nisp_PolynomialChaos_map_add ( pc );
	// Returns the token as the result
	nRows=1;
	nCols=1;
	allocMatrixOfDouble(pvApiCtx,Rhs+1, nRows, nCols, &pdblFinalVar);
	pdblFinalVar[0] = token;
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */
