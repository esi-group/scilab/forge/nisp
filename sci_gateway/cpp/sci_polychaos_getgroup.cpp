
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// g = polychaos_getgroup ( token )
//   returns the indices of the variables in the current group.
//
int sci_polychaos_getgroup (char *fname,unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int groupsize;
	double * mydata = NULL;
	int * igroup = NULL;

	CheckInputArgument(pvApiCtx,1,1) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	groupsize = pc-> GetGroupSize();
	nRows=groupsize;
	nCols=1;
	allocMatrixOfDouble(pvApiCtx, Rhs+1, nRows, nCols, &pdblFinalVar);
	igroup = (int *) malloc (groupsize * sizeof (int));
	pc -> GetGroup ( igroup );
	for(int i = 0; i < groupsize; i++) {
		pdblFinalVar[i] = (double)igroup[i];
	}
	free(igroup);
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

