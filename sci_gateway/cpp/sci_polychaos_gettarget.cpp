
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// output = polychaos_gettarget ( token , k , j )
//   returns the output value for experiment #k and input #j, where k = 1, np and j=1,nx
// output = polychaos_gettarget ( token , k )
//   returns the output value for experiment #k and input #j, where k = 1, np
//   where output is a 1 x (nx) matrix
// output = polychaos_gettarget ( token )
//   returns the output matrix as a (np) x (nx) matrix
//
int sci_polychaos_gettarget (char *fname, unsigned long fname_len)
{
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int k;
	int j;
	int ny;
	int np;

	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	if ( ( Rhs == 2 ) | (Rhs == 3 ) ) {
		// output = polychaos_gettarget ( token , k )
		// output = polychaos_gettarget ( token , k , j )
		nispgw_GetOneInteger ( fname , 2 , &k, pvApiCtx );
	}
	if ( Rhs == 3 ) {
		// output = polychaos_gettarget ( token , k , j )
		nispgw_GetOneInteger ( fname , 3 , &j, pvApiCtx );
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if ( Rhs == 1 ) {
		// output = polychaos_gettarget ( token )
		ny = pc -> GetDimensionOutput ();
		np = pc -> GetSizeTarget ();
		// Get target
		allocMatrixOfDouble(pvApiCtx, Rhs+1, np, ny, &pdblFinalVar);
		for(int j=1;j<=ny;j++) {
			for(int k=1;k<=np;k++) {
				*(pdblFinalVar + (j-1) * np + (k-1)) = pc -> GetTarget ( k , j );
			}
		}
	} else if ( Rhs == 2 ) {
		ny = pc -> GetDimensionOutput ();
		// Get target
		allocMatrixOfDouble(pvApiCtx, Rhs+1, 1, ny, &pdblFinalVar);
		for(int j=1;j<=ny;j++) {
			pdblFinalVar[j-1] = pc -> GetTarget ( k , j );
		}
	} else if ( Rhs == 3 ) {
		// Get target
		allocMatrixOfDouble(pvApiCtx,Rhs+1, 1, 1, &pdblFinalVar);
		pdblFinalVar[0] = pc -> GetTarget ( k , j );
	}
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

