
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// polychaos_settarget ( token , k , j , output )
//   set the output value for experiment #k and output #j, where k = 1, np and j=1,ny
// polychaos_settarget ( token , k , output ) 
//   set the output #k with 1<= k<= np, where np is the number of simulations 
//   where output is a matrix with size 1 x (ny)
// polychaos_settarget ( token , output ) 
//   set the output matrix where output is a matrix with size (np) x (ny)
//
int sci_polychaos_settarget (char *fname,unsigned long fname_len)
{
	int m, n;
	int token;
	PolynomialChaos * pc = NULL;
	double * outputVector = NULL;
	int k;
	int j;
	double output;
	int ny;
	int np;
	int *piAddr;
	int piType;
	CheckInputArgument(pvApiCtx,2,4) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	if ( (Rhs == 2) ) {
		// polychaos_settarget ( token , output ) 
		//
		// Get output
		//
		getVarAddressFromPosition(pvApiCtx,2,&piAddr);
		getVarType(pvApiCtx,piAddr,&piType);	
		if ( piType!= sci_matrix )
		{
			Scierror(204,_("%s: Wrong type for input argument #%d: Double expected.\n"),fname,2);
			return 0;
		}
		getVarAddressFromPosition(pvApiCtx,2,&piAddr);
		getMatrixOfDouble(pvApiCtx, piAddr, &m, &n, &outputVector);
	}
	if ( (Rhs == 3) | (Rhs == 4) ) {
		// polychaos_settarget ( token , k , output ) 
		// polychaos_settarget ( token , k , j , output )
		nispgw_GetOneInteger ( fname , 2 , &k, pvApiCtx);
	}
	if ( (Rhs == 3) ) {
		// polychaos_settarget ( token , k , output ) 
		//
		// Get output
		//
		
		getVarAddressFromPosition(pvApiCtx,3,&piAddr);
		getVarType(pvApiCtx,piAddr,&piType);
		if ( piType != sci_matrix )
		{
			Scierror(204,_("%s: Wrong type for input argument #%d: Double expected.\n"),fname,3);
			return 0;
		}
		getVarAddressFromPosition(pvApiCtx,3,&piAddr);
		getMatrixOfDouble(pvApiCtx,piAddr,&m, &n, &outputVector);
	}
	else if ( (Rhs == 4) ) {
		// polychaos_settarget ( token , k , j , output )
		nispgw_GetOneInteger ( fname , 3 , &j, pvApiCtx);
		nispgw_GetOneDouble ( fname , 4 , &output, pvApiCtx);
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if ( (Rhs == 2) ) {
		// polychaos_settarget ( token , output ) 
		ny = pc -> GetDimensionOutput ();
		np = pc -> GetSizeTarget ();
		// Check that outputVector's size matches PolynomialChaos's np
		if ( m != np )
		{
			Scierror(999,_("%s: Wrong number of rows in argument #%d: found %d rows but %d row expected.\n"),fname,2, m , np );
			return 0;
		}
		if ( n != ny )
		{
			Scierror(999,_("%s: Wrong number of columns in argument #%d: found %d rows but %d row expected.\n"),fname,2 , n , ny );
			return 0;
		}
		for(int j=1;j<=ny;j++) {
			for(int k=1;k<=np;k++) {
				pc -> SetTarget ( k, j, *(outputVector + (j-1) * np + (k-1)) );
			}
		}
	} else if ( (Rhs == 3) ) {
		// polychaos_settarget ( token , k , output ) 
		ny = pc -> GetDimensionOutput ();
		// Check that outputVector's size matches PolynomialChaos's ny
		if ( m != 1 )
		{
			Scierror(999,_("%s: Wrong number of rows in argument #%d: found %d rows but 1 row expected.\n"),fname,3, m );
			return 0;
		}
		if ( n != ny )
		{
			Scierror(999,_("%s: Wrong number of columns in argument #%d: found %d rows but %d row expected.\n"),fname,3 , n , ny );
			return 0;
		}
		// Set target
		for(int j=1;j<=ny;j++) {
			pc -> SetTarget ( k, j, outputVector[j-1] );
		}
	} else if ( (Rhs == 4) ) {
		// polychaos_settarget ( token , k , j , output )
		// Set target
		pc -> SetTarget ( k, j, output );
	}
	// Returns the token as the result
	nispgw_CreateLhsInteger ( 1 , token,pvApiCtx);
	return 0;
}
/* ==================================================================== */

