
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "gw_nisp.h"
}

#include "sci_nisp_startup.hxx" 

/* ==================================================================== */

// nisp_shutdown ( )
//   shutdown the NISP library
int sci_nisp_shutdown (char *fname,unsigned long fname_len) {

	CheckInputArgument(pvApiCtx,0,0) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	if (sci_nisp_startup_flag==1) {
		// Nothing to do for now
		sci_nisp_startup_flag = 0;
	}
	return 0;
}
