
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
extern int C2F(dcopy) (int *_iSize, double *_pdblSrc, int *_piIncSrc, double *_pdblDest, int *_piDest);

}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 

/* ==================================================================== */
// 
// polychaos_getindextotal ( token , ivar , ovar )
//   returns the total index of the input variable which index is ivar, where 1<= ivar <= nx
//     and the output variable which index is ovar, where 1<= ovar <= ny.
// polychaos_getindextotal ( token , ivar )
//   returns the total index of the input variable which index is ivar, where 1<= ivar <= nx
//     and the output variable which index is ovar=1.
// polychaos_getindextotal ( token )
//   returns all total indices, as a (nx) x (ny) matrix
//
int sci_polychaos_getindextotal (char *fname, unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int ivar , ovar;
	double index;
	int nx , ny;
	double * mydata = NULL;
	double ** indMatrix;

	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	if ( ( Rhs == 2 ) | ( Rhs == 3 ) ) {
		// polychaos_getindextotal ( token , ivar )
		// polychaos_getindextotal ( token , ivar , ovar )
		nispgw_GetOneInteger ( fname , 2 , &ivar, pvApiCtx);
	}
	if ( Rhs == 3 ) {
		// polychaos_getindextotal ( token , ivar , ovar )
		nispgw_GetOneInteger ( fname , 3 , &ovar, pvApiCtx);
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if ( Rhs == 1 ) {
		// polychaos_getindextotal ( token )
		nx = pc-> GetDimensionInput();
		ny = pc-> GetDimensionOutput();
		nRows=nx;
		nCols=ny;
		// Allocate a double ** and put the index matrix in it.
		indMatrix = new double * [nx];
		for(int k = 0; k < nx; k++) {
			indMatrix[k] = new double[ny];
		}
		pc -> GetIndiceTotalOrder ( indMatrix );
		// Copy the double ** into the double *
		allocMatrixOfDouble(pvApiCtx,Rhs+1, nRows, nCols, &pdblFinalVar);
		// The main loop is based on a line by line browsing of the output matrix,
		// which is stored in Fortran order, i.e. column by column.
		// For each line #k, the routine dcopy 
		//   * reads the variable sampleMatrix[k] element by element (i.e. INCX = 1), 
		//   * writes the variable pdblFinalVar is column by column (i.e. INCY = ny).
		// The first element of the line #k of the output matrix is at index pdblFinalVar+k.
		int INCX = 1;
		int INCY = ny;
		// Fill Scilab's matrix with sample, row by row
		for(int k = 0; k < nx; k++) {
			C2F(dcopy)(&ny,indMatrix[k],&INCX,pdblFinalVar+k,&INCY);
		}
		for(int k = 0; k < nx; k++) {
			delete [] indMatrix[k];
		}
		delete [] indMatrix;
		LhsVar(1) = Rhs+1;
	} else if ( Rhs == 2 ) {
		// polychaos_getvariance ( token , ivar )
		index = pc -> GetIndiceTotalOrder ( ivar );
		nispgw_CreateLhsDouble ( 1 , index, pvApiCtx);
	} else if ( Rhs == 3 ) {
		// polychaos_getvariance ( token , ivar , ovar )
		index = pc -> GetIndiceTotalOrder ( ivar , ovar );
		nispgw_CreateLhsDouble ( 1 , index, pvApiCtx);
	}
	return 0;
}
/* ==================================================================== */

