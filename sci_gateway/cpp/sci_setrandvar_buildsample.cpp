
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// sample=setrandvar_buildsample ( token , token2 )
//   build a sampling from another set
// sample=setrandvar_buildsample ( token , name , np )
//   build a sampling with sampling method "name" and np samples
// sample=setrandvar_buildsample ( token , name , np , ne )
//   build a sampling with sampling method "name" and np samples, with selected ne samples
//   name = "MonteCarlo"
//   name = "Lhs"
//   name = "QmcSobol"
//   name = "Quadrature"
//   name = "Petras"
//   name = "SmolyakGauss"
//   name = "SmolyakTrapeze"
//   name = "SmolyakFejer"
//   name = "SmolyakClenshawCurtis"

//
int sci_setrandvar_buildsample (char *fname, unsigned long fname_len)
{
	int token;
	double * mydata = NULL;
	SetRandomVariable * srv = NULL;
	SetRandomVariable * srv2 = NULL;
	char * name = NULL;
	int np;
	int ne;
	int token2;
	int nx;
	int m;
	int n;
	double *pdblFinalVar = NULL; //SCILAB return Var

	CheckInputArgument(pvApiCtx,2,4) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	if ( (Rhs == 2) ) {
		nispgw_GetOneInteger ( fname , 2 , &token2, pvApiCtx);
	} else {
		nispgw_GetOneChar ( fname , 2 , &name, pvApiCtx);
		nispgw_GetOneInteger ( fname , 3 , &np, pvApiCtx);
		if ( (Rhs == 4) ) {
			nispgw_GetOneInteger ( fname , 4 , &ne, pvApiCtx);
		}
	}
	nispgw_token2Setrandvar ( fname , 1 , token , &srv );
	if ( (Rhs == 2) ) 
	{
		nispgw_token2Setrandvar ( fname , 2 , token2 , &srv2 );
		srv->BuildSample ( srv2 );
	}
	else if ( (Rhs == 3) ) 
	{
		srv->BuildSample ( name , np );
	}
	else if ( (Rhs == 4) ) 
	{
		srv->BuildSample ( name , np , ne );
	}
	// Get the sample
	nx = srv->GetDimension();
	np = srv->GetSize();
	m=np;
	n=nx;
	allocMatrixOfDouble(pvApiCtx,Rhs+1, m, n, &pdblFinalVar);
	for(int i=1;i<=nx;i++) {
		for(int k = 1; k <= np; k++) {
			*(pdblFinalVar + (i-1) * np + (k-1)) = srv->GetSample(k, i);
		}
	}
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

