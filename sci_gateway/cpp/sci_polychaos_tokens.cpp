
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "stdlib.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// tokens = polychaos_tokens () 
//   returns the list of objects currently in use
//
int sci_polychaos_tokens (char *fname,unsigned long fname_len)
{
	int m, n;
	int size;
	int * tokens = NULL;
	double * doubletokens = NULL;

	CheckInputArgument(pvApiCtx,0,0) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	size = nisp_PolynomialChaos_map_size ();
	if (size > 0) {
		tokens = (int *) malloc (size * sizeof (int));
		doubletokens = (double *) malloc (size * sizeof (double));
	}
	nisp_PolynomialChaos_map_tokens (tokens);
	for(int i = 0; i < size; i++) {
		doubletokens[i] = (double)tokens[i];
	}
	// Returns the matrix of tokens as the result
	m=1;
	n=size;
	//CreateVarFromPtr(Rhs+1,MATRIX_OF_DOUBLE_DATATYPE,&m,&n,&doubletokens);
	createMatrixOfDouble(pvApiCtx, Rhs+1,m,n,doubletokens);
	if (size > 0) {
		free(tokens);
		tokens = NULL;
		free(doubletokens);
		doubletokens = NULL;
	}
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

