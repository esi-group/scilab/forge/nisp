
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_RandomVariable_map.hxx" 


/* ==================================================================== */
// 
// value = randvar_getvalue ( token ) 
//   returns one value sampled from the current random variable
// value = randvar_getvalue ( token , token2 , invalue ) 
//   returns one value sampled from the current random variable and the random variable token2 with given value.
//
int sci_randvar_getvalue (char *fname, unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	int token2;
	double value;
	double *pdblFinalVar = NULL; //SCILAB return Var
	double *invalue = NULL;
	RandomVariable * rv = NULL;
	RandomVariable * rv2 = NULL;
	double * mydata = NULL;
	int* piAddr;

	/* check that we have from 1 to 3 parameter input */
	/* check that we have from 0 to 1 parameters output */
	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_AssertVarType(fname , 1 , sci_matrix, pvApiCtx);
	// Get token
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	if ( (Rhs == 1) ) 
	{
		// Perform the sampling
		nispgw_token2Randvar ( fname , 1 , token , &rv );
		value = rv->GetValue();
	} 
	else if ( (Rhs == 3) ) 
	{
		nispgw_AssertVarType(fname , 2 , sci_matrix, pvApiCtx);
		// Get random variable #2
		getVarAddressFromPosition(pvApiCtx,2,&piAddr);	
		getMatrixOfDouble(pvApiCtx,piAddr,&nRows,&nCols,&mydata);	
		//GetRhsVarMatrixDouble(2, &nRows, &nCols, &mydata);
		token2 = (int)mydata[0];
		nispgw_AssertVarType(fname , 3 , sci_matrix,pvApiCtx);
		// Get input value
		//GetRhsVarMatrixDouble(3, &nRows, &nCols, &invalue);
		getVarAddressFromPosition(pvApiCtx,3,&piAddr);	
		getMatrixOfDouble(pvApiCtx,piAddr,&nRows,&nCols,&invalue);
		
		nispgw_AssertNColumns ( fname , 3 , nCols , 1 );
		nispgw_AssertNRows ( fname , 3 , nRows , 1 );
		// Perform the sampling
		nispgw_token2RandomVariable ( fname , 1 , token , &rv );
		nispgw_token2RandomVariable ( fname , 2 , token2 , &rv2 );
		value = rv->GetValue( rv2 , invalue[0] );
	}
	nispgw_CreateLhsDouble ( 1 ,value, pvApiCtx);
	return 0;
}
/* ==================================================================== */

