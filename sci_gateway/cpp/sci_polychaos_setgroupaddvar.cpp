
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// polychaos_setgroupaddvar ( token , ivar )
//   adds an input variable ivar to the group, with 1<= ivar<= nx
//
int sci_polychaos_setgroupaddvar (char *fname,unsigned long fname_len)
{
	int token;
	PolynomialChaos * pc = NULL;
	int ivar;

	CheckInputArgument(pvApiCtx,2,2) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	nispgw_GetOneInteger ( fname , 2 , &ivar, pvApiCtx);
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	pc-> SetGroupAddVar ( ivar );
	nispgw_CreateLhsInteger ( 1 , token, pvApiCtx);
	return 0;
}
/* ==================================================================== */

