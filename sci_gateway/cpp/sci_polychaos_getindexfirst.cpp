
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// polychaos_getindexfirst ( token , ivar , ovar )
//   returns the first index order of the input variable which index is ivar, where 1<= ivar <= nx
//     and the output variable which index is ovar, where 1<= ovar <= ny.
// polychaos_getindexfirst ( token , ivar )
//   returns the first index order of the input variable which index is ivar, where 1<= ivar <= nx
//     and the output variable which index is ovar=1.
// polychaos_getindexfirst ( token )
//   returns all first index order indices, as a (nx) x (ny) matrix
//
int sci_polychaos_getindexfirst (char *fname,unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int ivar , ovar;
	double index;
	int nx , ny;
	double * mydata = NULL;

	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	if ( ( Rhs == 2 ) | ( Rhs == 3 ) ) {
        // polychaos_getindexfirst ( token , ivar )
		// polychaos_getindexfirst ( token , ivar , ovar )
		nispgw_GetOneInteger ( fname , 2 , &ivar, pvApiCtx);
	}
	if ( Rhs == 3 ) {
		// polychaos_getindexfirst ( token , ivar , ovar )
		nispgw_GetOneInteger ( fname , 3 , &ovar, pvApiCtx);
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if ( Rhs == 1 ) {
		// polychaos_getindexfirst ( token )
		nx = pc-> GetDimensionInput();
		ny = pc-> GetDimensionOutput();
		nRows=nx;
		nCols=ny;
		allocMatrixOfDouble(pvApiCtx, Rhs+1, nRows, nCols, &pdblFinalVar);
		for(int j = 1; j <= ny; j++) {
			for(int i = 1; i <= nx; i++) {
				*(pdblFinalVar + (j-1) * nx + (i-1)) = pc -> GetIndiceFirstOrder ( i , j  );
			}
		}

		LhsVar(1) = Rhs+1;
	} else if ( Rhs == 2 ) {
		// polychaos_getvariance ( token , ivar )
		index = pc -> GetIndiceFirstOrder ( ivar );
		nispgw_CreateLhsDouble ( 1 , index,pvApiCtx);
	} else if ( Rhs == 3 ) {
		// polychaos_getvariance ( token , ivar , ovar )
		index = pc -> GetIndiceFirstOrder ( ivar , ovar );
		nispgw_CreateLhsDouble ( 1 , index,pvApiCtx);
	}
	return 0;
}
/* ==================================================================== */

