
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 

/* ==================================================================== */
// 
// token = setrandvar_new ( )
//   creates a new set of random variables
// token = setrandvar_new ( n ) 
//   creates a new set of n uniformly distributed random variables
// token = setrandvar_new ( file ) 
//   creates a new set of random variables from a file
//
int sci_setrandvar_new (char *fname,unsigned long fname_len)
{
	int token;
	int nbvars;
	char * file = NULL;
	int* piAddr;
	int piType;
	SetRandomVariable * srv = NULL;

	CheckInputArgument(pvApiCtx,0,1) ;
	CheckOutputArgument(pvApiCtx,1,1) ;
	if ( Rhs == 1 ) 
	{
		getVarAddressFromPosition(pvApiCtx,1,&piAddr);
		getVarType(pvApiCtx,piAddr,&piType);
		if ( piType == sci_matrix )		
		{
			nispgw_GetOneInteger ( fname , 1 , &nbvars, pvApiCtx);
		} 
		else if ( piType == sci_strings )		
		{
			nispgw_GetOneChar ( fname , 1 , &file, pvApiCtx);
		} 
		else 
		{
			// The 1st parameter is unknown
			Scierror(204,_("%s: Wrong type for input argument #%d: Double or string expected.\n"),fname,1);
			return 0;
		}
	}
	if ( Rhs == 0 )
	{
		srv = new SetRandomVariable ( );
	} 
	else if ( Rhs == 1 ) 
	{
		getVarAddressFromPosition(pvApiCtx,1,&piAddr);
		getVarType(pvApiCtx,piAddr,&piType);
		if ( piType == sci_matrix ) 
		{
			srv = new SetRandomVariable ( nbvars );
		} else if ( piType == sci_strings ) 
		{
			srv = new SetRandomVariable ( file );
		}
	}
	token = nisp_SetRandomVariable_map_add ( srv );
	nispgw_CreateLhsInteger ( 1 , token,pvApiCtx );
	return 0;
}
/* ==================================================================== */
