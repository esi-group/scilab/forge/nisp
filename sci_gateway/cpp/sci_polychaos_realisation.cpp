
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// polychaos_realisation ( token )
//   Polynomial chaos is a random variable : a realisation of this random variable (GetOutput()).
//
int sci_polychaos_realisation (char *fname, unsigned long fname_len)
{
	int token;
	PolynomialChaos * pc = NULL;

	CheckInputArgument(pvApiCtx,1,1) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	pc -> Realisation ( );
	nispgw_CreateLhsInteger ( 1 , token,pvApiCtx);
	return 0;
}
/* ==================================================================== */

