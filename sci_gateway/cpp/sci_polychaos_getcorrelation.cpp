
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// polychaos_getcorrelation ( token , ovar1 , ovar2 )
//   returns the correlation of the output variables which index are ovar1 and ovar2, 
//     where 1<= ovar1, ovar2 <= ny.
// polychaos_getcorrelation ( token )
//   returns the correlation of the all output variables, as a (ny) x (ny) matrix
//
int sci_polychaos_getcorrelation (char *fname, unsigned long fname_len)
{
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int ovar1 , ovar2;
	int ny;

	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	// Get token
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	if ( Rhs == 2 ) {
		Scierror(999,_("%s: Wrong number of input arguments: 1 or 3 expected.\n"),fname);
		return 1;
	}
	if ( Rhs == 3 ) {
		// polychaos_getcorrelation ( token , ovar1 , ovar2 )
		nispgw_GetOneInteger ( fname , 2 , &ovar1, pvApiCtx);
		nispgw_GetOneInteger ( fname , 3 , &ovar2, pvApiCtx);
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if ( Rhs == 1 ) {
		// polychaos_getcorrelation ( token )
		ny = pc-> GetDimensionOutput();
		allocMatrixOfDouble(pvApiCtx, Rhs+1, ny, ny, &pdblFinalVar);
		for(int j=1;j<=ny;j++) {
			for(int i=1;i<=ny;i++) {
				*(pdblFinalVar + (j-1) * ny + (i-1)) = pc -> GetCorrelation ( i , j );
			}
		}
	} else if ( Rhs == 3 ) {
		// polychaos_getvariance ( token , ovar1 , ovar2 )
		allocMatrixOfDouble(pvApiCtx, Rhs+1, 1, 1, &pdblFinalVar);
		pdblFinalVar[0] = pc -> GetCorrelation ( ovar1 , ovar2 );
	}
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

