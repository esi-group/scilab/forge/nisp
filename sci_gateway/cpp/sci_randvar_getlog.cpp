
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_RandomVariable_map.hxx" 


/* ==================================================================== */
// 
// randvar_getlog (token) 
//   prints the object associated with token
//
int sci_randvar_getlog (char *fname, unsigned long fname_len)
{
	int token;
	double * mydata = NULL;
	RandomVariable * rv = NULL;
	double *pdblFinalVar = NULL; //SCILAB return Var

	CheckInputArgument(pvApiCtx,1,1) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	nispgw_token2Randvar ( fname , 1 , token , &rv );
	rv->GetLog();
	nispgw_CreateLhsInteger ( 1 , token, pvApiCtx);
	return 0;
}
/* ==================================================================== */

