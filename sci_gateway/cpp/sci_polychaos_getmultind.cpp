
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// polychaos_getmultind ( token )
//   prints the multiple indices
//
int sci_polychaos_getmultind (char *fname, unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;

	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	pc -> GetMultipleIndices ( );
	// Returns the token as result
	nRows=1;
	nCols=1;
	allocMatrixOfDouble(pvApiCtx,Rhs+1, nRows, nCols, &pdblFinalVar);
	pdblFinalVar[0] = token;
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

