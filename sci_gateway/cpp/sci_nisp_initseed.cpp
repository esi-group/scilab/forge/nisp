
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

/* ==================================================================== */


#include "nisp_gwsupport.hxx" 
#include "nisp_conf.h"


// nisp_initseed ( seed )
//  sets the seed for the uniform random number generator
int sci_nisp_initseed (char *fname, unsigned long fname_len) {
	int seedint;

	CheckInputArgument(pvApiCtx,1,1) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_GetOneIntegerArgument ( fname , 1 , &seedint, pvApiCtx );
	nisp_initseed ( seedint );
	return 0;
}
