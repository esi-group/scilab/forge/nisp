Non Intrusive Spectral Projection Toolbox

Purpose
=======

This module allows to perform sensitivity analysis. 
This is the analysis of the uncertainty in the 
output of a given model, depending on the uncertainty in its 
inputs. 

The analysis is based on chaos polynomials, which are orthogonal 
polynomials which are used as an approximation of the original model. 
Once the coefficients of the chaos polynomial are computed, the 
associated sensitivity indices are straightforward to get.

The internal design of the system is based on the following components :
 * nisp provides function to configure the global behaviour of the 
   toolbox. This allows to startup and shutdown the library, configure and
   quiery the verbose level or initialize the seed of the random number
   generator.
 * randvar is the class which allows to manage a random variable. 
   Various types of random variables are available, including uniform,
   normal, exponential, etc...
 * setrandvar, is the class which allows to manage a set of random 
   variables. Several methods are available to build a sampling from a set
   of random variables. We can use, for example, a Monte-Carlo sampling or
   a Sobol low discrepancy sequence. This feature allows to use the class
   as Design of Experiment tool (DOE).
 * polychaos is the class which allows to manage a polynomial chaos 
   expansion. The coefficients of the expansion are computed based on given
   numerical experiments which creates the association between the inputs
   and the outputs. Once computed, the expansion can be used as a regular
   function. The mean, standard deviation or quantile can also be directly
   retrieved.
The current toolbox provides an object-oriented approach of the C++ NISP library.

This toolbox has been created in the context of the 
OPUS project :

http://opus-project.fr/

within the workpackage 2.1.1 "Construction de meta-modeles".
This project has received funding (2008-2011) by Agence Nationale de la recherche :

http://www.agence-nationale-recherche.fr/

See in the help provided in the help/en_US directory of the 
toolbox for more information about its use.
Use cases are presented in the demos directory.

Features
--------

Main Features:

 * randvar: 
   * Manage various types of random variables
   * uniform, normal, exponential, log-normal
 * setrandvar: 
   * Manage various sampling methods for sets of random variables 
   * Monte-Carlo, Sobol Quasi-Random, Latin Hypercube Sampling, 
     LHS Max Min sampling, and various samplings based 
     on Smolyak Cubature points.
 * polychaos: 
   * Manage polynomial chaos expansion and get specific outputs
   * mean, variance, sensitivity indices, quantiles, Wilks quantiles, 
     correlation, etc...
   * Generate a stand-alone C source code which computes the output of 
     the polynomial chaos expansion.

Tutorials

 * nisp_theory � Introduction to PC decomposition.
 * setrandvar_tutorial � A tutorial for the designs from setrandvar.

Configuration Functions:

 * nisp_destroyall : Destroy all current objects.
 * nisp_getpath : Returns the path to the current module.
 * nisp_initseed : Sets the seed of the uniform random number generator.
 * nisp_printall : Prints all current objects.
 * nisp_shutdown : Shuts down the NISP toolbox.
 * nisp_startup : Starts up the NISP toolbox.
 * nisp_verboselevelget : Returns the current verbose level.
 * nisp_verboselevelset : Sets the current verbose level.

Sensitivity Analysis

 * nisp_bruteforcesa : Compute sensitivity indices by brute force.
 * nisp_plotsa : Plot sensitivity indices.
 * nisp_sobolsaAll : Compute sensitivity indices by Sobol, Ishigami, Homma.
 * nisp_sobolsaFirst : Compute sensitivity indices by Sobol, Ishigami, Homma.
 * nisp_sobolsaTotal : Compute sensitivity indices by Sobol, Ishigami, Homma.
 * nisp_srcsa : Compute SRC sensitivity indices.
   
Test functions:

 * nisp_ishigami : Returns the Ishigami function.
 * nisp_ishigamisa : Exact sensitivity analysis for the Ishigami function
 * nisp_product : Returns the value of the product function
 * nisp_productsa : Exact sensitivity analysis for the Product function
 * nisp_sum : Returns the value of the sum function
 * nisp_sumsa : Returns the sensitivity indices of the Sum function

Dependencies
------------

 * This module depends on the Scilab >=v5.4
 * This module depends on the apifun module (>=0.2).
 * This module depends on the specfun module (>=0.4).
 * This module depends on the distfun module (>=0.7).
 * This module depends on the stixbox module (>=2.2).

Authors
=======

 * Copyright (C) 2012-2014 - Michael Baudin
 * Copyright (C) 2008-2011 - Jean-Marc Martinez - CEA
 * Copyright (C) 2008-2011 - INRIA - Michael Baudin
 * Copyright (C) 2004-2006 - John Burkardt
 * Copyright (C) 2000-2001 - Knut Petras
 * Copyright (C) 2002 - Chong Gu

Forge
-----

http://forge.scilab.org/index.php/p/nisp/

ATOMS
-----

http://atoms.scilab.org/toolboxes/NISP

TODO
====

TODO list
 * let the user get the seed.
 * fix settarget , gettarget for the size of output
 * fix setinput for the size of input
 * fix getoutput for the size of ovar
 * connect the cleaner of the NISP_library
 * fix computeexp for the size of inputvector , intvaropt
 * create a setrandvar_getvalue(srv) based on randvar_getvalue ( rv ) 
 * create a setrandvar_getvalue(srv,srv2,value2) based on randvar_getvalue ( rv , rv2 , value2 )
 * remove the constructor pc = polychaos_new ( srv , ny ) and replace it by 
   pc = polychaos_new ( vartype , ny ), where vartype is a matrix of flints 
   containing the type of each variable.
   No: strings are clearer.
 * Remove these functions for index management :
  void GetMean(double * mean);
  void GetVariance( double * var );
  void GetIndiceFirstOrder( double ** ind );
  void GetIndiceTotalOrder( double ** ind );
  void GetGroupIndice( double * ind );
  void GetGroupIndiceInteraction( double * ind );
  void GetQuantile(double alpha, double * quantile );
  void GetQuantileWilks(double alpha, double beta, double * quantile );
  void GetInvQuantile(double seuil, double * quantile);
 * Create the values = randvar_getvalue(rv,nbshots) calling sequence 
   to avoid for loops.
 * Add unit tests for lognormalpdf, cdf, inv and exppdf, cdf, inv.
 * Get the LHSmaxmin Matlab implementation from Burkardt : 
   http://people.sc.fsu.edu/~jburkardt/m_src/ihs/ihs.m
 * TODO : remove "polychaos_getoutput" "sci_polychaos_getoutput"
   "polychaos_computeoutput" "sci_polychaos_computeoutput"
   "polychaos_setinput" "sci_polychaos_setinput"
   "sci_polychaos_getoutput.cpp"
   "sci_polychaos_computeoutput.cpp"
   "sci_polychaos_setinput.cpp"
 * Get the exact sensitivity indices for the product x1*x2 with x1=Normal and x2=Uniform.
 * Display the Petras sampling with increasing level.
 * Add the chaos polynomial theory in the theory section.
 * Add the computations from the gfunction, for the Dirichlet function.
 * Add the suggested map from the srvu random variable types to the 
   srvx random variable types, i.e. from the 
   uncertain variables to the stochastic basis of the chaos polynomial.
 * Make a separate module from the sobolsa function. Add a bruteforesa function.
 * Translate Matlab functions from http://sensitivity-analysis.jrc.ec.europa.eu/software/index.htm

Acknowledgements
----------------

 * Paul Beaucaire
 * Allan Cornet

Licence
-------

This toolbox is released under the terms of the GNU Lesser General Public License license :

http://www.gnu.org/copyleft/lesser.html

History
-------

 * The C++ version of LINPACK is based on the original Fortran 77 source code 
by Jack Dongarra, Jim Bunch, Cleve Moler, Pete Stewart. 

Jack Dongarra, Jim Bunch, Cleve Moler, Pete Stewart,
LINPACK User's Guide, SIAM, 1979,

 * The C++ version of BLAS is based on the original Fortran 77 source code 
by Charles Lawson, Richard Hanson, David Kincaid, Fred Krogh.

Charles Lawson, Richard Hanson, David Kincaid, Fred Krogh,
Algorithm 539: Basic Linear Algebra Subprograms for Fortran Usage,
ACM Transactions on Mathematical Software,
Volume 5, Number 3, September 1979, pages 308-323.

 * The C++ version of the Sobol sequence is based on the original Fortran 77 
source code by Bennett Fox.
 
Paul Bratley, Bennett Fox,
Algorithm 659: Implementing Sobol's Quasirandom Sequence Generator,
ACM Transactions on Mathematical Software,
Volume 14, Number 1, March 1988, pages 88-100.

Bennett Fox,
Algorithm 647: Implementation and Relative Efficiency of Quasirandom Sequence Generators,
ACM Transactions on Mathematical Software,
Volume 12, Number 4, December 1986, pages 362-376.
