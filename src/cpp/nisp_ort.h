//
// Copyright (C) 2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#ifndef _NISP_ORT_H_
#define _NISP_ORT_H_

void hermite(double  *phi, double x, int no);
void legendre(double *phi, double x, int no);
void laguerre(double *phi, double x, int no);



#endif /* _NISP_ORT_H_ */