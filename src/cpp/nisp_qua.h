//
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#ifndef _NISP_QUA_H_
#define _NISP_QUA_H_

void Quadrature(double x[], double w[], int nq, string type);
void Rec(int i,int n);
void QuadratureTensorise(double **yt, double *wt, double **xq, double **wq, int nx, int nq, int nt);
void Quadrature0(double x[], double w[], int nq, string type);

#endif /* _NISP_QUA_H_ */

