//
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#ifndef _NISP_UTIL_H_
#define _NISP_UTIL_H_


int nisp_calculP ( int,int);
int nisp_puissance2 ( int);
void nisp_readtarget2 ( double **target, int np, int ny, char *fichier);
void dindex(int n1, double *a, int *index, int down);

double nisp_normrnd(double mean, double std);
double nisp_lognrnd(double mean, double std);
double nisp_unifrnd(double min, double max);
double nisp_logunifrnd(double min, double max);
double nisp_exprnd(double rate);
double nisp_unifinv(double a, double b, double p);
double nisp_norminv(double mean, double std, double p);
double nisp_expinv(double rate, double p);
double nisp_logninv(double mean, double std, double p);
double nisp_logunifinv(double a, double b, double p);
double nisp_normcdf(double mean, double sigma, double x);
double nisp_logncdf(double mean, double sigma, double x);
double nisp_unifcdf(double a, double b, double x);
double nisp_logunifcdf(double a, double b, double x);
double nisp_expcdf(double rate, double x);
double nisp_expm1(double x);

#endif /* _NISP_UTIL_H_ */

