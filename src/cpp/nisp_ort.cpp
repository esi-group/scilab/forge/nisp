//
// Calcul des polynomes orthogonaux
//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#include <math.h>

#include "nisp_ort.h"

/*!
  \fn void Legendre(double *phi, double x, int no)
  \brief Polynome de Legendre
  \param phi Pointeur sur le tableau regroupant les monomes
  \param x Valeur de la variable
  \param no  Le degre maximal
*/
void legendre(double *phi, double x, int no) {
  int i;
  x=2.*x-1.;
  phi[0]=1.; phi[1]=x;
  for(i=1;i<no;i++)    phi[i+1]= ((2.*i+1.) * x * phi[i] - i * phi[i-1]) / (i+1.);
  for(i=0;i<=no;i++)   phi[i]  = phi[i] * sqrt(2.* i + 1.); /* normalisation */
}

double hercof(int n) {
  int i;
  double c1,c2;
  c1=pow(2.,(double) n);
  for(c2=1.,i=1;i<=n;i++) c2=c2*i;
  return(c1*c2);
}

/*!
  \fn void Hermite(double *phi, double x, int no)
  \brief Polynome d'Hermite
  \param phi Pointeur sur le tableau regroupant les monomes
  \param x Valeur de la variable
  \param no  Le degre maximal
*/
void hermite(double *phi, double x, int no) {
  int i;
  x=x/sqrt(2.);
  phi[0]=1.; phi[1]=2.*x;
  for(i=1;i<no;i++)    phi[i+1]= 2. * x * phi[i] - 2. * i * phi[i-1];
  for(i=0;i<=no;i++)   phi[i]=phi[i]/sqrt(hercof(i)); /* normalisation */
}

/*!
  \fn void Laguerre(double *phi, double x, int no)
  \brief Polynome d'Hermite
  \param phi Pointeur sur le tableau regroupant les monomes
  \param x Valeur de la variable
  \param no  Le degre maximal
*/
void laguerre(double *phi, double x, int no) {
  int i;
  phi[0]=1.; phi[1]=1.-x;
  for(i=1;i<no;i++)    phi[i+1]= ((2*i+1-x) * phi[i] - i * phi[i-1])/(i+1.);
}


