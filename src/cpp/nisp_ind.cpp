//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

#include "nisp_util.h"
#include "nisp_msg.h"
#include "nisp_ind.h"

static int *IndiceMultiple;
static int nx,no,kk;

void IndiceMultipleEvalRec(int r,int **indmul) {
  int i,i0;
  for(i0=no,i=1;i<r;i++) i0-=IndiceMultiple[i-1];
  for(IndiceMultiple[r-1]=i0;IndiceMultiple[r-1]>=0;IndiceMultiple[r-1]=IndiceMultiple[r-1]-1) {
    if((r+1)<nx) IndiceMultipleEvalRec(r+1,indmul);
    else {
      for(IndiceMultiple[nx-1]=no,i=1;i<nx;i++) IndiceMultiple[nx-1]-=IndiceMultiple[i-1];
      for(i=1;i<=nx;i++) indmul[kk][i]=IndiceMultiple[i-1];
      kk++;
    }
  }
}

int IndiceMultipleEval(int nnx, int noi, int nof, int **indmul) {
  int i;
  kk=0; /* kk = 0,1,...,p */
  nx=nnx;

  IndiceMultiple = new int[nx];

  if((nx>=1) && (noi>=0) && (nof>=noi)) {
    if(nx==1) { for(i=noi;i<=nof;i++) indmul[kk++][1]=i;}
    else {
      for(no=noi;no<=nof;no++) {
	for(i=1;i<=nx;i++) IndiceMultiple[i-1]=0;
	IndiceMultipleEvalRec(1,indmul);
      }
    }
    delete[] IndiceMultiple;
    return(kk-1);
  }
  else {
    ostringstream msg;
    msg << "Nisp(IndiceMultipleEval) : Error nx = " << nx << " noi = " << noi << " nof = " << nof << endl;
    nisp_error ( msg.str() );
    return(0);
  }
}

