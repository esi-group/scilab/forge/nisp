// 2009 - INRIA - Michael Baudin
// 2009 - Digiteo - Michael Baudin

#ifndef _NISP_FORMQUA_H_
#define _NISP_FORMQUA_H_

#include <vector>

#ifdef _MSC_VER
#pragma warning( disable : 4251 )
#endif

#include "nisp_va.h" // Class of random variables
using namespace std;


#ifdef _MSC_VER
	#if LIBNISP_EXPORTS 
		#define NISP_IMPORTEXPORT __declspec (dllexport)
	#else
		#define NISP_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define NISP_IMPORTEXPORT
#endif


// Class to manage a 1-dimensional quadrature formula.

class NISP_IMPORTEXPORT FormuleQuadrature {
public:
    // The type of formula
	string type;
    // The level of the formula
	int niveau;
	// For each level, the number of experiments
	// np[0 ... niveau-1]
	int *np;    
	// For each level, the abcissas
	// x[i=0 ... niveau-1][0,...,np[i]]
	double **x; 
	// For each level, the weight
	// w[i=0 ... niveau-1][0,...,np[i]]
	double **w; 
public:
	FormuleQuadrature(string type_va, string type_quadrature, int level);
	~FormuleQuadrature();
};

#endif /* _NISP_FORMQUA_H_ */
