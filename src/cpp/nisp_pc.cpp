//
// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <cstring>
#include <cmath> // necessaire a l'utilisation de sqrt()
#include <stdlib.h>

#include "linpack_d.h"

#include "nisp_pc.h"
#include "nisp_util.h"
#include "nisp_ort.h"
#include "nisp_math.h"
#include "nisp_ind.h"
#include "nisp_msg.h"
#include "nisp_inv.h"

#pragma warning(disable : 4996)

using namespace std;

#define HERMITE  1
#define LEGENDRE 2
#define LAGUERRE 3

void nisp_coefficientsregression ( double **beta, double *Z, double **y, int p, int np, int ny);

// ===========
// Methodes PolynomialChaos
// ===========


PolynomialChaos::PolynomialChaos(SetRandomVariable *gva, int nny) {
	int i;
	// Set all pointers to NULL
	gpx = NULL;
	x = NULL;
	y = NULL;
	sample = NULL;
	sampleindx = NULL;
	target = NULL;
	phi = NULL;
	psi = NULL;
	indmul = NULL;
	beta = NULL;
	moyenne = NULL;
	variance = NULL;
	indices = NULL;
	indices_globaux = NULL;
	indices_setvar = NULL;
	groupe = NULL;
	rank = NULL;
	// Set all integers to 0
	nx = 0;
	ni = 0;
	no = 0;
	p = 0;
	ny = 0;
	np = 0;

	if(gva->nx<1) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::PolynomialChaos) : size of set random variables is < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	nx=gva->nx;
	for(i=0;i<nx;i++) {
		if(gva->va[i]->type=="Normale")       
		{
			typol.push_back("Hermite"); 
			funpol.push_back(HERMITE); 
		}
		else if(gva->va[i]->type=="Uniforme")      
		{
			typol.push_back("Legendre");
			funpol.push_back(LEGENDRE);
		}
		else if(gva->va[i]->type=="Exponentielle") 
		{
			typol.push_back("Laguerre");
			funpol.push_back(LAGUERRE);
		}
		else {
			ostringstream msg;
			msg << "Nisp(PolynomialChaos::PolynomialChaos) : law  " << gva->va[i]->type << " is not available" << endl;
			nisp_error ( msg.str() );
			return;
		}
	}
	// fonction de repartition, quantiles, ...
	gpx = new SetRandomVariable();
	for(i=0;i<nx;i++) {
		gpx->AddRandomVariable(new RandomVariable(gva->va[i]->type));
	}

	no=0; // degre
	p=0;  // nombre de coefficients (p+1)
	ny=nny;
	x=dvector(nx+1);
	y=dvector(ny+1);
	moyenne=dvector(ny+1);
	variance=dvector(ny+1);
	groupe=ivector(nx+1);
	this->SetGroupEmpty();
	ni=nisp_puissance2(nx)-1; // le nombre d'indices (ni=2**nx-1)
	indices=dmatrix(ny+1,ni+1);
	indices_globaux=dmatrix(ny+1,nx+1);
	indices_setvar=imatrix(ni+1,nx+1);
}

PolynomialChaos::~PolynomialChaos() {
	this -> FreeMemory();
}

void PolynomialChaos::Realisation() {
	for(int i=1;i<=nx;i++) {
		x[i]=gpx->va[i-1]->GetValue();
	}
	ComputeOutput(x);
}

void PolynomialChaos::GetLog() {
	ostringstream msg;
	msg << "************************************" << endl;
	msg << "Nisp(PolynomialChaos::GetLog) for PC" << endl;
	for(int i=0;i<nx;i++) msg << typol[i] << endl;
	msg << "************************************" << endl;
	nisp_message ( msg.str() );
}
void PolynomialChaos::SetDimensionOutput(int nny) {
	if(nny<1) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::SetDimensionOutput) : number of output " << nny << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	// TODO : once indices_setvar is fixed (see below), put all this into a single function FreeMemoryNx, and connect FreeMemory to it.
	free_dvector(y);
	y = NULL;
	free_dvector(moyenne);
	moyenne = NULL;
	free_dvector(variance);
	variance = NULL;
	free_dmatrix(indices,ny+1);
	indices = NULL;
	free_dmatrix(indices_globaux,ny+1);
	indices_globaux = NULL;
	free_imatrix(indices_setvar,ni+1);
	indices_setvar = NULL;

	ny=nny;
	y=dvector(ny+1);
	moyenne=dvector(ny+1);
	variance=dvector(ny+1);
	indices=dmatrix(ny+1,ni+1);
	indices_globaux=dmatrix(ny+1,nx+1);
	indices_setvar=imatrix(ni+1,nx+1); // TODO : why to unalloc/realloc indices_setvar ? It depends only on nx (since ni=f(nx)=2**nx-1) and nx never changes (because it is set at creation).
}

PolynomialChaos::PolynomialChaos(char *fichier) {
	// Set all pointers to NULL
	gpx = NULL;
	x = NULL;
	y = NULL;
	sample = NULL;
	sampleindx = NULL;
	target = NULL;
	phi = NULL;
	psi = NULL;
	indmul = NULL;
	beta = NULL;
	moyenne = NULL;
	variance = NULL;
	indices = NULL;
	indices_globaux = NULL;
	indices_setvar = NULL;
	groupe = NULL;
	rank = NULL;
	// Set all integers to 0
	nx = 0;
	ni = 0;
	no = 0;
	p = 0;
	ny = 0;
	np = 0;
	// Read the file
	ifstream entree(fichier,ios::in);
	if(!entree) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::PolynomialChaos) : problem when open file " << fichier << endl;
		nisp_error ( msg.str() );
		return;
	}
	string str, type; int nnx,nno,pp,nny;
	entree >> str >> nnx;
	nx=nnx;
	int i,j,k;
	gpx = new SetRandomVariable(); // fonction de repartition, quantiles, ...
	for(i=1;i<=nnx;i++) {
		entree >> type;
		if(type=="Hermite")            
		{
			typol.push_back("Hermite"); 
			funpol.push_back(HERMITE);
			gpx->AddRandomVariable(new RandomVariable("Normale"));
		}
		else if(type=="Legendre")      
		{
			typol.push_back("Legendre");
			funpol.push_back(LEGENDRE);
			gpx->AddRandomVariable(new RandomVariable("Uniforme"));
		}
		else if(type=="Laguerre")      
		{
			typol.push_back("Laguerre");
			funpol.push_back(LAGUERRE);
			gpx->AddRandomVariable(new RandomVariable("Exponentielle"));
		}
		else 
		{
			ostringstream msg;
			
			msg << "Nisp(PolynomialChaos::PolynomialChaos) : law  " << type << " is not available" << endl;
			nisp_error ( msg.str() );
			return;
		}
	}

	entree >> str >> nno; no=nno;
	entree >> str >> pp;  p=pp;
	entree >> str >> nny; ny=nny;

	x=dvector(nx+1);
	y=dvector(ny+1);
	moyenne=dvector(ny+1);
	variance=dvector(ny+1);
	groupe=ivector(nx+1);
	ni=nisp_puissance2(nx)-1; // le nombre d'indices (ni=2**nx-1)
	indices=dmatrix(ny+1,ni+1);
	indices_globaux=dmatrix(ny+1,nx+1);
	indices_setvar=imatrix(ni+1,nx+1);

	phi=dmatrix(nx+1,no+1); 
	psi=dvector(p+1);
	indmul=imatrix(p+1,nx+1); 
	beta=dmatrix(ny+1,p+1);
	// Indices multiples
	IndiceMultipleEval(nx,0,no,indmul);
	// Lecture des coefficients
	for(j=1;j<=ny;j++) {
		entree >> str;
		for(k=0;k<=p;k++) {
			entree >> beta[j][k];
		}
	}
	SetAnova();
	entree.close();
}

// Utilitaire pour le calcul des esperances et variances conditionnelles
// en optimisation robuste
// *pc est le polynome construit a partir du polynome global regroupant
// variables de conception et variables aleatoires
// *ind est l'indice multiple du polynome global
// *groupe designe le groupe des variables aleatoires (complementaires
// des variables de conception
// retourne la valeur liant le multi-indice du polynome global au multi-indice
// du polynome reduit
// Remarque : groupe[0,1,....] on commence a 0
int pcond_rank_indice(int *ind, PolynomialChaos * pc, int * groupe) {
	int i,k,flag;
	for(k=0;k<=pc->p;k++) {
		flag=1;
		for(i=1;i<=pc->nx;i++) {
			if(ind[groupe[i-1]]!=pc->indmul[k][i]) flag=0;
		}
		if(flag) return(k);
	}
	if(k==pc->p+1) {
		// echec
		ostringstream msg;
		
		msg << "Nisp(pcond_rank_indice) : error on the rank = " << k << endl;
		nisp_error ( msg.str() );
		return(-1);
	}
	return(-1);
}

PolynomialChaos::PolynomialChaos(PolynomialChaos *pc, int *var_opt, int nopt) {
	// Set all pointers to NULL
	gpx = NULL;
	x = NULL;
	y = NULL;
	sample = NULL;
	sampleindx = NULL;
	target = NULL;
	phi = NULL;
	psi = NULL;
	indmul = NULL;
	beta = NULL;
	moyenne = NULL;
	variance = NULL;
	indices = NULL;
	indices_globaux = NULL;
	indices_setvar = NULL;
	groupe = NULL;
	rank = NULL;
	// Set all integers to 0
	nx = 0;
	ni = 0;
	no = 0;
	p = 0;
	ny = 0;
	np = 0;
	// Check inputs
	if( (pc->nx <= nopt) || (nopt<1)) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::PolynomialChaos) : error on the number of optimization variables = " << nopt << endl;
		nisp_error ( msg.str() );
		return;
	}

	// On definit le groupe var_sto complementaire au groupe var_opt
	int i,j,k,flag,nsto,*var_sto;
	nsto = pc->nx - nopt;
	var_sto=ivector(nsto);
	for(k=0,i=1;i<=pc->nx;i++) {
		flag=1;
		for(j=0;j<nopt;j++) {
			if(var_opt[j]==i) {
				flag=0;
			}
		}
		if(flag) {
			var_sto[k++]=i;
		}
	}

	for(i=0;i<nsto;i++) {
		if( (var_sto[i]<1) || (var_sto[i]>pc->nx)) {
			ostringstream msg;
			
			msg << "Nisp(PolynomialChaos::PolynomialChaos) : error on the set of variables " << endl;
			nisp_error ( msg.str() );
			return;
		}
	}
	gpx = new SetRandomVariable(); // fonction de repartition, quantiles, ...
	for(i=0;i<nsto;i++) {
		if(pc->typol[var_sto[i]-1]=="Hermite")            
		{
			typol.push_back("Hermite"); 
			funpol.push_back(HERMITE);
			gpx->AddRandomVariable(new RandomVariable("Normale"));
		}
		else if(pc->typol[var_sto[i]-1]=="Legendre")      
		{
			typol.push_back("Legendre");
			funpol.push_back(LEGENDRE);
			gpx->AddRandomVariable(new RandomVariable("Uniforme"));
		}
		else if(pc->typol[var_sto[i]-1]=="Laguerre")      
		{
			typol.push_back("Laguerre");
			funpol.push_back(LAGUERRE);
			gpx->AddRandomVariable(new RandomVariable("Exponentielle"));
		}
		else {
			ostringstream msg;
			
			msg << "Nisp(PolynomialChaos::PolynomialChaos) : law  " << pc->typol[var_sto[i]-1] << " is not available" << endl;
			nisp_error ( msg.str() );
			return;
		}
	}

	nx=nsto;
	no=0; // degre
	p=0;  // nombre de coefficients (p+1)
	ny=pc->ny;
	x=dvector(nx+1);
	y=dvector(ny+1);
	moyenne=dvector(ny+1);
	variance=dvector(ny+1);
	groupe=ivector(nx+1);
	ni=nisp_puissance2(nx)-1; // le nombre d'indices (ni=2**nx-1)
	indices=dmatrix(ny+1,ni+1);
	indices_globaux=dmatrix(ny+1,nx+1);
	indices_setvar=imatrix(ni+1,nx+1);

	no=pc->no;
	p=nisp_calculP(nx,no);
	phi=dmatrix(nx+1,no+1);
	psi=dvector(p+1);
	indmul=imatrix(p+1,nx+1);
	beta=dmatrix(ny+1,p+1);
	// Indices multiples
	IndiceMultipleEval(nx,0,no,indmul);

	rank=ivector(pc->p+1);
	int pcond_rank_indice(int *,PolynomialChaos *,int *);
	for(int k=0;k<=pc->p;k++) {
		rank[k]=pcond_rank_indice(pc->indmul[k],this,var_sto);
	}

	free_ivector(var_sto);
	var_sto = NULL;
}

void PolynomialChaos::SetDegree(int nno) {
	if(nno<0) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::SetDegree) : degree  " << nno << " is < 0" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(nx<1) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::SetDegree) : stochastic dimension  " << nx << " is < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(no) {
		free_dmatrix(phi,nx+1);
		phi = NULL;
		free_dvector(psi);
		psi = NULL;
		free_imatrix(indmul,p+1);
		indmul = NULL;
		free_dmatrix(beta,ny+1);
		beta = NULL;
	}
	no=nno;
	p=nisp_calculP(nx,no);
	phi=dmatrix(nx+1,no+1);
	psi=dvector(p+1);
	indmul=imatrix(p+1,nx+1);
	beta=dmatrix(ny+1,p+1);
	// Indices multiples
	IndiceMultipleEval(nx,0,no,indmul);
}

void PolynomialChaos::coefficients_integration (SetRandomVariable *gva) {
	if(this->np != gva->np) {
		ostringstream msg;
		msg << "Nisp(coefficients_integration) : output number of PC'sample " << this->np << " != output number of sample = " << gva->np << endl;
		nisp_error ( msg.str() );
		return;
	}

	if ((gva->typesampling!="Quadrature") && 
		(gva->typesampling!="Petras") && 
		(gva->typesampling!="SmolyakGauss") && 
		(gva->typesampling!="SmolyakTrapeze") && 
		(gva->typesampling!="SmolyakFejer") && 
		(gva->typesampling!="SmolyakClenshawCurtis")) {
		ostringstream msg;
		msg << "Nisp(coefficients_integration) : integration is not compatible with sample type : " << gva->typesampling << endl;
		msg << "Nisp(coefficients_integration) : compatible sample types are Quadrature, Petras, SmolyakGauss, SmolyakTrapeze, SmolyakFejer, SmolyakClenshawCurtis."<< endl;
		nisp_error ( msg.str() );
		return;
	}

	if(gva->degre_max < this->no) {
		ostringstream msg;
		msg << "Nisp(coefficients_integration) : degree of PC " << this->no << " > maximal value = " << gva->degre_max << endl;
		nisp_error ( msg.str() );
		return;
	}
	int i,j,k; 
	double * w; 
	w=gva->w; 
	double ** X = gva->x;
	for(k=0;k<=this->p;k++) {
		for(j=1;j<=this->ny;j++) {
			this->beta[j][k]=0.;
		}
	}
	for(i=1;i<=gva->np;i++) {
		this->PropagateInput(X[i]);
		for(k=0;k<=this->p;k++) {
			for(j=1;j<=this->ny;j++) {
				this->beta[j][k]+=this->target[i][j]*this->psi[k]*w[i];
			}
		}
	}
}

void PolynomialChaos::coefficients_regression(SetRandomVariable *gva) {
	if(	(gva->typesampling!="Lhs")&&
		(gva->typesampling!="LhsMaxMin")&&
		(gva->typesampling!="QmcSobol")&&
		(gva->typesampling!="MonteCarlo")) {
			ostringstream msg;
			
			msg << "Nisp(coefficients_regression) : regression is not compatible with sample type : " << gva->typesampling << endl;
			msg << "Nisp(coefficients_regression) : compatible sample types are Lhs, LhsMaxMin, QmcSobol, MonteCarlo."<< endl;
			nisp_error ( msg.str() );
			return;
	}
	int np=gva->np;  // nombre de simulations
	int p =this->p;
	// matrice des regresseurs (Z'Z)=matrice d'information
	double * Z = NULL;
	Z = dvector(np * (p+1));
	for(int i=1;i<=np;i++) {
		this->PropagateInput(gva->x[i]);
		for(int k=1;k<=p+1;k++) {
			Z[(i-1)+(k-1)*np]=this->psi[k-1];
		}
	}
	nisp_coefficientsregression(this->beta,Z,this->target,p,np,this->ny);
	free_dvector(Z);
	Z = NULL;
}

void PolynomialChaos::ComputeChaosExpansion(PolynomialChaos * pc, double *input, int *var_opt, int nopt) {
	int i,j,k;
	for(i=1;i<=nopt;i++) {
		j=var_opt[i-1];
		if(pc->funpol[j-1]==HERMITE)        
		{
			hermite(pc->phi[j], input[i],pc->no);
		}
		else if (pc->funpol[j-1]==LEGENDRE) 
		{
			legendre(pc->phi[j],input[i],pc->no);
		}
		else 
		{
			laguerre(pc->phi[j],input[i],pc->no);
		}
	}
	// calcul du vecteur psi
	for(k=0;k<=pc->p;k++) {
		for(pc->psi[k]=1.,i=1;i<=nopt;i++) 
		{
			j=var_opt[i-1];
			pc->psi[k]=pc->psi[k]*pc->phi[j][pc->indmul[k][j]];
		}
	}
	for(j=1;j<=ny;j++) 
	{
		for(k=0;k<=p;k++)        
		{
			beta[j][k]=0;
		}
		for(k=0;k<=pc->p;k++)    
		{
			beta[j][rank[k]] += pc->psi[k] * pc->beta[j][k];
		}
	}
	SetAnova();
}

void PolynomialChaos::ComputeChaosExpansion(SetRandomVariable *gva, char * Methode) {
	string smethod;
	smethod.assign(Methode);
	this->ComputeChaosExpansion ( gva , smethod );
}

void PolynomialChaos::ComputeChaosExpansion(SetRandomVariable *gva, string Methode) {
	if(Methode=="Integration") {
		this->coefficients_integration(gva);
	}
	else if(Methode=="Regression") {
		this->coefficients_regression(gva);
	}
	else {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::ComputeChaosExpansion) :  method " << Methode << " is not known" << endl;
		nisp_error ( msg.str() );
		return;
	}
	SetAnova();
}
// TODO : put new lines at the end of the line to make the file human-readable.
void PolynomialChaos::Save(char *fichier) {
	int j,k;
	ofstream sortie(fichier,ios::out);
	if(!sortie) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::Save) :  problem when open file " << fichier << endl;
		nisp_error ( msg.str() );
		return;
	}
	sortie.setf(ios::scientific, ios::floatfield);
	sortie.precision(6);
	sortie << "nx= " << nx << " ";
	for(j=1;j<=nx;j++) {
		sortie << typol[j-1] << " ";
	}
	sortie << "no= " << no << " p= "  << p << " ny= " << ny;
	for(j=1;j<=ny;j++) {
		sortie << " Coefficients[" << j << "]= "; 
		for(k=0;k<=p;k++) {
			sortie << beta[j][k] << " ";
		}
	}
	sortie << endl;
	sortie.close();
}

// Calcul de la moyenne, de la variance, des indices de sobol (ANOVA) et des indices globaux
void PolynomialChaos::SetAnova() {
	// Decomposition ANOVA (indice[0]=variance)
	int i,j,k,ip;
	for(j=1;j<=ny;j++) {
		for(i=1;i<=ni;i++) {
			indices[j][i]=0.;
		}
	}
	for(j=1;j<=ny;j++) {
		for(i=1;i<=nx;i++) {
			indices_globaux[j][i]=0.;
		}
	}
	for(i=1;i<=ni;i++) {
		for(j=1;j<=nx;j++) {
			indices_setvar[i][j]=0;
		}
	}
	for(k=0;k<=p;k++) {
		for(ip=0,i=1;i<=nx;i++) { 
			if(indmul[k][i]) {
				ip=ip+nisp_puissance2(i-1); 
			}
		}
		if(ip) {
			for(j=1;j<=ny;j++) {
				indices[j][ip]+=(beta[j][k]*beta[j][k]);
			}
			for(i=1;i<=nx;i++) { // Groupe de variables
				if(indmul[k][i]) {
					indices_setvar[ip][i]=1; 
				}
			}
		}
	}
	// Indices globaux
	for(j=1;j<=ny;j++) {
		for(k=0;k<=p;k++) {
			for(i=1;i<=nx;i++) {
				if(indmul[k][i]) {
					indices_globaux[j][i]+=(beta[j][k]*beta[j][k]);
				}
			}
		}
	}
	// Calcul de la moyenne
	for(j=1;j<=ny;j++) {
		moyenne[j]=beta[j][0];
	}
	// Calcul de la variance
	for(j=1;j<=ny;j++) {
		for(variance[j]=0.,k=1;k<=p;k++) {
			variance[j]+=(beta[j][k]*beta[j][k]);
		}
	}
	// Normalisation par la variance -> indices
	for(j=1;j<=ny;j++) {
		if(variance[j]) {
			for(i=1;i<=ni;i++) {
				indices[j][i]=indices[j][i]/variance[j];
			}
		}
	}
	for(j=1;j<=ny;j++) {
		if(variance[j]) {
			for(i=1;i<=nx;i++) {
				indices_globaux[j][i]=indices_globaux[j][i]/variance[j];
			}
		}
	}
}

// Moyenne
double PolynomialChaos::GetMean(int j) {
	if (j<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetMean) :  the rank of output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetMean) :  the rank of output " << j << " > number of output = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	return(moyenne[j]);
}

// Moyenne
// TODO : fix that damn index problem !
void PolynomialChaos::GetMean( double * mean ) {
	for(int j=1;j<=ny;j++) {
		mean[j-1] = moyenne[j];
	}
}

// Variance
double PolynomialChaos::GetVariance(int j) {
	if (j<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetVariance) :  rank of output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetVariance) :  rank of output " << j << " > number of output = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	return(variance[j]);
}

// Variance
void PolynomialChaos::GetVariance( double * var ) {
	for(int j=1;j<=ny;j++) {
		var[j-1] = variance[j];
	}
}

double PolynomialChaos::GetCovariance(int i, int j) {
	if (i<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetCovariance) :  rank of the first variable " << i << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (i>ny) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetCovariance) :  rank of the first variable " << i << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetCovariance) :  rank of the second variable " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetCovariance) :  rank of the second variable " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	double cov; 
	int k;
	cov = 0.;
	for(k=1;k<=p;k++) {
		cov+=beta[i][k]*beta[j][k];
	}
	return(cov);
}

double PolynomialChaos::GetCorrelation(int i, int j) {
	if (i<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetCorrelation) :  rank of the first variable " << i << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (i>ny) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetCorrelation) :  rank of the first variable " << i << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetCorrelation) :  rank of the second variable " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetCorrelation) :  rank of the second variable " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	double corr;
	corr = GetCovariance(i,j)/sqrt(variance[i]*variance[j]);
	return(corr);
}


double PolynomialChaos::GetIndiceFirstOrder(int i, int j) {
	if (i<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetIndiceFirstOrder) :  rank of the variable " << i << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (i>nx) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetIndiceFirstOrder) :  rank of the variable " << i << " > stochastic dimension = " << nx << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetIndiceFirstOrder) :  rank of the output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetIndiceFirstOrder) :  rank of the output " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	int ii=nisp_puissance2(i-1);
	return(indices[j][ii]);
}
void PolynomialChaos::GetIndiceFirstOrder( double ** ind ) {
	int ii;
	for(int i=1;i<=nx;i++) {
		ii=nisp_puissance2(i-1);
		for(int j=1;j<=ny;j++) {
			ind[i-1][j-1] = indices[j][ii];
		}
	}
}

double PolynomialChaos::GetIndiceTotalOrder(int i, int j) {
	if (i<1)  {
		ostringstream msg;
		msg << "Nisp(PolynomialChaos::GetCorrelation) :  rank of the variable " << i << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (i>nx) {
		ostringstream msg;
		msg << "Nisp(PolynomialChaos::GetCorrelation) :  rank of the variable " << i << " > stochastic dimension = " << nx << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j<1)  {
		ostringstream msg;
		msg << "Nisp(PolynomialChaos::GetCorrelation) :  rank of the output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::GetCorrelation) :  rank of the output " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	return(indices_globaux[j][i]);
}

void PolynomialChaos::GetIndiceTotalOrder( double ** ind ) {
	for(int i=1;i<=nx;i++) {
		for(int j=1;j<=ny;j++) {
			ind[i-1][j-1] = indices_globaux[j][i];
		}
	}
}
// affiche le tableau des indices
void PolynomialChaos::GetMultipleIndices() {
	int k,j;
	ostringstream msg;
	for(k=0;k<=p;k++) {
		for(j=1;j<nx;j++) {
			msg << indmul[k][j] << ":"; 
			msg << indmul[k][nx];
		}
		msg << endl;
	}
	nisp_message ( msg.str() );
}

// Retourne l'indice de sensibilite d'un groupe de variables - sortie 1
double PolynomialChaos::GetGroupIndice(int j) {
	int i, k, r; 
	double s;
	if (j<1)  {
		ostringstream msg;
		msg << "Nisp(PolynomialChaos::GetGroupIndice) :  rank of the output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		ostringstream msg;
		msg << "Nisp(PolynomialChaos::GetGroupIndice) :  rank of the output " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	for(s=0.,k=1;k<=p;k++) {
		for(r=1,i=1;i<=nx;i++) {
			if(indmul[k][i] > groupe[i]*indmul[k][i]) {
				r=0;
			}
		}
		if(r) {
			s+=beta[j][k]*beta[j][k];
		}
	}
	if(variance[j]) {
		return(s/variance[j]);
	} else {
		return(0.);
	}
}

// Retourne l'indice de sensibilite d'un groupe de variables - sortie 1
void PolynomialChaos::GetGroupIndice( double * ind ) {
	for ( int j = 1; j<= ny; j++) {
			ind[j-1] = this->GetGroupIndice(j);
	}
}
double PolynomialChaos::GetGroupIndiceInteraction(int j) {
	int i, k, r; double s;
	if (j<1)  {
		ostringstream msg;
		msg << "Nisp(PolynomialChaos::GetGroupIndiceInteraction) :  rank of the output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		ostringstream msg;
		msg << "Nisp(PolynomialChaos::GetGroupIndiceInteraction) :  rank of the output " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	for(s=0.,k=1;k<=p;k++) {
		for(r=1,i=1;i<=nx;i++)  {
			if(indmul[k][i] > groupe[i]*indmul[k][i]) {
				r=0;
			}
		}
		for(i=1;i<=nx;i++) {
			if((indmul[k][i]==0)&&(groupe[i])) {
				r=0;
			}
		}
		if(r) {
			s+=beta[j][k]*beta[j][k];
		}
	}
	if(variance[j]) {
		return(s/variance[j]);
	}
	else {
		return(0.);
	}
}
void PolynomialChaos::GetGroupIndiceInteraction( double * ind ) {
	for ( int j = 1; j<= ny; j++) {
			ind[j-1] = this-> GetGroupIndiceInteraction ( j );
	}
}

void PolynomialChaos::SetGroupAddVar(int i) {
	if (i<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::SetGroupAddVar) :  rank of the variable " << i << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if (i>nx) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::SetGroupAddVar) :  rank of the variable " << i << " > stochastic dimension = " << nx << endl;
		nisp_error ( msg.str() );
		return;
	}
	if (groupe[i]==0) 
	{
		groupe[i]=1;
	}
	else
	{
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::SetGroupAddVar) :  the variable #" << i << " is already in the group"<< endl;
		nisp_error ( msg.str() );
	}
}

int PolynomialChaos::GetGroupSize()
{
	int s;
	s=0;
	for(int i=1;i<=nx;i++) {
		if (groupe[i]==1) 
		{
			s=s+1;
		}
	}
	return s;
}

void PolynomialChaos::GetGroup(int * vararray) {
	int s;
	s=0;
	for(int i=1;i<=nx;i++) {
		if (groupe[i]==1) 
		{
			vararray[s]=i;
			s = s+1;
		}
	}
}

void PolynomialChaos::PrintGroup() {
	int s;
	ostringstream msg;
	s = this->GetGroupSize();
	msg << "***********************************************" << endl;
	msg << "Nisp(PolynomialChaos::PrintGroup)" << endl;
	msg << "Total number of variables       : " << nx << endl;
	msg << "Number of variables in the group: " << s << endl;
	for(int i=1;i<=nx;i++) {
		if (groupe[i]==1) 
		{
			msg << "Variable #" << i << endl;
		}
	}
	msg << "***********************************************" << endl;
	nisp_message ( msg.str() );
}

void PolynomialChaos::SetGroupEmpty() {
	for(int i=1;i<=nx;i++) {
		groupe[i]=0;
	}
}

void PolynomialChaos::GetAnova(int r) {
	int i,j;
	ostringstream msg;
	for(i=1;i<=ni;i++) { // ni = 2**nx - 1 = 3 dans cet exemple
		SetGroupEmpty(); // On vide le groupe
		for(j=1;j<=nx;j++) {
			if(indices_setvar[i][j]) {
				SetGroupAddVar(j); // on construit les 2**nx - 1 groupes
			}
		}
		for(j=1;j<=nx;j++) {
			msg << indices_setvar[i][j] << " "; // On affiche le groupe
		}
		msg << " : " << GetGroupIndiceInteraction(r) << endl; // et on edite la valeur de l'indice du groupe
	}
	nisp_message ( msg.str() );
}

void PolynomialChaos::GetAnovaOrdered(double seuil, int r) { // seuil = 1. et r=1 par defaut
	int i,j;
	double *AnovaOrder = dvector(ni);
	for(i=1;i<=ni;i++) { // ni = 2**nx - 1
		SetGroupEmpty(); // On vide le groupe
		for(j=1;j<=nx;j++) {
			if(indices_setvar[i][j]) {
				SetGroupAddVar(j); // on construit les 2**nx - 1 groupes
			}
		}
		AnovaOrder[i-1]=GetGroupIndiceInteraction(r);
	}
	// On ordonne
	int *indx=ivector(ni);
	dindex(ni,AnovaOrder,indx,1);

	// On affiche
	ostringstream msg;
	//msg << "Nisp(PolynomialChaos::GetAnovaOrdered) output = " << r << " threshold = " << seuil << "\n"<<endl;
	double dv=0.;
	for(i=1;i<=ni;i++) { // ni = 2**nx - 1 = 3 dans cet exemple
		dv+=AnovaOrder[indx[i-1]];
		for(j=1;j<=nx;j++) {
			msg << indices_setvar[indx[i-1]+1][j] << " ";   // On affiche le groupe
		}
		msg << " : " << AnovaOrder[indx[i-1]] << " : cumul = " << dv << endl; // et on edite la valeur de l'indice du groupe
		if(dv>seuil) break;
	}
	nisp_message ( msg.str() );
	free_dvector(AnovaOrder);
	AnovaOrder = NULL;
	free_ivector(indx);
	indx = NULL;
}

void PolynomialChaos::GetAnovaOrderedCoefficients(double seuil, int r) { // sortie r = 1 par defaut
	int i,k;
	ostringstream msg;
	if (r<1)  {
		
		msg << "Nisp(PolynomialChaos::GetAnovaOrderedCoefficients) :  rank of the output " << r << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if (r>ny) {
		
		msg << "Nisp(PolynomialChaos::GetAnovaOrderedCoefficients) :  rank of the output " << r << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(variance[r]==0.) {
		
		msg << "Nisp(PolynomialChaos::GetAnovaOrdererCoefficients) :  variance of the output " << r << " = 0" << endl;
		nisp_error ( msg.str() );
		return;
	}
	//double *Ratio = dvector(p+1);
	double * Ratio = new double[p];
	for(k=1;k<=p;k++) {
		Ratio[k-1]= beta[r][k]*beta[r][k]/variance[r];
	}

	//int *indx=ivector(p);
	int * indx = new int[p];
	dindex(p,Ratio,indx, 1);
	double dv=0;
	msg << "Nisp(PolynomialChaos::GetAnovaOrdererCoefficients)"<<endl;
	for(k=0;k<p;k++) {
		dv+=Ratio[indx[k]];
		for(i=1;i<=nx;i++) {
			msg << indmul[indx[k]+1][i] << " "; 
		}
		msg << " : " << Ratio[indx[k]] << " : cumul = " << dv << endl;
		if(dv>seuil) {
			break;
		}
	}
	nisp_message ( msg.str() );
	// Mise a zero des autres coefficients;
	for(;k<p;k++) {
		beta[r][indx[k]+1]=0.;
	}
	delete[] indx;
	delete[] Ratio;
}

// TODO : it is not clear why the polynomialchaos should contain a SetRandomVariable ?
void PolynomialChaos::BuildSample(char * type, int np, int order) {
	string stype;
	stype.assign(type);
	this->BuildSample ( stype , np , order );
}
void PolynomialChaos::BuildSample(string type, int np, int order) {
	if(gpx->np) {
		free_dmatrix(sample,ny);
		sample = NULL;
		free_imatrix(sampleindx,ny);
		sampleindx = NULL;
	}
	gpx->BuildSample(type,np);
	sample=dmatrix(ny,gpx->np);
	sampleindx=imatrix(ny,gpx->np);
	int j,k;
	for(k=1;k<=gpx->np;k++) {
		ComputeOutput(gpx->x[k]);
		for(j=1;j<=ny;j++) {
			sample[j-1][k-1]=y[j];
		}
	}
	if(order) 
	{
		for(j=0;j<ny;j++) {
			dindex(gpx->np,sample[j],sampleindx[j],0);
		}
	}
	else 
	{
		for(j=0;j<ny;j++) 
		{
			for(k=0;k<gpx->np;k++) {
				sampleindx[j][k]=k;
			}
		}
	}
}
double PolynomialChaos::GetSample(int k, int j) {
	ostringstream msg;
	if (j<1)  {
		
		msg << "Nisp(PolynomialChaos::GetSample) :  rank of the output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		
		msg << "Nisp(PolynomialChaos::GetSample) :  rank of the output " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if(gpx->np<1) {
		
		msg << "Nisp(PolynomialChaos::GetSample) :  size of sample = " << gpx->np << " < 1 " << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if(k<1) {
		
		msg << "Nisp(PolynomialChaos::GetSample) :  rank of simulation " << k << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if(k>gpx->np) {
		
		msg << "Nisp(PolynomialChaos::GetSample) :  rank of simulation " << k << " > size of sample = " << gpx->np << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	return sample[j-1][sampleindx[j-1][k-1]];
}
int PolynomialChaos::GetSampleSize( ) {
	return gpx->np;
}

double PolynomialChaos::GetQuantile(double alpha, int j) {
	ostringstream msg;
	if (j<1)  {
		
		msg << "Nisp(PolynomialChaos::GetQuantile) :  rank of the output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		
		msg << "Nisp(PolynomialChaos::GetQuantile) :  rank of the output " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if((alpha<0) || (alpha>1)) {
		
		msg << "Nisp(PolynomialChaos::GetQuantile) :  value of quantile " << alpha << " is not in [0,1]" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if(gpx->np<1) {
		
		msg << "Nisp(PolynomialChaos::GetQuantile) :  size of sample = " << gpx->np << " < 1 " << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	int k = (int) (alpha * (unsigned long) gpx->np);
	if(k<1) 
	{
		k=1;
	}
	else if(k>gpx->np) 
	{
		k=gpx->np;
	}
	return(sample[j-1][sampleindx[j-1][k-1]]);
}
void PolynomialChaos::GetQuantile(double alpha, double * quantile ) {
	for(int j=1;j<=ny;j++) {
		quantile[j-1] = this->GetQuantile( alpha, j);
	}
}

double PolynomialChaos::GetQuantileWilks(double alpha, double beta, int j) {
	ostringstream msg;
	if (j<1)  {
		
		msg << "Nisp(PolynomialChaos::GetQuantileWilks) :  rank of the output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		
		msg << "Nisp(PolynomialChaos::GetQuantileWilks) :  rank of the output " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if((alpha<0) || (alpha>1)) {
		
		msg << "Nisp(PolynomialChaos::GetQuantileWilks) :  value of quantile " << alpha << " is not in [0,1]" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if((beta<0.5) || (beta>0.999999)) {
		
		msg << "Nisp(PolynomialChaos::GetQuantileWilks) :  value of confidence " << beta << " is not in [0.5,0.999999]" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	int n=gpx->np;
	if(n<1) {
		
		msg << "Nisp(PolynomialChaos::GetQuantileWilks) :  size of sample = " << n << " < 1 " << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	int ns;
	ns=(int) (floor(log(1.-beta)/log(alpha)))+1;
	if(n<ns) {
		
		msg << "Nisp(PolynomialChaos::GetQuantileWilks) :  size of sample = " << n << " < " << ns << " = minimal size " << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	int r = (int) ceil(nisp_standardnorminv(beta)*sqrt(alpha*(1.-alpha)*n));
	int k =  (int) floor(n*alpha) + r;
	if(k<1) {
		k=1;
	}
	else if(k>n) {
		k=n;
	}
	return(sample[j-1][sampleindx[j-1][k-1]]);
}
void PolynomialChaos::GetQuantileWilks(double alpha, double beta, double * quantile ) {
	for(int j=1;j<=ny;j++) {
		quantile[j-1] = this->GetQuantileWilks( alpha , beta , j);
	}
}

double PolynomialChaos::GetInvQuantile(double seuil, int j) {
	ostringstream msg;
	if (j<1)  {
		
		msg << "Nisp(PolynomialChaos::GetInvQuantile) :  rank of the output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		
		msg << "Nisp(PolynomialChaos::GetInvQuantile) :  rank of the output " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if(gpx->np<1) {
		
		msg << "Nisp(PolynomialChaos::GetInvQuantile) :  size of sample = " << gpx->np << " < 1 " << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	int k;
	for(k=1;k<=gpx->np;k++) {
		if(sample[j-1][sampleindx[j-1][k-1]]>seuil) {
			break;
		}
	}
	return((double) (k) / gpx->np);
}
void PolynomialChaos::GetInvQuantile(double seuil, double * quantile) {
	for ( int j = 1; j<= ny; j++) {
		quantile[j-1] = this->GetInvQuantile( seuil,  j);
	}
}
void PolynomialChaos::SetSizeTarget(int nnp) {
	this -> FreeMemoryTarget();
	np=nnp;
	target=dmatrix(nnp+1,ny+1);
}
void PolynomialChaos::ReadTarget(char *fichier) {
	nisp_readtarget2(target,np,ny,fichier);
}
void PolynomialChaos::FreeMemoryTarget() {
	if(np) {
		free_dmatrix(target,np+1);
		target = NULL;
	}
}
void PolynomialChaos::SetTarget(int k, int j, double z) {
	if(k<1)  {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::SetTarget) : the index of sample k= " << k << " is < 1 " <<endl;
		nisp_error ( msg.str() );
		return;
	}
	if(k>np) {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::SetTarget) : the index of sample k= " << k << " is > " << np << " = size of sampling" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(j<1)  {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::SetTarget) : the index of variable j= " << j << " is < 1 " <<endl;
		nisp_error ( msg.str() );
		return;
	}
	if(j>ny) {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::SetTarget) : the index of variable j= " << j << " is > output dimension = " << ny << endl;
		nisp_error ( msg.str() );
		return;
	}
	target[k][j]=z;
}

double PolynomialChaos::GetTarget(int k, int j) {
	double result = 0.0;
	if (ny==0) {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::GetTarget) : The size of the target (number of output variables) is zero ; ny= " << ny << endl;
		nisp_error ( msg.str() );
		return result;
	}
	if (np==0) {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::GetTarget) : There is no sample in database ; np= " << np << endl;
		nisp_error ( msg.str() );
		return result;
	}
	if(k<1)  {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::GetTarget) : the index of sample k= " << k << " is < 1 " <<endl;
		nisp_error ( msg.str() );
		return result;
	}
	if(k>np) {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::GetTarget) : the index of sample k= " << k << " is > " << np << " = size of sampling" << endl;
		nisp_error ( msg.str() );
		return result;
	}
	if(j<1)  {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::GetTarget) : the index of variable j= " << j << " is < 1 " <<endl;
		nisp_error ( msg.str() );
		return result;
	}
	if(j>ny) {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::GetTarget) : the index of variable j= " << j << " is > output dimension = " << ny << endl;
		nisp_error ( msg.str() );
		return result;
	}
	return target[k][j];
}

int PolynomialChaos::GetSizeTarget() {
	return np;
}
int  PolynomialChaos::GetDimensionInput() {
	return(nx);
}
int  PolynomialChaos::GetDimensionOutput() {
	return(ny);
}
int  PolynomialChaos::GetDimensionExpansion() {
	// TODO : what p+1 instead of p ?
	return(p+1);
}
int  PolynomialChaos::GetDegree() {
	return(no);
}

void PolynomialChaos::FreeMemory() {
	if (x!=NULL) {
		free_dvector(x);
		x = NULL;
	}
	if (y!=NULL) {
		free_dvector(y);
		y = NULL;
	}
	if (moyenne=NULL) {
		free_dvector(moyenne);
		moyenne = NULL;
	}
	if (variance!=NULL) {
		free_dvector(variance);
		variance = NULL;
	}
	if (groupe!=NULL) {
		free_ivector(groupe);
		groupe = NULL;
	}
	if (indices!=NULL) {
		free_dmatrix(indices,ny+1);
		indices = NULL;
	}
	if (indices_globaux!=NULL) {
		free_dmatrix(indices_globaux,ny+1);
		indices_globaux = NULL;
	}
	if (indices_setvar!=NULL) {
		free_imatrix(indices_setvar,ni+1);
		indices_setvar = NULL;
	}
	if(np) {
		if (target!=NULL) {
			free_dmatrix(target,np+1);
			target = NULL;
		}
	}
	if(no) {
		if (phi!=NULL) {
			free_dmatrix(phi,nx+1);
			phi = NULL;
		}
		if (psi!=NULL) {
			free_dvector(psi);
			psi = NULL;
		}
		if (indmul!=NULL) {
			free_imatrix(indmul,p+1);
			indmul = NULL;
		}
		if (beta!=NULL) {
			free_dmatrix(beta,ny+1);
			beta = NULL;
		}
	}
}
void PolynomialChaos::SetInput(int i, double value) {
	if(i<1)  {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::SetInput) : rank input " << i << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(i>nx) {
		ostringstream msg;
		
		msg << "Nisp(PolynomialChaos::SetInput) : rank input " << i << " is > stochastic dimension " << nx << endl;
		nisp_error ( msg.str() );
		return;
	}
	else {
		x[i]=value;
	}
}

void PolynomialChaos::PropagateInput() {
	int i,k;
	// calcul du tableau phi[1...nx][0...no]
	for(i=1;i<=nx;i++) {
		if(funpol[i-1]==HERMITE) {
			hermite(phi[i], x[i],no);
		}
		else if (funpol[i-1]==LEGENDRE) {
			legendre(phi[i],x[i],no);
		}
		else {
			laguerre(phi[i],x[i],no);
		}
	}
	// calcul du vecteur psi
	for(k=0;k<=p;k++) { 
		for(psi[k]=1.,i=1;i<=nx;i++) {
			psi[k]=psi[k]*phi[i][indmul[k][i]];
		}
	}
}

// Juillet 08 2010
void PolynomialChaos::PropagateInput(double *input) {
	for(int i=1;i<=nx;i++) {
		SetInput(i, input[i]);
	}
	PropagateInput();
} 

void PolynomialChaos::ComputeOutput() {
	int j,k; 
	double s;
	PropagateInput();
	for(j=1;j<=ny;j++) {
		for(s=0.,k=0;k<=p;k++) {
			s+=beta[j][k]*psi[k];
		}
		y[j]=s;
	}
}

void PolynomialChaos::ComputeOutput(double *entree) {
	PropagateInput(entree);
	ComputeOutput();
}

double PolynomialChaos::GetOutput(int j) {
	ostringstream msg;
	if (j<1)  {
		
		msg << "Nisp(PolynomialChaos::GetOutput) :  rank of the output " << j << " < 1" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if (j>ny) {
		
		msg << "Nisp(PolynomialChaos::GetOutput) :  rank of the output " << j << " > number of ouput = " << ny << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	return(y[j]);
}
void PolynomialChaos::GetOutput ( double * output ) {
	for(int j=1;j<=ny;j++) {
		output[j] = y[j];
	}
}
void PolynomialChaos::Compute(double *input, double *output) {
	PropagateInput ( input );
	ComputeOutput();
	GetOutput ( output );
}

// Calcul des coefficients par integration (quadrature de Gauss)
void CoefficientsIntegration(double **beta, double **Z, double *w, double **y, int p, int np, int ny) {
	int i,j,k; double s;
	for(j=1;j<=ny;j++) {
		for(k=0;k<=p;k++) {
			for(s=0.,i=1;i<=np;i++) {
				s+=Z[i][k]*w[i]*y[i][j]; 
			}
			beta[j][k]=s;
		}
	}
}

void nisp_coefficientsregression(double **beta, double *Z, double **y, int p, int np, int ny) {
	int i,j,k;

	int m=np;
	int n=p+1;
	int lda=m;
	int ldv=n;
	int ldu=m;
	int job = 21;
	double * s = dvector(m+n);
	double * e = dvector(m+n);
	double * u = dvector(m*n);
	double * v = dvector(n*n);
	double * work = dvector(m);
	double * tmp = dvector(n);

	dsvdc(Z,lda,m,n,s,e,u,ldu,v,ldv,work,job);

	// Transformation des vecteurs de la matrice V (regularisation)
	double coef, alpha=1.e-6;
	for(j=1;j<=n;j++) {
		coef=0.; 
		if(s[j-1]) {
			coef=s[j-1]/(s[j-1]*s[j-1]+alpha);
		}
		for(i=1;i<=n;i++) {
			v[(i-1)+(j-1)*n]=coef*v[(i-1)+(j-1)*n];
		}
	}
	double st;
	for(j=1;j<=ny;j++) {
		for(i=1;i<=n;i++) {
			for(st=0.,k=1;k<=m;k++) {
				st+=u[(k-1)+(i-1)*m]*y[k][j];  
			}
			tmp[i-1]=st;
		}
		for(k=1;k<=n;k++) {
			for(st=0.,i=1;i<=n;i++) {
				st+=v[(k-1)+(i-1)*n]*tmp[i-1]; 
			}
			beta[j][k-1]=st;
		}
	}
	free_dvector(s);
	free_dvector(e);
	free_dvector(u);
	free_dvector(v);
	free_dvector(work);
	free_dvector(tmp);
}

// Coefficients du PolynomialChaos
void WriteCoef(PolynomialChaos * pc, ofstream & ficsrc, char *name) {
	int j,k,nx,no,ny, p;
	nx=pc->nx;
	no=pc->no;
	ny=pc->ny;
	p=pc->p;
	char racine[100]; 
	strcpy(racine,name); 
	strcat(racine,"_beta");
	ficsrc << "double "<<racine<<"["<<ny<<"][" <<p+1<< "]={"<<endl;
	int nl=0;
	for(j=1;j<=ny-1;j++) {
		for(k=0;k<=p;k++) {
			if(nl==8) {
				nl=0; 
				ficsrc<<endl;
			}
			nl++; 
			ficsrc << pc->beta[j][k] << ",";
		}
	}
	for(k=0;k<p;k++) {
		if(nl==8) {
			nl=0; 
			ficsrc<<endl;
		}
		nl++; 
		ficsrc << pc->beta[ny][k] << ",";
	}
	ficsrc << pc->beta[ny][p] << "};"<<endl;
}

// Tableau des multi indices
void WriteIndiceMultiple(PolynomialChaos * pc, ofstream & ficsrc, char *name) {
	int i,k,nx,no,ny, p;
	nx=pc->nx;
	no=pc->no;
	ny=pc->ny;
	p=pc->p;
	char racine[100]; 
	strcpy(racine,name); 
	strcat(racine,"_indmul");
	ficsrc << "int "<<racine<<"["<<p+1<<"][" <<nx<< "]={"<<endl;
	for(k=0;k<p;k++) {
		for(i=1;i<=nx;i++) {
			ficsrc << pc->indmul[k][i] <<",";
		}
		if(!(((k+1)*nx)%25)) {
			ficsrc<<endl;
		}
	}
	for(i=1;i<nx;i++) {
		ficsrc << pc->indmul[p][i] <<",";
	}
	ficsrc << pc->indmul[p][nx] <<"};"<<endl;
}

void WriteCodeLegendre(ofstream & ficsrc, char *name) {
	char racine[100]; 
	strcpy(racine,name); 
	strcat(racine,"_legendre");
	ficsrc<<"void "<<racine<<"(double *phi,double x, int no) {"<<endl;
	ficsrc<<"   int i;"<<endl;
	ficsrc<<"   x=2.*x-1.;"<<endl;
	ficsrc<<"   phi[0]=1.;"<<endl;
	ficsrc<<"   phi[1]=x;"<<endl;
	ficsrc<<"   for(i=1;i<no;i++) {"<<endl;
	ficsrc<<"	    phi[i+1]= ((2.*i+1.) * x * phi[i] - i * phi[i-1]) / (i+1.);"<<endl;
	ficsrc<<"   }"<<endl;
	ficsrc<<"   for(i=0;i<=no;i++) {"<<endl;
	ficsrc<<"	    phi[i]  = phi[i] * sqrt(2.* i + 1.);"<<endl;
	ficsrc<<"   }"<<endl;
	ficsrc<<"}"<<endl;
}

void WriteCodeHermite(ofstream & ficsrc, char *name) {
	char racine[100]; 
	strcpy(racine,name); 
	strcat(racine,"_hermite");
	ficsrc<<"void "<<racine<<"(double *phi,double x, int no) {"<<endl;
	ficsrc<<"   int i,j;"<<endl;
	ficsrc<<"   double c1,c2;"<<endl;
	ficsrc<<"   x=x/sqrt(2.);"<<endl;
	ficsrc<<"   phi[0]=1.;"<<endl;
	ficsrc<<"   phi[1]=2*x;"<<endl;
	ficsrc<<"   for(i=1;i<no;i++) {"<<endl;
	ficsrc<<"	    phi[i+1]= 2. * x * phi[i] - 2. * i * phi[i-1];"<<endl;
	ficsrc<<"   }"<<endl;
	ficsrc<<"   for(i=0;i<=no;i++) {"<<endl;
	ficsrc<<"      c1=pow(2.,(double) i);"<<endl;
	ficsrc<<"      for(c2=1.,j=1;j<=i;j++) {"<<endl;
	ficsrc<<"        c2=c2*j;"<<endl;
	ficsrc<<"      }"<<endl;
	ficsrc<<"      phi[i]  = phi[i] / sqrt(c1*c2);"<<endl;
	ficsrc<<"   }"<<endl;
	ficsrc<<"}"<<endl;
}
void WriteCodeLaguerre(ofstream & ficsrc, char *name) {
	char racine[100]; 
	strcpy(racine,name); 
	strcat(racine,"_laguerre");
	ficsrc<<"void "<<racine<<"(double *phi,double x, int no) {"<<endl;
	ficsrc<<"   int i;"<<endl;
	ficsrc<<"   phi[0]=1.;"<<endl;
	ficsrc<<"   phi[1]=1.-x;"<<endl;
	ficsrc<<"   for(i=1;i<no;i++) {"<<endl;
	ficsrc<<"	    phi[i+1]= ((2.*i+1.-x) * phi[i] - i * phi[i-1]) / (i+1.);"<<endl;
	ficsrc<<"   }"<<endl;
	ficsrc<<"}"<<endl;
}

// Polynomes orthogonaux
void WriteCodeSrc(PolynomialChaos * pc, ofstream & ficsrc, char *name) {
	int i,nx,no,ny, p;
	nx=pc->nx;
	no=pc->no;
	ny=pc->ny;
	p=pc->p;
	ficsrc <<"   int i,j,k,nx,ny,no,p;"<<endl;
	ficsrc <<"   double psi["<<p+1<<"],phi["<<nx<<"]["<<no+1<<"],xi["<<nx<<"],s;"<<endl;
	ficsrc <<"   nx="<<nx<<";"<<endl;
	ficsrc <<"   ny="<<ny<<";"<<endl;
	ficsrc <<"   no="<<no<<";"<<endl;
	ficsrc <<"   p="<<p<<";"<<endl;
	ficsrc <<"   for(i=0;i<nx;i++) {"<<endl;
	ficsrc <<"	    xi[i]=x[i];"<<endl;
	ficsrc <<"   }"<<endl;
	char racine[100];
	for(i=1;i<=nx;i++) {
		if(pc->funpol[i-1]==HERMITE) {
			strcpy(racine,name); 
			strcat(racine,"_hermite");
			ficsrc<<"   "<<racine<<"(phi["<<i-1<<"],xi["<<i-1<<"],"<<no<<");"<<endl;
		}
		else if (pc->funpol[i-1]==LEGENDRE){
			strcpy(racine,name); 
			strcat(racine,"_legendre");
			ficsrc<<"   "<<racine<<"(phi["<<i-1<<"],xi["<<i-1<<"],"<<no<<");"<<endl;
		}
		else {
			strcpy(racine,name); 
			strcat(racine,"_laguerre");
			ficsrc<<"   "<<racine<<"(phi["<<i-1<<"],xi["<<i-1<<"],"<<no<<");"<<endl;
		}
	}
	strcpy(racine,name); 
	strcat(racine,"_indmul");
	ficsrc<<"   for(k=0;k<=p;k++) {"<<endl;
	ficsrc<<"      for(psi[k]=1.,i=0;i<nx;i++) {"<<endl;
	ficsrc<<"		   psi[k]=psi[k]*phi[i]["<<racine<<"[k][i]];"<<endl;
	ficsrc<<"      }"<<endl;
	ficsrc<<"   }"<<endl;

	strcpy(racine,name); 
	strcat(racine,"_beta");
	ficsrc<<"   for(j=0;j<ny;j++) {"<<endl;
	ficsrc<<"      for(s=0.,k=0;k<=p;k++) {"<<endl;
	ficsrc<<"		   s+="<<racine<<"[j][k]*psi[k];"<<endl;
	ficsrc<<"      }"<<endl;
	ficsrc<<"      y[j]=s;"<<endl;
	ficsrc<<"   }"<<endl;

}
// Generation du code source du PolynomialChaos
void PolynomialChaos::GenerateCode(char *pcsrc, char *name) {

	ofstream ficsrc(pcsrc,ios::out);
	if(!ficsrc) {
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Nisp(PolynomialChaos::GenerateCode) : Problem when open the file " << pcsrc << endl;
		nisp_error ( msg.str() );
		return;
	}
	ficsrc<<"#include <math.h>"<<endl<<endl;

	WriteCoef(this,ficsrc,name);
	WriteIndiceMultiple(this,ficsrc,name);

	int flaghermite=0;
	int flaglegendre=0;
	int flaglaguerre=0;
	for(int i=1;i<=nx;i++) {
		if(funpol[i-1]==HERMITE)         {
			if(!flaghermite)  {
				WriteCodeHermite(ficsrc,name); 
				flaghermite =1;
			}
		}
		else if (funpol[i-1]==LEGENDRE)  {
			if(!flaglegendre) {
				WriteCodeLegendre(ficsrc,name);
				flaglegendre=1;
			}
		}
		else if (funpol[i-1]==LEGENDRE)  {
			if(!flaglaguerre) {
				WriteCodeLaguerre(ficsrc,name);
				flaglaguerre=1;
			}
		}
	}


	ficsrc << "void "<<name<<"(double *x, double *y) {" << endl;
	WriteCodeSrc(this,ficsrc,name);
	ficsrc << "}"<<endl;
	ficsrc.close();
}


