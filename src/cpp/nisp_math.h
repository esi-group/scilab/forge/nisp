//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#ifndef _NISP_MATH_H_
#define _NISP_MATH_H_

int * ivector(int n);
int ** imatrix(int l, int c);
void free_ivector(int * v);
void free_imatrix(int ** mat, int l);

double **dmatrix(int,int);
double *dvector(int);
void free_dmatrix(double **,int);
void free_dvector(double *);

#endif /* _NISP_MATH_H_ */

