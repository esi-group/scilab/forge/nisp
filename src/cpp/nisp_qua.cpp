//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - Digiteo - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>

using namespace std;

#include "nisp_qua.h"
#include "nisp_polyrule.h"
#include "nisp_util.h"
#include "nisp_math.h"
#include "nisp_msg.h"

static int *tx;

void Quadrature(double x[], double w[], int nq, string type) {
  int i;
  double * xt = dvector(nq);
  double * wt = dvector(nq);

  if(type=="Normale") {         
    hermite_compute(nq,xt,wt);
    for(i=1;i<=nq;i++) {
    x[i] = sqrt((double) 2.) * xt[i-1];
    w[i] = wt[i-1] / sqrt(M_PI);
    }
  }
  else if(type=="Uniforme") {
    legendre_compute(nq,xt,wt);
    for(i=1;i<=nq;i++) { 
    x[i] = (xt[i-1]+1.)/2.;
    w[i] = wt[i-1]/2.;
    }
  }
  else if(type=="Exponentielle") {
    laguerre_compute(nq,xt,wt);
    for(i=1;i<=nq;i++) { 
    x[i] = xt[i-1];
    w[i] = wt[i-1];
    }
  }
  else {
    ostringstream msg;
    msg << "Nisp(Quadrature) : law" << type << " unknown" << endl;
    nisp_error ( msg.str() );
    return;
  }
  free_dvector(xt);
  free_dvector(wt);
}

void Quadrature0(double x[], double w[], int nq, string type) {
  int i;
  if(type=="Normale") {         
    hermite_compute(nq,x,w);
    for(i=0;i<nq;i++) { 
    x[i] = sqrt((double) 2.) * x[i];
    w[i] = w[i] / sqrt(M_PI);
    }
  }
  else if(type=="Uniforme") {
    legendre_compute(nq,x,w);
    for(i=0;i<nq;i++) { 
    x[i] = (x[i]+1.)/2.;
    w[i] = w[i]/2.;
    }
  }
  else if(type=="Exponentielle") {
    laguerre_compute(nq,x,w);
  }
  else {
    ostringstream msg;
    msg << "Nisp(Quadrature) : law" << type << " unknown" << endl;
    nisp_error ( msg.str() );
    return;
  }
}

void Rec(int i,int n) {
  if(tx[i]<n) tx[i]++;
  else {tx[i]=1;Rec(i-1,n);}
}

/*********************************************************************************************
  Calcule la quadratrure tensorisee de nx variables aleatoires et recopie dans les tableaux
  yt[1...nt][1...nx] et wt[1...nt]. Les points et poids de quadrature sont respectivement dans
  xq[1...nx][1...nq], wq[1...nx][1...nq]. Remarque : $nt = nq^{nx}$.
**********************************************************************************************/

/*!
  \fn QuadratureTensorise(double **yt, double *wt, double **xq, double **wq, int nx, int nq, int nt)
  \brief Quadrature tensorisee
  \param yt Adresse de la matrice des nt points de la quadrature tensoriseee (yt[1,...,nt][1,...,nx])
  \param wt Adresse du vecteur des nt poids correspondants (wt[1,...,nt])
  \param xq Adresse de la matrice des nx quadratures monodimensionnels xt[1,...,nx][1,...,nq]
  \param wq Adresse de la matrice des nx poids associes wt[1,...,nx][1,...,nq]
  \param nx Nombre de variables stochastiques
  \param nq Nombre de points par quadrature
  \param nt nombre total de points de la quadrature tensorisee ($nq^{nx})
*/
void QuadratureTensorise(double **yt, double *wt, double **xq, double **wq, int nx, int nq, int nt) {
  int i,k;

  tx=ivector(nx+1);
  for(i=1;i<=nx;i++) {
  tx[i]=1;
  }
  for(k=1;k<=nt;k++) {
    for(i=1;i<=nx;i++) {
    yt[k][i]=xq[i][tx[i]];
    }
    for(wt[k]=1.,i=1;i<=nx;i++) {
    wt[k]=wt[k]*wq[i][tx[i]];
    }
    if(k<nt) {
    Rec(nx,nq);
    }
  }
  free_ivector(tx);
}

