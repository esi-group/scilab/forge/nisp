//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#ifndef _NISP_INV_H_
#define _NISP_INV_H_

// Returns x such that cumnorm(x) = p where cumnorm is the 
// standard normal cumulative density function.
double nisp_standardnorminv(double p);

#endif /* _NISP_INV_H_ */
