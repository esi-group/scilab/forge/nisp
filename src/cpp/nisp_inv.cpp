//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#define _USE_MATH_DEFINES
#include <math.h>

#include <stdlib.h>
using namespace std;

#include "nisp_inv.h"
#include "nisp_util.h"
#include "nisp_msg.h"
#include "dcdflib.h"

static double borne     = 1.e-13;
static double precision = 1.e-13;

static double p_min = 0.02425;
static double p_max = 0.97575;

static double a[]={0.,
  -3.969683028665376e+01,
  +2.209460984245205e+02,
  -2.759285104469687e+02,
  +1.383577518672690e+02,
  -3.066479806614716e+01,
  +2.506628277459239e+00};

static double b[]={0.,
  -5.447609879822406e+01,
  +1.615858368580409e+02,
  -1.556989798598866e+02,
  +6.680131188771972e+01,
  -1.328068155288572e+01,};

static double c[]={0.,
  -7.784894002430293e-03,
  -3.223964580411365e-01,
  -2.400758277161838e+00,
  -2.549732539343734e+00,
  +4.374664141464968e+00,
  +2.938163982698783e+00};

static double d[]={0.,
  +7.784695709041462e-03,
  +3.224671290700398e-01,
  +2.445134137142996e+00,
  +3.754408661907416e+00};

double nisp_standardnorminv ( double p ) {
  double x,q,r,e,u;
  int ind;
  double xerf;

  if( (p<0.) || ( p>1.) ) {
    ostringstream msg;
    msg << "Nisp(nisp_standardnorminv) : value = " << p << " is not in [0,1]" << endl;
    nisp_error ( msg.str() );
    return(0);
  }

  /********* JMM 11/09/2006 ********************
     Definition de l'intervalle [borne,1-borne]
  **********************************************/
  if(p<borne) {
	  p=borne;
  }
  else if (p>1.-borne) 
  {
	  p=1.-borne;
  }

  if( (0. < p) && (p < p_min) ) 
  {
    q=sqrt(-2.*log(p));
    x=(((((c[1]*q+c[2])*q+c[3])*q+c[4])*q+c[5])*q+c[6]) / ((((d[1]*q+d[2])*q+d[3])*q+d[4])*q+1.);
  }
  else if( (p_min <= p) && (p <= p_max) ) 
  {
    q = p - 0.5;
    r=q*q;
    x = (((((a[1]*r+a[2])*r+a[3])*r+a[4])*r+a[5])*r+a[6])*q / (((((b[1]*r+b[2])*r+b[3])*r+b[4])*r+b[5])*r+1.);
  }
  else 
  {
    q=sqrt(-2*log(1.-p));
    x=-(((((c[1]*q+c[2])*q+c[3])*q+c[4])*q+c[5])*q+c[6]) / ((((d[1]*q+d[2])*q+d[3])*q+d[4])*q+1.);
  }

  /***** JMM : 11/09/2006 *************
     Raffinement par la fonction erfc()
     precision absolue : 1.e-16;
  *************************************/
  ind = 0;
  xerf = -x/sqrt(2.);
  e = error_fc ( &ind , &xerf )/2. - p;
  do {
    u=e*sqrt(2.*M_PI)*exp(x*x/2.);
    x=x-u/(1+x*u/2.);
    ind = 0;
    xerf = -x/sqrt(2.);
    e = error_fc ( &ind , &xerf )/2. - p;
  } while(fabs(e)>precision);
  return(x);
}
