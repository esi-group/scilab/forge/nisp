//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - Digiteo - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#ifndef _NISP_POLYRULE_H_
#define _NISP_POLYRULE_H_


// hermite_compute: computes a Gauss-Hermite quadrature rule.
void hermite_compute ( int order, double xtab[], double weight[] );

// legendre_compute: computes a Gauss-Legendre quadrature rule.
void legendre_compute ( int order, double xtab[], double weight[] );

// laguerre_compute: computes a Gauss-Laguerre quadrature rule.
void laguerre_compute ( int order, double xtab[], double weight[] );

#endif /* _NISP_POLYRULE_H_ */

