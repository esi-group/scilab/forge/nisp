// Copyright (C) 2008 - INRIA   - Michael Baudin
// Copyright (C) 2009 - DIGITEO - Pierre MARECHAL
// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function nispBuildSrcCpp()
    cpp_dir   = get_absolute_file_path("builder_cpp.sce");
    src_path  = "c";
    linknames = ["nisp"];
    files = [
    "nisp_gva.cpp"
    "nisp_ind.cpp"
    "nisp_inv.cpp"
    "nisp_math.cpp"
    "nisp_msg.cpp"
    "nisp_conf.cpp"
    "nisp_ort.cpp"
    "nisp_pc.cpp"
    "nisp_polyrule.cpp"
    "nisp_qua.cpp"
    "nisp_util.cpp"
    "nisp_va.cpp"
    "nisp_formqua.cpp"
    ];

    ldflags = "";

    if ( getos() == "Windows" ) then
        include1 = "..\includes";
        include2 = "..\thirdparty";
        cflags   = "-DWIN32"+ ..
		" -DLIBNISP_EXPORTS"+..
		" -I""" + include1 + ..
        """ -I""" + include2 + """";
        libs     = ["..\thirdparty\libthirdparty"];
    else
        include =  "-I""" + cpp_dir + ..
        ["" "/../includes" "/../thirdparty"] + """ ";
        cflags   = strcat(include);
        libs     = ["../thirdparty/libthirdparty"];
    end

    tbx_build_src(linknames, files, src_path, ..
	cpp_dir, libs, ldflags, cflags);

endfunction 
nispBuildSrcCpp();
clear nispBuildSrcCpp
