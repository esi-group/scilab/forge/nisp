//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
// Copyright (C)  2009 - Digiteo - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>

#include "dcdflib.h"
#include "nisp_va.h"
#include "nisp_util.h"
#include "nisp_msg.h"
#include "nisp_inv.h"

using namespace std;

// ===========
// Methodes RandomVariable
// ===========
void RandomVariable::SetDefaultParameters() {
	if (type=="Normale")       
	{
		a=0.;  
		b=1.;
	}
	else if(type=="Uniforme")      
	{
		a=0.;  
		b=1.;
	}
	else if(type=="Exponentielle") 
	{
		a=1.;  
		b=0.; 
	}
	else if(type=="LogNormale")    
	{
		a=0.; 
		b=1.;
	}
	else if(type=="LogUniforme")   
	{
		a=0.; 
		b=1.;
	}
	else  
	{
		ostringstream msg;
		
		msg << "Nisp(RandomVariable::RandomVariable) : law " << type << " unknown" << endl;
		nisp_error ( msg.str() );
	}
}
void RandomVariable::CheckParameters() {
	ostringstream msg;
	if (type=="Normale") 
	{
		if(b<0) 
		{
			
			msg << "Nisp(RandomVariable::RandomVariable) : law " << type << " and standard error " << b << " < 0 " << endl;
			nisp_error ( msg.str() );
		}
	}
	else if(type=="Uniforme") 
	{
		if(a>=b) 
		{
			
			msg << "Nisp(RandomVariable::RandomVariable) : law " << type << " and min = " << a << " >= max = " << b << endl;
			nisp_error ( msg.str() );
		}
	}
	else if(type=="LogNormale")
	{
		if(b<=0.) 
		{
			
			msg << "Nisp(RandomVariable::RandomVariable) : law " << type << " and b= " << b << " <=0" << endl;
			nisp_error ( msg.str() );
		}
	}
	else if(type=="LogUniforme")   
	{
		if(a>b) 
		{
			
			msg << "Nisp(RandomVariable::RandomVariable) : law " << type << " and min = " << a << " > max = " << b << endl;
			nisp_error ( msg.str() );
		}
	}
	else if (type=="Exponentielle") 
	{
		// Nothing to do
	}
	else 
	{
		ostringstream msg;
		
		msg << "Nisp(RandomVariable::RandomVariable) : unknown law " << type << " with 2 parameters" << endl;
		nisp_error ( msg.str() );
	}
}
RandomVariable::RandomVariable(string name) {
	type=name;
	SetDefaultParameters();
	CheckParameters();
}
RandomVariable::RandomVariable(char * name) {
	type.assign(name);
	SetDefaultParameters();
	CheckParameters();
}
RandomVariable::~RandomVariable() {
	// Nothing to do for now
}
RandomVariable::RandomVariable(string name, double x) {
	type=name; 
	a=x; 
	b=0.;
	CheckParameters();
}
RandomVariable::RandomVariable(char * name, double x) {
	type.assign(name); 
	a=x; 
	b=0.;
	CheckParameters();
}
RandomVariable::RandomVariable(string name, double x, double y) { 
	type=name; 
	a=x; 
	b=y;
	CheckParameters();
}
RandomVariable::RandomVariable(char * name, double x, double y) {
	type.assign(name);
	a=x; 
	b=y;
	CheckParameters();
}
double RandomVariable::GetValue() {
	double x;
	if (type=="Normale") 
	{
		x = nisp_normrnd(a,b);
	}
	else if(type=="Uniforme") 
	{
		x = nisp_unifrnd(a,b);
	}
	else if(type=="LogNormale") 
	{
		x = nisp_lognrnd(a,b);
	}
	else if(type=="LogUniforme")   
	{
		x = nisp_logunifrnd(a,b);
	}
	else if (type=="Exponentielle") 
	{
		x = nisp_exprnd(a);
	}
	else 
	{
		ostringstream msg;
		
		msg << "Nisp(RandomVariable::RandomVariable) : unknown law " << type << " with 2 parameters" << endl;
		nisp_error ( msg.str() );
	}
	return x;
}
double RandomVariable::GetValue(RandomVariable *mere, double x) {
	double value;
	value = this->pdfChange(mere,x);
	return(value);
}
void RandomVariable::GetLog() {
	ostringstream msg;
	msg << "***********************************************" << endl;
	msg << "Nisp(RandomVariable::GetLog) for RandomVariable" << endl;
	msg << type << " : " << a << " : " << b << endl;
	msg << "***********************************************" << endl;
	nisp_message ( msg.str() );
}


// mere est uniforme dans [0,1]
double RandomVariable::pdfChange ( double z) {
	double y;
	double a=this->a; 
	double b=this->b;
	string typefils=this->type;
	if((z<0.) || (z>1.)) 
	{
		ostringstream msg;
		
		msg << "Nisp(pdfChange) : value " << z << "is not in [0,1]" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if(typefils=="Uniforme") 
	{
		y=nisp_unifinv(a, b, z);
	}
	else if(typefils=="Normale") 
	{
		y=nisp_norminv(a, b, z);
	}
	else if(typefils=="Exponentielle") 
	{
		y=nisp_expinv(a, z);
	}
	else if(typefils=="LogNormale") 
	{
		y=nisp_logninv(a, b, z);
	}
	else if(typefils=="LogUniforme") 
	{
		y=nisp_logunifinv(a, b, z);
	}
	else 
	{
		ostringstream msg;
		
		msg << "Nisp(pdfChange) : law " << typefils << " unknown" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	return(y);
}
double RandomVariable::pdfChange ( RandomVariable *mere, double x) {
	// this = fils
	double y;
	double a=this->a; 
	double b=this->b;
	// u uniform in [0,1];
	double u;
	string typefils=this->type; 
	string typemere=mere->type;
	ostringstream msg;

	// Compute u
	if(typemere=="Normale") 
	{
		double mean=mere->a; 
		double sigma=mere->b;
		u = nisp_normcdf(mean, sigma, x);
	}
	else if(typemere=="LogNormale") 
	{
		if(x<=0.) 
		{
			
			msg << "Nisp(pdfChange) : law (mother)is LogNormale and value " << x << " < 0" << endl;
			nisp_error ( msg.str() );
			return(0.);
		}
		double mean=mere->a; 
		double sigma=mere->b; 
		u=nisp_logncdf(mean, sigma, x);
	}
	else if(typemere=="Uniforme") 
	{
		double a=mere->a; 
		double b=mere->b;
		if((x<a) || (x>b)) 
		{
			
			msg << "Nisp(pdfChange) : value " << x << " must be in ["<<a<<","<<b<<"]"<< endl;
			nisp_error ( msg.str() );
			return(0.);
		}
		u=nisp_unifcdf(a, b, x);
	}
	else if(typemere=="LogUniforme") 
	{
		double a=mere->a;
		double b=mere->b;
		if(x<=0.) 
		{
			
			msg << "Nisp(pdfChange) : law (mother)is LogUniform and value " << x << " < 0" << endl;
			nisp_error ( msg.str() );
			return(0.);
		}
		u=nisp_logunifcdf(a, b, x);
	}
	else if(typemere=="Exponentielle") 
	{ 
		double rate=mere->a;
		if(x<=0.) {
			
			msg << "Nisp(pdfChange) : law (mother)is Exponential and value " << x << " <= 0" << endl;
			nisp_error ( msg.str() );
			return(0.);
		}
		u=nisp_expcdf(rate, x);
	}
	else 
	{
		
		msg << "Nisp(pdfChange) : law " << typemere << " unknown" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	// u uniform in [0,1]
	if((u<0.) || (u>1.)) 
	{
		
		msg << "Nisp(pdfChange) : law (mother) is LogUniform and error on value " << x << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	if(typefils=="Normale") 
	{
		y=nisp_norminv(a, b, u);
	}
	else if(typefils=="Uniforme") 
	{
		y=nisp_unifinv(a, b, u);
	}
	else if(typefils=="Exponentielle") 
	{
		y=nisp_expinv(a, u);
	}
	else if(typefils=="LogNormale") 
	{
		y=nisp_logninv(a, b, u);
	}
	else if(typefils=="LogUniforme") 
	{
		y=nisp_logunifinv(a, b, u);
	}
	else 
	{
		
		msg << "Nisp(pdfChange) : Transformation Normale -> " << typefils << " not available" << endl;
		nisp_error ( msg.str() );
		return(0.);
	}
	return(y);
}

