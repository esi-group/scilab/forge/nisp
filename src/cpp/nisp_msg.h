//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
// Copyright (C)  2009 - Digiteo - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#ifndef _NISP_MSG_H_
#define _NISP_MSG_H_

// The verbosity level.
// nisp_verboselevel = 0 : no messages are printed out.
// nisp_verboselevel = 1 : messages are printed out.
// This level can be configure with the public function "nisp_verboselevelset"
extern int nisp_verboselevel;

// If this function is non-NULL, it is used to print messages out.
// It can be configure with the public function "nisp_msgsetfunction".
extern void (* nisp_messagefunction)(char * message);

// If this function is non-NULL, it is used to when an error is generated.
// It can be configure with the public function "nisp_errorsetfunction".
extern void (* nisp_errorfunction)(char * message);

// nisp_verbosemessage --
//   Prints out a message, if verbose level is at least 1.
//   If no message function has been defined, prints the message on standard output.
//   If one message function has been defined, call back the message function.
void nisp_verbosemessage ( char * message );
void nisp_verbosemessage ( const string & str );

// nisp_message --
//   Prints out a message.
//   If no message function has been defined, prints the message on standard output.
//   If one message function has been defined, call back the message function.
void nisp_message ( char * message );
void nisp_message ( const string & str );

// nisp_error --
//   Prints out an error message and generates an error.
//   If no message function has been defined, prints the message on standard output.
//   If one message function has been defined, call back the message function.
//   If no error function has been defined, exits.
//   If one error function has been defined, call back the error function.
void nisp_error ( char * message );
void nisp_error ( const string & str );

#endif /* _NISP_MSG_H_ */

