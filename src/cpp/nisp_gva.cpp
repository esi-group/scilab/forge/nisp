//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <exception>
#include <algorithm>

#define _USE_MATH_DEFINES
#include "math.h"

// From Thirdparties
#include "sobol.h"
#include "smolyak.h"

// From NISP
#include "nisp_gva.h"
#include "nisp_util.h"
#include "nisp_math.h"
#include "nisp_msg.h"
#include "nisp_qua.h"
#include "nisp_formqua.h"

using namespace std;

//======================================================
// Private functions:
void teststo(string type,double a, double b);
void nisp_Build_MultiIndex_Filtre_norme_L1(vector<int *> * indices, int * min, int * max, int nx, int vmin, int vmax);
void nisp_Compute_Coef_Cnp_Signe(int *coef_Cnp, int *signe, vector<int *> * indices, int nx, int l);
int nisp_incrementation(int k[], int min[], int max[], int nx);
int Cnp(int n, int p);
int Filtre_norme_L1(int k[], int nx, int vmin, int vmax);
int norme_Linf(int k[], int nx);
int norme_L1(int k[], int nx);
//======================================================

// ===================
// Methodes classe SetRandomVariable
// ===================
SetRandomVariable::SetRandomVariable() {
	nx=0;
	np=0;
	datafile_nbchar = 17;
	datafile_precision = 8;
	degre_max=-1;
}
SetRandomVariable::~SetRandomVariable() {
	// TODO : destroy the included random variables if they were automatically created with a read in the data file
	this->FreeMemory();
}


SetRandomVariable::SetRandomVariable(int n) { // par defaut les variables sont uniformes et normalisees 
	nx=n;
	np=0;
	datafile_nbchar = 17;
	datafile_precision = 8;
	degre_max=-1;
	for(int j=1;j<=n;j++) {
		this->AddRandomVariable(new RandomVariable("Uniforme"));
	}
}
SetRandomVariable::SetRandomVariable(char *fichier) {
	// Caution: This has to be consistent with SetRandomVariable::Save, i.e.
	// if the file fichier has been saved with SetRandomVariable::Save(fichier),  
	// then we must be able to create the same SetRandomVariable with 
	// SetRandomVariable(fichier)
	datafile_nbchar = 17;
	datafile_precision = 8;
	ifstream entree(fichier,ios::in);
	if(!entree) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::SetRandomVariable) : problem to open the file " << fichier << endl;
		nisp_error ( msg.str() );
		return;
	}
	string ttypesampling,type,str; 
	int nnp,nnx,dmax; 
	double a,b;
	// 1. Read the header
	entree >> str >> str >> str >> ttypesampling;
	entree >> str >> str >> str >> nnp;
	entree >> str >> str >> str >> nnx;
	entree >> str >> str >> str >> dmax;
	for(int i=1;i<=nnx;i++) {
		entree >> str >> str >> str >> type >> a >> b;
		this->AddRandomVariable(new RandomVariable(type,a,b));
	}
	// Ignore the line with the Weight comment
	entree >> str >> str >> str;
	// 2. Read the sampling
	if(nnp) {
		x=dmatrix(nnp+1,nx+1); 
		w=dvector(nnp+1);
		for(int k=1;k<=nnp;k++) {
			for(int i=1;i<=nnx;i++) {
				entree >> x[k][i]; 
			}
			entree >> w[k];
		}
	}
	entree.close();
	nx=nnx; 
	np=nnp; 
	typesampling=ttypesampling; 
	degre_max=dmax;
}

void SetRandomVariable::FreeMemory() {
	if(np) {
		free_dmatrix(x,np+1);
		free_dvector(w);
		np=0;
	}
}
void SetRandomVariable::AddRandomVariable(RandomVariable *x) {
	va.push_back(x); 
	nx=(int) va.size();
}
void SetRandomVariable::BuildSample(char * name, int n) {
	string sname;
	sname.assign(name);
	this->BuildSample ( sname , n );
}
void SetRandomVariable::BuildSample(string name, int n) {
	typesampling=name;
	int flag;
	if(n<0) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::BuildSample) : level or size " << n << " < 0" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(nx<1) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::BuildSample) : stochastic dimension = " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	// Check the sample type
	flag = this->CheckSampleType();
	if(flag==0) 
	{
		// Error: the sample type is unknown.
		return;
	}

	if(np) {
		FreeMemory();
	}
	if (typesampling=="MonteCarlo") 
	{
		this->nisp_BuildSampleMontecarlo(n);
	}
	else if(typesampling=="Lhs")        
	{
		this->nisp_BuildSampleLHS(n);
	}
	else if(typesampling=="QmcSobol")  
	{
		this->nisp_BuildSampleQMCSobol(n);
	}
	else if(typesampling=="Quadrature") 
	{
		this->nisp_BuildSampleQuadrature(n);
	}
	else if(typesampling=="Petras")     
	{
		this->nisp_BuildSamplePetras(n);
	}
	else if(typesampling=="SmolyakGauss")   
	{
		this->nisp_BuildSampleSmolyak(typesampling,n);
	}
	else if(typesampling=="SmolyakTrapeze") 
	{
		this->nisp_BuildSampleSmolyak(typesampling,n);
	}
	else if(typesampling=="SmolyakFejer")   
	{
		this->nisp_BuildSampleSmolyak(typesampling,n);
	}
	else if(typesampling=="SmolyakClenshawCurtis")   
	{
		this->nisp_BuildSampleSmolyak(typesampling,n);
	}
}
int SetRandomVariable::CheckSampleType() {
	// Returns 1 if the sample type is OK.
	// Returns 0 if the sample type does not exist.
	if (typesampling=="MonteCarlo" || 
		typesampling=="Lhs" || 
		typesampling=="QmcSobol" || 
		typesampling=="Quadrature" || 
		typesampling=="Petras" || 
		typesampling=="SmolyakGauss" || 
		typesampling=="SmolyakTrapeze" || 
		typesampling=="SmolyakFejer" || 
		typesampling=="SmolyakClenshawCurtis")   
	{
		return 1;
	}
	else 
	{
		ostringstream msg;
		msg << "Nisp(SetRandomVariable::CheckSampleType) : the sample type " << typesampling << " is unknown" << endl;
		nisp_error ( msg.str() );
		return 0;
	}
}

void SetRandomVariable::SetSampleType(string name) {
	typesampling = name;
	int flag;
	flag = this->CheckSampleType();
}

void SetRandomVariable::SetSampleType(char * name) {
	string sname;
	sname.assign(name);
	this->SetSampleType ( sname );
}

void SetRandomVariable::SetSampleSize(int n) {
	if(n<1) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::SetSampleSize) : size " << n << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	degre_max=-1;
	if(nx<1) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::SetSampleSize) : stochastic dimension = " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(np!=0) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::SetSampleSize) : the current number of simulations is np=" << np << endl;
		msg << "Call FreeMemory before configuring a new sampling." << endl;
		nisp_error ( msg.str() );
		return;
	}
	np=n; 
	x=dmatrix(np+1,nx+1); 
	w=dvector(np+1);
	for(int k=1;k<=np;k++) {
		for(int i=1;i<=nx;i++) {
			x[k][i] = 0.0;
		}
		w[k] = 0.0;
	}
}

void SetRandomVariable::SetSample(int k, int i, double value) {
	if(k<1)  {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::SetSample) : the index of sample k= " << k << " is < 1 " <<endl;
		nisp_error ( msg.str() );
		return;
	}
	if(k>np) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::SetSample) : the index of sample k= " << k << " is > " << np << " = size of sampling" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(i<1)  {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::SetSample) : the index of variable i= " << i << " is < 1 " <<endl;
		nisp_error ( msg.str() );
		return;
	}
	if(i>nx) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::SetSample) : the index of variable i= " << i << " is > stochastic dimension = " << nx << endl;
		nisp_error ( msg.str() );
		return;
	}
	x[k][i]=value;
}


void SetRandomVariable::BuildSample(char * name, int n, int ne) {
	string sname;
	sname.assign(name);
	this->BuildSample ( sname , n , ne );
}
void SetRandomVariable::BuildSample(string name, int n, int ne) {
	if(n<1) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::BuildSample) : size " << n << " < 1" <<endl;
		nisp_error ( msg.str() );
		return;
	} 
	if(ne<1) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::BuildSample) : number of Lhs for LhsMaxMin = " << ne << " < 1"<<endl;
		nisp_error ( msg.str() );
		return;
	} 
	if(nx<1) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::BuildSample) : stochastique dimension = " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	typesampling=name;
	if(typesampling=="LhsMaxMin")  {
		if(np) {
			FreeMemory();
		}
		this->nisp_BuildSampleLHSmaxmin(n,ne);
	}
	else {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::BuildSample) : method " << name << " different of LhsMaxMin" << endl;
		nisp_error ( msg.str() );
		return;
	}
}

void SetRandomVariable::BuildSample(SetRandomVariable *mother) {
	if(nx != mother->nx) {
		ostringstream msg;
		
		msg << "Nisp(setRandomVariable::BuildSample) : stochastic dimension are not equal between the two set of random variables" << endl;
		msg << "The child is " << nx << "while the mother is " << mother->nx << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(mother->np==0) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::BuildSample) : the size of the mother set is empty" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(np) {
		FreeMemory();
	}
	typesampling=mother->typesampling;
	np=mother->np;
	degre_max=mother->degre_max;
	// TODO : a matrix with (np) x (nx) elements should be sufficient, provided that k=0,...,np-1 and i=0,...,nx-1
	x=dmatrix(np+1,nx+1); 
	w=dvector(np+1);
	for(int k=1;k<=np;k++) {
		for(int i=1;i<=nx;i++) {
			x[k][i]= va[i-1]->GetValue(mother->va[i-1],mother->x[k][i]);
		}
	}
	for(int k=1;k<=np;k++) {
		w[k]=mother->w[k];
	}
}

void SetRandomVariable::Save(char *fichier) {
	ofstream sortie(fichier,ios::out);
	if(!sortie) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::Save) : Problem when open file " << fichier << endl;
		nisp_error ( msg.str() );
		return;
	}
	sortie << "# Type         : " << typesampling << endl;
	sortie << "# Simulations  : " << np << endl;
	sortie << "# Variables    : " << nx << endl;
	sortie << "# DegreMax     : " << degre_max << endl;
	for(int i=0;i<nx;i++) {
		sortie << "# Column#"<<i+1<<", PDF: " << va[i]->type << " " << va[i]->a << " " << va[i]->b << endl;
	}
	sortie << "# Column#"<<nx+1<<": Weight" << endl;
	sortie << endl;
	sortie.setf(ios::scientific, ios::floatfield);
	sortie.precision(datafile_precision);
	for(int k=1;k<=np;k++) {
		for(int i=1;i<=nx;i++) { 
			sortie.width(datafile_nbchar); 
			sortie << x[k][i]; 
		}
		sortie.width(datafile_nbchar); 
		sortie << w[k];
		sortie << endl;
	}
	sortie.close();
}

int SetRandomVariable::GetDimension() {
	return(nx);
}

int SetRandomVariable::GetSize() {
	return(np);
}

void SetRandomVariable::GetLog() {
	for(int i=0;i<nx;i++) {
		va[i]->GetLog();
	}
}

double SetRandomVariable::GetSample(int k, int i) {
	double result = 0.0;
	if (np==0) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::GetSample) : There is no sample in database ; np= " << np << endl;
		nisp_error ( msg.str() );
		return result;
	}
	if(k<1)  {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::GetSample) : the index of sample k= " << k << " is < 1 " <<endl;
		nisp_error ( msg.str() );
		return result;
	}
	if (k>np) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::GetSample) : The index k=" << k << "is larger than the number of samples np= " << np << endl;
		nisp_error ( msg.str() );
		return result;
	}
	if(i<1)  {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::GetSample) : the index of variable i= " << i << " is < 1 " <<endl;
		nisp_error ( msg.str() );
		return result;
	}
	if(i>nx) {
		ostringstream msg;
		
		msg << "Nisp(SetRandomVariable::GetSample) : the index of variable i= " << i << " is > stochastic dimension = " << nx << endl;
		nisp_error ( msg.str() );
		return result;
	}
	result = x[k][i];
	return result;
}

// Latin Hypercube Sampling
void SetRandomVariable::nisp_BuildSampleLHS(int np) {
	this->degre_max=-1;
	int nx=this->nx;
	if(nx<1) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSampleLHS) : stochastic dimension " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	this->np=np;
	this->x=dmatrix(np+1,nx+1); 
	this->w=dvector(np+1);
	int k,j; 
	double pas=1./(double) np;
	vector<double> v(np);
	for(k=0;k<np;k++) {
		v[k]=(0.5 + (double) k)*pas;
	}
	for(j=1;j<=nx;j++) {
		random_shuffle(v.begin(),v.end());
		for(k=1;k<=np;k++) {
			this->x[k][j]=this->va[j-1]->pdfChange(v[k-1]);
		}
	}
	for(k=1;k<=np;k++) {
		this->w[k]= pas;
	}
}
// Quasi Monte Carlo Sobol sequence 
void SetRandomVariable::nisp_BuildSampleQMCSobol(int np) {
	RandomVariable *va;
	this->degre_max=-1;
	int nx=this->nx;
	if(nx<1) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSampleQMCSobol) : stochastic dimension " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	this->np=np; 
	this->x=dmatrix(np+1,nx+1); 
	this->w=dvector(np+1);

	long long int seed=0;

	double * x = dvector(nx);
	double pas=1./(double) np;
	int k,j;
	i8_sobol(nx, &seed, x); // on passe le premier point qui est toujours x[i]=0 i=0,1,...,nx-1
	for(k=1;k<=np;k++) {
		i8_sobol(nx, &seed, x);
		for(j=1;j<=nx;j++) {
			va = this->va[j-1];
			this->x[k][j]=va->pdfChange(x[j-1]);
		}
		this->w[k]=pas;
	}
	free_dvector(x);
}
// Latin Hypercube Sampling critere Max Min
void SetRandomVariable::nisp_BuildSampleLHSmaxmin(int np, int ne) {
	int i,j,k,ii,kk,k1;
	double **xt,dmin,dmaxmin,d,dx;
	int nx=this->nx;
	if(nx<1) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSampleLHSmaxmin) : stochastic dimension " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	this->np=np; 
	this->x=dmatrix(np+1,nx+1); 
	this->w=dvector(np+1);
	this->degre_max=-1;
	vector<double> v(np); 
	double pas=1./((double) np);
	for(k=0;k<np;k++) {
		v[k]=(0.5 + (double) k)*pas;
	}
	dmaxmin=0.;
	xt=dmatrix(np+1,nx+1);
	for(int b=1;b<=ne;b++) {
		for(j=1;j<=nx;j++) {
			random_shuffle(v.begin(),v.end());
			for(k=1;k<=np;k++) {
				xt[k][j]=v[k-1];
			}
		}
		// MaxMin
		dmin=1.e45;
		for(k=1;k<=np;k++) {
			for(k1=k+1;k1<=np;k1++) {
				for(d=0.,i=1;i<=nx;i++) { 
					dx=xt[k][i]-xt[k1][i]; 
					d+=dx*dx;
				}
				if(d<dmin) {
					dmin=d;
				}
			}
		}
		if(dmaxmin<dmin) { // Sauvegarde du plan Maximisant la plus petite distance entre 2 points
			dmaxmin=dmin;
			for(ii=1;ii<=nx;ii++) {
				for(kk=1;kk<=np;kk++) {
					this->x[kk][ii]=xt[kk][ii];
				}
			}
		}
	}
	// Changements de variable
	for(i=1;i<=nx;i++) {
		for(k=1;k<=np;k++) {
			this->x[k][i]=this->va[i-1]->pdfChange(this->x[k][i]);
		}
	}
	free_dmatrix(xt,np+1);
	for(k=1;k<=np;k++) {
		this->w[k]=pas;
	}
}
// Quadrature Tensorisee
void SetRandomVariable::nisp_BuildSampleQuadrature(int degre) {
	int nx=this->nx;
	int nq=degre+1; // le nombre de points doit etre egal au degre + 1

	if(nx<1) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSampleQuadrature) : stochastic dimension = " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(degre<0) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSampleQuadrature) : degree of quadrature == " << degre << " < 0" << endl;
		nisp_error ( msg.str() );
		return;
	}
	// degr?
	this->degre_max=degre;
	// On verifie que les variables aleatoires sont normalisees
	int j,np;
	for(j=1;j<=nx;j++) {
		teststo(this->va[j-1]->type,this->va[j-1]->a,this->va[j-1]->b);
	}
	for(np=1,j=1;j<=nx;j++) {
		np=np*nq;
	}
	this->np=np;
	this->x=dmatrix(np+1,nx+1); 
	this->w=dvector(np+1);
	// Calcul la quadrature tensorisee
	double **xqt, **wqt;
	xqt=dmatrix(nx+1,nq+1); 
	wqt=dmatrix(nx+1,nq+1);
	for(j=1;j<=nx;j++) {
		Quadrature(xqt[j],wqt[j],nq,this->va[j-1]->type);
	}
	QuadratureTensorise(this->x,this->w,xqt,wqt,nx,nq,np);
	free_dmatrix(xqt,nx+1); free_dmatrix(wqt,nx+1);
}
// Quadrature Tensorisee Incomplete (Smolyak, Petras)
// Le niveau correspond au degre max des PC qu'on peut calculer
void SetRandomVariable::nisp_BuildSamplePetras(int degre) {
	int niveau=degre;
	if(degre<1) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSamplePetras) : degree of cubature == " << degre << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(niveau>40) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSamplePetras) : level of cubature == " << niveau << " > 40 (value max of the level)" << endl;
		nisp_error ( msg.str() );
		return;
	}
	int k,icpt,i,j,nnx,q,nquad;
	double *ps,*ws;
	int nx=this->nx;
	if(nx<1) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSamplePetras) : stochastic dimension = " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	// Degre maximum
	this->degre_max=degre;
	// On verifie que les variables aleatoires sont normalisees
	for(j=1;j<=nx;j++) {
		teststo(this->va[j-1]->type,this->va[j-1]->a,this->va[j-1]->b);
	}
	nnx=nx;
	q=niveau+nnx;
	size_smolyak(&nnx,&q,&nquad);
	ws=dvector(nquad+1);
	ps=dvector(nquad*nnx + 1);
	quad_smolyak(&nx,&q,ps,ws);
	this->np=nquad;
	this->x=dmatrix(nquad+1,nx+1);
	this->w=dvector(nquad+1);
	for(k=1,icpt=0,i=0;i<nquad;i++) {
		for(j=1;j<=this->nx;j++) {
			this->x[k][j]=this->va[j-1]->pdfChange(ps[icpt++]);
		}
		this->w[k]=ws[i];
		k++;
	}
	free_dvector(ws);
	free_dvector(ps);
}
// Monte Carlo
void SetRandomVariable::nisp_BuildSampleMontecarlo(int np) {
	this->degre_max=-1;
	int nx=this->nx;
	if(nx<1) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSampleMontecarlo) : stochastic dimension " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	this->np=np; 
	this->x=dmatrix(np+1,nx+1); 
	this->w=dvector(np+1);
	int k,j; 
	double pas=1./(double) np;
	for(k=1;k<=np;k++) {
		for(j=1;j<=nx;j++) {
			this->x[k][j]=this->va[j-1]->GetValue();
		}
	}
	for(k=1;k<=np;k++) {
		this->w[k]= pas;
	}
}
// Formule est exacte au degre 2*l-1
// On peut donc construire des polynomes de degre l-1
// Remarque : on peut integrer des polynomes de dege > 2*l-1 en faisant
// une legere erreur.
// Par exemple n=9 l=3, l'integrale de x**8 dans [0,1] = 0.11111 et on
// trouve 0.108458 et donc une erreur de l'ordre de 3%
//
//
// type = {"SmolyakTrapeze", "SmolyakGauss", "SmolyakFejer"}
// Pour Fejer : le niveau est degre + 1
//            : si on veut calculer un PC de degre 5 => niveau 6
//            : REMARQUE : si la vraie fonction est un polynome de degre d+1
//                         le calcul des coefficients du PC de degre d est exacte
// Pour Gauss : idem 
// IMPORTANT : ces regles sont penalisantes lorsque la dimension
//             est petite. Par exemple en prenant nx=2 Fejer de niveau 7
//             permet de calculer un PC de degre 11 > 6 !! 
//
// Methodologie : pour nx < 3 utiliser la quadrature tensorisee
//
void SetRandomVariable::nisp_BuildSampleSmolyak(string type, int degre) {
	int nx=this->nx;
	int l;
	if(type=="SmolyakGauss") {
		l=degre+1; // comme pour Quadrature tensorisee
	}
	else {
		l=degre + 1; // la meme regle pour Fejer (voir ci dessous)
	}
	// si nx==2 : si degre<=5  l=degre+1
	//               degre<=7  l=6
	//               degre<=11 l=7
	//               degre<=15 l=8
	//               degre<=23 l=9
	// si nx==3 : si degre<=5  l=degre+1
	//               degre<=7  l=7
	//               degre==8  l=8
	//               degre==9  l=8
	//               degre==10 l=9
	// si nx==4  on prend la regle l=degre+1

	if(nx<1) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSampleSmolyak) : stochastic dimension = " << nx << " < 1" << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(degre<0) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(nisp_BuildSampleSmolyak) : degree  == " << degre << " < 0" << endl;
		nisp_error ( msg.str() );
		return;
	}
	// Formule de quadrature a 1 dimension
	//FormuleQuadrature * fq[nx];
	vector<FormuleQuadrature *> fq(nx);
	for(int i=0;i<nx;i++) {
		fq[i]=new FormuleQuadrature(this->va[i]->type,type,l);
	}

	// Points et poids de la sparse grid
	// Ils seront ensuite recopies dans le champ (x et w) dans this (SetRandomVariable)
	vector<double *> * point  = new vector<double *>();
	vector<double>   * weight = new vector<double>();

	// Creation des multi indices dont la norme L1
	// est comprise entre l et l+nx-1 (bornes incluses)
	vector<int *>   * indices = new vector<int *>();
	int * min = ivector(nx);
	int * max = ivector(nx);
	for(int i=0;i<nx;i++) {
		min[i]=1;
	}
	for(int i=0;i<nx;i++) {
		max[i]=l;
	}
	nisp_Build_MultiIndex_Filtre_norme_L1(indices,min,max,nx,l,l+nx-1);

	int * coef_Cnp = ivector(indices->size());
	int * signe    = ivector(indices->size());
	nisp_Compute_Coef_Cnp_Signe(coef_Cnp,signe,indices,nx,l);

	vector<int *>::iterator it;
	int *k,i,j=0;
	int * nmin = ivector(nx); // premier point : en general 1
	int * nmax = ivector(nx); // dernier point : en general egal au nombre maximal de points de
	// la quadrature a une dimension
	int * kc   = ivector(nx);

	double * xc = dvector(nx);
	double * xcc;
	double poids;
	for(i=0;i<nx;i++) nmin[i]=1;
	for(it=indices->begin();it!=indices->end();++it) {
		k=*it; // c'est k qui determine la tensorisation
		// initialisation du multi indice courant
		for(i=0;i<nx;i++) 
		{
			kc[i]=nmin[i];
		}
		// Premier point ...
		for(poids=coef_Cnp[j]*signe[j],i=0;i<nx;i++) 
		{
			poids=poids*fq[i]->w[k[i]-1][0];
		}
		for(i=0;i<nx;i++) {
			xc[i]=fq[i]->x[k[i]-1][0];
		}
		xcc = dvector(nx); for(i=0;i<nx;i++) 
			xcc[i]=xc[i]; point->push_back(xcc);
		weight->push_back(poids);

		// ... et points suivants eventuellement
		for(i=0;i<nx;i++) nmax[i]=fq[i]->np[k[i]-1];
		while(nisp_incrementation(kc,nmin,nmax,nx)) {
			for(poids=coef_Cnp[j]*signe[j],i=0;i<nx;i++) 
			{
				poids=poids*fq[i]->w[k[i]-1][kc[i]-1];
			}
			for(i=0;i<nx;i++) 
			{
				xc[i]=fq[i]->x[k[i]-1][kc[i]-1];
			}
			xcc = dvector(nx); 
			for(i=0;i<nx;i++) 
			{
				xcc[i]=xc[i]; 
			}
			point->push_back(xcc);
			weight->push_back(poids);
		}
		j++; // l'indice pour coef_Cnp et signe
	}

	// On sauvegarde le plan stochastique dans this (SetRandomVariable)
	int np=point->size();
	this->typesampling=type;
	this->degre_max=degre;
	this->x=dmatrix(np+1,nx+1);
	this->w=dvector(np+1);
	this->np=np;
	int ki=0;
	double *xp,wp;
	vector<double *>::iterator ipoint;
	for(ipoint=point->begin();ipoint!=point->end();++ipoint) {
		xp=*ipoint;
		wp=(*weight)[ki];
		for(int i=0;i<nx;i++) 
		{
			this->x[ki+1][i+1]=xp[i];
		}
		this->w[ki+1]=wp;
		ki++;
	}

	// nettoyage
	free_dvector(xc);
	free_ivector(coef_Cnp);
	free_ivector(signe);
	free_ivector(min);
	free_ivector(max);
	for(it=indices->begin();it!=indices->end();++it) {
		k=*it;
		free_ivector(k);
	}

	// Algo pas du tout efficace !!!
	// Si formules emboitees on doit retirer les doublons
	if(type=="SmolyakGauss") {
		return; // Formule non emboitees pour Gauss
	}

	// Si doublon on recopie le poids dans celui qui lui correspond puis on le met a 0
	int k1, k2, flag;
	for(k1=1;k1<=np;k1++) {
		if(this->w[k1]) {
			for(k2=k1+1;k2<=np;k2++) {
				if(this->w[k2]) {
					flag=1;
					for(i=1;i<=nx;i++) 
					{
						if(fabs(this->x[k1][i]-this->x[k2][i])>1.e-6) {
							flag=0;
							i=nx+1;
						}
					}
					if(flag) {
						this->w[k1]+=this->w[k2];
						this->w[k2]=0.;
					}
				}
			}
		}
	}

	// Nombre de points differents (on ne compte pas ceux pour lesquels le poids = 0)
	int npr=0;
	for(k1=1;k1<=np;k1++) {
		if(this->w[k1]) {
			npr++;
		}
	}

	// On reconstitue l'echantillon
	double **xr = dmatrix(npr+1,nx+1);
	double *wr  = dvector(npr+1);
	int kt;
	for(kt=1,k1=1;k1<=np;k1++) {
		if(this->w[k1]) {
			for(i=1;i<=nx;i++) {
				xr[kt][i]=this->x[k1][i];
			}
			wr[kt]=this->w[k1];
			kt++;
		}
	}
	free_dmatrix(this->x,np+1);
	free_dvector(this->w);
	this->x=xr;
	this->w=wr;
	this->np=npr;
	for(int i=0;i<nx;i++) {
		delete fq[i];
	}
	delete point;
	delete weight;
	delete indices;
}

// Test des variables stochastiques
void teststo(string type,double a, double b) {
	int test=0;
	if(type=="Normale")            { 
		if((a==0.)&&(b=1.)) {
			test=1;
		}
	}
	else if(type=="Uniforme")      { 
		if((a==0.)&&(b=1.)) {
			test=1;
		}
	}
	else if(type=="Exponentielle") { 
		if((a==1.)&&(b=1.)) {
			test=1;
		}
	}
	if(!test) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(teststo) : variables are not stochastic " << type << " " << a << " " << b << endl;
		nisp_error ( msg.str() );
	}
}
// Test des variables stochastiques uniformes
void testnor(string type,double a, double b) {
	int test=0;
	if(type=="Uniforme")            { 
		if((a==0.)&&(b=1.)) {
			test=1;
		}
	}
	if(!test) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(teststo) : variables are not stochastic-uniform " << type << " " << a << " " << b << endl;
		nisp_error ( msg.str() );
	}
}
// Constition de l'ensemble des multi indices
// A l'etat initial *indices est un vecteur nul
// A l'etat final *indices contient tous les multi indices k
// qui verifient la contrainte vmin <= norme_L1(k) <= vmax
void nisp_Build_MultiIndex_Filtre_norme_L1(vector<int *> * indices, int * min, int * max, int nx, int vmin, int vmax) {
	int * k = ivector(nx);
	int * knew;
	int i;

	for(i=0;i<nx;i++) k[i]=min[i];
	if(Filtre_norme_L1(k,nx,vmin,vmax)) {
		knew = ivector(nx);
		for(i=0;i<nx;i++) {
			knew[i]=k[i];
		}
		indices->push_back(knew);
	}
	while (nisp_incrementation(k,min,max,nx)) {
		if(Filtre_norme_L1(k,nx,vmin,vmax)) {
			knew = ivector(nx);
			for(i=0;i<nx;i++) {
				knew[i]=k[i];
			}
			indices->push_back(knew);
		}
	}
	free_ivector(k);
}

void nisp_Compute_Coef_Cnp_Signe(int *coef_Cnp, int *signe, vector<int *> * indices, int nx, int l) {
	vector<int *>::iterator it;
	int *k, norme, j=0;
	for(it=indices->begin();it!=indices->end();++it) {
		k=*it;
		norme = norme_L1(k,nx);
		coef_Cnp[j]= Cnp(nx-1,norme-l);
		if((l+nx-norme-1)%2) {
			signe[j]=-1;
		}
		else {
			signe[j]=1;
		}
		j++;
	}
}

int Cnp(int n, int p) {
	if(n<p) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(Cnp) : n =" << n << " < p=" << p << " !! " << endl;
		nisp_error ( msg.str() );
		return(0);
	}
	if(p<0) {
		ostringstream msg;
		msg<<"NISP - ERROR"<<endl;
		msg << "Nisp(Cnp) : p=" << p << " < 0 !! " << endl;
		nisp_error ( msg.str() );
		return(0);
	}
	int i,fn,fd;
	// factoriel n sur factoriel p
	for(fn=1,i=p+1;i<=n;i++) {
		fn=fn*i;
	}
	// factoriel n-p
	for(fd=1,i=1;i<=(n-p);i++) {
		fd=fd*i;
	}
	return(fn/fd);
}

// incrementation du multi indice (k(0),k(1),...,k(nx-1))
// min(i)<= k(i) <= max(i)
// Remarque : avant l'appel l'initialisation doit avoir ete effectuee
// Valeur initiale du multi indice (min(0),min(1),...,min(nx-1))
// retour 1 si l'incrementation a ete realisee 
// retour 0 si l'incrementation n'a pas ete realisee
// car le maximun a ete atteint (max(0),max(1),...,max(nx-1)
int nisp_incrementation(int k[], int min[], int max[], int nx) {
	for(int i=0;i<nx;i++) {
		if(k[i]<max[i]) {
			k[i]+=1;
			for(int j=0;j<i;j++) {
				k[j]=min[j];
			}
			return(1);
		}
	}
	return(0);
}

// Norme L1 du multi indice
int norme_L1(int k[], int nx) {
	int i,s=0;
	for(i=0;i<nx;i++) s+=k[i];
	return(s);
}

//Norme Linfini du multi indice
int norme_Linf(int k[], int nx) {
	int i,s;
	for(s=k[0],i=1;i<nx;i++) {
		if(k[i]>s) {
			s=k[i];
		}
	}
	return(s);
}

// Filtre : norme min <= L1 <= max
int Filtre_norme_L1(int k[], int nx, int vmin, int vmax) {
	int norme=norme_L1(k,nx);
	if((norme>=vmin) && (norme<=vmax)) {
		return(1);
	}
	else {
		return(0);
	}
}

