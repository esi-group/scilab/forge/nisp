//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#ifndef _NISP_IND_H_
#define _NISP_IND_H_

int IndiceMultipleEval(int nnx, int noi, int nof, int **indmul);

#endif /* _NISP_IND_H_ */

