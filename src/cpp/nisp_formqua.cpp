//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>

#define _USE_MATH_DEFINES
#include "math.h"

#include "nisp_pc.h"

using namespace std;

#include "nisp_util.h"
#include "nisp_math.h"
#include "nisp_msg.h"
#include "nisp_qua.h"
#include "nisp_formqua.h"

//======================================================
// Private functions:
void TrapezoidalRule(double x[], double w[], int n);
void FejerRule(double x[], double w[], int n);
void ClenshawCurtisRule(double x[], double w[], int n);
//======================================================

FormuleQuadrature::FormuleQuadrature(string type_va, string type_quadrature, int level) {
	int i,j;
	type=type_quadrature;
	niveau=level;
	np=ivector(level);
	if(type_quadrature=="SmolyakTrapeze") {   // np = 1 3 7 15 ... n = 2**level - 1
		np[0]=1;
		for(i=1;i<level;i++) {
			np[i]=2*np[i-1]+1;
		}
	}
	else if(type_quadrature=="SmolyakFejer") { // np = 1 3 7 15 ... n = 2**level - 1
		np[0]=1;
		// TODO : check these nested loops
		for(i=1;i<level;i++) {
			for(np[i]=1,j=0;j<=i;j++) {
				np[i]=np[i]*2;
			}
			np[i]=np[i]-1;
		}
	}
	else if(type_quadrature=="SmolyakClenshawCurtis") { // np = 1 3 5 9 ... n = 2**(level-1) + 1 si level >=2
		np[0]=1;
		// TODO : check these nested loops
		for(i=1;i<level;i++) {
			for(np[i]=1,j=0;j<i;j++) {
				np[i]=np[i]*2;
			}
			np[i]=np[i]+1;
		}
	}
	else {
		for(i=0;i<level;i++) {
			np[i]=i+1; // Gauss
		}
	}

	x = new double * [level];
	w = new double * [level];
	for(i=0;i<level;i++) {
		x[i] = new double[np[i]];
	}
	for(i=0;i<level;i++) {
		w[i] = new double[np[i]];
	}

	if(type_quadrature=="SmolyakTrapeze")    {
		for(i=0;i<level;i++) {
			TrapezoidalRule(x[i],w[i],np[i]);
		}
	}
	else if(type_quadrature=="SmolyakFejer") {
		for(i=0;i<level;i++) {
			FejerRule(x[i],w[i],np[i]);
		}
	}
	else if(type_quadrature=="SmolyakClenshawCurtis") {
		for(i=0;i<level;i++) {
			ClenshawCurtisRule(x[i],w[i],np[i]);
		}
	}
	else {
		for(i=0;i<level;i++) {
			Quadrature0(x[i],w[i],np[i],type_va); // Gauss
		}
	}
}

FormuleQuadrature::~FormuleQuadrature() {
	for(int i=0;i<niveau;i++) {
		delete[] x[i]; 
		delete[] w[i];
	}
	free_ivector(np);
}

// Methode du trapeze (bords retires n = 2 ** (l-1) + 1)
void TrapezoidalRule(double x[], double w[], int n) {
	int i;
	double delta = 1./ (n + 1.);
	for(i=0;i<n;i++) {
		x[i]= (i+1.)*delta; w[i]=1./n;
	}
}

// Methode de Fejer
void FejerRule(double x[], double w[], int n) {
	int i,j;
	double theta;
	for(i=0;i<n;i++) {
		theta=(i+1.)*M_PI/(n+1.);
		x[i]=cos(theta);
		for(w[i]=0,j=0;j<(n+1)/2;j++) {
			w[i]+=sin((2.*j+1.)*theta)/(2.*j+1.);
		}
		w[i]=w[i]*sin(theta)*4./(n+1.);
	}
	for(i=0;i<n;i++) {
		x[i]=(x[i]+1.)/2.; 
		w[i]=w[i]/2.;
	} // entre [0,1]
}

// Methode de Clenshaw Curtis
void ClenshawCurtisRule(double x[], double w[], int n) {
	int i,j;
	double theta;
	if(n==1) {x[0]=0.; w[0]=2.;}
	else { // ici n est > 2
		for(i=0;i<n;i++) {
			x[i]=-cos(M_PI * i / (n-1));
		}
		w[0]=w[n-1]=1./(n*(n-2.));
		for(i=1;i<n-1;i++) {
			theta=M_PI * i / (n-1);
			for(w[i]=0.,j=1;j<=(n-1)/2-1;j++) {
				w[i]+=2./(1-4*j*j) * cos(2.*theta*j);
			}
			w[i]+=1./(1-4*j*j) * cos(theta*(n-1));
			w[i]=(2./(n-1))*(1.+w[i]);
		}
	}
	for(i=0;i<n;i++) {
		x[i]=(x[i]+1.)/2.; 
		w[i]=w[i]/2.;
	} // entre [0,1]
}


