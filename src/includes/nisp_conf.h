// 2009 - Digiteo - Michael Baudin
#ifndef _NISP_CONF_H_
#define _NISP_CONF_H_

#ifdef _MSC_VER
	#if LIBNISP_EXPORTS 
		#define NISP_IMPORTEXPORT __declspec (dllexport)
	#else
		#define NISP_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define NISP_IMPORTEXPORT
#endif

#ifdef __cplusplus
extern "C" {
#endif 

// nisp_msgsetfunction --
//   Configure the message function
NISP_IMPORTEXPORT void nisp_msgsetfunction ( void (* f)(char * message));

// nisp_verboselevelset --
//   Configure the verbose level
NISP_IMPORTEXPORT void nisp_verboselevelset ( int newlevel);

// nisp_verboselevelget --
//   Returns the current verbose level
NISP_IMPORTEXPORT int nisp_verboselevelget ();

// nisp_errorsetfunction --
//   Configure the error function.
NISP_IMPORTEXPORT void nisp_errorsetfunction ( void (* f)(char * message) );

// nisp_initseed --
//   Configure the seed for the random number generator.
NISP_IMPORTEXPORT void nisp_initseed ( int );

#ifdef __cplusplus
}
#endif 

#endif /* _NISP_CONF_H_ */

