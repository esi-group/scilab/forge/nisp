// 2009 - Digiteo - Michael Baudin
/*!
  \file   nisp_pc.h
  \brief  Class of polynomial chaos
*/

#ifndef _NISP_PC_H_
#define _NISP_PC_H_

#ifdef _MSC_VER
	#if LIBNISP_EXPORTS 
		#define NISP_IMPORTEXPORT __declspec (dllexport)
	#else
		#define NISP_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define NISP_IMPORTEXPORT
#endif

#ifdef _MSC_VER
#pragma warning( disable : 4251 )
#endif

#include "nisp_gva.h" // Class of the set of random variables

using namespace std;

//! Class of polynomial chaos
class NISP_IMPORTEXPORT PolynomialChaos {
 public :
  //! Stochastic dimension (number of orthogonal polynomials)
  int nx;
  //! Set of stochastic variables
  SetRandomVariable * gpx;
  //! Number of terms of ANOVA decomposition
  int ni; // 2**nx-1
  //! Degree
  int no;
  //! Number of coefficients = p+1
  int p;
  //! Number of output
  int ny;
  //! Number of simulations
  int np;
  //! Types of orthogonal polynomials ("Hermite","Legendre","Laguerre")
  vector<string> typol;
  //! Function associated wtih orthogonal polynomials ("hermite()","legendre()","laguerre()")
  vector<int> funpol;
  //! Input x[1...nx]
  double *x;
  //! Output y[1...ny]
  double *y;
  //! Sample sample[1...ny][1...gpx->np]
  double **sample;
  //! Ordored sample
  int **sampleindx;
  //! Experimental design target [1...np][1...ny]
  double **target;
  //! Monomial of orthogonal polynomials phi[1...nx][0...no]
  double **phi;
  //! Monomial multidimensional polynomial  psi[0...p] 
  double *psi;
  //! Matrix of relation between indice k=[0...p] and multiple indice
  int    **indmul;
  //! Matrix of coefficients beta[1...ny][0...p]
  double **beta;
  //! Mean moyenne[1...ny]=beta[1...ny][0]
  double *moyenne;
  //! Variance variance[1...ny]
  double *variance;
  //! Indices of sensitivity : indices[1...ny][1...ni]
  double **indices;
  //! Total sensitivity indices : indices_globaux[1...ny][1...nx]
  double **indices_globaux;
  //! Blackboard of the rank of variables in the set (groupe)
  int **indices_setvar;
  //! Set of random variable for ANOVA
  int *groupe;
  //! Expectation conditionnal
  int *rank;
  //===================================================
 public:
  /** @name Constructor and Destructor
   **/
  //@{
	 // TODO : remove the argument ny, since it is available from the SetDimensionOutput method
  //! Constructor from a set of stochastic variables, where ny is the number of output
  PolynomialChaos(SetRandomVariable *gva, int ny=1);
  //! Constructor from a file
  // TODO : move that char * into a proper string
  PolynomialChaos(char *file);
  //! Constructor from another PC for conditional expectation
  PolynomialChaos(PolynomialChaos *pc, int *var_sto, int nsto);
  // Destructor (free the allocated memory)
  ~PolynomialChaos();
  //@}
  //! Definition of the degree (no)
  void SetDegree(int no);
  //! Dimension of output (ny)
  void SetDimensionOutput(int ny);
  //! Allocation of memory for the target
  // TODO : rename into SetSize, so that the name is consistent with the SetRandomVariable class
  void SetSizeTarget(int np);
  // TODO : rename "Target" into "Output" ?
  //! Returns the size of the target (np)
  int GetSizeTarget();
  //! Assign a target experiment #k (where 1<= k <=np), for output #j (where 1<= j <= ny)
  void SetTarget(int k, int j, double output);
  //! Returns the value of experiment #k (where 1<= k <=np), for output #j (where 1<= j <= ny)
  double GetTarget(int k, int j);
  //! Read a set of target from a file
  void ReadTarget(char *file);
  //TODO : void WriteTarget(char *file); : Write a set of target to a file;
  //! Freeing the target memory 
  void FreeMemoryTarget();
  // TODO : why giving a SetRandomVariable once again ? It has already been given at object's creation.
  //! Computation of coefficients by method ("Integration", "Regression")
  // TODO : remove the string, take an integer as argument, define the possible integers as NISP_EXPANSION_REGRESSION, NISP_EXPANSION_INTEGRATION, etc...
  void ComputeChaosExpansion(SetRandomVariable *gva, string methode);
  void ComputeChaosExpansion(SetRandomVariable *gva, char * methode);
  //! Conditionnal Expectation with input[1...nx] and var_opt[0...nopt-1]
  void ComputeChaosExpansion(PolynomialChaos * pc, double *input, int *var_opt, int nopt);
  //! Save the polynomial chaos in a "file"
  void Save(char *file);
  //! Mean
  double GetMean(int=1);
  void GetMean(double * mean);
  //! Variance
  double GetVariance(int=1);
  void GetVariance( double * var );
  //! Covariance
  double GetCovariance(int i, int j);
  //! Correlation coefficient
  double GetCorrelation(int i, int j);
  //! First indice of sensitivity
  double GetIndiceFirstOrder(int i, int j=1);
  void GetIndiceFirstOrder( double ** ind );
  //! Total indice of sensitivity
  double GetIndiceTotalOrder(int i, int j=1);
  void GetIndiceTotalOrder( double ** ind );
  //=====================================================
  // Gestion d'un groupe de variables
  //
  //! Set the group as empty
  void SetGroupEmpty();
  //! Add a random variable (rank i) in the set
  void SetGroupAddVar(int i);
  //! Prints the multiple indices
  // TODO : rename into PringMultipleIndices
  void GetMultipleIndices();
  //! Returns the current number of variables in the group
  int GetGroupSize();
  //! Returns the current variables in the group
  // The vararray array must have size GetGroupSize().
  // vararray[i] is the i-th variable in the group.
  void GetGroup(int * vararray);
  // Print the current group of variables
  void PrintGroup();
  //! Indice of sensitivity of a set of variables - output 1 by default
  double GetGroupIndice(int=1);
  void GetGroupIndice( double * ind );
  //! Indice of interaction sensitivity of a set of variables - output 1 by default
  double GetGroupIndiceInteraction(int=1);
  void GetGroupIndiceInteraction( double * ind );
  //=====================================================
  
  //! Edition of the ANOVA decomposition
  void GetAnova(int r=1);
  //! Edition of the ANOVA ordered decomposition
  void GetAnovaOrdered(double seuil=1., int r=1);
  //!  Edition of the ANOVA ordered decomposition / coefficients
  void GetAnovaOrderedCoefficients(double seuil=1., int r=1);
  //! Writing the program C of the ploynomial chaos in a file
  void GenerateCode(char *pcsrc, char *name);
  //! Number of input (nx)
  int GetDimensionInput();
  //! Number of output (ny)
  int GetDimensionOutput();
  //! Number of the coefficients of the polynomial chaos (p=number-1)
  int GetDimensionExpansion();
  //! Degree
  int GetDegree();
  //! Informations : type of the set of orthogonal polynomial
  void GetLog();
  //! Build a sample for statitical analysis (quantile)
  // TODO : what is order ?
  void BuildSample(string type, int np, int order=1);
  void BuildSample(char * type, int np, int order);
  //! The value of the output j for the example k
  double GetSample(int k, int j=1);
  // Returns the size used to sample the polynomial (this is gpx->np and not np)
  int GetSampleSize( );
  //! Polynomial chaos is a random variable : a realisation of this random variable (GetOutput()).
  void Realisation();
  //! Quantile of order alpha
  double GetQuantile(double alpha, int j=1);
  void GetQuantile(double alpha, double * quantile );
  //! Wilks's Quantile of order alpha with confidence beta
  double GetQuantileWilks(double alpha, double beta, int j=1);
  void GetQuantileWilks(double alpha, double beta, double * quantile );
  //! Probability of a value overrun 
  double GetInvQuantile(double seuil, int j=1);
  void GetInvQuantile(double seuil, double * quantile);
  // Free the allocated memory
  void FreeMemory();
  //! Compute the output[1...ny], depending on the input.
  void Compute(double *input, double *output);
  //
  //===================================================
  private:
  //! Propagation of input which has been specified by SetInput()
  // TODO : What for ? It is called by ComputeOutput anyway, so is it really necessary ? May be it should be kept private.
  void PropagateInput();
  //! Propagation of input[1...nx]
  // TODO : remove this function, since setinput serves the same purpose
  void PropagateInput(double *input);
  //
  void coefficients_integration (SetRandomVariable *gva);
  void coefficients_regression(SetRandomVariable *gva);
  //! Preparation on the ANOVA decomposition (for Sobol'indices)
  void SetAnova();
  //! Set an input : i=1,...,nx
  void SetInput(int i, double x);
  //! Computation of output y[1...ny]
  void ComputeOutput();
    //! Computation of output in relation with input : input[1...nx]
  // TODO : What for ? ComputeOutput should take both input and output as argument so that one call should be sufficient ?
  // TODO : remove this function, since setinput serves the same purpose
  void ComputeOutput(double *input);
  //! Get an output (rank of output is 1 by default) : i=1,...,nx
  double GetOutput(int=1);
  //! Computation of output output[1...ny]
  void GetOutput ( double * output );

};
#endif /* _NISP_PC_H_ */
