// 2013 - Michael Baudin
// 2009 - INRIA - Michael Baudin
// 2009 - Digiteo - Michael Baudin
/*!
\file   nisp_gva.h
\brief  Class of set of random variables
*/

#ifndef _NISP_GVA_H_
#define _NISP_GVA_H_

#include <vector>

#ifdef _MSC_VER
#pragma warning( disable : 4251 )
#endif

#include "nisp_va.h" // Class of random variables
using namespace std;


#ifdef _MSC_VER
#if LIBNISP_EXPORTS 
#define NISP_IMPORTEXPORT __declspec (dllexport)
#else
#define NISP_IMPORTEXPORT __declspec (dllimport)
#endif
#else
#define NISP_IMPORTEXPORT
#endif



//! Class of set of random variables
class NISP_IMPORTEXPORT SetRandomVariable {
public :
	//! number of random variables
	int nx; // Int_t
	//! vector of random variables  va[0],va[1],...,va[nx-1]
	vector<RandomVariable *> va;
	//! size of the sample (number of simulations)
	int np;
	//! maximum value of the degree
	int degre_max;
	//! experimental matrix x[1...np][1...nx]
	// x[k][i] is the sample for the experiment #k, 
	// for the variable #i, for k=1,2,...,np and i=1,2,...,nx.
	// The matrix x has the size (np+1)*(nx+1):
	// x[0][*] is ignored
	// x[*][0] is ignored
	double **x;
	//! weights when the sample is built from quadrature w[1...np]
	// TODO : why are the weights in this class ?
	double *w;
	//! name of the sampling method : "Quadrature","Petras","MonteCarlo",
	// "Lhs","SmolyakGauss","SmolyakTrapeze", "SmolyakFejer", "SmolyakClenshawCurtis"
	string typesampling;
	// Number of characters for the fields in the data file
	int datafile_nbchar;
	// Precision of floats in the data file
	int datafile_precision;
public :
	/** @name Constructor and Destructor
	**/
	//@{
	//! Constructor
	SetRandomVariable();
	// Destructor
	~SetRandomVariable();
	//! Constructor from a set of n uniform variables U[0,1]
	SetRandomVariable(int n);
	//! Constructor from a file
	// This file may have been created by the Save method.
	SetRandomVariable(char *file);
	//@}
	//! Free memory : matrix **x and weights *w
	void FreeMemory();
	//! Add of a random variable
	void AddRandomVariable(RandomVariable *va);
	// TODO : create the SetType method and remove the "type" argument
	// TODO : create the GetType method.
	//! Realisation of a sample by the method "type", size "np"
	void BuildSample(string type, int np);
	void BuildSample(char * name, int n);
	//! Realisation of a sample by the method "type", size "np" (and selection one from ne samples)
	void BuildSample(string type, int np, int ne);
	void BuildSample(char * name, int n, int ne);
	//! Realisation of a sample from another sample of "*gva" (change of variables)
	void BuildSample(SetRandomVariable *gva);
	//! Set the type of a sample
	void SetSampleType(string type);
	void SetSampleType(char * name);
	//! Set the number of experiments in a sample
	void SetSampleSize(int size);
	// TODO : rename into SetInput
	//! Specification of the element i of the example k of the sample defined by anther component (OPENTURNS, URANIE, ...)
	void SetSample(int k, int i, double value);
	//! Save in the file fichier the set of random variables and its sample, if it exists
	// TODO : transform fichier from C / char * into a C++ / string
	void Save(char *fichier);
	//! Stochastic dimension (nx)
	int GetDimension();
	// TODO : create a method SetSize ( int np ) and remove the np argument from the BuildSample method
	//! Size of the sample (np)
	int GetSize();
	// TODO : rename GetSample into GetInput
	//! Value of the input #i=1,...nx of the sample #k=1,...np
	double GetSample(int k, int i);
	//! Informations on the set of random variables
	void GetLog();
	// TODO : add WeightsSet, if the weights really are to be stored in this class
	// TODO : add WeightsGet, if the weights really are to be stored in this class
	//
	//===================================================
private:

	void nisp_BuildSampleLHS(int np);
	void nisp_BuildSampleQMCSobol(int np);
	void nisp_BuildSampleLHSmaxmin(int np, int ne);
	void nisp_BuildSamplePetras(int degre);
	void nisp_BuildSampleQuadrature(int degre);
	void nisp_BuildSampleMontecarlo(int np);
	void nisp_BuildSampleSmolyak(string type, int degre);
	int CheckSampleType();
};

#endif /* _NISP_GVA_H_ */
