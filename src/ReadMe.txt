   Non Intrusive Spectral Projection
   v2.1 - July 2009

Jean-Marc Martinez - CEA
Michael Baudin - Digiteo

This document is a brief installation guide for the scientific work 
about the library NISP, Non Intrusive Spectral Projection, for the Chaos 
Polynomial of the OPUS platform. This work is one of deliverable of the 
WP1 dealing with the methodology and the use of Polynomial Chaos.

Languages
---------

NISP is mainly a set of C++ classes. 
To solve portability issues, a small set of fortran 
source code provide some elementary functions.

Portability issues
------------------

The basic operating system for NISP is Linux,
but the library can also be generated under Windows.
This document explains both procedures.

How to compile NISP under Linux
-------------------------------

1 - Installer NispOpus.tgz en faisant :

	tar xzf NispOpus.tgz

2 - Vérifier la création du répertoire NispV2.1

3 - aller dans NispV2.1

4 - Vérifier par ls -als la présence de :

total 32
4 drwxr-xr-x  7 jm088440 gletr 4096 2009-07-15 15:15 ./
4 drwxr-xr-x 12 jm088440 gletr 4096 2009-07-15 15:03 ../
4 drwxr-xr-x  2 jm088440 gletr 4096 2009-07-15 14:51 includes/
4 -rw-r--r--  1 jm088440 gletr  260 2009-07-15 15:15 ReadMe.txt
4 drwxr-xr-x  2 jm088440 gletr 4096 2009-07-15 14:56 src/
4 drwxr-xr-x  3 jm088440 gletr 4096 2009-07-15 14:04 Usecase/


5 - aller dans le répertoire src et faire :
	
	make all

This creates the lib directory, which contains :
<TODO>

6 - Dans vos chemins d'acces aux librairies dynamiques il faut rajouter
    le chemin vers les librairies "libnisp_c.so" et "libnisp_f.so". Il faut modifier votre
    .cshrc :

	setenv LD_LIBRARY_PATH /path/to/NispV2.1/lib:$LD_LIBRARY_PATH

7 - aller dans le répertoire NispV2.1/Usecase/Example1 et faire
	
	make all

8 - Puis sous le même répertoire pour construire une representation fonctionnelle
    à base de chaos polynomial de degre 5 de cet exemple simple, faire :

	nisp 5

vous devriez avoir sur la sortie standard :

Mean     = 1.75
Variance = 1
Indice de sensibilite du 1er ordre
    Variable X1 = 0.765625
    Variable X2 = 0.1875
Indice de sensibilite Totale
    Variable X1 = 0.8125
    Variable X2 = 0.234375

How to compile NISP under Windows
---------------------------------

Under Windows, NISP can be compiled into a native 
binary form with Visual Studio 2008.
The files provided are :
 - src/LibNisp.sln : the main solution 
 - src/cpp/libnisp_c.vcproj : the project for the C++ library
 - src/cpp/libnisp_f.vcproj : the project for the Fortran library
Compiling the source code generates the following files :
- src/cpp/libnisp_c.dll : the dll for the C++ library
- src/fortran/libnisp_f.dll : the dll for the Fortran library

Licence
-------
Nisp Library

This toolbox is released under the terms of the GNU Lesser General Public License license :
http://www.gnu.org/copyleft/lesser.html

