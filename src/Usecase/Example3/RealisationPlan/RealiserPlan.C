/*!
  \file   RealiserPlan.C
  \brief  Polynome de Chaos - Quadrature
*/
#include <iostream>
#include <fstream>
#include "nisp_pc.h" // classes Nisp

using namespace std;
// Modele mathematique : y = x1 * x2
void Exemple1(double x[], double y[]) {
  y[0]=x[0]*x[1];
}

/*!
  \fn void RealiserPlan(char * fichierplanExperiences)
  \brief Realiser un plan d'experiences
  \param fichierPlanExperiences : fichier contenant le plan d'experiences a realiser
*/
void RealiserPlan(char *fichierPlanExperiences, char *fichierReponses) {
  SetRandomVariable * gu = new SetRandomVariable(fichierPlanExperiences);

  int nx = gu->GetDimension();  // nx=2
  int np=gu->GetSize();         // taille du plan
  int ny=1;
  double input[nx], output[ny];

  ofstream sortie(fichierReponses,ios::out);
  if(!sortie) { cout << "Creation du fichier " << fichierReponses << " impossible\n"; exit(-1); }
  sortie << "#NombreSimulations " << np << endl;
  sortie << "#NombreOutput      " << ny << endl;
  sortie << endl;
  sortie.setf(ios::scientific, ios::floatfield);
  sortie.precision(8);

  int i,j,k;
  for(int k=1;k<=np;k++) {
    for(int i=0;i<nx;i++) input[i]=gu->GetSample(k,i+1);  // on recupere les entrees
    Exemple1(input,output);                               // appel au modele numerique
    for(j=0;j<ny;j++) { sortie.width(16); sortie << output[j]; }
    sortie << endl;
  }
  sortie.close();
}

int main(int argc, char *argv[]) {
  if(argc != 3) {
    cout << "Erreur !! la commande est de la forme << nisp fichierPlanExperiences fichierReponses>>" << endl;
  }
  else {
    RealiserPlan(argv[1], argv[2]);
  }
}
