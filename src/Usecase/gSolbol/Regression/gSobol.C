/*!
  \file   gSobol.C
  \brief  Polynomes de Chaos - Analyse de Sensibilite
*/
#include <iostream>
#include <cmath>     // pour disposer de la macro M_PI = 3.14159...
#include "nisp_pc.h" // les classes Nisp

using namespace std;

// Fonction gSobol
void gSobol(double x[], double y[], int nx) {
  int i;
  double a,v;
  for(v=1.,i=0;i<nx;i++) {
    a=((double) i)/2.;
    v=v*(fabs(4.*x[i]-2.)+a)/(1.+a);
  }
  y[0]=v;
}


/*!
  \fn void CasegSobol(int np)
  \brief Polynome de Chaos - Regression
  \param nx : nombre de variables aleatoires
  \param np : nombre de simulations
*/
void CasegSobol(int nx, int np) {

  SetRandomVariable * gx = new SetRandomVariable(nx);
  gx->BuildSample("QmcSobol",np);

  PolynomialChaos * pc = new PolynomialChaos(gx);

  // Realisation du plan d'experiences
  int ny=pc->GetDimensionOutput();
  pc->SetSizeTarget(np);
  double input[nx], output[ny];
  for(int k=0;k<np;k++) {
    for(int i=0;i<nx;i++) input[i]=gx->GetSample(k+1,i+1);  // on recupere les entrees
    gSobol(input,output,nx);                                   // on realise l'experience numerique
    for(int j=0;j<ny;j++) pc->SetTarget(k+1,j+1,output[j]); // on transmet les sorties calculees au PC
  }

  // Calcul du PC
  pc->SetDegree(6);
  pc->ComputeChaosExpansion(gx,"Regression");

  // Calcul des valeurs exactes
  double mean=1.;
  double variance, s;
  variance=1.;
  for(int i=0;i<nx;i++) {s=3*(1+i/2.)*(1+i/2.); variance*=(1./s+1.);} variance=variance-1.;
  // Moyenne et variance
  cout << "Moyenne     : " << pc->GetMean()     << " : " << mean     << endl;
  cout << "Variance    : " << pc->GetVariance() << " : " << variance << endl;

  // Indices de sensibilite du premier ordre
  for(int i=1;i<=nx;i++)
   cout << "Indice First Order : " << i << " = " << pc->GetIndiceFirstOrder(i) << endl;

  // Indices de sensibilite totale
  for(int i=1;i<=nx;i++)
   cout << "Indice Total Order : " << i << " = " << pc->GetIndiceTotalOrder(i) << endl;

  pc->GetAnovaOrderedCoefficients(0.995);
}


int main(int argc, char *argv[]) {
  if(argc != 3) {
    cout << "Erreur !! la commande est de la forme << nisp nx np >>" << endl;
    cout << "nx : nombre de variables aleatoires" << endl;
    cout << "np : nombre de simulations" << endl;
  }
  else {
    int nx = atoi(argv[1]);
    int np = atoi(argv[2]);
    CasegSobol(nx,np);
  }
}
