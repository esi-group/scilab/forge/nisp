/*
 * Copyright (C) 2012 - Michael Baudin
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2008 - INRIA
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
#ifndef _MTRNG_H_
#define _MTRNG_H_

#ifdef _MSC_VER
	#if LIBTHIRDPARTY_EXPORTS 
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllexport)
	#else
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define THIRDPARTY_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

// This is the status of a function which performs correctly.
static int MTRNG_OK = 0;

// The state of a function which generates an error.
static int MTRNG_ERROR = 1;

// The maximum integer produced by mtrng_irand();
// This is 2^32-1
static unsigned int MTRNG_INTMAX = 4294967295;

// Configure the function which prints messages.
// This function must be called before calling any 
// function.
THIRDPARTY_IMPORTEXPORT void mtrng_messagesetfunction ( void (* f)(char * message) );

// Returns a double uniform in (0,1)
THIRDPARTY_IMPORTEXPORT double mtrng_urand(void);

// Returns an integer uniform in [1,MTRNG_INTMAX]
THIRDPARTY_IMPORTEXPORT unsigned int mtrng_irand(void);

// Set the seed of the generator with one single double.
THIRDPARTY_IMPORTEXPORT int mtrng_set_state_simple(double s);

// Set the seed of the mt generator.
// Returns MTRNG_OK if the seed can be set.
// Returns MTRNG_ERROR in case of error.
THIRDPARTY_IMPORTEXPORT int mtrng_set_state(double seed_array[]);

// Returns the current seed of the random number generator.
THIRDPARTY_IMPORTEXPORT void mtrng_get_state(double state[]);

__END_DECLS

#endif /** _MTRNG_H_   **/

