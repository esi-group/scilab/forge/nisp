//
// Some utilities to manage time, precision of real/doubles and primes.
//
// Copyright (C) 2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#ifndef _THIRDPARTY_UTILS_H_
#define _THIRDPARTY_UTILS_H_

#ifdef _MSC_VER
	#if LIBTHIRDPARTY_EXPORTS 
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllexport)
	#else
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define THIRDPARTY_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS


THIRDPARTY_IMPORTEXPORT int i4_log_i4 ( int i4, int j4 );
THIRDPARTY_IMPORTEXPORT int i4_min ( int i1, int i2 );
THIRDPARTY_IMPORTEXPORT int i4_max ( int i1, int i2 );
THIRDPARTY_IMPORTEXPORT int i4_power ( int i, int j );
THIRDPARTY_IMPORTEXPORT int prime ( int n );
THIRDPARTY_IMPORTEXPORT int prime_ge ( int n );
THIRDPARTY_IMPORTEXPORT int i4_characteristic ( int q );
THIRDPARTY_IMPORTEXPORT int i4_power ( int i, int j );
THIRDPARTY_IMPORTEXPORT double r8_epsilon ( void );
THIRDPARTY_IMPORTEXPORT void timestamp ( void );
THIRDPARTY_IMPORTEXPORT char *timestring ( void );

__END_DECLS

#endif /* _THIRDPARTY_UTILS_H_ */


