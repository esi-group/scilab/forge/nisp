//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#ifndef _NISP_SMOLYAK_H_
#define _NISP_SMOLYAK_H_

#ifdef _MSC_VER
	#if LIBTHIRDPARTY_EXPORTS 
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllexport)
	#else
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define THIRDPARTY_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

THIRDPARTY_IMPORTEXPORT void size_smolyak(int *dd, int *qq, int *size);
THIRDPARTY_IMPORTEXPORT void quad_smolyak(int *dd, int *qq, double *pt, double *wt);

__END_DECLS

#endif /* _NISP_SMOLYAK_H_ */