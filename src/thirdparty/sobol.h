//
// The Sobol Quasirandom Sequence 
//
// Copyright (C) 2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#ifndef _THIRDPARTY_SOBOL_D_H_
#define _THIRDPARTY_SOBOL_D_H_

#ifdef _MSC_VER
	#if LIBTHIRDPARTY_EXPORTS 
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllexport)
	#else
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define THIRDPARTY_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS


//
//  32 bit integer routines.
//
THIRDPARTY_IMPORTEXPORT int i4_bit_hi1 ( int n );
THIRDPARTY_IMPORTEXPORT int i4_bit_lo0 ( int n );
THIRDPARTY_IMPORTEXPORT void i4_sobol ( int dim_num, int *seed, float quasi[ ] );
THIRDPARTY_IMPORTEXPORT int i4_uniform ( int b, int c, int *seed );
THIRDPARTY_IMPORTEXPORT unsigned int i4_xor ( unsigned int i, unsigned int j );
//
//  64 bit integer routines.
//
THIRDPARTY_IMPORTEXPORT int i8_bit_hi1 ( long long int n );
THIRDPARTY_IMPORTEXPORT int i8_bit_lo0 ( long long int n );
THIRDPARTY_IMPORTEXPORT long long int i8_max ( long long int i1, long long int i2 );
THIRDPARTY_IMPORTEXPORT long long int i8_min ( long long int i1, long long int i2 );
THIRDPARTY_IMPORTEXPORT void i8_sobol ( int dim_num, long long int *seed, double quasi[ ] );
THIRDPARTY_IMPORTEXPORT long long int i8_uniform ( long long int b, long long int c, int *seed );
THIRDPARTY_IMPORTEXPORT unsigned long long int i8_xor ( unsigned long long int i, 
  unsigned long long int j );
//
//  32 bit real routines.
//
THIRDPARTY_IMPORTEXPORT float r4_abs ( float x );
THIRDPARTY_IMPORTEXPORT int r4_nint ( float x );
THIRDPARTY_IMPORTEXPORT float r4_uniform_01 ( int *seed );
//
//  64 bit real routines.
//
THIRDPARTY_IMPORTEXPORT double r8_abs ( double x );
THIRDPARTY_IMPORTEXPORT int r8_nint ( double x );
THIRDPARTY_IMPORTEXPORT double r8_uniform_01 ( int *seed );

__END_DECLS

#endif /* _THIRDPARTY_SOBOL_D_H_ */

