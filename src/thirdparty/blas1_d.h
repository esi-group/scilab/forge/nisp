//
// Copyright (C) 2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#ifndef _THIRDPARTY_BLAS1_D_H_
#define _THIRDPARTY_BLAS1_D_H_

#ifdef _MSC_VER
	#if LIBTHIRDPARTY_EXPORTS 
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllexport)
	#else
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define THIRDPARTY_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

THIRDPARTY_IMPORTEXPORT double dasum ( int n, double x[], int incx );
THIRDPARTY_IMPORTEXPORT void daxpy ( int n, double da, double dx[], int incx, double dy[], int incy );
THIRDPARTY_IMPORTEXPORT void dcopy ( int n, double dx[], int incx, double dy[], int incy );
THIRDPARTY_IMPORTEXPORT double ddot ( int n, double dx[], int incx, double dy[], int incy );
THIRDPARTY_IMPORTEXPORT double dmach ( int job );
THIRDPARTY_IMPORTEXPORT double dnrm2 ( int n, double x[], int incx );
THIRDPARTY_IMPORTEXPORT void drot ( int n, double x[], int incx, double y[], int incy, double c, double s );
THIRDPARTY_IMPORTEXPORT void drotg ( double *sa, double *sb, double *c, double *s );
THIRDPARTY_IMPORTEXPORT void dscal ( int n, double sa, double x[], int incx );
THIRDPARTY_IMPORTEXPORT void dswap ( int n, double x[], int incx, double y[], int incy );
THIRDPARTY_IMPORTEXPORT int idamax ( int n, double dx[], int incx );
THIRDPARTY_IMPORTEXPORT bool lsame ( char ca, char cb );
THIRDPARTY_IMPORTEXPORT double r8_abs ( double x );
THIRDPARTY_IMPORTEXPORT double r8_max ( double x, double y );
THIRDPARTY_IMPORTEXPORT double r8_sign ( double x );
THIRDPARTY_IMPORTEXPORT void xerbla ( char *srname, int info );

__END_DECLS

#endif /* _THIRDPARTY_BLAS1_D_H_ */


