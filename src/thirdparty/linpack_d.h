//
// Copyright (C) 2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#ifndef _NISP_LINPACK_D_H_
#define _NISP_LINPACK_D_H_

#ifdef _MSC_VER
	#if LIBTHIRDPARTY_EXPORTS 
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllexport)
	#else
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define THIRDPARTY_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS


THIRDPARTY_IMPORTEXPORT int dchdc ( double a[], int lda, int p, double work[], int ipvt[], int job );
THIRDPARTY_IMPORTEXPORT int dchdd ( double r[], int ldr, int p, double x[], double z[], int ldz, 
  int nz, double y[], double rho[], double c[], double s[] );
THIRDPARTY_IMPORTEXPORT void dchex ( double r[], int ldr, int p, int k, int l, double z[], int ldz, 
  int nz, double c[], double s[], int job );
THIRDPARTY_IMPORTEXPORT void dchud ( double r[], int ldr, int p, double x[], double z[], int ldz, 
  int nz, double y[], double rho[], double c[], double s[] );
THIRDPARTY_IMPORTEXPORT double dgbco ( double abd[], int lda, int n, int ml, int mu, int ipvt[], 
  double z[] );
THIRDPARTY_IMPORTEXPORT void dgbdi ( double abd[], int lda, int n, int ml, int mu, int ipvt[], 
  double det[2] );
THIRDPARTY_IMPORTEXPORT int dgbfa ( double abd[], int lda, int n, int ml, int mu, int ipvt[] );
THIRDPARTY_IMPORTEXPORT void dgbsl ( double abd[], int lda, int n, int ml, int mu, int ipvt[], 
  double b[], int job );
THIRDPARTY_IMPORTEXPORT double dgeco ( double a[], int lda, int n, int ipvt[], double z[] );
THIRDPARTY_IMPORTEXPORT void dgedi ( double a[], int lda, int n, int ipvt[], double det[], 
  double work[], int job );
THIRDPARTY_IMPORTEXPORT int dgefa ( double a[], int lda, int n, int ipvt[] );
THIRDPARTY_IMPORTEXPORT void dgesl ( double a[], int lda, int n, int ipvt[], double b[], int job );
THIRDPARTY_IMPORTEXPORT int dgtsl ( int n, double c[], double d[], double e[], double b[] );
THIRDPARTY_IMPORTEXPORT double dpbco ( double abd[], int lda, int n, int m, double z[] );
THIRDPARTY_IMPORTEXPORT void dpbdi ( double abd[], int lda, int n, int m, double det[] );
THIRDPARTY_IMPORTEXPORT int dpbfa ( double abd[], int lda, int n, int m );
THIRDPARTY_IMPORTEXPORT void dpbsl ( double abd[], int lda, int n, int m, double b[] );
THIRDPARTY_IMPORTEXPORT double dpoco ( double a[], int lda, int n, double z[] );
THIRDPARTY_IMPORTEXPORT void dpodi ( double a[], int lda, int n, double det[], int job );
THIRDPARTY_IMPORTEXPORT int dpofa ( double a[], int lda, int n );
THIRDPARTY_IMPORTEXPORT void dposl ( double a[], int lda, int n, double b[] );
THIRDPARTY_IMPORTEXPORT double dppco ( double ap[], int n, double z[] );
THIRDPARTY_IMPORTEXPORT void dppdi ( double ap[], int n, double det[2], int job );
THIRDPARTY_IMPORTEXPORT int dppfa ( double ap[], int n );
THIRDPARTY_IMPORTEXPORT void dppsl ( double ap[], int n, double b[] );
THIRDPARTY_IMPORTEXPORT void dptsl ( int n, double d[], double e[], double b[] );
THIRDPARTY_IMPORTEXPORT void dqrdc ( double a[], int lda, int n, int p, double qraux[], int jpvt[], 
  double work[], int job );
THIRDPARTY_IMPORTEXPORT int dqrsl ( double a[], int lda, int n, int k, double qraux[], double y[], 
  double qy[], double qty[], double b[], double rsd[], double ab[], int job );
THIRDPARTY_IMPORTEXPORT double dsico ( double a[], int lda, int n, int kpvt[], double z[] );
THIRDPARTY_IMPORTEXPORT void dsidi ( double a[], int lda, int n, int kpvt[], double det[2], 
  int inert[3], double work[], int job );
THIRDPARTY_IMPORTEXPORT int dsifa ( double a[], int lda, int n, int kpvt[] );
THIRDPARTY_IMPORTEXPORT void dsisl ( double a[], int lda, int n, int kpvt[], double b[] );
THIRDPARTY_IMPORTEXPORT double dspco ( double ap[], int n, int kpvt[], double z[] );
THIRDPARTY_IMPORTEXPORT void dspdi ( double ap[], int n, int kpvt[], double det[2], int inert[3], 
  double work[], int job );
THIRDPARTY_IMPORTEXPORT int dspfa ( double ap[], int n, int kpvt[] );
THIRDPARTY_IMPORTEXPORT void dspsl ( double ap[], int n, int kpvt[], double b[] );
THIRDPARTY_IMPORTEXPORT int dsvdc ( double a[], int lda, int m, int n, double s[], double e[], 
  double u[], int ldu, double v[], int ldv, double work[], int job );
THIRDPARTY_IMPORTEXPORT double dtrco ( double t[], int ldt, int n, double z[], int job );
THIRDPARTY_IMPORTEXPORT int dtrdi ( double t[], int ldt, int n, double det[], int job );
THIRDPARTY_IMPORTEXPORT int dtrsl ( double t[], int ldt, int n, double b[], int job );
__END_DECLS

#endif /* _NISP_LINPACK_D_H_ */


