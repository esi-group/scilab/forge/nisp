//
// Copyright (C) 2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#ifndef _THIRDPARTY_DCDFLIB_H_
#define _THIRDPARTY_DCDFLIB_H_

#ifdef _MSC_VER
	#if LIBTHIRDPARTY_EXPORTS 
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllexport)
	#else
		#define THIRDPARTY_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define THIRDPARTY_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

THIRDPARTY_IMPORTEXPORT double algdiv ( double *a, double *b );
THIRDPARTY_IMPORTEXPORT double alnrel ( double *a );
THIRDPARTY_IMPORTEXPORT double apser ( double *a, double *b, double *x, double *eps );
THIRDPARTY_IMPORTEXPORT double bcorr ( double *a0, double *b0 );
THIRDPARTY_IMPORTEXPORT double beta ( double a, double b );
THIRDPARTY_IMPORTEXPORT double beta_asym ( double *a, double *b, double *lambda, double *eps );
THIRDPARTY_IMPORTEXPORT double beta_frac ( double *a, double *b, double *x, double *y, double *lambda,  double *eps );
THIRDPARTY_IMPORTEXPORT void beta_grat ( double *a, double *b, double *x, double *y, double *w,  double *eps,int *ierr );
THIRDPARTY_IMPORTEXPORT void beta_inc ( double *a, double *b, double *x, double *y, double *w,  double *w1, int *ierr );
THIRDPARTY_IMPORTEXPORT void beta_inc_values ( int *n_data, double *a, double *b, double *x, double *fx );
THIRDPARTY_IMPORTEXPORT double beta_log ( double *a0, double *b0 );
THIRDPARTY_IMPORTEXPORT double beta_pser ( double *a, double *b, double *x, double *eps );
THIRDPARTY_IMPORTEXPORT double beta_rcomp ( double *a, double *b, double *x, double *y );
THIRDPARTY_IMPORTEXPORT double beta_rcomp1 ( int *mu, double *a, double *b, double *x, double *y );
THIRDPARTY_IMPORTEXPORT double beta_up ( double *a, double *b, double *x, double *y, int *n, double *eps );
THIRDPARTY_IMPORTEXPORT void binomial_cdf_values ( int *n_data, int *a, double *b, int *x, double *fx );
THIRDPARTY_IMPORTEXPORT void cdfbet ( int *which, double *p, double *q, double *x, double *y,  double *a, double *b, int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdfbin ( int *which, double *p, double *q, double *s, double *xn,
  double *pr, double *ompr, int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdfchi ( int *which, double *p, double *q, double *x, double *df,
  int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdfchn ( int *which, double *p, double *q, double *x, double *df,
  double *pnonc, int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdff ( int *which, double *p, double *q, double *f, double *dfn,
  double *dfd, int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdffnc ( int *which, double *p, double *q, double *f, double *dfn,
  double *dfd, double *phonc, int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdfgam ( int *which, double *p, double *q, double *x, double *shape,
  double *scale, int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdfnbn ( int *which, double *p, double *q, double *s, double *xn,
  double *pr, double *ompr, int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdfnor ( int *which, double *p, double *q, double *x, double *mean,
  double *sd, int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdfpoi ( int *which, double *p, double *q, double *s, double *xlam,
  int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void cdft ( int *which, double *p, double *q, double *t, double *df,
  int *status, double *bound );
THIRDPARTY_IMPORTEXPORT void chi_noncentral_cdf_values ( int *n_data, double *x, double *lambda, 
  int *df, double *cdf );
THIRDPARTY_IMPORTEXPORT void chi_square_cdf_values ( int *n_data, int *a, double *x, double *fx );
THIRDPARTY_IMPORTEXPORT void cumbet ( double *x, double *y, double *a, double *b, double *cum,
  double *ccum );
THIRDPARTY_IMPORTEXPORT void cumbin ( double *s, double *xn, double *pr, double *ompr,
  double *cum, double *ccum );
THIRDPARTY_IMPORTEXPORT void cumchi ( double *x, double *df, double *cum, double *ccum );
THIRDPARTY_IMPORTEXPORT void cumchn ( double *x, double *df, double *pnonc, double *cum,
  double *ccum );
THIRDPARTY_IMPORTEXPORT void cumf ( double *f, double *dfn, double *dfd, double *cum, double *ccum );
THIRDPARTY_IMPORTEXPORT void cumfnc ( double *f, double *dfn, double *dfd, double *pnonc,
  double *cum, double *ccum );
THIRDPARTY_IMPORTEXPORT void cumgam ( double *x, double *a, double *cum, double *ccum );
THIRDPARTY_IMPORTEXPORT void cumnbn ( double *s, double *xn, double *pr, double *ompr,
  double *cum, double *ccum );
THIRDPARTY_IMPORTEXPORT void cumnor ( double *arg, double *result, double *ccum );
THIRDPARTY_IMPORTEXPORT void cumpoi ( double *s, double *xlam, double *cum, double *ccum );
THIRDPARTY_IMPORTEXPORT void cumt ( double *t, double *df, double *cum, double *ccum );
THIRDPARTY_IMPORTEXPORT double dbetrm ( double *a, double *b );
THIRDPARTY_IMPORTEXPORT double dexpm1 ( double *x );
THIRDPARTY_IMPORTEXPORT double dinvnr ( double *p, double *q );
THIRDPARTY_IMPORTEXPORT void dinvr ( int *status, double *x, double *fx,
  unsigned long *qleft, unsigned long *qhi );
THIRDPARTY_IMPORTEXPORT double dlanor ( double *x );
THIRDPARTY_IMPORTEXPORT double dpmpar ( int *i );
THIRDPARTY_IMPORTEXPORT void dstinv ( double *zsmall, double *zbig, double *zabsst,
  double *zrelst, double *zstpmu, double *zabsto, double *zrelto );
THIRDPARTY_IMPORTEXPORT double dstrem ( double *z );
THIRDPARTY_IMPORTEXPORT void dstzr ( double *zxlo, double *zxhi, double *zabstl, double *zreltl );
THIRDPARTY_IMPORTEXPORT double dt1 ( double *p, double *q, double *df );
THIRDPARTY_IMPORTEXPORT void dzror ( int *status, double *x, double *fx, double *xlo,
  double *xhi, unsigned long *qleft, unsigned long *qhi );
THIRDPARTY_IMPORTEXPORT void erf_values ( int *n_data, double *x, double *fx );
THIRDPARTY_IMPORTEXPORT double error_f ( double *x );
THIRDPARTY_IMPORTEXPORT double error_fc ( int *ind, double *x );
THIRDPARTY_IMPORTEXPORT double esum ( int *mu, double *x );
THIRDPARTY_IMPORTEXPORT double eval_pol ( double a[], int *n, double *x );
THIRDPARTY_IMPORTEXPORT double exparg ( int *l );
THIRDPARTY_IMPORTEXPORT void f_cdf_values ( int *n_data, int *a, int *b, double *x, double *fx );
THIRDPARTY_IMPORTEXPORT void f_noncentral_cdf_values ( int *n_data, int *a, int *b, double *lambda, 
  double *x, double *fx );
THIRDPARTY_IMPORTEXPORT double fifdint ( double a );
THIRDPARTY_IMPORTEXPORT double fifdmax1 ( double a, double b );
THIRDPARTY_IMPORTEXPORT double fifdmin1 ( double a, double b );
THIRDPARTY_IMPORTEXPORT double fifdsign ( double mag, double sign );
THIRDPARTY_IMPORTEXPORT long fifidint ( double a );
THIRDPARTY_IMPORTEXPORT long fifmod ( long a, long b );
THIRDPARTY_IMPORTEXPORT double fpser ( double *a, double *b, double *x, double *eps );
THIRDPARTY_IMPORTEXPORT void ftnstop ( char *msg );
THIRDPARTY_IMPORTEXPORT double gam1 ( double *a );
THIRDPARTY_IMPORTEXPORT void gamma_inc ( double *a, double *x, double *ans, double *qans, int *ind );
THIRDPARTY_IMPORTEXPORT void gamma_inc_inv ( double *a, double *x, double *x0, double *p, double *q,
  int *ierr );
THIRDPARTY_IMPORTEXPORT void gamma_inc_values ( int *n_data, double *a, double *x, double *fx );
THIRDPARTY_IMPORTEXPORT double gamma_ln1 ( double *a );
THIRDPARTY_IMPORTEXPORT double gamma_log ( double *a );
THIRDPARTY_IMPORTEXPORT void gamma_rat1 ( double *a, double *x, double *r, double *p, double *q,
  double *eps );
THIRDPARTY_IMPORTEXPORT void gamma_values ( int *n_data, double *x, double *fx );
THIRDPARTY_IMPORTEXPORT double gamma_x ( double *a );
THIRDPARTY_IMPORTEXPORT double gsumln ( double *a, double *b );
THIRDPARTY_IMPORTEXPORT int ipmpar ( int *i );
THIRDPARTY_IMPORTEXPORT void negative_binomial_cdf_values ( int *n_data, int *f, int *s, double *p, 
  double *cdf );
THIRDPARTY_IMPORTEXPORT void normal_cdf_values ( int *n_data, double *x, double *fx );
THIRDPARTY_IMPORTEXPORT void poisson_cdf_values ( int *n_data, double *a, int *x, double *fx );
THIRDPARTY_IMPORTEXPORT double psi ( double *xx );
THIRDPARTY_IMPORTEXPORT void psi_values ( int *n_data, double *x, double *fx );
THIRDPARTY_IMPORTEXPORT double rcomp ( double *a, double *x );
THIRDPARTY_IMPORTEXPORT double rexp ( double *x );
THIRDPARTY_IMPORTEXPORT double rlog ( double *x );
THIRDPARTY_IMPORTEXPORT double rlog1 ( double *x );
THIRDPARTY_IMPORTEXPORT void student_cdf_values ( int *n_data, int *a, double *x, double *fx );
THIRDPARTY_IMPORTEXPORT double stvaln ( double *p );

__END_DECLS

#endif /* _THIRDPARTY_DCDFLIB_H_ */


